package org.bitbucket.bradleysmithllc.etlagent.test;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import org.bitbucket.bradleysmithllc.etlagent.AbstractAgent;
import org.bitbucket.bradleysmithllc.etlagent.VirtualFiles;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.junit.Assert;

import java.util.Arrays;
import java.util.List;

class TestAgent extends AbstractAgent
{
	private final String id;
	private RuntimeSupport runtimeSupport;

	TestAgent(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getVersion() {
		return "test-1.0";
	}

	@Override
	public JsonNode getConfigurationValidator() {
		return null;
	}

	@Override
	public void setAgentConfiguration(JsonNode jsonNode) {
	}

	public class RequestObj extends GenericResponseDTO
	{
		@Inject
		private String name;

		public String getName() {
			return name;
		}
	}

	@Override
	public List<RequestHandler> getRequestHandlers() {
		return Arrays.asList((RequestHandler) new RequestHandler<Object, GenericResponseDTO>() {
			@Override
			public String getId() {
				return "request";
			}

			@Override
			public GsonBuilder configureBuilder(GsonBuilder builder) {
				return builder;
			}

			@Override
			public Object getRequestContainerObject() {
				return null;
			}

			@Override
			public JsonNode getValidator() {
				return null;
			}

			@Override
			public GenericResponseDTO process(JsonNode request, Object container) throws Exception {
				Assert.assertEquals("sessionId", request.get("session-id").asText());
				return null;
			}
		});
	}

	@Override
	public List<MultipartRequestHandler> getMultipartRequestHandlers() {
		return Arrays.asList((MultipartRequestHandler) new MultipartRequestHandler<RequestObj, RequestObj>() {
			@Override
			public String getId() {
				return "multipart-request";
			}

			@Override
			public GsonBuilder configureBuilder(GsonBuilder builder) {
				return builder;
			}

			@Override
			public RequestObj getRequestContainerObject() {
				return new RequestObj();
			}

			@Override
			public JsonNode getValidator() {
				return null;
			}

			@Override
			public RequestObj process(JsonNode request, VirtualFiles vflRequest, VirtualFiles vflResponse, RequestObj container) throws Exception {
				RequestObj obj = new RequestObj();

				obj.setResponse(GenericResponseDTO.response_code.okay);
				obj.name = "Brad";

				return obj;
			}
		}, new MultipartRequestHandler<Object, GenericResponseDTO>() {
			@Override
			public String getId() {
				return "die-request";
			}

			@Override
			public GsonBuilder configureBuilder(GsonBuilder builder) {
				return builder;
			}

			@Override
			public Object getRequestContainerObject() {
				return new RequestObj();
			}

			@Override
			public JsonNode getValidator() {
				return null;
			}

			@Override
			public GenericResponseDTO process(JsonNode request, VirtualFiles vflRequest, VirtualFiles vflResponse, Object container) throws Exception {
				throw new Exception();
			}
		});
	}
}