package org.bitbucket.bradleysmithllc.etlagent.test;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.report.ProcessingMessage;
import com.github.fge.jsonschema.report.ProcessingReport;
import org.junit.Test;

import java.io.IOException;
import java.util.Iterator;

public class JsonValidatorTest {
	@Test
	public void test() throws IOException, ProcessingException {
		JsonSchemaFactory jsf = JsonSchemaFactory.byDefault();

		JsonNode jsonNode = JsonLoader.fromString(
				"{" +
						"\"$schema\" : \"http://json-schema.org/draft-03/schema#\"," +
						"\"properties\":" +
						"{" +
						"\"test\":" +
						"{" +
						"\"type\": \"string\"," +
						"\"enum\": [\"yes\", \"no\"]," +
						"\"required\": true" +
						"}" +
						"}," +
						"\"additionalProperties\": {" +
						"\"type\": \"string\"," +
						"\"enum\": [\"one\", \"zero\"]" +
						"}" +
						"}"
		);

		JsonSchema jsonSchema = jsf.getJsonSchema(jsonNode);


		ProcessingReport val = jsonSchema.validate(JsonLoader.fromString("{\"test\": \"yes\", \"bin1\": \"one\", \"bin2\": \"zero\"}"));

		Iterator<ProcessingMessage> it = val.iterator();

		while (it.hasNext()) {
			ProcessingMessage pm = it.next();

			System.out.println(pm.toString());
		}
	}
}