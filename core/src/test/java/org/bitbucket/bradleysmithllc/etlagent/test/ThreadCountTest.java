/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2010-2011 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * http://glassfish.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.bitbucket.bradleysmithllc.etlagent.test;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.header.MediaTypes;
import com.sun.jersey.test.framework.JerseyTest;
import org.bitbucket.bradleysmithllc.etlagent.dto.InitializeResponse;
import org.bitbucket.bradleysmithllc.etlagent.resources.EtlAgentRequestResource;
import org.bitbucket.bradleysmithllc.etlagent.resources.ResourceClasses;
import org.bitbucket.bradleysmithllc.etlagent.resources.WaitResource;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

import static org.junit.Assert.*;

/**
 * @author Naresh
 */
public class ThreadCountTest extends JerseyTest {
	private static int port = 9888;

	@Override
	protected int getPort(int defaultPort) {
		return port;
	}

	public ThreadCountTest() throws Exception {
		super(ResourceClasses.class.getPackage().getName());
	}

	/**
	 * Test to see that the message "Hello World" is sent in the response.
	 */
	@Test
	public void maxConcurrent() throws InterruptedException {
		if (true) return;

		CountDownLatch cdl = new CountDownLatch(1);
		CountDownLatch cdlFinish = new CountDownLatch(20);

		for (int i = 0; i < 20; i++)
		{
			new Thread(new Worker(cdl, cdlFinish)).start();
		}

		Thread.sleep(1000L);

		cdl.countDown();

		cdlFinish.await();

		System.out.println("Max: " + WaitResource.maxSleepers.get());
	}


	class Worker implements Runnable
	{
		private final CountDownLatch countDownLatch;
		private final CountDownLatch countDownLatch2;

		public Worker(CountDownLatch cdl, CountDownLatch cdlFinish) {
			countDownLatch = cdl;
			countDownLatch2 = cdlFinish;
		}

		@Override
		public void run() {
			try {
				countDownLatch.await();

				WebResource webResource = resource();
				webResource.path("wait/2000").get(String.class);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			finally
			{
				countDownLatch2.countDown();
			}
		}
	}
}