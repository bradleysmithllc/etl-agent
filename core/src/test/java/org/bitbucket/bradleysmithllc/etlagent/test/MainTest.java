/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2010-2011 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * http://glassfish.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.bitbucket.bradleysmithllc.etlagent.test;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.FieldNamingPolicy;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.header.MediaTypes;
import org.bitbucket.bradleysmithllc.etlagent.dto.InitializeResponse;
import org.bitbucket.bradleysmithllc.etlagent.resources.ResourceClasses;
import com.sun.jersey.test.framework.JerseyTest;
import org.bitbucket.bradleysmithllc.etlunit.util.Hex;
import org.h2.util.IOUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.io.OutputStream;

import static org.junit.Assert.*;

/**
 * @author Naresh
 */
public class MainTest extends JerseyTest {
	private static int port;

	@BeforeClass
	public static void setPort()
	{
		port = (int) ((Math.random() * Long.MAX_VALUE) % 10000L) + 49152;
	}

	@Override
	protected int getPort(int defaultPort) {
		return port;
	}

	public MainTest() throws Exception {
		super(ResourceClasses.class.getPackage().getName());
	}

	/**
	 * Test to see that the message "Hello World" is sent in the response.
	 */
	@Test
	public void testHelloWorld() throws InterruptedException {
		WebResource webResource = resource();
		String responseMsg = webResource.path("helloworld").get(String.class);
		assertEquals("Hello World", responseMsg);
	}

	/**
	 * Test to see that the message "Hello World" is sent in the response.
	 */
	@Test
	public void testWait() throws InterruptedException {
		WebResource webResource = resource();

		long start = System.currentTimeMillis();
		String responseMsg = webResource.path("wait/1000").get(String.class);
		long end = System.currentTimeMillis();
		assertEquals("Slept 1000 ms", responseMsg);

		assertTrue(end - start > 850L);
		assertTrue(end - start < 2450L);

		start = System.currentTimeMillis();
		responseMsg = webResource.path("wait/1700").get(String.class);
		end = System.currentTimeMillis();

		assertEquals("Slept 1700 ms", responseMsg);

		assertTrue(end - start > 1650L);
		assertTrue(end - start < 2750L);
	}

	/**
	 * Test if a WADL document is available at the relative path
	 * "application.wadl".
	 */
	@Test
	public void testApplicationWadl() {
		WebResource webResource = resource();
		String serviceWadl = webResource.path("application.wadl").
				accept(MediaTypes.WADL).get(String.class);

		assertTrue(serviceWadl.length() > 0);
	}

	/**
	 * Test to see that the message "Hello World" is sent in the response.
	 */
	@Test
	public void testLogin() throws InterruptedException {
		WebResource webResource = resource();
		String responseMsg = webResource.path("rest/initialize/clientId").get(String.class);

		InitializeResponse ir = new GsonBuilder()
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create().fromJson(responseMsg, InitializeResponse.class);

		assertEquals("clientId", ir.getClientId());
		assertNotNull(ir.getSessionId());
	}
}