package org.bitbucket.bradleysmithllc.etlagent.test;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.DirectoryCleaner;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class DirectoryCleanerTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test(expected = IllegalStateException.class)
	public void cantStopThis()
	{
		new DirectoryCleaner(temporaryFolder.getRoot()).stop();
	}

	@Test
	public void cantStartStarted()
	{
		DirectoryCleaner directoryCleaner = new DirectoryCleaner(temporaryFolder.getRoot());

		directoryCleaner.start();

		try
		{
			directoryCleaner.start();
			Assert.fail();
		}
		catch(IllegalStateException exc)
		{
			// this is good
			directoryCleaner.stop();
		}
	}

	@Test
	public void stopMeIfYouveHeardIt()
	{
		DirectoryCleaner directoryCleaner = new DirectoryCleaner(temporaryFolder.getRoot());

		directoryCleaner.start();
		directoryCleaner.stop();
	}

	@Test(expected = IllegalArgumentException.class)
	public void minimumRunInterval()
	{
		DirectoryCleaner directoryCleaner = new DirectoryCleaner(temporaryFolder.getRoot(), 999L, 0L);

		directoryCleaner.start();
		directoryCleaner.stop();
	}

	@Test
	public void basicPurgeAll() throws IOException, InterruptedException {
		final Semaphore sem = new Semaphore(1);
		sem.acquireUninterruptibly();

		// touch some files
		final File f = temporaryFolder.newFile();

		Assert.assertTrue(f.exists());

		DirectoryCleaner directoryCleaner = new DirectoryCleaner(temporaryFolder.getRoot(), 5000L, 0L);
		directoryCleaner.setObserver(new DirectoryCleaner.CleanerObserver() {
			@Override
			public void cleanerStarting(DirectoryCleaner dc) {
			}

			@Override
			public void cleanerPurging(DirectoryCleaner dc, File path) {
				Assert.assertEquals(f, path);
			}

			@Override
			public void cleanerComplete(DirectoryCleaner dc, int count) {
				Assert.assertEquals(1, count);
				Assert.assertFalse(f.exists());
				dc.stop();
				sem.release();
			}
		});

		directoryCleaner.start();

		sem.acquireUninterruptibly();
	}

	@Test
	public void noObserver() throws IOException, InterruptedException {
		System.out.println("noObserver");
		final Semaphore sem = new Semaphore(1);
		sem.acquireUninterruptibly();

		// touch some files
		final File f = temporaryFolder.newFile();

		Assert.assertTrue(f.exists());

		DirectoryCleaner directoryCleaner = new DirectoryCleaner(temporaryFolder.getRoot(), 1000L, 0L);

		directoryCleaner.start();

		Thread.sleep(5000L);

		directoryCleaner.stop();

		Assert.assertFalse(f.exists());
	}

	@Test
	public void folder() throws IOException, InterruptedException {
		System.out.println("folder");
		final Semaphore sem = new Semaphore(1);
		sem.acquireUninterruptibly();

		// touch some files
		File f1 = temporaryFolder.newFolder();
		Assert.assertTrue(f1.exists());

		File f2 = temporaryFolder.newFolder();
		Assert.assertTrue(f1.exists());

		File f3 = new File(f2, "folder");
		f3.mkdirs();
		Assert.assertTrue(f3.exists());

		DirectoryCleaner directoryCleaner = new DirectoryCleaner(temporaryFolder.getRoot(), 1000L, 0L);

		directoryCleaner.start();

		Thread.sleep(5000L);

		directoryCleaner.stop();

		Assert.assertFalse(f1.exists());
		Assert.assertFalse(f2.exists());
	}

	long eligibleTime = -1L;
	long cleanTime = -1L;

	@Test
	public void catAndMouse() throws IOException, InterruptedException {
		System.out.println("catAndMouse");
		final Semaphore sem = new Semaphore(1);
		sem.acquireUninterruptibly();
		final CountDownLatch latch = new CountDownLatch(1);

		// touch some files
		final File f = temporaryFolder.newFile();

		Assert.assertTrue(f.exists());

		DirectoryCleaner directoryCleaner = new DirectoryCleaner(temporaryFolder.getRoot(), 5000L, 5000L);
		directoryCleaner.setObserver(new DirectoryCleaner.CleanerObserver() {
			@Override
			public void cleanerStarting(DirectoryCleaner dc) {
			}

			@Override
			public void cleanerPurging(DirectoryCleaner dc, File path) {
				Assert.assertEquals(f, path);
			}

			@Override
			public void cleanerComplete(DirectoryCleaner dc, int count) {
				if (count != 0)
				{
					cleanTime = System.currentTimeMillis();
					dc.stop();
					sem.release();
				}
				else
				{
					// failsafe check in case we are running forever
					if (latch.getCount() == 0)
					{
						if (System.currentTimeMillis() > (eligibleTime + 1000L))
						{
							dc.stop();
							sem.release();
						}
					}
				}
			}
		});

		directoryCleaner.start();

		// mess with the cleaner - touch the file every 3.5 seconds to keep it fresh
		for (int i = 0; i < 10; i++)
		{
			Thread.sleep(3500L);
			Assert.assertTrue(f.exists());
			FileUtils.write(f, "");
		}

		// now let it clean it up
		// record the time so it can be asserted on
		eligibleTime = System.currentTimeMillis();

		latch.countDown();
		sem.acquireUninterruptibly();

		Assert.assertTrue(String.format("%d;%d - %d", cleanTime, eligibleTime, cleanTime - eligibleTime), Math.abs(5000L - (cleanTime - eligibleTime)) < 500L);
	}
}