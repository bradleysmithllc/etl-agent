package org.bitbucket.bradleysmithllc.etlagent.test;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlagent.Agent;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.VirtualFiles;
import org.bitbucket.bradleysmithllc.etlagent.ZipVirtualFilesImpl;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.util.Arrays;
import java.util.zip.ZipException;

public class AgentDispatcherTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private VirtualFiles vflResponse;

	@Before
	public void createResponse() throws IOException {
		vflResponse = new ZipVirtualFilesImpl(temporaryFolder.newFile(), temporaryFolder.newFolder());
	}

	@Test(expected = IllegalArgumentException.class)
	public void duplicateAgent()
	{
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		ad.registerAgent(new TestAgent("id"));
	}

	@Test(expected = IllegalArgumentException.class)
	public void duplicateAgentList()
	{
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		ad.registerAgents(Arrays.asList((Agent) new TestAgent("id")));
	}

	@Test
	public void requestBadAgent1() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		GenericResponseDTO res = ad.dispatch("agent", "", null);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, res.getResponse());
		Assert.assertEquals(AgentDispatcher.AGENT_DOES_NOT_EXIST, res.getResponseMessage());
	}

	@Test
	public void requestBadAgent2() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		GenericResponseDTO res = ad.dispatch("agent", "", null);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, res.getResponse());
		Assert.assertEquals(AgentDispatcher.AGENT_DOES_NOT_EXIST, res.getResponseMessage());
	}

	@Test
	public void requestBadRequest() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		GenericResponseDTO res = ad.dispatch("id", "request1", null);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, res.getResponse());
		Assert.assertEquals(AgentDispatcher.AGENT_DOES_NOT_HANDLE_REQUEST, res.getResponseMessage());
	}

	@Test
	public void request() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		ad.dispatch("id", "request", JsonLoader.fromString("{\"session-id\": \"sessionId\"}"));
	}

	@Test
	public void emptyMultipartRequest() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		VirtualFiles vf = new ZipVirtualFilesImpl(temporaryFolder.newFile(), temporaryFolder.newFolder());
		MultipartResponseDTO res = ad.dispatchMultipart("id", "multipart-request", "", "", vf, vflResponse);

		GenericResponseDTO gres = res.getRequestDTO(GenericResponseDTO.class);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, gres.getResponse());
		Assert.assertEquals(AgentDispatcher.INVALID_REQUEST_MANGLED_CONTAINER, gres.getResponseMessage());
	}

	@Test
	public void invalidMultipartRequestNoRequest() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		VirtualFiles vf = new ZipVirtualFilesImpl(temporaryFolder.newFile(), temporaryFolder.newFolder());
		VirtualFiles.VWriter writer = vf.getWriter();
		writer.writePath("test").write("H".getBytes());

		writer.dispose();
		writer._package();

		MultipartResponseDTO res = ad.dispatchMultipart("id", "multipart-request", "", "", vf, vflResponse);

		GenericResponseDTO gres = res.getRequestDTO(GenericResponseDTO.class);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, gres.getResponse());
		Assert.assertEquals(AgentDispatcher.INVALID_REQUEST_MISSING_REQUEST_NODE, gres.getResponseMessage());
	}

	@Test
	public void requestResponse() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		VirtualFiles vf = new ZipVirtualFilesImpl(temporaryFolder.newFile(), temporaryFolder.newFolder());
		VirtualFiles.VWriter writer = vf.getWriter();
		writer.writeRequestNode().beginObject().name("name").value("Brad").endObject().close();
		writer.writePath("test").write("H".getBytes());

		writer.dispose();
		writer._package();

		MultipartResponseDTO res = ad.dispatchMultipart("id", "multipart-request", "", "", vf, vflResponse);

		TestAgent.RequestObj gres = res.getRequestDTO(TestAgent.RequestObj.class);

		Assert.assertEquals(GenericResponseDTO.response_code.okay, gres.getResponse());
		Assert.assertEquals("Brad", gres.getName());
	}

	@Test
	public void badAgent() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		VirtualFiles vf = new ZipVirtualFilesImpl(temporaryFolder.newFile(), temporaryFolder.newFolder());
		VirtualFiles.VWriter writer = vf.getWriter();
		writer.writeRequestNode().beginObject().name("name").value("Brad").endObject().close();

		writer.dispose();
		writer._package();

		MultipartResponseDTO res = ad.dispatchMultipart("id1", "multipart-request", "", "", vf, vflResponse);

		GenericResponseDTO gres = res.getRequestDTO(GenericResponseDTO.class);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, gres.getResponse());
		Assert.assertEquals(AgentDispatcher.AGENT_DOES_NOT_EXIST, gres.getResponseMessage());
	}

	@Test
	public void goodAgentBadRequest() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		VirtualFiles vf = new ZipVirtualFilesImpl(temporaryFolder.newFile(), temporaryFolder.newFolder());
		VirtualFiles.VWriter writer = vf.getWriter();
		writer.writeRequestNode().beginObject().name("name").value("Brad").endObject().close();

		writer.dispose();
		writer._package();

		MultipartResponseDTO res = ad.dispatchMultipart("id", "multipart-request2", "", "", vf, vflResponse);

		GenericResponseDTO gres = res.getRequestDTO(GenericResponseDTO.class);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, gres.getResponse());
		Assert.assertEquals(AgentDispatcher.AGENT_DOES_NOT_HANDLE_REQUEST, gres.getResponseMessage());
	}

	@Test
	public void agentException() throws Exception {
		AgentDispatcher ad = new AgentDispatcher();

		ad.registerAgent(new TestAgent("id"));
		VirtualFiles vf = new ZipVirtualFilesImpl(temporaryFolder.newFile(), temporaryFolder.newFolder());
		VirtualFiles.VWriter writer = vf.getWriter();
		writer.writeRequestNode().beginObject().name("name").value("Brad").endObject().close();

		writer.dispose();
		writer._package();

		MultipartResponseDTO res = ad.dispatchMultipart("id", "die-request", "", "", vf, vflResponse);

		GenericResponseDTO gres = res.getRequestDTO(GenericResponseDTO.class);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, gres.getResponse());
		Assert.assertEquals(AgentDispatcher.AGENT_FAILURE, gres.getResponseMessage());
	}
}