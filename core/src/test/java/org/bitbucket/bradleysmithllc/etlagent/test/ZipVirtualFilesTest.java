package org.bitbucket.bradleysmithllc.etlagent.test;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlagent.VirtualFiles;
import org.bitbucket.bradleysmithllc.etlagent.ZipVirtualFilesImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipException;

public class ZipVirtualFilesTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	ZipVirtualFilesImpl zipVirtualFiles;

	@Before
	public void newVfiles() throws IOException {
		zipVirtualFiles = new ZipVirtualFilesImpl(temporaryFolder.newFile(), temporaryFolder.newFolder());
	}

	@Test(expected = ZipException.class)
	public void requestMissing() throws IOException {
		zipVirtualFiles.getReader().getRequestNode();
	}

	@Test(expected = IOException.class)
	public void requestMissingAgain() throws IOException {
		VirtualFiles.VWriter writer = zipVirtualFiles.getWriter();
		writer.writePath("test").write("hi".getBytes());
		writer.dispose();

		zipVirtualFiles.getReader().getRequestNode();
	}

	@Test
	public void filesInFilesOut() throws IOException {
		VirtualFiles.VWriter writer = zipVirtualFiles.getWriter();

		OutputStream path = writer.writePath("path/file");

		path.write("Test".getBytes());
		path.close();

		writer.dispose();
		writer._package();

		VirtualFiles.VReader reader = zipVirtualFiles.getReader();

		InputStream rin = reader.readPath("path/file");
		List<String> lineList = IOUtils.readLines(rin);

		Assert.assertEquals(1, lineList.size());
		Assert.assertEquals("Test", lineList.get(0));

		// assert that the extracted file matches exactly
		File rfile = reader.extractPath("path/file");

		Assert.assertEquals(lineList.get(0), FileUtils.readFileToString(rfile));
	}

	@Test
	public void deepFiles() throws IOException {
		int MAX_J = 15;
		int MAX_K = 20;
		int MAX_L = 13;

		List<File> inFiles = makeInFiles();

		VirtualFiles.VWriter writer = zipVirtualFiles.getWriter();

		for (int j = 0; j < MAX_J; j++)
		{
			for (int k = 0; k < MAX_K; k++)
			{
				for (int l = 0; l < MAX_L; l++)
				{
					int counter = (j * 10) + (k * 5) + l;
					int index = counter % inFiles.size();
					writer.writePath("str-" + j + "/trs-" + k + "/rst-" + l, inFiles.get(index));
				}
			}
		}

		writer.dispose();
		writer._package();

		Pattern pat = Pattern.compile("str-(\\d{1,3})/trs-(\\d{1,3})/rst-(\\d{1,3})");

		// reverse and verify
		VirtualFiles.VReader reader = zipVirtualFiles.getReader();

		List<String> paths = reader.listPaths();

		for (String path : paths)
		{
			Matcher mat = pat.matcher(path);

			Assert.assertTrue(mat.matches());

			int j = Integer.parseInt(mat.group(1));
			int k = Integer.parseInt(mat.group(2));
			int l = Integer.parseInt(mat.group(3));

			File fpath = reader.extractPath(path);

			int counter = (j * 10) + (k * 5) + l;
			int index = counter % inFiles.size();
			//Assert.assertArrayEquals(
			//	FileUtils.readFileToByteArray(inFiles.get(index)),
			//	FileUtils.readFileToByteArray(fpath)
			//);
		}
	}

	@Test
	public void cantWriteRequestPath() throws IOException {
		try
		{
			zipVirtualFiles.getWriter().writePath(VirtualFiles.REQUEST_JSON);
			Assert.fail();
		}
		catch(IOException exc)
		{
			Assert.assertTrue(exc.getMessage().startsWith(VirtualFiles.ERR_WRITE_JSON_PATH));
		}
	}

	@Test
	public void cantReadRequestPath() throws IOException {
		// create the request node so that the reader will deny access to it
		VirtualFiles.VWriter writer = zipVirtualFiles.getWriter();
		writer.writeRequestNode().beginObject().endObject().close();
		writer.dispose();
		writer._package();

		try
		{
			zipVirtualFiles.getReader().readPath(VirtualFiles.REQUEST_JSON);
			Assert.fail();
		}
		catch(IOException exc)
		{
			Assert.assertTrue(exc.getMessage().startsWith(VirtualFiles.ERR_READ_JSON_PATH));
		}
	}

	@Test
	public void cantExtractRequestPath() throws IOException {
		// create the request node so that the reader will deny access to it
		VirtualFiles.VWriter writer = zipVirtualFiles.getWriter();
		writer.writeRequestNode().beginObject().endObject().close();
		writer.dispose();
		writer._package();

		try
		{
			zipVirtualFiles.getReader().extractPath(VirtualFiles.REQUEST_JSON);
			Assert.fail();
		}
		catch(IOException exc)
		{
			Assert.assertTrue(exc.getMessage().startsWith(VirtualFiles.ERR_READ_JSON_PATH));
		}
	}

	@Test
	public void canReadAndWriteRequest() throws IOException {
		// create the request node so that the reader will deny access to it
		VirtualFiles.VWriter writer = zipVirtualFiles.getWriter();
		writer.writeRequestNode().beginObject().name("a").value("b").endObject().close();
		writer.dispose();
		writer._package();

		VirtualFiles.VReader reader = zipVirtualFiles.getReader();
		JsonNode jsonNode = reader.getRequestNode();

		Assert.assertEquals(1, jsonNode.size());
		Assert.assertEquals("b", jsonNode.get("a").asText());

		// list the contents and ensure that they are empty
		Assert.assertEquals(0, reader.listPaths().size());
	}

	private List<File> makeInFiles() throws IOException {
		List<File> files = new ArrayList<File>();

		for (int i = 0; i < 100; i++)
		{
			File in = temporaryFolder.newFile();
			files.add(in);

			DataOutputStream dout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(in)));

			for (int j = 0; j < 1000; j++)
			{
				dout.writeInt(i);
				dout.writeLong(System.currentTimeMillis());
				dout.writeInt(j);
				dout.writeLong(j * i);
				double random = Math.random();
				dout.writeDouble(random);

				if (random >= 0.5d)
				{
					dout.writeLong(System.currentTimeMillis());
				}

				dout.writeInt(i);
			}

			dout.close();
		}

		return files;
	}
}