package org.bitbucket.bradleysmithllc.etlagent.resources;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.concurrent.atomic.AtomicInteger;

@Path("/wait/{sleepTime}")
public class WaitResource {
	@PathParam("sleepTime")
	private String sleepTime;

	public static AtomicInteger sleepers = new AtomicInteger();
	public static AtomicInteger maxSleepers = new AtomicInteger();

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String returnErrorGet() {
		try {
			sleepers.incrementAndGet();

			maxSleepers.set(Math.max(maxSleepers.get(), sleepers.get()));

			Thread.sleep(Long.parseLong(sleepTime));
			return "Slept " + sleepTime + " ms";
		} catch (InterruptedException e) {
			return e.toString();
		}
		finally
		{
			sleepers.decrementAndGet();
		}
	}
}