package org.bitbucket.bradleysmithllc.etlagent;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.stream.JsonWriter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

public class ZipVirtualFilesImpl implements VirtualFiles
{
	public static interface TempFileBaker
	{
		File createTempFile();
	}

	public static final VReader.PathFilter PATH_FILTER = new VReader.PathFilter() {
		@Override
		public boolean accept(String path) {
			return true;
		}
	};

	final static class ZipVFSEntry
	{
		String path;
		File data;
	}

	List<ZipVFSEntry> vfsEntries = new ArrayList<ZipVFSEntry>();
	enum state {reading, writing, none}

	private final File temp;
	private final File zipFileTarget;

	private state this_state = state.none;
	private ZipVReader activeReader = null;
	private ZipVWriter activeWriter = null;

	public ZipVirtualFilesImpl(File target, File tempDir) throws IOException {
		zipFileTarget = target;
		temp = tempDir;
	}

	@Override
	public File getFile() {
		return zipFileTarget;
	}

	@Override
	public VReader getReader() throws IOException {
		switch (this_state) {
			case reading:
				break;
			case writing:
				throw new IllegalStateException("Currently writing");
			case none:
				activeReader = new ZipVReader();
				break;
		}

		this_state = state.reading;
		return activeReader;
	}

	@Override
	public VWriter getWriter() throws IOException {
		switch (this_state) {
			case reading:
				throw new IllegalStateException("Currently reading");
			case writing:
			case none:
				activeWriter = new ZipVWriter();
				break;
		}

		this_state = state.writing;
		return activeWriter;
	}

	private class ZipVReader implements VReader {
		private final ZipFile zFile;
		private InputStream zin;

		private ZipVReader() throws IOException {
			zFile = new ZipFile(zipFileTarget);
		}

		@Override
		public List<String> listPaths() {
			return listPaths(PATH_FILTER);
		}

		@Override
		public List<String> listPaths(PathFilter filter) {
			List<String> entryList = new ArrayList<String>();

			Enumeration<? extends ZipEntry> entries = zFile.entries();

			while (entries.hasMoreElements())
			{
				String path = entries.nextElement().getName();

				// Hide the json request from probing the fs
				if (!path.equals(REQUEST_JSON))
				{
					if (filter.accept(path))
					{
						entryList.add(path);
					}
				}
			}

			return entryList;
		}

		@Override
		public File extractPath(String path) throws IOException {
			if (path.equals(REQUEST_JSON))
			{
				throw new IOException(ERR_READ_JSON_PATH + "- May not use reserved path: " + REQUEST_JSON);
			}

			return extractPathInternal(path);
		}

		@Override
		public void extractPath(String path, File file) throws IOException {
			if (path.equals(REQUEST_JSON))
			{
				throw new IOException(ERR_READ_JSON_PATH + "- May not use reserved path: " + REQUEST_JSON);
			}

			extractPathInternal(path, file);
		}

		private File extractPathInternal(String path) throws IOException {
			File tFile = new File(temp, path);

			extractPathInternal(path, tFile);

			return tFile;
		}

		private void extractPathInternal(String path, File file) throws IOException {
			FileUtils.copyInputStreamToFile(readPathInternal(path), file);
		}

		@Override
		public InputStream readPath(String path) throws IOException {
			if (path.equals(REQUEST_JSON))
			{
				throw new IOException(ERR_READ_JSON_PATH + "- May not use reserved path: " + REQUEST_JSON);
			}

			return readPathInternal(path);
		}

		private InputStream readPathInternal(String path) throws IOException {
			if (this_state == state.none)
			{
				throw new IOException("Resource not open for reading.");
			}
			else if (this_state == state.writing)
			{
				throw new IOException("Resource open for writing.");
			}

			if (zin != null)
			{
				zin.close();
			}

			ZipEntry entry = zFile.getEntry(path);

			if (entry == null)
			{
				throw new FileNotFoundException("Entry " + path + " not found");
			}

			zin = zFile.getInputStream(entry);

			return zin;
		}

		@Override
		public JsonNode getRequestNode() throws IOException {
			return JsonLoader.fromFile(extractPathInternal(REQUEST_JSON));
		}

		@Override
		public void dispose() throws IOException {
			switch(this_state)
			{
				case writing:
					zin.close();
					break;
				case reading:
					zFile.close();
					break;
				case none:
					throw new IOException("Resource not open");
			}

			this_state = state.none;
		}
	}

	private class ZipVWriter implements VWriter {
		@Override
		public void writePath(String path, File data) throws IOException {
			OutputStream os = writePath(path);

			try
			{
				FileUtils.copyFile(data, os);
			}
			finally
			{
				os.close();
			}
		}

		@Override
		public void writePath(String path, InputStream data) throws IOException {
			OutputStream os = writePath(path);

			try
			{
				IOUtils.copy(data, os);
			}
			finally
			{
				os.close();
			}
		}

		/**
		 * Return a wrapped stream to protect from the client closing the stream.
		 * @param path
		 * @return
		 * @throws java.io.IOException
		 */
		@Override
		public OutputStream writePath(String path) throws IOException {
			if (path.equals(REQUEST_JSON))
			{
				throw new IOException(ERR_WRITE_JSON_PATH + "- May not use reserved path: " + REQUEST_JSON);
			}

			return writePathInternal(path);
		}

		private OutputStream writePathInternal(String path) throws IOException {
			if (this_state == state.none)
			{
				throw new IOException("Resource not open for writing.");
			}
			else if (this_state == state.reading)
			{
				throw new IOException("Resource open for reading.");
			}

			ZipVFSEntry entry = new ZipVFSEntry();
			entry.path = path;
			entry.data = new File(temp, UUID.randomUUID().toString());

			vfsEntries.add(entry);

			return new BufferedOutputStream(new FileOutputStream(entry.data));
		}

		@Override
		public JsonWriter writeRequestNode() throws IOException {
			return new JsonWriter(new OutputStreamWriter(writePathInternal(REQUEST_JSON)));
		}

		@Override
		public void writeRequestNode(JsonNode node) throws IOException {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(writePathInternal(REQUEST_JSON));
			ObjectMapper om = new ObjectMapper();
			outputStreamWriter.write(om.writerWithDefaultPrettyPrinter().writeValueAsString(node));
			outputStreamWriter.close();
		}

		@Override
		public void dispose() throws IOException {
			this_state = state.none;
		}

		@Override
		public void _package() throws IOException {
			ZipOutputStream zOut = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFileTarget)));

			for (ZipVFSEntry entry : vfsEntries)
			{
				zOut.putNextEntry(new ZipEntry(entry.path));

				FileUtils.copyFile(entry.data, zOut);
			}

			zOut.close();

			vfsEntries.clear();
		}
	}
}