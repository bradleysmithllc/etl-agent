package org.bitbucket.bradleysmithllc.etlagent.dto;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.codehaus.plexus.util.StringUtils;

import java.io.IOException;

public class GenericResponseDTO
{
	public static enum response_code
	{
		okay,
		failed
	}

	private String responseMessage;
	private response_code response;
	private String agentId;
	private String requestId;
	private long requestSystemDateTime;
	private long responseSystemDateTime;
	private String agentVersion;

	public boolean customBuilder()
	{
		return false;
	}

	public JsonNode buildJson() throws IOException {
		throw new UnsupportedOperationException();
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public GenericResponseDTO.response_code getResponse() {
		return response;
	}

	public void setResponse(GenericResponseDTO.response_code response) {
		this.response = response;
	}

	public String getAgentId() {
		return agentId;
	}

	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public long getRequestSystemDateTime() {
		return requestSystemDateTime;
	}

	public void setRequestSystemDateTime(long requestSystemDateTime) {
		this.requestSystemDateTime = requestSystemDateTime;
	}

	public long getResponseSystemDateTime() {
		return responseSystemDateTime;
	}

	public void setResponseSystemDateTime(long responseSystemDateTime) {
		this.responseSystemDateTime = responseSystemDateTime;
	}

	public String getAgentVersion() {
		return agentVersion;
	}

	public void setAgentVersion(String agentVersion) {
		this.agentVersion = agentVersion;
	}

	@Override
	public String toString() {
		return "GenericResponseDTO{" +
				"responseMessage='" + StringUtils.abbreviate(responseMessage, 100) + '\'' +
				", response=" + response +
				", agentId='" + agentId + '\'' +
				", requestId='" + requestId + '\'' +
				", requestSystemDateTime=" + requestSystemDateTime +
				", responseSystemDateTime=" + responseSystemDateTime +
				", agentVersion=" + agentVersion +
				'}';
	}
}