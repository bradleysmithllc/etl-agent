package org.bitbucket.bradleysmithllc.etlagent.dto;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class AgentDTO
{
	private String id;
	private String version;

	public AgentDTO(String name, String version) {
		this.id = name;
		this.version = version;
	}

	public AgentDTO() {
	}

	public String getVersion() {
		return version;
	}

	public String getId() {
		return id;
	}

	@Override
	public String toString() {
		return "AgentDTO{" +
				"id='" + id + '\'' +
				", version='" + version + '\'' +
				'}';
	}
}