package org.bitbucket.bradleysmithllc.etlagent.resources;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jackson.JacksonUtils;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.Gson;
import org.bitbucket.bradleysmithllc.etlagent.Agent;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.dto.AgentDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.AgentRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.AgentRequestsDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.StringWriter;

@Path("/rest/agent/{agent-type}")
public class AgentResource {
	@Context
	AgentDispatcher agentDispatcher;

	@PathParam("agent-type")
	private String agentId;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String processAgentRequest() throws IOException {
		Agent agent = agentDispatcher.getAgentMap().get(agentId);

		AgentRequestsDTO agentsDTO = new AgentRequestsDTO();

		for (Agent.RequestHandler handler : agent.getRequestHandlers())
		{
			agentsDTO.getAgentRquests().add(
				new AgentRequestDTO(
					handler.getId(),
					handler.getRequestContainerObject().getClass().getName(),
						JacksonUtils.prettyPrint(handler.getValidator())
				)
			);
		}

		Gson gson = new Gson();

		return gson.toJson(agentsDTO);
	}
}