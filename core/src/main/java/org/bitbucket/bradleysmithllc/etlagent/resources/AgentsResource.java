package org.bitbucket.bradleysmithllc.etlagent.resources;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.google.gson.Gson;
import org.bitbucket.bradleysmithllc.etlagent.Agent;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.dto.AgentDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.AgentsDTO;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.io.StringWriter;

@Path("/rest/agents")
public class AgentsResource {
	@Context
	AgentDispatcher agentDispatcher;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String processAgentRequest() throws IOException {
		JsonFactory jfactory = new JsonFactory();

		AgentsDTO agents = new AgentsDTO();

		for (Agent agent : agentDispatcher.getAgents())
		{
			agents.getAgents().add(new AgentDTO(agent.getId(), agent.getVersion()));
		}

		Gson gson = new Gson();

		return gson.toJson(agents);
	}
}