package org.bitbucket.bradleysmithllc.etlagent;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.GsonBuilder;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.util.List;
import java.util.Map;

public interface Agent
{
	String getId();
	String getVersion();

	JsonNode getConfigurationValidator();
	void setAgentConfiguration(JsonNode jsonNode);

	List<RequestHandler> getRequestHandlers();
	Map<String, RequestHandler> getRequestHandlerMap();

	List<MultipartRequestHandler> getMultipartRequestHandlers();
	Map<String, MultipartRequestHandler> getMultipartRequestHandlerMap();

	void receiveRuntimeSupport(RuntimeSupport runtimeSupport);

	interface RequestHandlerBase<T, J extends GenericResponseDTO>
	{
		String getId();

		GsonBuilder configureBuilder(GsonBuilder builder);
		T getRequestContainerObject();

		JsonNode getValidator();

	}

	interface RequestHandler<T, J extends GenericResponseDTO> extends RequestHandlerBase<T, J>
	{
		J process(JsonNode request, T container) throws Exception;
	}

	interface MultipartRequestHandler<T, J extends GenericResponseDTO> extends RequestHandlerBase<T, J>
	{
		J process(JsonNode request, VirtualFiles vflRequest, VirtualFiles vflResponse, T container) throws Exception;
	}
}