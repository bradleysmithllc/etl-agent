package org.bitbucket.bradleysmithllc.etlagent;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileFilter;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class DirectoryCleaner
{
	private Logger logger = LogManager.getLogger(getClass());

	public static interface CleanerObserver
	{
		void cleanerStarting(DirectoryCleaner dc);
		void cleanerPurging(DirectoryCleaner dc, File path);
		void cleanerComplete(DirectoryCleaner dc, int cleanCount);
	}

	public static final long MINIMUM_RUN_INTERVAL = 1000L; /** 1 second **/
	public static final long DEFAULT_RUN_INTERVAL = 60000L * 5; /** 5 minutes **/
	public static final long DEFAULT_TERM_DATE = 60000L * 60L * 8; /** 8 Hours **/

	private CountDownLatch latch;
	private final long runInterval;
	private final long termDate;

	private static final CleanerObserver nullObserver = new CleanerObserver() {
		@Override
		public void cleanerStarting(DirectoryCleaner dc) {
		}

		@Override
		public void cleanerPurging(DirectoryCleaner dc, File path) {
		}

		@Override
		public void cleanerComplete(DirectoryCleaner dc, int cleanCount) {
		}
	};

	private CleanerObserver observer = nullObserver;

	private final File root;

	public DirectoryCleaner(File root) {
		this(root, DEFAULT_RUN_INTERVAL, DEFAULT_TERM_DATE);
	}

	public DirectoryCleaner(File root, long runInterval, long termDate) {
		if (runInterval < MINIMUM_RUN_INTERVAL)
		{
			throw new IllegalArgumentException("Run interval [" + runInterval + "L] is less than the minimum allowed [" + MINIMUM_RUN_INTERVAL + "L]");
		}

		this.runInterval = runInterval;
		this.root = root;
		this.termDate = termDate;
	}

	public void setObserver(CleanerObserver observer) {
		if (observer == null)
		{
			observer = nullObserver;
		}
		else
		{
			this.observer = observer;
		}
	}

	public void stop()
	{
		synchronized(this)
		{
			if (latch == null)
			{
				throw new IllegalStateException("Latch is null");
			}
		}

		latch.countDown();
	}

	public void start()
	{
		synchronized (this)
		{
			if (latch != null)
			{
				throw new IllegalStateException("Latch is not null");
			}

			latch = new CountDownLatch(1);
		}

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				logger.info(
					"Directory cleaner active on [{}] with [{} ms] run interval",
					root,
					runInterval
				);

				try {
					while (!latch.await(runInterval, TimeUnit.MILLISECONDS)) {
						CleanerObserver obs = observer;

						obs.cleanerStarting(DirectoryCleaner.this);

						int cleanCount = 0;

						try
						{
							logger.info("Directory cleaner [{}] activating", root.getAbsolutePath());
							cleanCount = clean(root, obs);
						}
						finally
						{
							logger.info("Directory cleaner [{}] purged [{}] files", root.getAbsolutePath(), cleanCount);
							obs.cleanerComplete(DirectoryCleaner.this, cleanCount);
						}
					}
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				} finally {
					latch = null;
				}

				logger.info("Directory cleaner shutting down");
			}
		});
		thread.setDaemon(true);
		thread.start();
	}

	private int clean(File root, final CleanerObserver obs) {
		final AtomicInteger counter = new AtomicInteger();

		root.listFiles(new FileFilter() {
			@Override
			public boolean accept(File pathname) {
				long lastModified = pathname.lastModified();

				boolean expired = false;

				if (lastModified < (System.currentTimeMillis() - termDate))
				{
					expired = true;
				}

				if (pathname.isDirectory())
				{
					counter.addAndGet(clean(pathname, obs));

					// if out of date, clean it.
					if (expired)
					{
						FileUtils.deleteQuietly(pathname);
						counter.incrementAndGet();
					}
				}
				else
				{
					if (lastModified < (System.currentTimeMillis() - termDate))
					{
						obs.cleanerPurging(DirectoryCleaner.this, pathname);
						if (FileUtils.deleteQuietly(pathname))
						{
							counter.incrementAndGet();
						}
					}
				}

				return false;
			}
		});

		return counter.get();
	}
}