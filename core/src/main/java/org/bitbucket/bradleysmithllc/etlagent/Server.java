/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 *
 * Copyright (c) 2010-2011 Oracle and/or its affiliates. All rights reserved.
 *
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common Development
 * and Distribution License("CDDL") (collectively, the "License").  You
 * may not use this file except in compliance with the License.  You can
 * obtain a copy of the License at
 * http://glassfish.java.net/public/CDDL+GPL_1_1.html
 * or packager/legal/LICENSE.txt.  See the License for the specific
 * language governing permissions and limitations under the License.
 *
 * When distributing the software, include this License Header Notice in each
 * file and include the License file at packager/legal/LICENSE.txt.
 *
 * GPL Classpath Exception:
 * Oracle designates this particular file as subject to the "Classpath"
 * exception as provided by Oracle in the GPL Version 2 section of the License
 * file that accompanied this code.
 *
 * Modifications:
 * If applicable, add the following below the License Header, with the fields
 * enclosed by brackets [] replaced by your own identifying information:
 * "Portions Copyright [year] [name of copyright owner]"
 *
 * Contributor(s):
 * If you wish your version of this file to be governed by only the CDDL or
 * only the GPL Version 2, indicate your decision by adding "[Contributor]
 * elects to include this software in this distribution under the [CDDL or GPL
 * Version 2] license."  If you don't indicate a single choice of license, a
 * recipient has the option to distribute your version of this file under
 * either the CDDL, the GPL Version 2 or to extend the choice of license to
 * its licensees as provided above.  However, if you add GPL Version 2 code
 * and therefore, elected the GPL Version 2 license, then the option applies
 * only if the new code is made subject to such option by the copyright
 * holder.
 */

package org.bitbucket.bradleysmithllc.etlagent;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.sun.jersey.api.container.ContainerFactory;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlagent.resources.ResourceClasses;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.module_signer_mojo.IMavenProject;
import org.bitbucket.bradleysmithllc.module_signer_mojo.ModuleVersions;
import org.glassfish.grizzly.http.server.HttpHandler;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.http.server.ServerConfiguration;
import org.glassfish.grizzly.nio.transport.TCPNIOTransport;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;

import javax.ws.rs.core.UriBuilder;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.List;


public class Server {
	public static final String VERSION;

	static
	{
		String version = "UNKNOWN";

		ModuleVersions mv = new ModuleVersions(Thread.currentThread().getContextClassLoader());

		List<IMavenProject> av = mv.getAvailableVersions();

		for (IMavenProject vers : av)
		{
			if (vers.getMavenGroupId().equals("org.bitbucket.bradleysmithllc.etl-agent") && vers.getMavenArtifactId().equals("server"))
			{
				version = vers.getMavenVersionNumber();
			}
		}

		VERSION = version;
	}

	private static boolean agentActive = false;
	private static DirectoryCleaner directoryCleaner;

	public static boolean isAgentActive() {
		return agentActive;
	}

	private static HttpServer httpServer;

	private static int getPort(int defaultPort) {
		String port = System.getProperty("jersey.test.port");
		if (null != port) {
			try {
				return Integer.parseInt(port);
			} catch (NumberFormatException e) {
			}
		}
		return defaultPort;
	}

	public static URI getBaseURI() throws UnknownHostException {
		return UriBuilder.fromUri("http://" + InetAddress.getLocalHost().getHostName() + "/").port(getPort(8188)).build();
	}

	protected static HttpServer startServer() throws IOException {
		System.out.println("Starting grizzly... " + Server.VERSION);
		ResourceConfig rc = new PackagesResourceConfig(ResourceClasses.class.getPackage().getName());

		//return GrizzlyServerFactory.createHttpServer(getBaseURI(), rc);

		final HttpServer server = new HttpServer();
		final NetworkListener listener = new NetworkListener("grizzly", InetAddress.getLocalHost().getHostName(), getPort(8188));
		listener.setSecure(false);

		server.addListener(listener);

		// Map the path to the processor.
		final ServerConfiguration config = server.getServerConfiguration();
		config.addHttpHandler(ContainerFactory.createContainer(
				HttpHandler.class, rc), getBaseURI().getPath());

		TCPNIOTransport transport = listener.getTransport();

		ThreadPoolConfig tconfig = transport.getKernelThreadPoolConfig().setCorePoolSize(10).setMaxPoolSize(20);
		transport.setKernelThreadPoolConfig(tconfig);

		tconfig = transport.getWorkerThreadPoolConfig().setCorePoolSize(10).setMaxPoolSize(20);
		transport.setWorkerThreadPoolConfig(tconfig);

		server.start();

		return server;
	}

	public static void start() throws IOException {
		agentActive = true;
		httpServer = startServer();
		System.out.println(String.format("Jersey app started with WADL available at "
				+ "%sapplication.wadl",
				getBaseURI(), getBaseURI()));

		directoryCleaner = new DirectoryCleaner(ContextBase.runtimeSupport.getTempDirectory());
		directoryCleaner.start();

		// wipe out all left-over lock files
		File lck = new FileBuilder(ContextBase.runtimeSupport.getGeneratedSourceDirectory("agent")).subdir("sessionLock").file();

		if (lck.exists())
		{
			FileUtils.cleanDirectory(lck);
		}
	}

	public static void stop() throws IOException {
		directoryCleaner.stop();
		httpServer.stop();
		agentActive = false;
	}
}