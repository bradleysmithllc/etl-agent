package org.bitbucket.bradleysmithllc.etlagent;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.gson.stream.JsonWriter;

import java.io.*;
import java.util.List;

public interface VirtualFiles
{
	String REQUEST_JSON = "request.json";
	String ERR_READ_JSON_PATH = "0x00 ";
	String ERR_WRITE_JSON_PATH = "0xFF ";

	File getFile();

	interface VReader
	{
		interface PathFilter
		{
			boolean accept(String path);
		}

		List<String> listPaths();
		List<String> listPaths(PathFilter filter);

		File extractPath(String path) throws IOException;
		void extractPath(String path, File file) throws IOException;
		InputStream readPath(String path) throws IOException;

		JsonNode getRequestNode() throws IOException;

		void dispose() throws IOException;
	}

	interface VWriter
	{
		void writePath(String path, File data) throws IOException;
		void writePath(String path, InputStream data) throws IOException;
		OutputStream writePath(String path) throws IOException;

		JsonWriter writeRequestNode() throws IOException;
		void writeRequestNode(JsonNode node) throws IOException;

		void dispose() throws IOException;

		/**
		 * Packages the contents of this VFS into the target file.
		 */
		void _package() throws IOException;
	}

	VReader getReader() throws IOException;
	VWriter getWriter() throws IOException;
}