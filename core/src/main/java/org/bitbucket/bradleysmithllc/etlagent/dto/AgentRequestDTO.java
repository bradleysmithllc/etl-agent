package org.bitbucket.bradleysmithllc.etlagent.dto;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;

public class AgentRequestDTO
{
	private String id;
	private String requestType;

	private String validatorNode;

	public AgentRequestDTO(String id, String requestType, String validatorNode) {
		this.id = id;
		this.requestType = requestType;
		this.validatorNode = validatorNode;
	}

	public String getId() {
		return id;
	}

	public String getRequestType() {
		return requestType;
	}

	public String getValidatorNode() {
		return validatorNode;
	}

	@Override
	public String toString() {
		return "AgentRequestDTO{" +
				"id='" + id + '\'' +
				", requestType='" + requestType + '\'' +
				", validatorNode=" + validatorNode +
				'}';
	}
}