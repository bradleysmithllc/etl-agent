package org.bitbucket.bradleysmithllc.etlagent.dto;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.bitbucket.bradleysmithllc.etlagent.VirtualFiles;

import java.io.IOException;

public class MultipartResponseDTO extends GenericResponseDTO
{
	private final VirtualFiles virtualFiles;

	public MultipartResponseDTO(VirtualFiles virtualFiles) {
		this.virtualFiles = virtualFiles;
		if (virtualFiles == null)
		{
			throw new IllegalArgumentException("Virtual Files may not be null.");
		}
	}

	public <T extends GenericResponseDTO> T getRequestDTO(Class<T> clas) throws IOException {
		Gson gson = new GsonBuilder()
				.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
				.create();

		return gson.fromJson(virtualFiles.getReader().getRequestNode().toString(), clas);
	}

	public VirtualFiles getVirtualFiles() {
		return virtualFiles;
	}
}