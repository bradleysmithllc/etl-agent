package org.bitbucket.bradleysmithllc.etlagent.resources;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlunit.*;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ContextBase
{
	public static final Map<String, String> testContextMap = Collections.synchronizedMap(new HashMap<String, String>());

	public static RuntimeSupportProxy runtimeSupport;
	public static AgentDispatcher agentDispatcher;

	public static String MULTIPART_OKAY = "OKAY";
	public static String MULTIPART_FORBIDDEN = "FORBIDDEN";

	public static boolean testMode = false;
	public static boolean multipartRequestEnabled = true;

	public static boolean agentActive()
	{
		return agentDispatcher != null;
	}

	public static void initiate(boolean testMode)
	{
		ContextBase.testMode = testMode;

		AgentDispatcher.lastRequestedAgentId = null;
		AgentDispatcher.lastRequestId = null;
		AgentDispatcher.lastRequestNode = null;
		AgentDispatcher.lastRequest = null;
		AgentDispatcher.lastResponse = null;

		if (testMode)
		{
			System.out.println("ETL Agent in test mode");
		}
		else
		{
			System.out.println("ETL Agent in normal mode");
		}

		if (multipartRequestEnabled)
		{
			System.out.println("Responding to multipart requests");
		}
		else
		{
			System.out.println("Forbidding multipart requests");
		}
	}

	public static boolean agentTestMode()
	{
		return ContextBase.testMode;
	}

	public static void setRoot(File root)
	{
		System.out.println("ETL Agent using root " + root);

		try {
			FileUtils.forceMkdir(root);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		BasicRuntimeSupport brs = new BasicRuntimeSupport(root);

		runtimeSupport = new RuntimeSupportProxy(brs);

		brs.setApplicationLogger(new PrintWriterLog());
		brs.setUserLogger(new PrintWriterLog());

		if (agentTestMode())
		{
			runtimeSupport.installProcessExecutor(new LoggingProcessExecutor());
		}

		agentDispatcher = new AgentDispatcher(runtimeSupport);
	}

	public static void initialize()
	{
		agentDispatcher.initializeAgents();
	}
}