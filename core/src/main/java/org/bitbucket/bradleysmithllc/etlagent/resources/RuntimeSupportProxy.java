package org.bitbucket.bradleysmithllc.etlagent.resources;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.feature.RuntimeOption;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestClass;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestMethod;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestOperation;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestPackage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;

public class RuntimeSupportProxy implements RuntimeSupport
{
	private final RuntimeSupport runtimeSupport;

	private final ThreadLocal<String> currentRequestUID = new ThreadLocal<String>();

	public RuntimeSupportProxy(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	@Override
	public String getLocalAddress() {
		return runtimeSupport.getLocalAddress();
	}

	@Override
	public boolean isTestActive() {
		return runtimeSupport.isTestActive();
	}

	@Override
	public String digestIdentifier(String id) {
		return runtimeSupport.digestIdentifier(id);
	}

	@Override
	public void activateTempFileNamingPolicy(TempFileNamingPolicy policy) {
		runtimeSupport.activateTempFileNamingPolicy(policy);
	}

	@Override
	public File resolveFile(File file) {
		return runtimeSupport.resolveFile(file);
	}

	@Override
	public File getProjectRoot() throws IOException {
		return runtimeSupport.getProjectRoot();
	}

	@Override
	public String getProjectRelativePath(File fke) throws IOException {
		return runtimeSupport.getProjectRelativePath(fke);
	}

	@Override
	public File getTempRoot() {
		return runtimeSupport.getTempRoot();
	}

	public void initiateSessionRequest(String sessionID)
	{
		currentRequestUID.set(sessionID);
	}

	@Override
	public File getTempDirectory() {
		return runtimeSupport.getTempDirectory();
	}

	@Override
	public File getTempDirectory(int executor) {
		return runtimeSupport.getTempDirectory(executor);
	}

	@Override
	public File createTempFolder(String dirname) {
		return runtimeSupport.createTempFolder(dirname);
	}

	@Override
	public File createAnonymousTempFolder() {
		return runtimeSupport.createAnonymousTempFolder();
	}

	@Override
	public File createTempFile(String name) {
		return runtimeSupport.createTempFile(name);
	}

	@Override
	public File createTempFile(String subdir, String name) {
		return runtimeSupport.createTempFile(subdir, name);
	}

	@Override
	public File createAnonymousTempFileWithPrefix(String name) {
		return runtimeSupport.createAnonymousTempFileWithPrefix(name);
	}

	@Override
	public File createAnonymousTempFile() {
		return runtimeSupport.createAnonymousTempFile();
	}

	@Override
	public File createAnonymousTempFileWithExtension(String ext) {
		return runtimeSupport.createAnonymousTempFileWithExtension(ext);
	}

	@Override
	public File createAnonymousTempFileInSubdir(String subdir) {
		return runtimeSupport.createAnonymousTempFileInSubdir(subdir);
	}

	@Override
	public File createAnonymousTempFile(String subdir, String ext) {
		return runtimeSupport.createAnonymousTempFile(subdir, ext);
	}

	@Override
	public File getGeneratedSourceDirectory(String feature) {
		return runtimeSupport.getGeneratedSourceDirectory(feature);
	}

	@Override
	public File createGeneratedSourceFile(String feature, String name) {
		return runtimeSupport.createGeneratedSourceFile(feature, name);
	}

	@Override
	public File getFeatureSourceDirectory(String feature) {
		return runtimeSupport.getFeatureSourceDirectory(feature);
	}

	@Override
	public File getSourceDirectory() {
		return runtimeSupport.getSourceDirectory();
	}

	@Override
	public File getTestSourceDirectory() {
		return runtimeSupport.getTestSourceDirectory();
	}

	@Override
	public File getTestSourceDirectory(ETLTestPackage _package) {
		return runtimeSupport.getTestSourceDirectory(_package);
	}

	@Override
	public File getCurrentTestSourceDirectory() {
		return runtimeSupport.getCurrentTestSourceDirectory();
	}

	@Override
	public File getTestResourceDirectory(String subdir) {
		return runtimeSupport.getTestResourceDirectory(subdir);
	}

	@Override
	public File getTestResourceDirectory(ETLTestPackage _package, String subdir) {
		return runtimeSupport.getTestResourceDirectory(_package, subdir);
	}

	@Override
	public File getCurrentTestResourceDirectory(String subdir) {
		return runtimeSupport.getCurrentTestResourceDirectory(subdir);
	}

	@Override
	public File getGlobalReportDirectory(String type) {
		return runtimeSupport.getGlobalReportDirectory(type);
	}

	@Override
	public File getReportDirectory(String type) {
		return runtimeSupport.getReportDirectory(type);
	}

	@Override
	public File getFeatureConfigurationDirectory(String feature) {
		return runtimeSupport.getFeatureConfigurationDirectory(feature);
	}

	@Override
	public File getSourceDirectory(ETLTestPackage feature) {
		return runtimeSupport.getSourceDirectory(feature);
	}

	@Override
	public File createSourceFile(ETLTestPackage feature, String name) {
		return runtimeSupport.createSourceFile(feature, name);
	}

	@Override
	public File getReferenceDirectory(String path_) {
		return runtimeSupport.getReferenceDirectory(path_);
	}

	@Override
	public File getReferenceFile(String path_, String name) {
		return runtimeSupport.getReferenceFile(path_, name);
	}

	@Override
	public File getReferenceFile(String path_) {
		return runtimeSupport.getReferenceFile(path_);
	}

	@Override
	public URL getReferenceResource(String path_, String name) {
		return runtimeSupport.getReferenceResource(path_, name);
	}

	@Override
	public URL getReferenceResource(String path_) {
		return runtimeSupport.getReferenceResource(path_);
	}

	@Override
	public ProcessExecutor getProcessExecutor() {
		return runtimeSupport.getProcessExecutor();
	}

	@Override
	public void installProcessExecutor(ProcessExecutor od) {
		runtimeSupport.installProcessExecutor(od);
	}

	@Override
	public ProcessFacade execute(ProcessDescription pd) throws IOException {
		return runtimeSupport.execute(pd);
	}

	@Override
	public String getProjectName() {
		return runtimeSupport.getProjectName();
	}

	@Override
	public String getProjectVersion() {
		return runtimeSupport.getProjectVersion();
	}

	@Override
	public String getProjectUser() {
		return runtimeSupport.getProjectUser();
	}

	/**
	 * Here is where we differ.  Substitute the session id for this request
	 * for the project UID.
	 * @return
	 */
	@Override
	public String getProjectUID() {
		return currentRequestUID.get();
	}

	@Override
	public String getProjectUID(int executor) {
		return runtimeSupport.getProjectUID(executor);
	}

	@Override
	public void overrideRuntimeOption(RuntimeOption runtimeOption) {
		runtimeSupport.overrideRuntimeOption(runtimeOption);
	}

	@Override
	public void overrideRuntimeOptions(List<RuntimeOption> runtimeOptions) {
		runtimeSupport.overrideRuntimeOptions(runtimeOptions);
	}

	@Override
	public RuntimeOption getRuntimeOption(String qualifiedName) {
		return runtimeSupport.getRuntimeOption(qualifiedName);
	}

	@Override
	public List<RuntimeOption> getRuntimeOptions(String featureName) {
		return runtimeSupport.getRuntimeOptions(featureName);
	}

	@Override
	public List<RuntimeOption> getRuntimeOptions() {
		return runtimeSupport.getRuntimeOptions();
	}

	@Override
	public List<ETLTestPackage> getPackages(File root) {
		return runtimeSupport.getPackages(root);
	}

	@Override
	public List<ETLTestPackage> getReferencePackages(String path) {
		return runtimeSupport.getReferencePackages(path);
	}

	@Override
	public List<ETLTestPackage> getTestPackages() {
		return runtimeSupport.getTestPackages();
	}

	@Override
	public ETLTestPackage getCurrentlyProcessingTestPackage() {
		return runtimeSupport.getCurrentlyProcessingTestPackage();
	}

	@Override
	public ETLTestClass getCurrentlyProcessingTestClass() {
		return runtimeSupport.getCurrentlyProcessingTestClass();
	}

	@Override
	public ETLTestMethod getCurrentlyProcessingTestMethod() {
		return runtimeSupport.getCurrentlyProcessingTestMethod();
	}

	@Override
	public ETLTestOperation getCurrentlyProcessingTestOperation() {
		return runtimeSupport.getCurrentlyProcessingTestOperation();
	}

	@Override
	public File getVendorBinaryDir(String path) {
		return runtimeSupport.getVendorBinaryDir(path);
	}

	@Override
	public File getVendorBinaryDir() {
		return runtimeSupport.getVendorBinaryDir();
	}

	@Override
	public Log getApplicationLog() {
		return runtimeSupport.getApplicationLog();
	}

	@Override
	public Log getUserLog() {
		return runtimeSupport.getUserLog();
	}

	@Override
	public int getExecutorId() {
		return runtimeSupport.getExecutorId();
	}

	@Override
	public String getRunIdentifier() {
		return runtimeSupport.getRunIdentifier();
	}

	@Override
	public void setTestStartTime()
	{
		runtimeSupport.setTestStartTime();
	}

	@Override
	public void setTestStartTime(Date date)
	{
		runtimeSupport.setTestStartTime(date);
	}

	@Override
	public void setTestStartTime(long l)
	{
		runtimeSupport.setTestStartTime(l);
	}

	@Override
	public Date getTestStartTime()
	{
		return runtimeSupport.getTestStartTime();
	}

	@Override
	public int getExecutorCount() {
		return runtimeSupport.getExecutorCount();
	}
}