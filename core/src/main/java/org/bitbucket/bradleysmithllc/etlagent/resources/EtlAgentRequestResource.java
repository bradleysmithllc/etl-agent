package org.bitbucket.bradleysmithllc.etlagent.resources;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.Server;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import java.io.File;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

@Path("/rest/service/{session}/{agent-type}/{agent-request}")
public class EtlAgentRequestResource {
	Logger logger = LogManager.getLogger(getClass());

	@Context
	private AgentDispatcher agentDispatcher;
	@PathParam("agent-type")
	private String agentId;
	@PathParam("session")
	private String sessionId;
	@PathParam("agent-request")
	private String requestId;
	@Context
	private RuntimeSupportProxy runtimeSupport;

	public static AtomicInteger sleepers = new AtomicInteger();
	public static AtomicInteger maxSleepers = new AtomicInteger();

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public String processAgentRequest(String postData) throws Exception {
		logger.info("Enter process request");

		sleepers.incrementAndGet();

		maxSleepers.set(Math.max(maxSleepers.get(), sleepers.get()));

		try {
			// create a session 'lock' file (FYI only)
			String requestUID = getRequestUID();

			ThreadContext.put("agent-type", agentId);
			ThreadContext.put("request-uid", requestUID);
			ThreadContext.put("agent-request", requestId);
			ThreadContext.put("session", sessionId);

			try {
				return processAgentRequestImpl(postData, requestUID);
			} finally {
				ThreadContext.clear();
			}
		} finally {
			sleepers.decrementAndGet();
			logger.info("Exit process request");
		}
	}

	public String processAgentRequestImpl(String postData, String requestUID) throws Exception {
		File lck = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory("agent")).subdir("sessionLock").name(
				requestUID + ".lck"
		).file();

		FileUtils.touch(lck);

		try {
			// grab the session id right off the bat.
			runtimeSupport.initiateSessionRequest(requestUID);

			long requestTime = System.currentTimeMillis();

			Gson gson = new GsonBuilder()
					.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
					.create();

			ObjectMapper om = new ObjectMapper();

			// log the request
			File requestFile = runtimeSupport.createTempFile("processLog", requestUID + ".request");

			JsonNode request = JsonLoader.fromString(postData);

			ObjectNode on = (ObjectNode) request;

			if (on != null) {
				on.put("session-id", sessionId);
				on.put("request-uid", requestUID);
				on.put("agent-version", Server.VERSION);
			} else {
				request = JsonLoader.fromString(
						"{\"session-id\": \"" + sessionId + "\"," +
								" \"request-uid\": \"" + requestUID + "\", " +
								" \"agent-version\": \"" + Server.VERSION + "\"" +
								"}"
				);
			}

			FileUtils.write(requestFile, om.writerWithDefaultPrettyPrinter().writeValueAsString(request));

			long responseSystemDateTime;
			String resJson = null;

			try {
				GenericResponseDTO res = agentDispatcher.dispatch(agentId, requestId, request);

				if (!res.customBuilder()) {
					resJson = gson.toJson(res);
				} else {
					resJson = res.buildJson().toString();
				}
			} catch (Exception exc) {
				GenericResponseDTO res = new GenericResponseDTO();

				res.setAgentId(agentId);
				res.setRequestId(requestId);
				res.setAgentVersion(Server.VERSION);
				res.setResponse(GenericResponseDTO.response_code.failed);
				responseSystemDateTime = System.currentTimeMillis();
				res.setResponseSystemDateTime(responseSystemDateTime);
				res.setRequestSystemDateTime(requestTime);
				res.setResponseMessage(exc.toString());

				resJson = gson.toJson(res);
			} finally {
				File responseFile = runtimeSupport.createTempFile("processLog", requestUID + ".response");

				JsonNode response = JsonLoader.fromString(resJson == null ? "{}" : resJson);

				FileUtils.write(responseFile, om.writerWithDefaultPrettyPrinter().writeValueAsString(response));
			}

			return resJson;
		} finally {
			lck.delete();
		}
	}

	private String getRequestUID() {
		StringBuilder stb = new StringBuilder();

		stb.append(agentId + "_" + requestId + "_" + sessionId);

		stb.append("_").append(UUID.randomUUID().toString());

		return stb.toString();
	}

	@GET
	@Produces("text/plain")
	public String errorGet() {
		return "Only post supported";
	}
}