package org.bitbucket.bradleysmithllc.etlagent.client;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.inject.Inject;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import org.bitbucket.bradleysmithllc.etlagent.dto.AgentDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.AgentRequestsDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.AgentsDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.InitializeResponse;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import javax.ws.rs.core.MediaType;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class EtlAgentClient
{
	public static int DEFAULT_PORT = 8188;

	private final String hostName;
	private final int port;
	private final String clientId;

	private String sessionId;

	private final Client client = Client.create();
	private final WebResource webResource;

	protected RuntimeSupport runtimeSupport;

	public EtlAgentClient(String clientId) throws UnknownHostException {
		this(clientId, InetAddress.getLocalHost().getHostAddress(), DEFAULT_PORT);
	}

	public EtlAgentClient(String clientId, int port) throws UnknownHostException {
		this(clientId, InetAddress.getLocalHost().getHostAddress(), port);
	}

	public EtlAgentClient(String clientId, String hostName)
	{
		this(clientId, hostName, DEFAULT_PORT);
	}

	public EtlAgentClient(String clientId, String hostName, int port)
	{
		this.port = port;
		this.hostName = hostName;

		webResource = client.resource("http://" +
				"" + hostName + ":" + port + "/rest");

		this.clientId = clientId;
	}

	@Inject
	public void receiveRuntimeSupport(RuntimeSupport runtimeSupport)
	{
		this.runtimeSupport = runtimeSupport;
	}

	private void connect()
	{
		if (sessionId == null)
		{
			// logon and create a session id/rest/initialize/
			String json = webResource.path("initialize").path(clientId).accept(MediaType.APPLICATION_JSON_TYPE).get(String.class);

			InitializeResponse ir =  new GsonBuilder()
					.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create().fromJson(json, InitializeResponse.class);

			sessionId = ir.getSessionId();
		}
	}

	public AgentsDTO agents()
	{
		String json = webResource.path("agents").accept(MediaType.APPLICATION_JSON_TYPE).get(String.class);

		Gson gson = new Gson();

		return gson.fromJson(json, AgentsDTO.class);
	}

	public AgentRequestsDTO agent(AgentDTO agent)
	{
		String json = webResource.path("agent/" + agent.getId()).accept(MediaType.APPLICATION_JSON_TYPE).get(String.class);

		Gson gson = new Gson();

		return gson.fromJson(json, AgentRequestsDTO.class);
	}

	public static void main(String [] argv) throws Exception
	{
		AgentsDTO agentsDTO = new EtlAgentClient("testcl", "bsmithvm").agents();
		System.out.println(agentsDTO);
		System.out.println(new EtlAgentClient("testcl", "bsmithvm").agent(agentsDTO.getAgents().get(0)));
	}

	/**
	 * The base service request type has the session id
	 * @return
	 */
	protected WebResource service()
	{
		connect();
		return webResource.path("service").path(sessionId);
	}

	/**
	 * The base service request type has the session id
	 * @return
	 */
	protected WebResource multipartService()
	{
		connect();
		return webResource.path("multipartservice").path(sessionId);
	}

	/**
	 * Base agent request is session id and agent name
	 * @param agentId
	 * @return
	 */
	protected WebResource agent(String agentId)
	{
		return service().path(agentId);
	}

	/**
	 * Base agent request is session id and agent name
	 * @param agentId
	 * @return
	 */
	protected WebResource multipartAgent(String agentId)
	{
		return multipartService().path(agentId);
	}

	/**
	 * A 'full' request url - session agent and request
	 * @param agentId
	 * @param requestId
	 * @return
	 */
	protected WebResource agentRequest(String agentId, String requestId)
	{
		return agent(agentId).path(requestId);
	}

	/**
	 * A 'full' request url - session agent and request
	 * @param agentId
	 * @param requestId
	 * @return
	 */
	protected WebResource multipartAgentRequest(String agentId, String requestId)
	{
		return multipartAgent(agentId).path(requestId);
	}
}