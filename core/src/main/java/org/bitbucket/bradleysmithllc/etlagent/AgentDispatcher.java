package org.bitbucket.bradleysmithllc.etlagent;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.exceptions.InvalidSchemaException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.report.ProcessingMessage;
import com.github.fge.jsonschema.report.ProcessingReport;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class AgentDispatcher {
	private Logger logger = LogManager.getLogger(getClass());

	public static final String INVALID_REQUEST_MISSING_REQUEST_NODE = "INVALID_REQUEST_MISSING_REQUEST_NODE";
	public static final String INVALID_REQUEST_MANGLED_CONTAINER = "INVALID_REQUEST_MANGLED_CONTAINER";
	public static final String AGENT_DOES_NOT_HANDLE_REQUEST = "AGENT_DOES_NOT_HANDLE_REQUEST";
	public static final String AGENT_DOES_NOT_EXIST = "AGENT_DOES_NOT_EXIST";
	public static final String AGENT_FAILURE = "AGENT_FAILURE";

	private final List<Agent> agents = new ArrayList<Agent>();
	private final Map<String, Agent> agentMap = new HashMap<String, Agent>();

	private RuntimeSupport runtimeSupport;

	public AgentDispatcher() {
		this(new BasicRuntimeSupport());
	}

	public AgentDispatcher(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	public AgentDispatcher(String loca) {
	}

	public void initializeAgents() {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		ServiceLoader<Agent> sl = ServiceLoader.load(Agent.class, cl);

		Iterator<Agent> it = sl.iterator();

		while (it.hasNext()) {
			registerAgent(it.next());
		}
	}

	public List<Agent> getAgents() {
		return Collections.unmodifiableList(agents);
	}

	public Map<String, Agent> getAgentMap() {
		return Collections.unmodifiableMap(agentMap);
	}

	public void registerAgent(Agent agent) throws IllegalArgumentException {
		if (agentMap.containsKey(agent.getId())) {
			throw new IllegalArgumentException("Agent type [" + agent.getId() + "] already registered");
		}

		agent.receiveRuntimeSupport(runtimeSupport);

		// look up a configuration and validate / load it.
		File dir = runtimeSupport.getFeatureConfigurationDirectory(agent.getId());

		File config = new File(dir, agent.getId() + ".json");

		try {
			if (config.exists()) {
				logger.debug("Loading agent configuration from {}", config);

				String text = FileUtils.readFileToString(config);

				JsonNode jnode = JsonLoader.fromString(text);

				// look for a validator
				JsonNode validatorNode = agent.getConfigurationValidator();

				// create a schema loader
				JsonSchemaFactory fact = JsonSchemaFactory.byDefault();

				JsonSchema res = fact.getJsonSchema(validatorNode);

				ProcessingReport results = res.validate(jnode);

				if (results.isSuccess()) {
					agent.setAgentConfiguration(jnode);
				} else {
					Iterator<ProcessingMessage> it = results.iterator();

					while (it.hasNext()) {
						logger.info(it.next().toString());
					}

					throw new IllegalArgumentException("Bad configuration for agent [" + agent.getId() + "] '" + text + "'");
				}
			} else {
				logger.info("No configuration found");
			}
		} catch (InvalidSchemaException ise) {
			throw new IllegalArgumentException(ise);
		} catch (Exception e) {
			throw new IllegalArgumentException(e);
		}

		agentMap.put(agent.getId(), agent);
		agents.add(agent);
	}

	public void registerAgents(List<Agent> agentList) {
		for (Agent agent : agentList) {
			registerAgent(agent);
		}
	}

	public GenericResponseDTO dispatch(final String agentId, final String requestId, final JsonNode request) throws Exception {
		return dispatchWrapperImpl(new Dispatch<GenericResponseDTO, GenericResponseDTO>() {
			@Override
			public String getAgentId() {
				return agentId;
			}

			@Override
			public String getRequestId() {
				return requestId;
			}

			@Override
			public JsonNode getRequest() throws IOException {
				return request;
			}

			@Override
			public GenericResponseDTO fail(String cause) throws IOException {
				return AgentDispatcher.this.fail(agentId, requestId, cause);
			}

			@Override
			public Agent.RequestHandlerBase [] getRequestHandlers(Agent agent) {
				return agent.getRequestHandlers().toArray(new Agent.RequestHandlerBase[0]);
			}

			@Override
			public GenericResponseDTO process(Agent.RequestHandlerBase requestHandler, JsonNode request, Object vobj) throws Exception {
				return ((Agent.RequestHandler) requestHandler).process(request, vobj);
			}

			@Override
			public GenericResponseDTO success() {
				long time = System.currentTimeMillis();

				GenericResponseDTO res = new GenericResponseDTO();

				res.setAgentId(agentId);
				res.setRequestId(requestId);
				res.setResponse(GenericResponseDTO.response_code.okay);
				res.setResponseSystemDateTime(time);
				res.setRequestSystemDateTime(time);

				return res;
			}

			@Override
			public GenericResponseDTO prepareResponse(GenericResponseDTO response) {
				return response;
			}
		});
	}

	interface Dispatch<J extends GenericResponseDTO, K extends GenericResponseDTO>
	{
		String getAgentId();
		String getRequestId();
		JsonNode getRequest() throws IOException;

		J fail(String cause) throws IOException;

		Agent.RequestHandlerBase [] getRequestHandlers(Agent agent);

		J process(Agent.RequestHandlerBase requestHandler, JsonNode request, Object vobj) throws Exception;

		J success() throws IOException;

		K prepareResponse(J response) throws IOException;
	}

	public <T extends GenericResponseDTO, K extends GenericResponseDTO> K dispatchWrapperImpl(Dispatch<T, K> dispatch) throws Exception {
		lastRequestedAgentId = dispatch.getAgentId();
		lastRequestId = dispatch.getRequestId();

		long requestTime = System.currentTimeMillis();

		logger.info("Agent dispatching.");
		T response = dispatchImpl(dispatch);
		logger.info("Agent dispatch complete {}", response);

		if (response == null)
		{
			response = dispatch.success();
		}

		if (response.getResponseMessage() == null)
		{
			response.setResponseMessage("<<empty>>");
		}

		if (response.getResponse() == null)
		{
			response.setResponse(GenericResponseDTO.response_code.okay);
		}

		response.setAgentVersion(Server.VERSION);
		response.setAgentId(dispatch.getAgentId());
		response.setRequestId(dispatch.getRequestId());

		response.setAgentVersion(Server.VERSION);
		response.setResponseSystemDateTime(System.currentTimeMillis());
		response.setRequestSystemDateTime(requestTime);

		long responseSystemDateTime = System.currentTimeMillis();
		response.setResponseSystemDateTime(responseSystemDateTime);
		response.setRequestSystemDateTime(requestTime);

		lastResponse = response;

		return dispatch.prepareResponse(response);
	}

	public <T extends GenericResponseDTO, K extends GenericResponseDTO> T dispatchImpl(Dispatch<T, K> dispatch) throws Exception {
		JsonNode request = null;

		try
		{
			request = dispatch.getRequest();
			lastRequestNode = request;
		}
		catch(FileNotFoundException fnfe)
		{
			return dispatch.fail(INVALID_REQUEST_MISSING_REQUEST_NODE);
		}
		catch(IOException ioe)
		{
			return dispatch.fail(INVALID_REQUEST_MANGLED_CONTAINER);
		}

		try {
			if (!agentMap.containsKey(dispatch.getAgentId())) {
				return dispatch.fail(AGENT_DOES_NOT_EXIST);
			}

			Agent agent = agentMap.get(dispatch.getAgentId());

			for (Agent.RequestHandlerBase requestHandler : dispatch.getRequestHandlers(agent)) {
				if (requestHandler.getId().equals(dispatch.getRequestId())) {
					Gson gson = requestHandler.configureBuilder(new GsonBuilder()
							.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
					).create();

					Object requestContainerObject = requestHandler.getRequestContainerObject();

					Object vobj = null;

					if (requestContainerObject != null) {
						vobj = gson.fromJson(request.toString(), requestContainerObject.getClass());
						lastRequest = vobj;
					}

					T response = dispatch.process(requestHandler, request, vobj);

					return response;
				}
			}
		} catch (Exception exc) {
			logger.error(exc);

			return dispatch.fail(AGENT_FAILURE);
		}

		return dispatch.fail(AGENT_DOES_NOT_HANDLE_REQUEST);
	}

	public MultipartResponseDTO dispatchMultipart(final String agentId, final String requestId, final String sessionId, final String requestUID, final VirtualFiles vflRequest, final VirtualFiles vflResponse) throws Exception {
		return dispatchWrapperImpl(new Dispatch<GenericResponseDTO, MultipartResponseDTO>() {
			@Override
			public String getAgentId() {
				return agentId;
			}

			@Override
			public String getRequestId() {
				return requestId;
			}

			@Override
			public JsonNode getRequest() throws IOException {
				VirtualFiles.VReader reader = vflRequest.getReader();

				try
				{
					JsonNode node = reader.getRequestNode();

					ObjectNode on = (ObjectNode) node;

					if (on != null) {
						on.put("session-id", sessionId);
						on.put("request-uid", requestUID);
						on.put("server-version", Server.VERSION);
					}

					return on;
				}
				finally
				{
					reader.dispose();
				}
			}

			@Override
			public GenericResponseDTO fail(String cause) throws IOException {
				return AgentDispatcher.this.fail(agentId, requestId, cause);
			}

			@Override
			public Agent.RequestHandlerBase[] getRequestHandlers(Agent agent) {
				return agent.getMultipartRequestHandlers().toArray(new Agent.RequestHandlerBase[0]);
			}

			@Override
			public GenericResponseDTO process(Agent.RequestHandlerBase requestHandler, JsonNode request, Object vobj) throws Exception {
				return ((Agent.MultipartRequestHandler) requestHandler).process(request, vflRequest, vflResponse, vobj);
			}

			@Override
			public GenericResponseDTO success() throws IOException {
				long time = System.currentTimeMillis();

				GenericResponseDTO res = new GenericResponseDTO();

				res.setAgentVersion(Server.VERSION);
				res.setAgentId(agentId);
				res.setRequestId(requestId);
				res.setResponse(GenericResponseDTO.response_code.okay);
				res.setResponseSystemDateTime(time);
				res.setRequestSystemDateTime(time);
				res.setResponseMessage("empty");

				return res;
			}

			@Override
			public MultipartResponseDTO prepareResponse(GenericResponseDTO response) throws IOException {
				Gson gson = new GsonBuilder()
					.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
					.create();

				// serialize the response and load back into a json tree
				String json = gson.toJson(response);

				VirtualFiles.VWriter writer = vflResponse.getWriter();

				try
				{
					writer.writeRequestNode(JsonLoader.fromString(json));
				}
				finally
				{
					// finish this deal
					writer.dispose();
					writer._package();
				}

				MultipartResponseDTO mresponse = new MultipartResponseDTO(vflResponse);

				return mresponse;
			}
		});
	}

	private GenericResponseDTO dispatchMultipartImpl(String agentId, String requestId, VirtualFiles vflRequest, VirtualFiles vflResponse) throws Exception {
		lastRequestedAgentId = agentId;
		lastRequestId = requestId;

		try
		{
			lastRequestNode = vflRequest.getReader().getRequestNode();
		}
		catch(FileNotFoundException fnfe)
		{
			return fail(agentId, requestId, INVALID_REQUEST_MISSING_REQUEST_NODE);
		}
		catch(IOException ioe)
		{
			return fail(agentId, requestId, INVALID_REQUEST_MANGLED_CONTAINER);
		}

		try {
			if (!agentMap.containsKey(agentId)) {
				return fail(agentId, requestId, AGENT_DOES_NOT_EXIST);
			}

			Agent agent = agentMap.get(agentId);

			for (Agent.MultipartRequestHandler requestHandler : agent.getMultipartRequestHandlers()) {
				if (requestHandler.getId().equals(requestId)) {
					Gson gson = requestHandler.configureBuilder(new GsonBuilder()
							.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
					).create();

					Object requestContainerObject = requestHandler.getRequestContainerObject();

					Object vobj = null;

					if (requestContainerObject != null) {
						vobj = gson.fromJson(lastRequestNode.toString(), requestContainerObject.getClass());
						lastRequest = vobj;
					}

					GenericResponseDTO response = requestHandler.process(lastRequestNode, vflRequest, vflResponse, vobj);

					lastResponse = response;

					return response;
				}
			}
		} catch (Exception exc) {
			exc.printStackTrace();

			return fail(agentId, requestId, AGENT_FAILURE);
		}

		return fail(agentId, requestId, AGENT_DOES_NOT_HANDLE_REQUEST);
	}

	private GenericResponseDTO fail(String agentId, String requestId, String invalidRequestMissingRequestNode) throws IOException {
		long time = System.currentTimeMillis();

		GenericResponseDTO res = new GenericResponseDTO();

		res.setAgentId(agentId);
		res.setRequestId(requestId);
		res.setAgentVersion(Server.VERSION);
		res.setResponse(GenericResponseDTO.response_code.failed);
		res.setResponseSystemDateTime(time);
		res.setRequestSystemDateTime(time);
		res.setResponseMessage(invalidRequestMissingRequestNode);

		return res;
	}

	public void receiveRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	public static String lastRequestedAgentId;
	public static String lastRequestId;

	public static JsonNode lastRequestNode;
	public static Object lastRequest;
	public static GenericResponseDTO lastResponse;
}