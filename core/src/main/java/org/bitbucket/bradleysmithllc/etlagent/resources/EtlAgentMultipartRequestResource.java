package org.bitbucket.bradleysmithllc.etlagent.resources;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.github.fge.jackson.JsonLoader;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.Server;
import org.bitbucket.bradleysmithllc.etlagent.VirtualFiles;
import org.bitbucket.bradleysmithllc.etlagent.ZipVirtualFilesImpl;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

@Path("/rest/multipartservice/{session}/{agent-type}/{agent-request}")
public class EtlAgentMultipartRequestResource {
	Logger logger = LogManager.getLogger(getClass());

	@Context
	private AgentDispatcher agentDispatcher;
	@PathParam("agent-type")
	private String agentId;
	@PathParam("session")
	private String sessionId;
	@PathParam("agent-request")
	private String requestId;
	@Context
	private RuntimeSupportProxy runtimeSupport;

	@POST
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	@Consumes(MediaType.APPLICATION_OCTET_STREAM)
	public StreamingOutput processAgentRequest(InputStream postData) throws Exception {
		logger.info("Enter process multipart request");

		try {
		// create a session 'lock' file (FYI only)
		String requestUID = getRequestUID();

		ThreadContext.put("agent-type", agentId);
		ThreadContext.put("request-uid", requestUID);
		ThreadContext.put("agent-request", requestId);
		ThreadContext.put("session", sessionId);

		try
		{
			return processAgentRequestImpl(postData, requestUID);
		}
		finally
		{
			ThreadContext.clear();
		}
		} finally {
			logger.info("Exit process multipart request");
		}
	}

	public StreamingOutput processAgentRequestImpl(InputStream postData, String requestUID) throws Exception {
		// grab the session id right off the bat.
		runtimeSupport.initiateSessionRequest(requestUID);

		File lck = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory("agent")).subdir("sessionLock").name(
				requestUID + ".lck"
		).file();

		AgentDispatcher.lastRequestedAgentId = agentId;
		AgentDispatcher.lastRequestId = requestId;

		FileUtils.touch(lck);

		try {
			final MultipartResponseDTO dto;

			File tmpResponseZip = runtimeSupport.createTempFile("processLog", requestUID + "_response.zip");

			if (!ContextBase.multipartRequestEnabled)
			{
				/**
				 * Error response package.
				 */
				VirtualFiles vfiles = new ZipVirtualFilesImpl(
					tmpResponseZip,
					runtimeSupport.createTempFolder(requestUID + "_response.ziptemp")
				);

				VirtualFiles.VWriter writer = vfiles.getWriter();
				writer.writeRequestNode(
						JsonLoader.fromString(
								"{" +
										"\"session-id\": \"" + sessionId + "\"," +
										"\"request-uid\": \"" + requestUID + "\"," +
										"\"agent-version\": \"" + Server.VERSION + "\"," +
										"\"response\": \"failed\"," +
										"\"response-message\": \"" + ContextBase.MULTIPART_FORBIDDEN + "\"" +
								"}"
						)
				);

				writer.dispose();
				writer._package();

				dto = new MultipartResponseDTO(vfiles);
				AgentDispatcher.lastResponse = dto;
			}
			else
			{
				System.out.println("Processing multipart request " + requestUID);

				// grab the input stream and copy to a local zip file
				File tmpRequestZip = runtimeSupport.createTempFile("processLog", requestUID + "_request.zip");

				FileUtils.copyInputStreamToFile(postData, tmpRequestZip);

				dto = agentDispatcher.dispatchMultipart(
						agentId,
						requestId,
						sessionId,
						requestUID,
						new ZipVirtualFilesImpl(tmpRequestZip, runtimeSupport.createTempFolder(requestUID + "_response.ziptemp")),
						new ZipVirtualFilesImpl(tmpResponseZip, runtimeSupport.createTempFolder(requestUID + "_response.ziptemp"))
				);
			}

			AgentDispatcher.lastResponse = dto;

			return new StreamingOutput() {
				@Override
				public void write(OutputStream output) throws IOException, WebApplicationException {
					FileUtils.copyFile(dto.getVirtualFiles().getFile(), output);
				}
			};
		} finally {
			lck.delete();
		}
	}

	private String getRequestUID() {
		StringBuilder stb = new StringBuilder();

		stb.append(agentId + "_" + requestId + "_" + sessionId);

		stb.append("_").append(UUID.randomUUID().toString());

		return stb.toString();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.TEXT_PLAIN)
	public String pingGet() {
		return ContextBase.multipartRequestEnabled ? ContextBase.MULTIPART_OKAY : ContextBase.MULTIPART_FORBIDDEN;
	}
}