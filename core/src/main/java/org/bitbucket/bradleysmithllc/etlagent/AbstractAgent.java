package org.bitbucket.bradleysmithllc.etlagent;

/*
 * #%L
 * Agent core
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractAgent implements Agent
{
	protected RuntimeSupport runtimeSupport;

	@Override
	public final void receiveRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	@Override
	public Map<String, RequestHandler> getRequestHandlerMap() {
		Map<String, RequestHandler> handlerMap = new HashMap<String, RequestHandler>();

		for (RequestHandler handler : getRequestHandlers())
		{
			handlerMap.put(handler.getId(), handler);
		}

		return handlerMap;
	}

	@Override
	public List<MultipartRequestHandler> getMultipartRequestHandlers() {
		return Collections.EMPTY_LIST;
	}

	@Override
	public Map<String, MultipartRequestHandler> getMultipartRequestHandlerMap() {
		Map<String, MultipartRequestHandler> handlerMap = new HashMap<String, MultipartRequestHandler>();

		for (MultipartRequestHandler handler : getMultipartRequestHandlers())
		{
			handlerMap.put(handler.getId(), handler);
		}

		return handlerMap;
	}

	protected GenericResponseDTO createMultipartResponse(GenericResponseDTO.response_code code) throws IOException {
		GenericResponseDTO mdto = new GenericResponseDTO();
		mdto.setResponse(code);

		return mdto;
	}
}