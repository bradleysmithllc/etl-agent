package org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.PmRepProcess;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.PmRepProcessImpl;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.ProcessTimeoutException;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.io.JavaForker;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.concurrent.CountDownLatch;

public class PmRepTest {

	public static final int PERMITS = 1;

	@Test
	public void impost() throws Exception {
		BasicRuntimeSupport rs = new BasicRuntimeSupport();
		rs.setApplicationLogger(new PrintWriterLog());

		JavaForker forker = new JavaForker(rs);
		forker.setMainClass(PmRepImposter.class);
		forker.setOutput(rs.createAnonymousTempFile());

		ProcessFacade process = forker.startProcess();

		CountDownLatch cdl = new CountDownLatch(PERMITS);

		PmRepProcess pmRepProcess = new PmRepProcessImpl(process, rs, Collections.EMPTY_MAP, "id");

		for (int i = 0; i < PERMITS; i++)
		{
			new Thread(
				new PmRepper(pmRepProcess, cdl)//.run();
			).start();
		}

		cdl.await();

		System.out.println("Disposing");

		pmRepProcess.dispose();
	}
}

class PmRepper implements Runnable {
	private final PmRepProcess pmRepProcess;
	private final CountDownLatch countDownLatch;

	PmRepper(PmRepProcess pmRepProcess, CountDownLatch cdl) {
		countDownLatch = cdl;
		this.pmRepProcess = pmRepProcess;
	}

	public void run() {
		try {
			Thread.currentThread().setName("repper-" + System.identityHashCode(this));
			run1();
		} catch (IOException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (ProcessTimeoutException e) {
			throw new RuntimeException(e);
		} finally
		{
			System.out.println(System.identityHashCode(this) + " countin' the latch down - " + countDownLatch.getCount());
			countDownLatch.countDown();
			System.out.println(System.identityHashCode(this) + " done - " + countDownLatch.getCount());
		}
	}

	public void run1() throws IOException, ProcessTimeoutException {
		// check for bogus command
		for (int i = 0; i < 2; i++) {
			System.out.println(System.identityHashCode(this) + "_" + i + "_1");
			String res = pmRepProcess.send("ping-" + i);

			Assert.assertEquals("ping-" + i + " is an unknown command. Type help to see a list of all available commands\r\n" +
					"pmrep>", res);

			System.out.println(System.identityHashCode(this) + "_" + i + "_2");
			res = pmRepProcess.send("connect -r REP_SVC_CI -n administrator -X INFAPASSWD -d DOM_CI");

			Assert.assertEquals("Connected to repository REP_SVC_CI in DOM_CI as user administrator\r\n" +
					"connect completed successfully.\r\n" +
					"pmrep>", res);

			System.out.println(System.identityHashCode(this) + "_" + i + "_3");
			pmRepProcess.send("ping-" + i);

			System.out.println(System.identityHashCode(this) + "_" + i + "_4");
			res = pmRepProcess.send("deletefolder -n __bsmith_f142aa7ae1ae71b191649b5bcef74816_SHARED_EDW");

			Assert.assertEquals("Deleting folder __bsmith_f142aa7ae1ae71b191649b5bcef74816_SHARED_EDW in repository REP_SVC_CI. All the data in the folder will be lost.\r\n" +
					"__bsmith_f142aa7ae1ae71b191649b5bcef74816_SHARED_EDW is a shared Folder. Deleting it will cause any shortcuts accessing it to become unusable.\r\n" +
					"deletefolder completed successfully.\r\n" +
					"pmrep>", res);

			System.out.println(System.identityHashCode(this) + "_" + i + "_5");
			pmRepProcess.send("ping-" + i);
			System.out.println(System.identityHashCode(this) + "_" + i + "_6");
			res = pmRepProcess.send("createfolder -n POS_NEW_TEST -s");

			Assert.assertEquals("Creating folder POS_NEW_TEST in repository REP_SVC_CI.\r\n" +
					"createfolder completed successfully.\r\n" +
					"pmrep>", res);

			System.out.println(System.identityHashCode(this) + "_" + i + "_7");
			pmRepProcess.send("ping-" + i);

			System.out.println(System.identityHashCode(this) + "_" + i + "_8");
			res = pmRepProcess.send("createconnection -s \"Microsoft SQL Server\" -n CONN_TEST -u jrottenseed -p clearTextPassword -v dbserver -b TEST_DB -e \"SET QUOTED_IDENTIFIER ON\" -l MS1252");

			Assert.assertEquals("createconnection completed successfully.\r\n" +
					"pmrep>", res);
		}
	}
}