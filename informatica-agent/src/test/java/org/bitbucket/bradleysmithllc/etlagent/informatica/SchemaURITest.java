package org.bitbucket.bradleysmithllc.etlagent.informatica;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.report.ProcessingMessage;
import com.github.fge.jsonschema.report.ProcessingReport;
import com.google.gson.Gson;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.junit.Test;

import java.io.IOException;
import java.util.Iterator;

public class SchemaURITest
{
	@Test
	public void testRefs() throws IOException {
		InformaticaAgent ia = new InformaticaAgent();

		JsonNode node = ia.getRequestHandlerMap().get("createInformaticaFolder").getValidator();

		JsonSchemaFactory fact = JsonSchemaFactory.byDefault();

		try {
			JsonSchema res = fact.getJsonSchema(node);

			JsonNode json = JsonLoader.fromString("{\"relationalConnectionName\": \"conn\", \"domainInfo\": \"hi\"}");

			ProcessingReport val = res.validate(json);

			System.out.println(val);

			Iterator<ProcessingMessage> it = val.iterator();

			while (it.hasNext())
			{
				ProcessingMessage next = it.next();
				System.out.println(next.asJson());
			}
		} catch (ProcessingException e) {
			System.out.println(e.getProcessingMessage().asJson());
		}

		System.out.println(getClass().getResource("/draftv4/schema"));
	}

	@Test
	public void t2()
	{
		GenericResponseDTO src = new GenericResponseDTO();

		src.setResponseMessage("Hi");
		src.setResponse(GenericResponseDTO.response_code.okay);
		src.setAgentId("agent");
		src.setRequestId("req");

		System.out.println(new Gson().toJson(src));
	}
}