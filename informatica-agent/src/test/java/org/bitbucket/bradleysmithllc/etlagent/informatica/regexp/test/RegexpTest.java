package org.bitbucket.bradleysmithllc.etlagent.informatica.regexp.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.regexp.*;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.junit.Assert;
import org.junit.Test;

public class RegexpTest {
	@Test
	public void testInfaConcurrentWorkflowExpression()
	{
		InfaConcurrentWorkflowExpression exp = InfaConcurrentWorkflowExpression.match("sdasd");

		Assert.assertFalse(exp.matches());
		Assert.assertFalse(exp.hasNext());

		exp = InfaConcurrentWorkflowExpression.match("Connected to Integration Service: [dev_is].\r\n"
						+ "Starting workflow [wkf_LOAD_STORE_DAILY_LABOR_PAYCLOCK]\r\n"
						+ "Waiting for workflow [wkf_LOAD_STORE_DAILY_LABOR_PAYCLOCK] to complete\r\n"
						+ "ERROR: Workflow [POS:wkf_LOAD_STORE_DAILY_LABOR_PAYCLOCK[version 16]]: Could not acquire the execute lock for Workflow [wkf_LOAD_STORE_DAILY_LABOR_PAYCLOCK] [].  Please check the Integration Service log for more information.\r\n"
						+ "INFO: Disconnecting from Integration Service\r\n");

		Assert.assertFalse(exp.matches());
		Assert.assertTrue(exp.hasNext());
		Assert.assertEquals(exp.getWorkflowName(), "LOAD_STORE_DAILY_LABOR_PAYCLOCK");
		Assert.assertFalse(exp.hasNext());
	}

	@Test
	public void testPowermart()
	{
		String s = "<POWERMART CREATION_DATE=\"11/30/2010 12:00:00\" REPOSITORY_VERSION=\"179.88\">".replaceAll("<POWERMART CREATION_DATE=\"\\d{1,2}/\\d{1,2}/\\d{4} \\d{1,2}:\\d{1,2}:\\d{1,2}\" REPOSITORY_VERSION=\"(\\d{1,4}.\\d{1,9})\">", "<POWERMART CREATION_DATE=\"11/30/2010 12:00:00\" REPOSITORY_VERSION=\"$1\">");

		System.out.println(s);
	}

	@Test
	public void testPmRepExpr()
	{
		InfaExportResultExpression iere = InfaExportResultExpression.match("Exported 13 object(s) - 0 Error(s), - 0 Warning(s)");

		Assert.assertTrue(iere.matches());

		Assert.assertEquals(13, iere.getObjects());
		Assert.assertEquals(0, iere.getErrors());
		Assert.assertEquals(0, iere.getWarnings());

		iere = InfaExportResultExpression.match("Exported 1 object(s) - 12 Error(s), - 9 Warning(s)");

		Assert.assertTrue(iere.matches());

		Assert.assertEquals(1, iere.getObjects());
		Assert.assertEquals(12, iere.getErrors());
		Assert.assertEquals(9, iere.getWarnings());

		InfaServerNameExpression isne = InfaServerNameExpression.match("<WORKFLOW DESCRIPTION =\"\" ISENABLED =\"YES\" ISRUNNABLESERVICE =\"NO\" ISSERVICE =\"NO\" ISVALID =\"YES\" NAME =\"wkf_LOAD_ITMMVMT_AUD\" REUSABLE_SCHEDULER =\"NO\" SCHEDULERNAME =\"Scheduler\" SERVERNAME =\"dev_is\" SERVER_DOMAINNAME =\"Domain_etldev01\" SUSPEND_ON_ERROR =\"NO\" TASKS_MUST_RUN_ON_SERVER =\"NO\" VERSIONNUMBER =\"5\">");

		Assert.assertTrue(isne.hasNext());
		Assert.assertEquals("dev_is", isne.getInformaticaIntegrationService());
		Assert.assertFalse(isne.hasNext());

		Assert.assertEquals("<WORKFLOW DESCRIPTION =\"\" ISENABLED =\"YES\" ISRUNNABLESERVICE =\"NO\" ISSERVICE =\"NO\" ISVALID =\"YES\" NAME =\"wkf_LOAD_ITMMVMT_AUD\" REUSABLE_SCHEDULER =\"NO\" SCHEDULERNAME =\"Scheduler\" SERVERNAME =\"svn_is\" SERVER_DOMAINNAME =\"Domain_etldev01\" SUSPEND_ON_ERROR =\"NO\" TASKS_MUST_RUN_ON_SERVER =\"NO\" VERSIONNUMBER =\"5\">", isne.replaceAll("SERVERNAME =\"svn_is\""));

		InfaDomainNameExpression idne = InfaDomainNameExpression.match("<WORKFLOW DESCRIPTION =\"\" ISENABLED =\"YES\" ISRUNNABLESERVICE =\"NO\" ISSERVICE =\"NO\" ISVALID =\"YES\" NAME =\"wkf_LOAD_ITMMVMT_AUD\" REUSABLE_SCHEDULER =\"NO\" SCHEDULERNAME =\"Scheduler\" SERVERNAME =\"dev_is\" SERVER_DOMAINNAME =\"Domain_etldev01\" SUSPEND_ON_ERROR =\"NO\" TASKS_MUST_RUN_ON_SERVER =\"NO\" VERSIONNUMBER =\"5\">", "Domain_etldev01");

		Assert.assertTrue(idne.hasNext());
		Assert.assertEquals("Domain_etldev01", idne.getdomainNameParameter());
		Assert.assertFalse(idne.hasNext());
	}

	@Test
	public void testFolderShortcutNames()
	{
		FolderShortcutExpression fse = FolderShortcutExpression.match("FOLDERNAME =\"SHARED_EDW\"");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getShortcutFolderName());

		fse = FolderShortcutExpression.match("FOLDERNAME=\"SHARED_EDW\"");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getShortcutFolderName());

		fse = FolderShortcutExpression.match("FOLDERNAME= \"SHARED_EDW\"");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getShortcutFolderName());
		fse = FolderShortcutExpression.match("FOLDERNAME = \"SHARED_EDW\"");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getShortcutFolderName());
	}

	@Test
	public void testFolderNames()
	{
		FolderNameExpression fse = FolderNameExpression.match("<FOLDER NAME=\"SHARED_EDW\" GROUP=\"\" OWNER=\"Administrator\" SHARED=\"SHARED\" DESCRIPTION=\"\" PERMISSIONS=\"rwx---r--\" UUID=\"6745aa29-d1d0-4ff3-a696-1bfea2539316\">");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("SHARED_EDW", fse.getFolderName());

		fse = FolderNameExpression.match("<FOLDER NAME=\"EDW_POS_TICKET\" GROUP=\"\" OWNER=\"Administrator\" SHARED=\"NOTSHARED\" DESCRIPTION=\"\" PERMISSIONS=\"rwx---r--\" UUID=\"7155f0e9-f99f-4aac-a9fe-80498f3be3cb\">");

		Assert.assertTrue(fse.matches());
		Assert.assertEquals("EDW_POS_TICKET", fse.getFolderName());
	}

	@Test
	public void testFolderExists()
	{
		InfaCreateFolderExistsExpression icfee = new InfaCreateFolderExistsExpression("A folder with name __BI_1701_POS_BSMITH_WS already exists in repository ci_pc_repo. Try again!", "__BI_1701_POS_BSMITH_WS", "ci_pc_repo");

		Assert.assertTrue(icfee.matches());

		icfee = new InfaCreateFolderExistsExpression("A folder with name __BI_1701_POS_BSMITH_WS already exists in repository dev_pc_repo. Try again!", "__BI_1701_POS_BSMITH_WS", "dev_pc_repo");

		Assert.assertTrue(icfee.matches());

		icfee = new InfaCreateFolderExistsExpression("A folder with name __BI_1701_POS_BSMITH_WS already exists in repository qa_pc_repo. Try again!", "__BI_1701_POS_BSMITH_WS", "qa_pc_repo");

		Assert.assertTrue(icfee.matches());
	}

	@Test
	public void testTestFolders()
	{
		TestFolderNameExpression tfne = new TestFolderNameExpression("__POS");

		Assert.assertTrue(tfne.matches());
		Assert.assertEquals(tfne.getFolderName(), "__POS");

		tfne = new TestFolderNameExpression("_POS");

		Assert.assertFalse(tfne.matches());

		tfne = new TestFolderNameExpression("POS__POS");

		Assert.assertFalse(tfne.matches());

		tfne = new TestFolderNameExpression("POS__POS\r\n__POS");

		Assert.assertTrue(tfne.hasNext());
		Assert.assertEquals(tfne.getFolderName(), "__POS");
	}

	@Test
	public void testConcurrentDelete() throws InformaticaError {
		StringBuffer stb = new StringBuffer("\n" +
				"Informatica(r) PMREP, version [9.1.0 HotFix4], build [449.0224], Windows 64-bit\n" +
				"Copyright (c) Informatica Corporation 1994 - 2012\n" +
				"All Rights Reserved.\n" +
				"This Software is protected by U.S. Patent Numbers 5,794,246; 6,014,670; 6,016,501; 6,029,178; 6,032,158; 6,035,307; 6,044,374; 6,092,086; 6,208,990; 6,339,775; 6,640,226; 6,789,096; 6,820,077; 6,823,373; 6,850,947; 6,895,471; 7,117,215; 7,162,643; 7,254,590; 7,281,001; 7,421,458; 7,496,588; 7,523,121; 7,584,422; 7,720,842; 7,721,270; and 7,774,791, international Patents and other Patents Pending.\n" +
				"\n" +
				"Invoked at Sat Jun 01 22:10:34 2013\n" +
				"\n" +
				" [[REP_57140] Object does not exist.]\n" +
				"Unable to find folder __31373731333530363533363738_1_SHARED_EDW in the repository.  Another user may have deleted this folder.\n" +
				"Failed to execute deletefolder.\n" +
				"\n" +
				"Completed at Sat Jun 01 22:10:39 2013\n" +
				"\t\t\t}");

		InfaDeleteFolderFailedExpression idcfe = new InfaDeleteFolderFailedExpression(stb);

		if (idcfe.hasNext()) {
			InfaDeleteFolderDoesNotExistExpression idcdnee = new InfaDeleteFolderDoesNotExistExpression(stb, "__31373731333530363533363738_1_SHARED_EDW");

			if (idcdnee.hasNext()) {
				if (false) {
					throw new InformaticaError("Folder __31373731333530363533363738_1_SHARED_EDW does not exist.");
				}
			} else {
				throw new InformaticaError("Could not delete folder " + stb);
			}
		} else {
			throw new InformaticaError("Could not delete folder " + stb);
		}
	}

	@Test
	public void testImportResult()
	{
		InfaImportResultsExpression ire = new InfaImportResultsExpression("5 Processed, 0 Errors, 1 Warnings");

		Assert.assertTrue(ire.matches());

		Assert.assertEquals(5, ire.getProcessedCount());
		Assert.assertEquals(0, ire.getErrorCount());
		Assert.assertEquals(1, ire.getWarningCount());

		ire = new InfaImportResultsExpression("0 Processed, 1 Errors, 0 Warnings");

		Assert.assertTrue(ire.matches());

		Assert.assertEquals(0, ire.getProcessedCount());
		Assert.assertEquals(1, ire.getErrorCount());
		Assert.assertEquals(0, ire.getWarningCount());
	}

	@Test
	public void testImportResultMaxResults()
	{
		InfaImportResultsExpression ire = new InfaImportResultsExpression("999 Processed, 999 Errors, 999 Warnings");

		Assert.assertTrue(ire.matches());

		Assert.assertEquals(999, ire.getProcessedCount());
		Assert.assertEquals(999, ire.getErrorCount());
		Assert.assertEquals(999, ire.getWarningCount());
	}

	@Test
	public void infaWorkflow()
	{
		InfaWorkflowNameExpression ine = new InfaWorkflowNameExpression("workflow wkf_INT_CONVERSION_TEST__bsmith_60z5jz88rks5_8");

		Assert.assertTrue(ine.matches());
	}

	@Test
	public void sqlServerDeadlock()
	{
		InfaRepositoryDeadlockExpression ifd = new InfaRepositoryDeadlockExpression("__32333933303431353936383833_0_SHARED_EDW is a shared Folder. Deleting it will cause any shortcuts accessing it to become unusable.\n" +
				" [[REP_12014] An error occurred while accessing the repository  Microsoft OLE DB Provider for SQL Server:\tTransaction (Process ID 53) was deadlocked on lock resources with another process and has been chosen as the deadlock victim. Rerun the transaction.\n" +
				"SQL State: 40001\tNative Error: 1205\n" +
				"State: 51\tSeverity: 13\n" +
				"SQL Server Message: Transaction (Process ID 53) was deadlocked on lock resources with another process and has been chosen as the deadlock victim. Rerun the transaction.\n" +
				"\n" +
				"\n" +
				"Database driver error...\n" +
				"Function Name : Execute\n" +
				"SQL Stmt : DELETE FROM OPB_WIDGET_ATTR WHERE OPB_WIDGET_ATTR.MAPPING_ID = 0 AND OPB_WIDGET_ATTR.WIDGET_ID IN (SELECT OPB_WIDGET.WIDGET_ID FROM OPB_WIDGET WHERE OPB_WIDGET.SUBJECT_ID = ?  AND OPB_WIDGET.WIDGET_TYPE = OPB_WIDGET_ATTR.WIDGET_TYPE  AND OPB_WIDGET.WIDGET_ID = OPB_WIDGET_ATTR.WIDGET_ID  AND OPB_WIDGET.VERSION_NUMBER = OPB_WIDGET_ATTR.VERSION_NUMBER )]\n" +
				"Failed to execute deletefolder.\n" +
				"pmrep>");

		Assert.assertTrue(ifd.hasNext());
	}

	@Test
	public void getWFDetailsFail()
	{
		InfaGetWorkflowDetailsFailureExpression ifd = new InfaGetWorkflowDetailsFailureExpression("ERROR: Workflow [wkf_LOAD_ITMMVMT_AU] not found in folder [EDW_POS_TICKET].");
		Assert.assertTrue(ifd.matches());

		ifd = new InfaGetWorkflowDetailsFailureExpression("ERROR: Workflow [] not found in folder [].");
		Assert.assertFalse(ifd.matches());
	}

	@Test
	public void getWFDetailsFailWFluff()
	{
		InfaGetWorkflowDetailsFailureExpression ifd = new InfaGetWorkflowDetailsFailureExpression("pmcmd>Hi\nERROR: Workflow [wkf_LOAD_ITMMVMT_AU] not found in folder [EDW_POS_TICKET].\npmcmd>");
		Assert.assertTrue(ifd.hasNext());
	}

	@Test
	public void getWFDetails()
	{
		InfaGetWorkflowDetailsExpression ifd = new InfaGetWorkflowDetailsExpression(
				"Integration Service status: [Running]\n" +
				"Integration Service startup time: [Sat Dec 21 21:03:14 2013]\n" +
				"Integration Service current time: [Fri Jan 24 11:15:38 2014]\n" +
				"Folder: [EDW_POS_TICKET]\n" +
				"Workflow: [wkf_LOAD_ITMMVMT_AUD] version [1].\n" +
				"Workflow run status: [Succeeded]\n" +
				"Workflow run error code: [0]\n" +
				"Workflow run error message: [Completed successfully.]\n" +
				"Workflow run id [16807855].\n" +
				"Start time: [Fri Jan 24 10:33:23 2014]\n" +
				"End time: [Fri Jan 24 10:33:27 2014]\n" +
				"Workflow log file: [E:\\Informatica\\infa_shared\\INT_SVC_QA\\WorkflowLogs\\wkf_LOAD_ITMMVMT_AUD.log.R2DLM9TS.16807855.20140124103323]\n" +
				"Workflow run type: [User request]\n" +
				"Run workflow as user: [dseries]\n" +
				"Run workflow with Impersonated OSProfile in domain: []\n" +
				"Integration Service: [INT_SVC_QA]");
		Assert.assertTrue(ifd.matches());
	}

	@Test
	public void getWFDetailsWithFluff()
	{
		InfaGetWorkflowDetailsExpression ifd = new InfaGetWorkflowDetailsExpression(
				"pmcmd>\nIntegration Service status: [Running]\n" +
						"Integration Service startup time: [Sat Dec 21 21:03:14 2013]\n" +
						"Integration Service current time: [Fri Jan 24 11:15:38 2014]\n" +
						"Folder: [EDW_POS_TICKET]\n" +
						"Workflow: [wkf_LOAD_ITMMVMT_AUD] version [1].\n" +
						"Workflow run status: [Succeeded]\n" +
						"Workflow run error code: [0]\n" +
						"Workflow run error message: [Completed successfully.]\n" +
						"Workflow run id [16807855].\n" +
						"Start time: [Fri Jan 24 10:33:23 2014]\n" +
						"End time: [Fri Jan 24 10:33:27 2014]\n" +
						"Workflow log file: [E:\\Informatica\\infa_shared\\INT_SVC_QA\\WorkflowLogs\\wkf_LOAD_ITMMVMT_AUD.log.R2DLM9TS.16807855.20140124103323]\n" +
						"Workflow run type: [User request]\n" +
						"Run workflow as user: [dseries]\n" +
						"Run workflow with Impersonated OSProfile in domain: []\n" +
						"Integration Service: [INT_SVC_QA]\r\npmcmd>");
		Assert.assertTrue(ifd.hasNext());
	}

	@Test
	public void intSvc()
	{
		InfaIntSvcFolderNotFoundExpression iint = new InfaIntSvcFolderNotFoundExpression("com.informatica.wsh.Fault: ERROR: Folder [id = 6458] not found.");

		Assert.assertTrue(iint.hasNext());
	}
}