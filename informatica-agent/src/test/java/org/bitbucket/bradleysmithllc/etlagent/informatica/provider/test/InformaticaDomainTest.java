package org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.*;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaIntegrationService;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

public class InformaticaDomainTest {
	@Test
	public void normalUsage() throws ParseException {
		DomainsProperty dp = new DomainsProperty();
		dp.setWorkingRoot("root");
		dp.setClientVersion("9.1.0hf3");
		dp.setConnectivityHost("etldev01");
		dp.setConnectivityPort(6005L);
		dp.setUsername("bsmith");
		dp.setPasswordEncrypted("passwd");

		dp.setRepositories(new Repositories());
		RepositoriesProperty repositoriesProperty = new RepositoriesProperty();
		dp.getRepositories().getAdditionalProperties().put("dev_pc_repo", repositoriesProperty);

		repositoriesProperty.getIntegrationServices().add("dev_ci_is");
		repositoriesProperty.getIntegrationServices().add("dev_is");
		repositoriesProperty.getIntegrationServices().add("dev_java_is");

		repositoriesProperty.setWebServicesHubs(new WebServicesHubs());

		WebServicesHubsProperty webServicesHubsProperty = new WebServicesHubsProperty();
		repositoriesProperty.getWebServicesHubs().getAdditionalProperties().put("web_svc_dev", webServicesHubsProperty);
		webServicesHubsProperty.setHostName("host");
		webServicesHubsProperty.setHostPort(1987L);

		webServicesHubsProperty = new WebServicesHubsProperty();
		repositoriesProperty.getWebServicesHubs().getAdditionalProperties().put("web_svc_dev_ii", webServicesHubsProperty);
		webServicesHubsProperty.setHostName("host");
		webServicesHubsProperty.setHostPort(1987L);

		InformaticaDomain id = new InformaticaDomain("Domain_etldev01", dp, null, new BasicRuntimeSupport(), null);

		Assert.assertEquals("root", id.getWorkingRoot().getName());
		Assert.assertEquals("Domain_etldev01", id.getDomainName());
		Assert.assertEquals("9.1.0hf3", id.getClientVersion());
		Assert.assertEquals("etldev01", id.getConnectivityHost());
		Assert.assertEquals(6005, id.getConnectivityPort());
		Assert.assertEquals("bsmith", id.getUserName());
		Assert.assertEquals("passwd", id.getPasswordEncrypted());

		Assert.assertEquals("dev_pc_repo", id.getDefaultRepository().getRepositoryName());

		InformaticaRepository dev_pc_repo = id.getRepository("dev_pc_repo");
		Assert.assertEquals("dev_pc_repo", dev_pc_repo.getRepositoryName());

		Assert.assertEquals("dev_ci_is", id.getDefaultIntegrationService().getIntegrationServiceName());

		Assert.assertEquals("web_svc_dev_ii", id.getDefaultWebServicesHub().getName());
		Assert.assertEquals("host", id.getDefaultWebServicesHub().getHostName());
		Assert.assertEquals(1987, id.getDefaultWebServicesHub().getPort());

		InformaticaIntegrationService dev_is = id.getIntegrationService("dev_ci_is");
		Assert.assertEquals("dev_ci_is", dev_is.getIntegrationServiceName());
	}
/*
	@Test
	public void defaultsOverride() throws ParseException {
		InformaticaDomain id = new InformaticaDomain("Domain_etldev01",
			ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd',repositories: {'dev_pc_repo': {integration-services: ['dev_ci_is']}, 'dev_pc_repo2': {integration-services: ['dev_is','dev_java_is']}}, default-repository: \"dev_pc_repo2\", default-integration-service: \"dev_java_is\"").getJsonNode(), new BasicRuntimeSupport(), null);

		Assert.assertEquals("root", id.getWorkingRoot().getName());
		Assert.assertEquals("Domain_etldev01", id.getDomainName());
		Assert.assertEquals("9.1.0hf3", id.getClientVersion());
		Assert.assertEquals("etldev01", id.getConnectivityHost());
		Assert.assertEquals(6005, id.getConnectivityPort());
		Assert.assertEquals("bsmith", id.getUserName());
		Assert.assertEquals("passwd", id.getPasswordEncrypted());

		Assert.assertEquals("dev_pc_repo2", id.getDefaultRepository().getRepositoryName());

		InformaticaRepository dev_pc_repo = id.getRepository("dev_pc_repo");
		Assert.assertEquals("dev_pc_repo", dev_pc_repo.getRepositoryName());

		Assert.assertEquals("dev_java_is", id.getDefaultIntegrationService().getIntegrationServiceName());

		InformaticaIntegrationService dev_is = id.getIntegrationService("dev_java_is");
		Assert.assertEquals("dev_java_is", dev_is.getIntegrationServiceName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void defaultRepositoryDoesNotExist() throws ParseException {
		InformaticaDomain id = new InformaticaDomain("Domain_etldev01",
			ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd',repositories: ['dev_pc_repo', 'dev_pc_repo2'], integration-services: ['dev_ci_is','dev_is','dev_java_is'], default-repository: \"dev_pc_repo3\", default-integration-service: \"dev_java_is\"").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void defaultIntegrationServiceDoesNotExist() throws ParseException {
		InformaticaDomain id = new InformaticaDomain("Domain_etldev01",
			ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd',repositories:['dev_pc_repo', 'dev_pc_repo2'], integration-services: ['dev_ci_is','dev_is','dev_java_is'], default-repository: \"dev_pc_repo2\", default-integration-service: \"dev_java_is2\"").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void missingRepositories() throws ParseException {
		InformaticaDomain id = new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd'}").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyRepositories() throws ParseException {
		InformaticaDomain id = new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd', repositories: []}").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test
	public void connectivityHostIsOptional() throws ParseException {
		InformaticaDomain id = new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd',repositories:{'dev_pc_repo': { integration-services: ['dev_ci_is','dev_is','dev_java_is']}}").getJsonNode(), new BasicRuntimeSupport(), null);

		Assert.assertNull(id.getConnectivityHost());
	}

	@Test
	public void connectivityPortIsOptional() throws ParseException {
		InformaticaDomain id = new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',username: 'bsmith',password-encrypted: 'passwd',repositories:{'dev_pc_repo': {integration-services: ['dev_ci_is','dev_is','dev_java_is']}}").getJsonNode(), new BasicRuntimeSupport(), null);

		Assert.assertNull(id.getConnectivityHost());
		Assert.assertEquals(-1, id.getConnectivityPort());
	}

	@Test(expected = IllegalArgumentException.class)
	public void workingRootIsMandatory() throws ParseException {
		new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("client-version: '9.1.0hf3', username: 'bsmith',password-encrypted: 'passwd',repositories:[dev_pc_repo], integration-services: ['dev_ci_is','dev_is','dev_java_is']").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void clientVersionIsMandatory() throws ParseException {
		new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', username: 'bsmith',password-encrypted: 'passwd',repositories:[dev_pc_repo], integration-services: ['dev_ci_is','dev_is','dev_java_is']").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void usernameIsMandatory() throws ParseException {
		new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',password-encrypted: 'passwd',repositories:[dev_pc_repo], integration-services: ['dev_ci_is','dev_is','dev_java_is']").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void passwordIsMandatory() throws ParseException {
		new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',username: 'bsmith',repositories:{dev_pc_repo: {}}, integration-services: ['dev_ci_is','dev_is','dev_java_is']").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void missingIntegrationServices() throws ParseException {
		new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd',repositories:[dev_pc_repo]").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyIntegrationServices() throws ParseException {
		new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd',repositories:[dev_pc_repo], integration-services: []").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void duplicateIntegrationService() throws ParseException {
		new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd',repositories:[dev_pc_repo], integration-services: ['d', 'd']").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void duplicateRepository() throws ParseException {
		new InformaticaDomain("Domain_etldev01", ETLTestParser.loadBareObject("working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: 'passwd',repositories:[dev_pc_repo, dev_pc_repo], integration-services: ['d', 'd']").getJsonNode(), new BasicRuntimeSupport(), null);
	}
	*/
}