package org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.*;

public class PmRepImposter
{
	public static void main(String [] argv) throws IOException, InterruptedException {
		PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(new File("/tmp/impostering_log"))));

		System.out.print("\r\n" +
				"Informatica(r) PMREP, version [9.1.0 HotFix4], build [449.0224], Windows 64-bit" +
				"Copyright (c) Informatica Corporation 1994 - 2012\r\n" +
				"All Rights Reserved.\r\n" +
				"This Software is protected by U.S. Patent Numbers 5,794,246; 6,014,670; 6,016,501; 6,029,178; 6,032,158; 6,035,307; 6,044,374; 6,092,086; 6,208,990; 6,339,775; 6,640,226; 6,789,096; 6,820,077; 6,823,373; 6,850,947; 6,895,471; 7,117,215; 7,162,643; 7,254,590; 7,281,001; 7,421,458; 7,496,588; 7,523,121; 7,584,422; 7,720,842; 7,721,270; and 7,774,791, international Patents and other Patents Pending.\r\n\r\n" +

				"Invoked at Thu Dec 19 12:26:12 2013\r\n\r\n" +

				"pmrep>");

		BufferedReader breader = new BufferedReader(new InputStreamReader(System.in));

		String line = null;

		while ((line = breader.readLine()) != null)
		{
			pw.println("Line: " + line);
			String command = line.trim();

			int index = command.indexOf(' ');

			if (index != -1)
			{
				command = command.substring(0, index);
			}

			pw.println("Command: " + command);

			if (command.equals("exit"))
			{
				pw.println("Bye");
				pw.flush();
				pw.close();
				System.exit(0);
			}
			else if (command.equals("connect"))
			{
				pw.println("Connect");
				//Thread.currentThread().sleep(1000L);
				System.out.print("Connected to repository REP_SVC_CI in DOM_CI as user administrator\r\n" +
				"connect completed successfully.\r\n");
			}
			else if(command.equals("deletefolder"))
			{
				pw.println("Delete Folder");
				//Thread.currentThread().sleep(100L);
				System.out.print("Deleting folder __bsmith_f142aa7ae1ae71b191649b5bcef74816_SHARED_EDW in repository REP_SVC_CI. All the data in the folder will be lost.\r\n");
				//Thread.currentThread().sleep(100L);
				System.out.print("__bsmith_f142aa7ae1ae71b191649b5bcef74816_SHARED_EDW is a shared Folder. Deleting it will cause any shortcuts accessing it to become unusable.\r\n");
				//Thread.currentThread().sleep(1000L);
				System.out.print("deletefolder completed successfully.\r\n");
			}
			else if (command.equals("createfolder"))
			{
				pw.println("Create Folder");
				//Thread.currentThread().sleep(100L);
				System.out.print("Creating folder POS_NEW_TEST in repository REP_SVC_CI.\r\n");
				//Thread.currentThread().sleep(1000L);
				System.out.print("createfolder completed successfully.\r\n");
			}
			else if (command.equals("createconnection"))
			{
				pw.println("Create Connection");
				//Thread.currentThread().sleep(1000L);
				System.out.print("createconnection completed successfully.\r\n");
			}
			else if (command.equals("objectimport"))
			{
				pw.println("Import Object");
				//Thread.currentThread().sleep(1000L);
				System.out.print("objectimport completed successfully.\r\n");
			}
			else
			{
				pw.println("Unknown");
				//Thread.currentThread().sleep(100L);
				System.out.print(command + " is an unknown command. Type help to see a list of all available commands\r\n");
			}

			pw.println("Prompt . . .");
			pw.flush();
			System.out.print("pmrep>");
			System.out.flush();
		}

		pw.close();
	}
}