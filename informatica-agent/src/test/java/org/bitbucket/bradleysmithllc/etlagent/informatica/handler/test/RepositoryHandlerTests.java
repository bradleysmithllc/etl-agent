package org.bitbucket.bradleysmithllc.etlagent.informatica.handler.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.*;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.CreateConnectionRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.handler.AbstractRequestHandler;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.ConnectionDetails;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaOutOfProcessRepositoryClient;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.SqlServerConnectionDetails;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test.PmRepProcessScripter;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.net.URL;
import java.util.*;

@RunWith(Parameterized.class)
public class RepositoryHandlerTests
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Parameterized.Parameters
	public static Collection<Object[]> data() throws Exception
	{
		List<Object[]> specs = new ArrayList<Object[]>();

		URL url = RepositoryHandlerTests.class.getResource("/repositoryHandlerTests/tests.json");

		JsonNode tests = JsonLoader.fromURL(url);

		ObjectNode onode = (ObjectNode) tests;

		Iterator<Map.Entry<String, JsonNode>> it = onode.fields();

		while (it.hasNext())
		{
			Map.Entry<String, JsonNode> fi = it.next();

			specs.add(new TestSpec[]{loadSpec(fi.getKey(), fi.getValue())});
		}

		return specs;
	}

	private static TestSpec loadSpec(String key, JsonNode value) throws Exception {
		TestSpec tspec = new TestSpec();
		tspec.id = key;
		tspec.execute = value.get("execute").asBoolean();
		tspec.request = value.get("request");
		tspec.response = value.get("response");
		tspec.handlerClass = (AbstractRequestHandler) Class.forName(value.get("handler-class").asText()).newInstance();
		tspec.resultException = loadException(value.get("result-exception"));

		// load the script
		ArrayNode scriptArray = (ArrayNode) value.get("script");

		Iterator<JsonNode> scit = scriptArray.iterator();

		while (scit.hasNext())
		{
			ObjectNode action = (ObjectNode) scit.next();

			tspec.script.add(loadScript(action));
		}

		return tspec;
	}

	private static PmRepProcessScripter.ScriptAction loadScript(ObjectNode action) throws Exception {
		String command = action.get("command").asText();

		long delay = -1L;

		JsonNode delay1 = action.get("delay");
		if (delay1 != null && !delay1.isNull())
		{
			delay = delay1.asLong();
		}

		String response = null;

		JsonNode response1 = action.get("response");
		if (response1 != null && !response1.isNull())
		{
			response = response1.asText();
		}

		PmRepProcessScripter.ScriptAction saction =  new PmRepProcessScripter.ScriptAction(command, response, delay);

		saction.setCheckedExceptionToRaise((IOException) loadException(action.get("checked-exception")));
		saction.setUncheckedExceptionToRaise((RuntimeException) loadException(action.get("unchecked-exception")));

		return saction;
	}

	private static Exception loadException(JsonNode jsonNode) throws Exception
	{
		if (jsonNode == null)
		{
			return null;
		}

		String clName = jsonNode.get("exception-class").asText();

		String message = "Don't give a crap";

		JsonNode messageN = jsonNode.get("exception-message");

		if (messageN != null && !messageN.isNull())
		{
			message = messageN.asText();
		}

		// grab the constructor and instantiate the exception

		Class cl = Class.forName(clName);
		Constructor constru = cl.getConstructor(String.class);

		return (Exception) constru.newInstance(message);
	}

	private static final class TestSpec
	{
		String id;
		boolean execute;
		JsonNode request;
		JsonNode response;
		AbstractRequestHandler handlerClass;
		List<PmRepProcessScripter.ScriptAction> script = new ArrayList<PmRepProcessScripter.ScriptAction>();
		Throwable resultException;
	}

	private final TestSpec testSpec;
	private final BasicRuntimeSupport brs = new BasicRuntimeSupport();

	public RepositoryHandlerTests(TestSpec spec) {
		testSpec = spec;
		brs.setApplicationLogger(new PrintWriterLog());
	}

	@Test
	public void runTest() throws Exception
	{
		if (testSpec.execute)
		{
			runTest(testSpec);
		}
	}

	public void runTest(TestSpec spec) throws Exception {
		AbstractRequestHandler handler = spec.handlerClass;

		PmRepProcessScripter poster = new PmRepProcessScripter();

		for (PmRepProcessScripter.ScriptAction action : spec.script)
		{
			poster.addScriptAction(action);
		}

		InformaticaConfiguration informaticaConfiguration = new InformaticaConfiguration(JsonLoader.fromString(
				"{\"domains\": {\"DOM_DEV\": {\"client-version\": \"1\", \"username\": \"user\", \"password-encrypted\": \"pass\", \"repositories\": {\"REP_SVC\": {\"integration-services\": [\"INT_SVC\"]}}}}}"
		), brs, temporaryFolder.newFile());

		InformaticaOutOfProcessRepositoryClient pclient = new InformaticaOutOfProcessRepositoryClient(
				informaticaConfiguration.getDefaultDomain().getDefaultRepository(),
				poster,
				brs
		);

		handler.setOverrideInformaticaRepositoryClient(pclient);

		handler.setInformaticaConfiguration(informaticaConfiguration);
		handler.receiveRuntimeSupport(brs);

		// deserialize request object with gson
		Object requestContainerObject = handler.getRequestContainerObject();

		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();

		Object requestObj = gson.fromJson(spec.request.toString(), requestContainerObject.getClass());

		try
		{
			GenericResponseDTO response = handler.process(spec.request, requestObj);

			if (spec.response != null)
			{
				Assert.assertNotNull(response);

				// verify
				JsonElement thisJson = gson.toJsonTree(response);
				JsonElement expectedJson = new JsonParser().parse(spec.response.toString());

				Assert.assertEquals(spec.id, expectedJson, thisJson);
			}
		}
		catch(AssertionError thr)
		{
			throw thr;
		}
		catch(Throwable thr)
		{
			if (spec.resultException == null)
			{
				thr.printStackTrace(System.out);
			}

			Assert.assertNotNull(spec.id + " - " + thr.toString(), spec.resultException);

			Assert.assertEquals(spec.resultException.getClass(), thr.getClass());
		}
	}

	//@Test
	public void two()
	{
		Gson gson = new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES).create();

		SqlServerConnectionDetails builder = new SqlServerConnectionDetails()
				.withServerName("server")
				.withDatabaseName("database")
				.withDatabaseUserName("username")
				.withDatabasePassword("password");

		CreateConnectionRequestDTO connReq = new CreateConnectionRequestDTO(new ConnectionDetails("bs@conn", builder));
		System.out.println(gson.toJson(connReq));
	}
}