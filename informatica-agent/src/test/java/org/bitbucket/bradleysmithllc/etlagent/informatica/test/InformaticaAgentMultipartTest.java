package org.bitbucket.bradleysmithllc.etlagent.informatica.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.Server;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.client.InformaticaAgentClient;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExecuteWorkflowRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExecuteWorkflowResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.handler.ExecuteWorkflowMultipart;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.hamcrest.core.IsInstanceOf;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class InformaticaAgentMultipartTest {
	private static final int testPort = 8288;

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private InformaticaAgentClient client;

	@Before
	public void startServer() throws Exception {
		ContextBase.multipartRequestEnabled = true;

		// use a test root
		// create a configuration file for informatica
		File file = temporaryFolder.newFolder();

		ContextBase.initiate(true);
		ContextBase.setRoot(file);

		File config = new FileBuilder(ContextBase.runtimeSupport.getFeatureConfigurationDirectory("informatica")).mkdirs().name("informatica.json").file();

		URL url = getClass().getResource("/informatica.json");

		if (url == null)
		{
			throw new IllegalArgumentException("Could not find informatica.json");
		}

		String jsonText = IOUtils.toString(url);

		FileUtils.write(config, jsonText);

		System.out.println("Copying informatica config to " + config);

		ContextBase.initialize();

		// use a test port
		System.setProperty("jersey.test.port", String.valueOf(testPort));

		Server.start();

		client = new InformaticaAgentClient("testclient", testPort);
		client.receiveRuntimeSupport(ContextBase.runtimeSupport);
	}

	@After
	public void stopServer() throws IOException {
		// every test should result in an informatica agent request
		Assert.assertEquals("informatica", AgentDispatcher.lastRequestedAgentId);
		Server.stop();
	}

	@Test
	public void legacyFallback() throws IOException {
		ContextBase.multipartRequestEnabled = false;

		client.executeWorkflow(new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance")).getWorkSpaceFiles();

		Assert.assertEquals(InformaticaAgentClient.multipart_status.ignore, client.getMultipart());
	}

	@Test
	public void multipartDenied() throws IOException {
		// disable the multipart request
		ContextBase.multipartRequestEnabled = false;

		// force the client to use multipart anyway
		client.setMultipart(InformaticaAgentClient.multipart_status.use);

		client.executeWorkflow(new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance")).getWorkSpaceFiles();

		// verify that the correct result was returned
		Assert.assertTrue(AgentDispatcher.lastResponse instanceof MultipartResponseDTO);

		MultipartResponseDTO mdto = (MultipartResponseDTO) AgentDispatcher.lastResponse;

		GenericResponseDTO dtoRes = mdto.getRequestDTO(GenericResponseDTO.class);

		Assert.assertEquals(GenericResponseDTO.response_code.failed, dtoRes.getResponse());
		Assert.assertEquals(ContextBase.MULTIPART_FORBIDDEN, dtoRes.getResponseMessage());
	}

	@Test
	public void multipartAvailable() throws IOException {
		ContextBase.multipartRequestEnabled = true;

		client.executeWorkflow(new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance")).getWorkSpaceFiles();

		Assert.assertEquals(InformaticaAgentClient.multipart_status.use, client.getMultipart());
	}

	@Test
	public void executeWorkflowNoParmOrFiles() throws IOException {
		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance")).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
		Assert.assertEquals("runinstance", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getRunInstanceName());
		Assert.assertEquals("folder", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getFolder());
		Assert.assertEquals("workflow", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getWorkflowName());
		Assert.assertEquals("task", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getTask());
	}

	@Test
	public void executeWorkflowRunInstance() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder");
		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeWorkflowWithHub() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder");

		request.setInformaticaDomain("DOM_TEST");
		request.setInformaticaRepository("REP_TEST_II");
		request.setInformaticaIntegrationService("INT_SVC_TEST_IV");
		request.setInformaticaWebServicesHub("WEB_SVC_HUB_II");

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
		ExecuteWorkflowRequestDTO reqdto = (ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest;
		Assert.assertEquals("DOM_TEST", reqdto.getInformaticaDomain());
		Assert.assertEquals("REP_TEST_II", reqdto.getInformaticaRepository());
		Assert.assertEquals("INT_SVC_TEST_IV", reqdto.getInformaticaIntegrationService());
		Assert.assertEquals("WEB_SVC_HUB_II", reqdto.getInformaticaWebServicesHub());
	}

	@Test
	public void executeWorkflowSessionId() throws IOException {
		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(new ExecuteWorkflowRequestDTO("workflow", "folder")).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		ExecuteWorkflowRequestDTO ewr = (ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest;

		Assert.assertEquals("testclient", ewr.getSessionId());
	}

	@Test
	public void executeWorkflowNoParm() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder");

		request.addRequestFile(newFile("name1", "name7.1", RemoteFile.fileType.badFiles));
		request.addRequestFile(newFile("name2", "name6.1", RemoteFile.fileType.lookupFiles));
		request.addRequestFile(newFile("name3", "name5.1", RemoteFile.fileType.sourceFiles));
		request.addRequestFile(newFile("name4", "name4.1", RemoteFile.fileType.targetFiles));
		request.addRequestFile(newFile("name5", "name3.1", RemoteFile.fileType.workflowLog));
		request.addRequestFile(newFile("name6", "name2.1", RemoteFile.fileType.sessionLog));
		request.addRequestFile(newFile("name7", "name1.1", RemoteFile.fileType.parameterFiles));

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertEquals(7, resFiles.size());

		List<RemoteFile> badFiles = resFiles.get(RemoteFile.fileType.badFiles);
		Assert.assertNotNull(badFiles);
		Assert.assertEquals(1, badFiles.size());
		Assert.assertEquals(RemoteFile.fileType.badFiles, badFiles.get(0).getType());
		Assert.assertEquals("name1", badFiles.get(0).getFileName());
		Assert.assertEquals("name7.1", FileUtils.readFileToString(badFiles.get(0).getFile()));

		List<RemoteFile> targetFiles = resFiles.get(RemoteFile.fileType.targetFiles);
		Assert.assertNotNull(targetFiles);
		Assert.assertEquals(1, targetFiles.size());
		Assert.assertEquals(RemoteFile.fileType.targetFiles, targetFiles.get(0).getType());
		Assert.assertEquals("name4", targetFiles.get(0).getFileName());
		Assert.assertEquals("name4.1", FileUtils.readFileToString(targetFiles.get(0).getFile()));

		List<RemoteFile> workflowLog = resFiles.get(RemoteFile.fileType.workflowLog);
		Assert.assertNotNull(workflowLog);
		Assert.assertEquals(1, workflowLog.size());
		Assert.assertEquals(RemoteFile.fileType.workflowLog, workflowLog.get(0).getType());
		Assert.assertEquals("name5", workflowLog.get(0).getFileName());
		Assert.assertEquals("name3.1", FileUtils.readFileToString(workflowLog.get(0).getFile()));

		List<RemoteFile> sessionFiles = resFiles.get(RemoteFile.fileType.sessionLog);
		Assert.assertNotNull(sessionFiles);
		Assert.assertEquals(1, sessionFiles.size());
		Assert.assertEquals(RemoteFile.fileType.sessionLog, sessionFiles.get(0).getType());
		Assert.assertEquals("name6", sessionFiles.get(0).getFileName());
		Assert.assertEquals("name2.1", FileUtils.readFileToString(sessionFiles.get(0).getFile()));

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertNotNull(parameterFiles);
		Assert.assertEquals(2, parameterFiles.size());
		Assert.assertEquals(RemoteFile.fileType.parameterFiles, parameterFiles.get(0).getType());
		//Assert.assertEquals("name1", parameterFiles.get(0).getFileContents());

		Assert.assertEquals(RemoteFile.fileType.parameterFiles, parameterFiles.get(1).getType());
		//Assert.assertEquals("workflow.folder.PRM", parameterFiles.get(1).getFileName());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	private RemoteFile newFile(String name1, String name7, RemoteFile.fileType badFiles) throws IOException {
		File file = temporaryFolder.newFile(name1);
		FileUtils.write(file, name7);

		return RemoteFile.newRemoteFile(file, badFiles);
	}

	@Test
	public void executeWorkflowParm() throws IOException {
		ParameterFile pf = new ParameterFile();

		pf.appendSection("[Folder.Workflow]").appendParameter("$Name", "Value");

		// make sure mapplet parameters make it through
		pf.getSection("[Folder.Workflow]").appendParameter("mapplet", "$Name", "Value");

		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", pf);

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);

		Assert.assertEquals(RemoteFile.fileType.parameterFiles, parameterFiles.get(0).getType());

		Assert.assertEquals(1, parameterFiles.size());

		Assert.assertEquals(0, pf.compareTo(new ParameterFile(parameterFiles.get(0).getFile())));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeWorkflowParmfile1() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", "task");

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertEquals(1, parameterFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeWorkflowParmfile2() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", "task", "runInstance");

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertEquals(1, parameterFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeWorkflowParmfile3() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", null, "runInstance");

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertEquals(1, parameterFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeGigafile() throws IOException {
		File bigFile = getBigFile();

		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", null, "runInstance");

		request.addRequestFile(new RemoteFile(bigFile, RemoteFile.fileType.sourceFiles));

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertEquals(1, parameterFiles.size());

		// grab the source file
		List<RemoteFile> sourceFiles = resFiles.get(RemoteFile.fileType.sourceFiles);
		Assert.assertEquals(1, sourceFiles.size());

		RemoteFile rf = sourceFiles.get(0);

		// compare to original
		Assert.assertEquals(bigFile.length(), rf.getFile().length());
		Assert.assertEquals(FileUtils.checksumCRC32(bigFile), FileUtils.checksumCRC32(rf.getFile()));
		Assert.assertTrue(FileUtils.contentEquals(bigFile, rf.getFile()));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	private File getBigFile() throws IOException {
		File temp = temporaryFolder.newFile();

		DataOutputStream dout = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(temp)));

		byte [] buff = "562retwgdfster45362782923482373256e5dtadtgdgasdghyr262364236eew66weryqwefaysdnvansfisdfnkrwerhiweuhfdawhufasduhfasdflasdluhfalsudfhalsudhfasdluhflalsdufhalsudhfalushdfalsudhf0348573208547234857208452308452304572340572304752340752307452374".getBytes();

		try
		{
			for (int i = 0; i < 500000; i++)
			{
				dout.writeLong((long) (Math.random() * Long.MAX_VALUE));
				dout.writeLong((long) (Math.random() * Long.MAX_VALUE));
				shuffle(buff);
				dout.write(buff);
				dout.writeLong((long) (Math.random() * Long.MAX_VALUE));
				shuffle(buff);
				dout.write(buff);
				dout.writeLong((long) (Math.random() * Long.MAX_VALUE));
				dout.writeLong((long) (Math.random() * Long.MAX_VALUE));
				dout.writeLong((long) (Math.random() * Long.MAX_VALUE));
				dout.writeLong((long) (Math.random() * Long.MAX_VALUE));
				dout.writeLong((long) (Math.random() * Long.MAX_VALUE));
			}
		}
		finally
		{
			dout.close();
		}

		System.out.println(temp.length() / (1024 * 1024));
		return temp;
	}

	@Test
	public void overrideDomain() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");
		request.setInformaticaDomain("DOM_TEST");

		client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		Assert.assertEquals("DOM_TEST", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaDomain());
		Assert.assertNull(( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaIntegrationService());
		Assert.assertNull(( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaRepository());
		Assert.assertNull(((ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest).getInformaticaWebServicesHub());

		Assert.assertEquals("[DOM_TEST].[DOM_TEST.REP_TEST_I].[DOM_TEST.REP_TEST_I.INT_SVC_TEST_I].[DOM_TEST.REP_TEST_I.WEB_SVC_HUB_I]", ExecuteWorkflowMultipart.getLastRequestedDomain());

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_TEST", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaDomain());
		Assert.assertEquals("DOM_TEST.REP_TEST_I", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaRepository());
		Assert.assertEquals("DOM_TEST.REP_TEST_I.INT_SVC_TEST_I", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaIntegrationService());
		Assert.assertEquals("DOM_TEST.REP_TEST_I.WEB_SVC_HUB_I", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaWebServicesHub());
	}

	@Test
	public void overrideRep() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");
		request.setInformaticaRepository("REP_SVC_DEV_II");

		client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		Assert.assertNull(( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaDomain());
		Assert.assertNull(( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaIntegrationService());
		Assert.assertNull(( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaWebServicesHub());
		Assert.assertEquals("REP_SVC_DEV_II", ((ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest).getInformaticaRepository());

		Assert.assertEquals("DOM_DEV", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaDomain());
		Assert.assertEquals("DOM_DEV.REP_SVC_DEV_II", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaRepository());
		Assert.assertEquals("DOM_DEV.REP_SVC_DEV_II.INT_SVC_DEV_II", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaIntegrationService());
		Assert.assertEquals("DOM_DEV.REP_SVC_DEV_II.WEB_SVC_HUB_II", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaWebServicesHub());
	}

	@Test
	public void overrideIs() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");
		request.setInformaticaIntegrationService("INT_SVC_CI_DEV_II");

		client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		Assert.assertNull(( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaDomain());
		Assert.assertNull(( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaRepository());
		Assert.assertNull(((ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest).getInformaticaWebServicesHub());
		Assert.assertEquals("INT_SVC_CI_DEV_II", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getInformaticaIntegrationService());

		Assert.assertEquals("DOM_DEV", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaDomain());
		Assert.assertEquals("DOM_DEV.REP_SVC_DEV", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaRepository());
		Assert.assertEquals("DOM_DEV.REP_SVC_DEV.INT_SVC_CI_DEV_II", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaIntegrationService());
		Assert.assertEquals("DOM_DEV.REP_SVC_DEV.WEB_SVC_HUB_I", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaWebServicesHub());
	}

	private void shuffle(byte[] buff) {
		for (int i = 0; i < buff.length; i++)
		{
			int swapIndex = ((int) (Math.random() * Integer.MAX_VALUE)) % buff.length;

			byte a = buff[swapIndex];
			buff[swapIndex] = buff[i];
			buff[i] = a;
		}
	}
}