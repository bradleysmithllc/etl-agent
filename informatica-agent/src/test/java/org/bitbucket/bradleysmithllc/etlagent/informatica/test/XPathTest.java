package org.bitbucket.bradleysmithllc.etlagent.informatica.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.WorkflowFolderScanner;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.WorkflowReference;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.List;

public class XPathTest {
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void t() throws Exception {
		File file = temporaryFolder.newFile();
		FileUtils.write(file, IOUtils.readURLToString(getClass().getResource("/wkf_INT_CONVERSION_TEST.xml")));

		List<WorkflowReference> list = WorkflowFolderScanner.getWorkflowReferences(file);

		Assert.assertEquals(2, list.size());
	}
}