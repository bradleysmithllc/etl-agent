package org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.parser.ETLTestParser;
import org.bitbucket.bradleysmithllc.etlunit.parser.ParseException;
import org.junit.Assert;
import org.junit.Test;

public class InformaticaConfigurationTest {
	@Test
	public void normalUsage() throws ParseException {
		InformaticaConfiguration ic = new InformaticaConfiguration(ETLTestParser.loadBareObject("default-domain: 'Domain_etldev01',domains: {Domain_etldev01: {working-root: 'root', client-version: '9.1.0hf3',connectivity-host: 'etldev01',connectivity-port: 6005,username: 'bsmith',password-encrypted: '',repositories: {'dev_pc_repo': { integration-services: ['dev_ci_is','dev_is','dev_java_is']}}}}").getJsonNode(), new BasicRuntimeSupport(), null);

		Assert.assertEquals("Domain_etldev01", ic.getDefaultDomain().getDomainName());
	}

	@Test
	public void firstDomainIsDefault() throws ParseException {
		InformaticaConfiguration ic = new InformaticaConfiguration(ETLTestParser.loadObject("{\n" +
				"\t\"domains\": {\n" +
				"\t\t\"Domain_etldev01\": {\n" +
				"\t\t\t\"repositories\": {\n" +
				"\t\t\t\t\"dev_pc_repo\": {\n" +
				"\t\t\t\t\t\"integration-services\": [\n" +
				"\t\t\t\t\t\t\"dev_ci_is\",\n" +
				"\t\t\t\t\t\t\"dev_is\",\n" +
				"\t\t\t\t\t\t\"dev_java_is\"\n" +
				"\t\t\t\t\t]\n" +
				"\t\t\t\t}\n" +
				"\t\t\t},\n" +
				"\t\t\t\"working-root\": \"root\",\n" +
				"\t\t\t\"connectivity-port\": 6005,\n" +
				"\t\t\t\"username\": \"bsmith\",\n" +
				"\t\t\t\"client-version\": \"9.1.0hf3\",\n" +
				"\t\t\t\"password-encrypted\": \"\",\n" +
				"\t\t\t\"connectivity-host\": \"etldev01\"\n" +
				"\t\t},\n" +
				"\t\t\"Domain_etlqa01\": {\n" +
				"\t\t\t\"repositories\": {\n" +
				"\t\t\t\t\"dev_pc_repo\": {\n" +
				"\t\t\t\t\t\"integration-services\": [\n" +
				"\t\t\t\t\t\t\"dev_ci_is\",\n" +
				"\t\t\t\t\t\t\"dev_is\",\n" +
				"\t\t\t\t\t\t\"dev_java_is\"\n" +
				"\t\t\t\t\t]\n" +
				"\t\t\t\t}\n" +
				"\t\t\t},\n" +
				"\t\t\t\"working-root\": \"root\",\n" +
				"\t\t\t\"connectivity-port\": 6005,\n" +
				"\t\t\t\"username\": \"bsmith\",\n" +
				"\t\t\t\"client-version\": \"9.1.0hf3\",\n" +
				"\t\t\t\"password-encrypted\": \"\",\n" +
				"\t\t\t\"connectivity-host\": \"etldev01\"\n" +
				"\t\t}\n" +
				"\t}\n" +
				"}").getJsonNode(), new BasicRuntimeSupport(), null);

		Assert.assertEquals("Domain_etldev01", ic.getDefaultDomain().getDomainName());
	}

	@Test(expected = IllegalArgumentException.class)
	public void missingDomains() throws ParseException {
		InformaticaConfiguration ic = new InformaticaConfiguration(ETLTestParser.loadBareObject("").getJsonNode(), new BasicRuntimeSupport(), null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void emptyDomains() throws ParseException {
		InformaticaConfiguration ic = new InformaticaConfiguration(ETLTestParser.loadBareObject("domains: {}").getJsonNode(), new BasicRuntimeSupport(), null);
	}
}