package org.bitbucket.bradleysmithllc.etlagent.informatica.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.Server;
import org.bitbucket.bradleysmithllc.etlagent.informatica.client.InformaticaAgentClient;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.*;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.create_informatica_folder.CreateInformaticaFolderRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.delete_informatica_connection.DeleteInformaticaConnectionRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.delete_informatica_folder.DeleteInformaticaFolderRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.import_workflow_from_xml.ImportWorkflowFromXmlRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.purge_test_folders.PurgeTestFoldersRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.ConnectionDetails;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDeadlockException;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.SqlServerConnectionDetails;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.hamcrest.core.IsInstanceOf;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class InformaticaAgentTest {
	private static final int testPort = 8288;

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private InformaticaAgentClient client;

	@Before
	public void startServer() throws Exception {
		ContextBase.multipartRequestEnabled = false;

		// use a test root
		// create a configuration file for informatica
		File file = temporaryFolder.newFolder();

		ContextBase.initiate(true);
		ContextBase.setRoot(file);

		File config = new FileBuilder(ContextBase.runtimeSupport.getFeatureConfigurationDirectory("informatica")).mkdirs().name("informatica.json").file();

		URL url = getClass().getResource("/informatica.json");

		if (url == null)
		{
			throw new IllegalArgumentException("Could not find informatica.json");
		}

		String jsonText = IOUtils.toString(url);

		FileUtils.write(config, jsonText);

		System.out.println("Copying informatica config to " + config);

		ContextBase.initialize();

		// use a test port
		System.setProperty("jersey.test.port", String.valueOf(testPort));

		Server.start();

		client = new InformaticaAgentClient("testclient", testPort);
		client.receiveRuntimeSupport(ContextBase.runtimeSupport);
	}

	@After
	public void stopServer() throws IOException {
		// every test should result in an informatica agent request
		Assert.assertEquals("informatica", AgentDispatcher.lastRequestedAgentId);
		Server.stop();
	}

	@Test
	public void importWorkflowNoFolders() throws IOException, InformaticaDeadlockException {
		client.importWorkflow(new ImportWorkflowRequestDTO("prefix", "workflow", temporaryFolder.newFile()));

		Assert.assertEquals("importWorkflowFromXml", AgentDispatcher.lastRequestId);

		// verify the parameters made it across in tact
		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ImportWorkflowFromXmlRequest.class));

		ImportWorkflowFromXmlRequest imReq = (ImportWorkflowFromXmlRequest) AgentDispatcher.lastRequest;

		Assert.assertEquals("prefix", imReq.getFolderPrefix());
		Assert.assertEquals("workflow", imReq.getWorkflowName());
		Assert.assertEquals("", imReq.getWorkflowXml());
		Assert.assertNull(imReq.getInformaticaDomain());
		Assert.assertNull(imReq.getDefaultDomain());
		Assert.assertNull(imReq.getDomains());
	}

	@Test
	public void importWorkflowFolders() throws Exception {
		client.importWorkflow(new ImportWorkflowRequestDTO("prefix", "workflow", temporaryFolder.newFile(), Arrays.asList("f1", "f2")));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ImportWorkflowFromXmlRequest.class));
		ImportWorkflowFromXmlRequest imReq = (ImportWorkflowFromXmlRequest) AgentDispatcher.lastRequest;
	}

	@Test
	public void deleteConnectionDefault() throws IOException {
		client.deleteConnection(new DeleteConnectionRequestDTO("folder"));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(DeleteInformaticaConnectionRequest.class));
	}

	@Test
	public void deleteConnectionFailIfNotExists() throws IOException {
		client.deleteConnection(new DeleteConnectionRequestDTO("folder", true));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(DeleteInformaticaConnectionRequest.class));
	}

	@Test
	public void deleteConnectionNoFailIfNotExists() throws IOException {
		client.deleteConnection(new DeleteConnectionRequestDTO("folder", false));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(DeleteInformaticaConnectionRequest.class));
	}

	@Test
	public void createConnection() throws IOException {
		client.createConnection(new CreateConnectionRequestDTO(new ConnectionDetails("rel" ,new SqlServerConnectionDetails().withServerName("server").withDatabaseName("database").withDatabaseUserName("user").withDatabasePassword("pass"))));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(CreateConnectionRequestDTO.class));
	}

	@Test
	public void deleteFolderFailIfNotExists() throws IOException {
		client.deleteFolder(new DeleteFolderRequestDTO("folder", true));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(DeleteInformaticaFolderRequest.class));
	}

	@Test
	public void deleteFolderNoFailIfNotExists() throws IOException {
		client.deleteFolder(new DeleteFolderRequestDTO("folder", false));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(DeleteInformaticaFolderRequest.class));
	}

	@Test
	public void deleteFolderDefault() throws IOException {
		client.deleteFolder(new DeleteFolderRequestDTO("folder"));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(DeleteInformaticaFolderRequest.class));
	}

	@Test
	public void createFolder() throws IOException, InformaticaError {
		client.createFolder(new CreateFolderRequestDTO("folder"));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(CreateInformaticaFolderRequest.class));
	}

	@Test
	public void purgeTestFolders() throws IOException {
		client.purgeTestFolders(new PurgeTestFoldersRequestDTO());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(PurgeTestFoldersRequest.class));
	}

	@Test
	public void executeWorkflowNoParmOrFiles() throws IOException {
		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance")).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
		Assert.assertEquals("runinstance", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getRunInstanceName());
		Assert.assertEquals("folder", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getFolder());
		Assert.assertEquals("workflow", ( (ExecuteWorkflowRequestDTO)AgentDispatcher.lastRequest).getWorkflowName());
		Assert.assertEquals("task", ((ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest).getTask());
	}

	@Test
	public void executeWorkflowRunInstance() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder");
		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeWorkflowWithHub() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder");

		request.setInformaticaDomain("DOM_TEST");
		request.setInformaticaRepository("REP_TEST_II");
		request.setInformaticaIntegrationService("INT_SVC_TEST_IV");
		request.setInformaticaWebServicesHub("WEB_SVC_HUB_II");

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
		ExecuteWorkflowRequestDTO reqdto = (ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest;
		Assert.assertEquals("DOM_TEST", reqdto.getInformaticaDomain());
		Assert.assertEquals("REP_TEST_II", reqdto.getInformaticaRepository());
		Assert.assertEquals("INT_SVC_TEST_IV", reqdto.getInformaticaIntegrationService());
		Assert.assertEquals("WEB_SVC_HUB_II", reqdto.getInformaticaWebServicesHub());
	}

	@Test
	public void executeWorkflowSessionId() throws IOException {
		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(new ExecuteWorkflowRequestDTO("workflow", "folder")).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		ExecuteWorkflowRequestDTO ewr = (ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest;

		Assert.assertEquals("testclient", ewr.getSessionId());
	}

	@Test
	public void executeWorkflowNoParm() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder");

		request.addRequestFile(newFile("name1", "name7.1", RemoteFile.fileType.badFiles));
		request.addRequestFile(newFile("name2", "name6.1", RemoteFile.fileType.lookupFiles));
		request.addRequestFile(newFile("name3", "name5.1", RemoteFile.fileType.sourceFiles));
		request.addRequestFile(newFile("name4", "name4.1", RemoteFile.fileType.targetFiles));
		request.addRequestFile(newFile("name5", "name3.1", RemoteFile.fileType.workflowLog));
		request.addRequestFile(newFile("name6", "name2.1", RemoteFile.fileType.sessionLog));
		request.addRequestFile(newFile("name7", "name1.1", RemoteFile.fileType.parameterFiles));

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertEquals(7, resFiles.size());

		List<RemoteFile> badFiles = resFiles.get(RemoteFile.fileType.badFiles);
		Assert.assertNotNull(badFiles);
		Assert.assertEquals(1, badFiles.size());
		Assert.assertEquals(RemoteFile.fileType.badFiles, badFiles.get(0).getType());
		Assert.assertEquals("name1", badFiles.get(0).getFileName());
		Assert.assertEquals("name7.1", FileUtils.readFileToString(badFiles.get(0).getFile()));

		List<RemoteFile> targetFiles = resFiles.get(RemoteFile.fileType.targetFiles);
		Assert.assertNotNull(targetFiles);
		Assert.assertEquals(1, targetFiles.size());
		Assert.assertEquals(RemoteFile.fileType.targetFiles, targetFiles.get(0).getType());
		Assert.assertEquals("name4", targetFiles.get(0).getFileName());
		Assert.assertEquals("name4.1", FileUtils.readFileToString(targetFiles.get(0).getFile()));

		List<RemoteFile> workflowLog = resFiles.get(RemoteFile.fileType.workflowLog);
		Assert.assertNotNull(workflowLog);
		Assert.assertEquals(1, workflowLog.size());
		Assert.assertEquals(RemoteFile.fileType.workflowLog, workflowLog.get(0).getType());
		Assert.assertEquals("name5", workflowLog.get(0).getFileName());
		Assert.assertEquals("name3.1", FileUtils.readFileToString(workflowLog.get(0).getFile()));

		List<RemoteFile> sessionFiles = resFiles.get(RemoteFile.fileType.sessionLog);
		Assert.assertNotNull(sessionFiles);
		Assert.assertEquals(1, sessionFiles.size());
		Assert.assertEquals(RemoteFile.fileType.sessionLog, sessionFiles.get(0).getType());
		Assert.assertEquals("name6", sessionFiles.get(0).getFileName());
		Assert.assertEquals("name2.1", FileUtils.readFileToString(sessionFiles.get(0).getFile()));

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertNotNull(parameterFiles);
		Assert.assertEquals(2, parameterFiles.size());
		Assert.assertEquals(RemoteFile.fileType.parameterFiles, parameterFiles.get(0).getType());
		//Assert.assertEquals("name1", parameterFiles.get(0).getFileContents());

		Assert.assertEquals(RemoteFile.fileType.parameterFiles, parameterFiles.get(1).getType());
		//Assert.assertEquals("workflow.folder.PRM", parameterFiles.get(1).getFileName());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	private RemoteFile newFile(String name1, String name7, RemoteFile.fileType badFiles) throws IOException {
		File file = temporaryFolder.newFile(name1);
		FileUtils.write(file, name7);

		return RemoteFile.newRemoteFile(file, badFiles);
	}

	@Test
	public void executeWorkflowParm() throws IOException {
		ParameterFile pf = new ParameterFile();

		pf.appendSection("[Folder.Workflow]").appendParameter("$Name", "Value");

		// make sure mapplet parameters make it through
		pf.getSection("[Folder.Workflow]").appendParameter("mapplet", "$Name", "Value");

		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", pf);

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);

		Assert.assertEquals(RemoteFile.fileType.parameterFiles, parameterFiles.get(0).getType());

		Assert.assertEquals(1, parameterFiles.size());

		Assert.assertEquals(0, pf.compareTo(new ParameterFile(parameterFiles.get(0).getFile())));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeWorkflowParmfile1() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", "task");

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertEquals(1, parameterFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeWorkflowParmfile2() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", "task", "runInstance");

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertEquals(1, parameterFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void executeWorkflowParmfile3() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("workflow", "folder", null, "runInstance");

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		List<RemoteFile> parameterFiles = resFiles.get(RemoteFile.fileType.parameterFiles);
		Assert.assertEquals(1, parameterFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
	}

	@Test
	public void listFolders() throws IOException {
		client.listFolders(new ListFoldersRequestDTO());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ListFoldersRequestDTO.class));
		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(ListFoldersResponseDTO.class));
	}

	@Test
	public void listConnections() throws IOException {
		client.listConnections(new ListConnectionsRequestDTO());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ListConnectionsRequestDTO.class));
		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(ListConnectionsResponseDTO.class));
	}

	@Test
	public void listWorkflows() throws IOException {
		client.listWorkflows(new ListWorkflowsRequestDTO("test"));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ListWorkflowsRequestDTO.class));
		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(ListWorkflowsResponseDTO.class));
	}

	//@Test
	public void validateWorkflowExists() throws IOException {
		client.validateWorkflowExists(new ValidateWorkflowExistsRequestDTO("folder", "wkf"));

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ValidateWorkflowExistsRequestDTO.class));
		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(ValidateWorkflowExistsResponseDTO.class));
	}
}