package org.bitbucket.bradleysmithllc.etlagent.informatica.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.client.InformaticaAgentClient;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.*;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.ConnectionDetails;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.SqlServerConnectionDetails;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.util.Hex;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSection;
import org.junit.Assert;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class AgentLoadTest {
	private static final int SWARM_COUNT = 20;
	private Semaphore semaphore = new Semaphore(1);
	private Semaphore successSemaphore = new Semaphore(SWARM_COUNT * 2);

	//@Test
	public void testSwarmingAgents() throws InterruptedException {
		successSemaphore.acquireUninterruptibly(SWARM_COUNT);

		// spawn off swarmers and let them go
		for (int i = 0; i < SWARM_COUNT; i++) {
			//new Thread(new AgentSwarm("etlqa02", semaphore, successSemaphore)).start();
			new Thread(new AgentSwarm("bsmithvm", semaphore, successSemaphore)).start();
		}

		Thread.sleep(360000L);

		semaphore.acquire();

		Assert.assertTrue(successSemaphore.tryAcquire(SWARM_COUNT, 600000L, TimeUnit.MILLISECONDS));
	}
}

class AgentSwarm implements Runnable {
	private static final Map<String, String> agentIds = new HashMap<String, String>();

	private final String host;
	private final Semaphore semaphore;
	private final Semaphore successSemaphore;

	private final String clientId = nextClientId();

	private int requestId = 0;

	private static long nextClientId = System.currentTimeMillis();

	private static synchronized String nextClientId() {
		return Hex.hexEncode(String.valueOf(nextClientId += 100302048908L).getBytes());
	}

	AgentSwarm(String host, Semaphore semaphore1, Semaphore semaphore) {
		this.host = host;
		this.semaphore = semaphore1;
		successSemaphore = semaphore;

		synchronized(agentIds)
		{
			if (agentIds.containsKey(clientId))
			{
				throw new IllegalArgumentException("Duplicated agent id: " + clientId);
			}

			agentIds.put(clientId, clientId);
		}
	}

	public void run() {
		while (semaphore.availablePermits() == 1) {
			String requestClientId = clientId + "_" + requestId++;

			System.out.println("Running agent pass " + requestClientId + " against host " + host);

			try {
				sleep(1000L);

				InformaticaAgentClient informaticaAgentClient = new InformaticaAgentClient(requestClientId, host);
				BasicRuntimeSupport runtimeSupport = new BasicRuntimeSupport();
				runtimeSupport.setUserLogger(new PrintWriterLog());
				runtimeSupport.setApplicationLogger(new PrintWriterLog());

				informaticaAgentClient.receiveRuntimeSupport(runtimeSupport);

				String requestFolderId = "__" + requestClientId + "_SHARED_EDW";

				sleep(100L);

				try
				{
					System.out.println("Delete folder");
					informaticaAgentClient.deleteFolder(new DeleteFolderRequestDTO(requestFolderId, false));
				}
				catch(Exception exc)
				{
					// stupid crap
					System.out.println("Delete folder failed . . .");
				}

				informaticaAgentClient.createFolder(new CreateFolderRequestDTO(requestFolderId));

				sleep(100L);

				try
				{
					System.out.println("Delete connection");
					informaticaAgentClient.deleteConnection(new DeleteConnectionRequestDTO(requestFolderId, false));
				}
				catch(Exception exc)
				{
					// stupid crap
					System.out.println("Delete connection failed . . .");
				}

				sleep(100L);
				System.out.println("Create connection");
				informaticaAgentClient.createConnection(new CreateConnectionRequestDTO(
						new ConnectionDetails(requestFolderId, new SqlServerConnectionDetails().withServerName("server").withDatabaseUserName("database").withDatabaseUserName("user").withDatabasePassword("password"))));

				sleep(500L);

				System.out.println("Delete connection");
				informaticaAgentClient.deleteConnection(new DeleteConnectionRequestDTO(requestFolderId, true));

				File src = new File("/Users/bsmith/git/etl-unit/integration/informatica-integration-test/src/main/informatica/SHARED_EDW/wkf_INT_CONVERSION_TEST.xml");
				File tgt = new File("wkf_INT_CONVERSION_TEST.xml");

				File xml = runtimeSupport.createAnonymousTempFile();

				String wkf = FileUtils.readFileToString(src);

				String repl = "INT_CONVERSION_TEST_" + requestClientId;
				String replacement = "wkf_" + repl;
				wkf = wkf.replace("wkf_INT_CONVERSION_TEST", replacement);

				FileUtils.write(xml, wkf);

				sleep(200L);

				System.out.println("Import workflow");
				informaticaAgentClient.importWorkflow(new ImportWorkflowRequestDTO("__" + requestClientId, replacement, xml));

				sleep(400L);

				System.out.println("Export workflow");
				informaticaAgentClient.exportWorkflow(new ExportWorkflowRequestDTO(requestFolderId, replacement, tgt));

				sleep(200L);

				final String[][] parameters =
						{
								new String[]{"$BadFileName1", "crap"},
								new String[]{"$InputFile1", "inputfile"},
								new String[]{"$OutputFile1", "outputfile"},
								new String[]{"$PMTargetFileDir", "#{targetFiles}"},
								new String[]{"$PMSourceFileDir", "#{sourceFiles}"},
								new String[]{"$PMLookupFileDir", "#{lookupFiles}"},
								new String[]{"$PMBadFileDir", "#{badFiles}"},
								new String[]{"$PMSessionLogDir", "#{sessionLog}"},
								new String[]{"$PMWorkflowLogDir", "#{workflowLog}"},
								new String[]{"$PMSessionLogFile", "s_m_int_conversion_test.log"}
						};

				ParameterFile pf = new ParameterFile();

				ScopeSection ss = pf.appendSection("[Global]");

				for (String[] params : parameters) {
					ss.appendParameter(params[0], params[1]);
				}

				ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO(requestFolderId, replacement, pf);

				File source1 = new File("source1");
				FileUtils.write(source1, "source1");
				File source2 = new File("source2");
				FileUtils.write(source2, "source2");

				File lookup1 = new File("lookup1");
				FileUtils.write(lookup1, "lookup1");
				File lookup2 = new File("lookup2");
				FileUtils.write(lookup2, "lookup2");

				request.addRequestFile(RemoteFile.newRemoteFile(source1, RemoteFile.fileType.sourceFiles));
				request.addRequestFile(RemoteFile.newRemoteFile(source2, RemoteFile.fileType.sourceFiles));
				request.addRequestFile(RemoteFile.newRemoteFile(lookup1, RemoteFile.fileType.lookupFiles));
				request.addRequestFile(RemoteFile.newRemoteFile(lookup2, RemoteFile.fileType.lookupFiles));

				source2 = new File("inputfile");
				FileUtils.write(source2, "Ordermatic|1D6|x|x|x|x\n" +
						"Ordermatic|0.01|x|x|x|x\n" +
						"Ordermatic|1.045|x|x|x|x\n" +
						"Ordermatic|1.033335|x|x|x|x\n" +
						"Ordermatic|-1D6|x|x|x|x\n" +
						"Ordermatic|-0.01|x|x|x|x\n" +
						"Ordermatic|-1.045|x|x|x|x\n" +
						"Ordermatic|-1.033335|x|x|x|x\n" +
						"SONIC NT|1D6|1D6|1D6|1D6|1D6\n" +
						"SONIC NT|0.01|0.01|0.01|0.01|0.01\n" +
						"SONIC NT|1.045|1.045|1.045|1.045|1.045\n" +
						"SONIC NT|1.033335|x|x|x|x\n" +
						"SONIC NT|-1D6|1D6|1D6|1D6|1D6\n" +
						"SONIC NT|-0.01|0.01|0.01|0.01|0.01\n" +
						"SONIC NT|-1.045|1.045|1.045|1.045|1.045\n" +
						"SONIC NT|-1.033335|x|x|x|x\n" +
						"Ordermatic|1234567890|x123|1.23||1 11\n" +
						"Ordermatic|100000|x|x|x|x\n" +
						"Ordermatic|x|100000|x|x|x\n" +
						"Ordermatic|x|x|100000|x|x\n" +
						"Ordermatic|x|x|x|100000|x\n" +
						"Ordermatic|x|x|x|x|100000\n" +
						"Ordermatic|10000|x|x|x|x\n" +
						"Ordermatic|x|10000|x|x|x\n" +
						"Ordermatic|x|x|10000|x|x\n" +
						"Ordermatic|x|x|x|10000|x\n" +
						"Ordermatic|x|x|x|x|10000\n" +
						"SONIC NT|100000|x|x|x|x\n" +
						"SONIC NT|x|100000|x|x|x\n" +
						"SONIC NT|x|x|100000|x|x\n" +
						"SONIC NT|x|x|x|100000|x\n" +
						"SONIC NT|x|x|x|x|100000\n" +
						"SONIC NT|10000|x|x|x|x\n" +
						"SONIC NT|x|10000|x|x|x\n" +
						"SONIC NT|x|x|10000|x|x\n" +
						"SONIC NT|x|x|x|10000|x\n" +
						"SONIC NT|x|x|x|x|10000\n" +
						"SONIC NT|1}|1}|1}|1}|1}\n" +
						"SONIC NT|1J|1J|1J|1J|1J\n" +
						"SONIC NT|1K|1K|1K|1K|1K\n" +
						"SONIC NT|1L|1L|1L|1L|1L\n" +
						"SONIC NT|1M|1M|1M|1M|1M\n" +
						"SONIC NT|1N|1N|1N|1N|1N\n" +
						"SONIC NT|1O|1O|1O|1O|1O\n" +
						"SONIC NT|1P|1P|1P|1P|1P\n" +
						"SONIC NT|1Q|1Q|1Q|1Q|1Q\n" +
						"SONIC NT|1R|1R|1R|1R|1R\n" +
						"SONIC NT|0|x|x|x|x\n" +
						"SONIC NT|x|0|x|x|x\n" +
						"SONIC NT|x|x|0|x|x\n" +
						"SONIC NT|x|x|x|0|x\n" +
						"SONIC NT|x|x|x|x|0\n" +
						"Ordermatic|0|x|x|x|x\n" +
						"Ordermatic|x|0|x|x|x\n" +
						"Ordermatic|x|x|0|x|x\n" +
						"Ordermatic|x|x|x|0|x\n" +
						"Ordermatic|x|x|x|x|0\n" +
						"SONIC NT|5|05|005|0005|00005\n" +
						"SONIC NT|50|050|0050|00050|000050\n" +
						"SONIC NT|500|0500|00500|000500|0000500\n" +
						"Ordermatic|5|05|005|0005|00005\n" +
						"Ordermatic|50|050|0050|00050|000050\n" +
						"Ordermatic|500|0500|00500|000500|0000500\n");
				request.addRequestFile(RemoteFile.newRemoteFile(source2, RemoteFile.fileType.sourceFiles));

				System.out.println("Execute workflow");
				informaticaAgentClient.executeWorkflow(request);

				System.out.println("Delete folder");
				informaticaAgentClient.deleteFolder(new DeleteFolderRequestDTO(requestFolderId, true));
			} catch (Exception exc) {
				exc.printStackTrace();
				System.out.println("Failure in client " + requestClientId + "." + host + " at " + new Date() + exc);
			}
		}

		successSemaphore.release();
	}

	private void sleep(long l) throws InterruptedException {
		long actSleep = (long) (Math.random() * Long.MAX_VALUE);

		Thread.sleep(Math.max(actSleep % l, 500L));
	}
}