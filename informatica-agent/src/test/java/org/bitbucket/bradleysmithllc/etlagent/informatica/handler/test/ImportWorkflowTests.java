package org.bitbucket.bradleysmithllc.etlagent.informatica.handler.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.import_workflow_from_xml.ImportWorkflowFromXmlRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.handler.ImportWorkflowFromXml;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaOutOfProcessRepositoryClient;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test.PmRepImposter;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test.PmRepProcessScripter;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;

public class ImportWorkflowTests
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void cacheException() throws Exception {
		BasicRuntimeSupport brs = new BasicRuntimeSupport();
		brs.setApplicationLogger(new PrintWriterLog());

		ImportWorkflowFromXml handler = new ImportWorkflowFromXml();

		PmRepProcessScripter poster = new PmRepProcessScripter();

		poster.addScriptAction(new PmRepProcessScripter.ScriptAction("connect", "connect completed successfully."));
		poster.addScriptAction(new PmRepProcessScripter.ScriptAction("objectimport", "1 Processed, 0 Errors, 0 Warnings"));

		InformaticaConfiguration informaticaConfiguration = new InformaticaConfiguration(JsonLoader.fromString(
				"{\"domains\": {\"DOM_DEV\": {\"client-version\": \"1\", \"username\": \"user\", \"password-encrypted\": \"pass\", \"repositories\": {\"REP_SVC\": {\"integration-services\": [\"INT_SVC\"]}}}}}"
		), brs, temporaryFolder.newFile());

		InformaticaOutOfProcessRepositoryClient pclient = new InformaticaOutOfProcessRepositoryClient(
				informaticaConfiguration.getDefaultDomain().getDefaultRepository(),
				poster,
				brs
		);
		handler.setOverrideInformaticaRepositoryClient(pclient);

		handler.setInformaticaConfiguration(informaticaConfiguration);
		handler.receiveRuntimeSupport(brs);
		handler.process(JsonLoader.fromString("{}"), handler.getRequestContainerObject());
	}
}