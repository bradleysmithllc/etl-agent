package org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.fge.jackson.JsonLoader;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaWebServicesHub;
import org.bitbucket.bradleysmithllc.etlunit.BasicRuntimeSupport;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Pattern;

public class InformaticaConfigurationTests {
	@Test
	public void normalUsage() throws IOException {
		URL url = InformaticaConfigurationTests.class.getResource("/jsonTests/testList.json");

		JsonNode testList = JsonLoader.fromURL(url);

		JsonNode lists = testList.get("tests");

		ArrayNode listsNode = (ArrayNode) lists;

		Iterator<JsonNode> listIt = listsNode.iterator();

		while (listIt.hasNext()) {
			JsonNode testNode = listIt.next();

			runTest(testNode.asText());
		}
	}

	private void runTest(String testName) throws IOException {
		System.out.println("Processing test source: " + testName);

		URL url = InformaticaConfigurationTests.class.getResource("/jsonTests/" + testName + ".json");

		JsonNode test = JsonLoader.fromURL(url);
		JsonNode excString = test.get("exception-string");

		JsonNode input = test.get("input");

		try {
			InformaticaConfiguration id = new InformaticaConfiguration(input, new BasicRuntimeSupport(), new File(""));

			// if we are here, there had better not be an expected exception
			Assert.assertTrue(excString.isNull());

			// test the default domain
			JsonNode defaultDomainNode = test.get("default-domain");
			if (defaultDomainNode != null)
			{
				Assert.assertEquals(defaultDomainNode.asText(), id.getDefaultDomain().getDomainName());
			}

			// check the list of domains loaded
			ObjectNode domainsList = (ObjectNode) test.get("domainList");

			Assert.assertEquals(id.getDomains().size(), domainsList.size());

			Iterator<Map.Entry<String, JsonNode>> fit = domainsList.fields();

			while (fit.hasNext()) {
				Map.Entry<String, JsonNode> domain = fit.next();

				// grab the matching domain from the id
				InformaticaDomain idomain = id.getDomain(domain.getKey());

				// test attributes
				JsonNode domainValue = domain.getValue();
				Assert.assertEquals(domainValue.get("default-repository").asText(), idomain.getDefaultRepository().getRepositoryName());
				Assert.assertEquals(domainValue.get("default-integration-service").asText(), idomain.getDefaultIntegrationService().getIntegrationServiceName());
				Assert.assertEquals(domainValue.get("default-web-services-hub").asText(), idomain.getDefaultWebServicesHub().getName());

				JsonNode jsonNode = domainValue.get("working-root");
				if (jsonNode != null)
				{
					Assert.assertEquals(jsonNode.asText(), idomain.getWorkingRoot().getName());
				}

				jsonNode = domainValue.get("username");
				if (jsonNode != null)
				{
					Assert.assertEquals(jsonNode.asText(), idomain.getUserName());
				}

				jsonNode = domainValue.get("password-encrypted");
				if (jsonNode != null)
				{
					Assert.assertEquals(jsonNode.asText(), idomain.getPasswordEncrypted());
				}

				jsonNode = domainValue.get("client-version");
				if (jsonNode != null)
				{
					Assert.assertEquals(jsonNode.asText(), idomain.getClientVersion());
				}

				jsonNode = domainValue.get("client-type");
				if (jsonNode != null)
				{
					Assert.assertEquals(jsonNode.asText(), idomain.getClientType().name().toLowerCase());
				}

				jsonNode = domainValue.get("connectivity-host");
				if (jsonNode != null)
				{
					Assert.assertEquals(jsonNode.asText(), idomain.getConnectivityHost());
				}

				jsonNode = domainValue.get("connectivity-port");
				if (jsonNode != null)
				{
					Assert.assertEquals(jsonNode.asInt(), idomain.getConnectivityPort());
				}

				jsonNode = domainValue.get("informatica-bin-directory");
				if (jsonNode != null)
				{
					Assert.assertNotNull("Missing bin dir", idomain.getInformaticaBinDirectory());
					Assert.assertEquals(jsonNode.asText(), idomain.getInformaticaBinDirectory().getName());
				}

				jsonNode = domainValue.get("security-domain");
				if (jsonNode != null)
				{
					Assert.assertEquals(jsonNode.asText(), idomain.getSecurityDomain());
				}

				JsonNode agentHostNode = domainValue.get("agent-host");
				if (agentHostNode.isNull())
				{
					Assert.assertNull(idomain.getAgentHost());
				}
				else
				{
					Assert.assertEquals(agentHostNode.asText(), idomain.getAgentHost());
				}

				Assert.assertEquals(domainValue.get("agent-port").asInt(), idomain.getAgentPort());

				ObjectNode repoList = (ObjectNode) domainValue.get("repositories");

				Assert.assertEquals(idomain.getRepositories().size(), repoList.size());

				Iterator<Map.Entry<String,JsonNode>> repoIt = repoList.fields();

				while (repoIt.hasNext()) {
					Map.Entry<String, JsonNode> jsonNodeEntry = repoIt.next();
					InformaticaRepository repository = idomain.getRepository(jsonNodeEntry.getKey());
					Assert.assertNotNull(repository);

					validateRepository(repository, jsonNodeEntry.getValue(), testName);
				}
			}
		} catch (Exception exc) {
			Assert.assertNotNull(excString);
			//exc.printStackTrace(System.out);
			Assert.assertFalse(exc.toString(), excString.isNull());
			Assert.assertTrue(testName + ": " + exc.toString(), Pattern.compile(excString.asText()).matcher(exc.toString()).find());
		}
	}

	private void validateRepository(InformaticaRepository repository, JsonNode value, String testName) throws Exception {
		ObjectNode jsonNode = (ObjectNode) value.get("web-services-hubs");
		if (jsonNode != null)
		{
			Iterator<Map.Entry<String,JsonNode>> hit = jsonNode.fields();

			while (hit.hasNext())
			{
				Map.Entry<String, JsonNode> nextHub = hit.next();

				InformaticaWebServicesHub webServicesHub = repository.getWebServicesHub(nextHub.getKey());
				Assert.assertNotNull(webServicesHub);

				validateWebServicesHub(webServicesHub, nextHub.getValue(), testName);
			}
		}
	}

	private void validateWebServicesHub(InformaticaWebServicesHub repository, JsonNode value, String testName) throws Exception {
		JsonNode hostNameNode = value.get("host-name");

		if (hostNameNode != null)
		{
			String expected = hostNameNode.asText();

			if (expected.equals("$LOCAL"))
			{
				expected = InetAddress.getLocalHost().getHostAddress();
			}

			Assert.assertEquals("Bad WSH host name for test: " + testName, expected, repository.getHostName());
		}

		JsonNode hostPortNode = value.get("host-port");

		if (hostPortNode != null)
		{
			Assert.assertEquals(hostPortNode.asInt(), repository.getPort());
		}
	}
}