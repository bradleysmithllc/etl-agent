package org.bitbucket.bradleysmithllc.etlagent.informatica.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.Server;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.client.InformaticaAgentClient;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExecuteWorkflowRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExecuteWorkflowResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.handler.ExecuteWorkflowMultipart;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.hamcrest.core.IsInstanceOf;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.*;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class InformaticaAgentMultipartDefaultsTest {
	private static final int testPort = 8289;

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private InformaticaAgentClient client;

	@Before
	public void startServer() throws Exception {
		ContextBase.multipartRequestEnabled = true;

		// use a test root
		// create a configuration file for informatica
		File file = temporaryFolder.newFolder();

		ContextBase.initiate(true);
		ContextBase.setRoot(file);

		File config = new FileBuilder(ContextBase.runtimeSupport.getFeatureConfigurationDirectory("informatica")).mkdirs().name("informatica.json").file();

		URL url = getClass().getResource("/informatica.json");

		if (url == null)
		{
			throw new IllegalArgumentException("Could not find informatica.json");
		}

		String jsonText = IOUtils.toString(url);

		FileUtils.write(config, jsonText);

		System.out.println("Copying informatica config to " + config);

		ContextBase.initialize();

		// use a test port
		System.setProperty("jersey.test.port", String.valueOf(testPort));

		Server.start();

		client = new InformaticaAgentClient("testclient", testPort);
		client.receiveRuntimeSupport(ContextBase.runtimeSupport);
	}

	@After
	public void stopServer() throws IOException {
		// every test should result in an informatica agent request
		Assert.assertEquals("informatica", AgentDispatcher.lastRequestedAgentId);
		Server.stop();
	}

	@Test
	public void defaultDomain() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");

		client.executeWorkflow(request);

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_DEV", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaDomain());
	}

	@Test
	public void overrideDomain() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");
		request.setInformaticaDomain("DOM_TEST");

		client.executeWorkflow(request);

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_TEST", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaDomain());
	}

	@Test
	public void defaultRepo() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");

		client.executeWorkflow(request);

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_DEV", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaDomain());
		Assert.assertEquals("DOM_DEV.REP_SVC_DEV", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaRepository());
	}

	@Test
	public void overrideRepo() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");
		request.setInformaticaRepository("REP_SVC_DEV_II");

		client.executeWorkflow(request);

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_DEV.REP_SVC_DEV_II", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaRepository());
	}

	@Test
	public void defaultIs() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");

		client.executeWorkflow(request);

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_DEV.REP_SVC_DEV.INT_SVC_CI_DEV", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaIntegrationService());
	}

	@Test
	public void overrideIs() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");
		request.setInformaticaIntegrationService("INT_SVC_CI_DEV_II");

		client.executeWorkflow(request);

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_DEV.REP_SVC_DEV.INT_SVC_CI_DEV_II", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaIntegrationService());
	}

	@Test
	public void defaultWSH() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");

		client.executeWorkflow(request);

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_DEV.REP_SVC_DEV.WEB_SVC_HUB_I", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaWebServicesHub());
	}

	@Test
	public void overrideWSH() throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow", "task", "runinstance");
		request.setInformaticaWebServicesHub("WEB_SVC_HUB_I_I");

		client.executeWorkflow(request);

		Assert.assertThat(AgentDispatcher.lastResponse, new IsInstanceOf(MultipartResponseDTO.class));

		Assert.assertEquals("DOM_DEV.REP_SVC_DEV.WEB_SVC_HUB_I_I", ( (MultipartResponseDTO)AgentDispatcher.lastResponse).getRequestDTO(ExecuteWorkflowResponseDTO.class).getExecuteDetails().getInformaticaWebServicesHub());
	}
}
