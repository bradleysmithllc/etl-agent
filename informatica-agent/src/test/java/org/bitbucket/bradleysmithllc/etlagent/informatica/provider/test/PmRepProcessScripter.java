package org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.PmRepProcess;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class PmRepProcessScripter implements PmRepProcess
{
	public static class ScriptAction
	{
		String command;
		String response;
		long delay = -1L;

		IOException checkedExceptionToRaise;
		RuntimeException uncheckedExceptionToRaise;

		public ScriptAction(String command, String response) {
			this.command = command;
			this.response = response;
		}

		public ScriptAction(String command, String response, long delay) {
			this.command = command;
			this.response = response;
			this.delay = delay;
		}

		public ScriptAction(String command, long delay, IOException checkedExceptionToRaise) {
			this.command = command;
			this.delay = delay;
			this.checkedExceptionToRaise = checkedExceptionToRaise;
		}

		public ScriptAction(String command, long delay, RuntimeException uncheckedExceptionToRaise) {
			this.command = command;
			this.delay = delay;
			this.uncheckedExceptionToRaise = uncheckedExceptionToRaise;
		}

		public ScriptAction(String command, RuntimeException uncheckedExceptionToRaise) {
			this.command = command;
			this.uncheckedExceptionToRaise = uncheckedExceptionToRaise;
		}

		public ScriptAction(String command, IOException checkedExceptionToRaise) {
			this.command = command;
			this.checkedExceptionToRaise = checkedExceptionToRaise;
		}

		public void setCommand(String command) {
			this.command = command;
		}

		public void setResponse(String response) {
			this.response = response;
		}

		public void setDelay(long delay) {
			this.delay = delay;
		}

		public void setCheckedExceptionToRaise(IOException checkedExceptionToRaise) {
			this.checkedExceptionToRaise = checkedExceptionToRaise;
		}

		public void setUncheckedExceptionToRaise(RuntimeException uncheckedExceptionToRaise) {
			this.uncheckedExceptionToRaise = uncheckedExceptionToRaise;
		}
	}

	List<ScriptAction> script = new ArrayList<ScriptAction>();
	int currentActionOffset = 0;

	public void addScriptAction(ScriptAction sAction)
	{
		script.add(sAction);
	}

	@Override
	public String send(String line) throws IOException {
		return send(line, -1L);
	}

	@Override
	public String send(String line, long timeout) throws IOException {
		if (currentActionOffset >= script.size())
		{
			throw new ScriptException("Too many requests");
		}

		ScriptAction sa = script.get(currentActionOffset++);

		Pattern pat = Pattern.compile(sa.command);

		if (pat.matcher(line).find())
		{
			if (sa.delay != -1L)
			{
				try {
					Thread.sleep(sa.delay);
				} catch (InterruptedException e) {
					throw new RuntimeException(e);
				}
			}

			// determine what to do
			if (sa.checkedExceptionToRaise != null)
			{
				throw sa.checkedExceptionToRaise;
			}
			else if (sa.uncheckedExceptionToRaise != null)
			{
				throw sa.uncheckedExceptionToRaise;
			}

			return sa.response;
		}
		else
		{
			throw new ScriptException("Command out of sequence: " + line + ", was expecting " + sa.command);
		}
	}

	@Override
	public void dispose() throws IOException {
	}
}