package org.bitbucket.bradleysmithllc.etlagent.informatica.provider.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepositoryClientImpl;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;

public class RespositoryClientExecuteResultsTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void testGood() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "INFO: Workflow [wkf]: Execution succeeded.", 10, "wkf", new PrintWriterLog(), false);
	}

	@Test
	public void testGoodTetsMode() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "INFO: Workflow [wkf]: Execution succeeded.", 10, "wkf", new PrintWriterLog(), true);
	}

	@Test(expected = InformaticaError.class)
	public void testBadRc() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(-1, "INFO: Workflow [wkf]: Execution succeeded.", 10, "wkf", new PrintWriterLog(), false);
	}

	@Test(expected = InformaticaError.class)
	public void testBadRcTestMode() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(-1, "INFO: Workflow [wkf]: Execution succeeded.", 10, "wkf", new PrintWriterLog(), true);
	}

	@Test
	public void testGoodConcurrent() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "\tCould not acquire the execute lock for Workflow [wkf_wkf]", 29, "wkf", new PrintWriterLog(), false);
	}

	@Test
	public void testGoodConcurrentTestMode() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "\tCould not acquire the execute lock for Workflow [wkf_wkf]", 29, "wkf", new PrintWriterLog(), true);
	}

	@Test(expected = InformaticaError.class)
	public void testBadConcurrent() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "\tCould not acquire the execute lock for Workflow [wkf_wkf]", 30, "wkf", new PrintWriterLog(), false);
	}

	@Test(expected = InformaticaError.class)
	public void testBadConcurrentTestMode() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "\tCould not acquire the execute lock for Workflow [wkf_wkf]", 30, "wkf", new PrintWriterLog(), true);
	}

	@Test(expected = InformaticaError.class)
	public void badGoodResultCode() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "Informatica(r) PMCMD, version [9.1.0 HotFix4], build [449.0224], Windows 64-bit\n" +
				"Copyright (c) Informatica Corporation 1994 - 2012\n" +
				"All Rights Reserved.\n" +
				"\n" +
				"Invoked at Mon Jun 17 17:22:25 2013\n" +
				"\n" +
				"Connected to Integration Service: [INT_SVC_CI].\n" +
				"ERROR: Folder [SHARED_EDW] not found.\n" +
				"Disconnecting from Integration Service\n" +
				"\n" +
				"Completed at Mon Jun 17 17:22:25 2013", 10, "wkf", new PrintWriterLog(), false);
	}

	@Test
	public void badGoodResultCodeTestMode() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "Informatica(r) PMCMD, version [9.1.0 HotFix4], build [449.0224], Windows 64-bit\n" +
				"Copyright (c) Informatica Corporation 1994 - 2012\n" +
				"All Rights Reserved.\n" +
				"\n" +
				"Invoked at Mon Jun 17 17:22:25 2013\n" +
				"\n" +
				"Connected to Integration Service: [INT_SVC_CI].\n" +
				"ERROR: Folder [SHARED_EDW] not found.\n" +
				"Disconnecting from Integration Service\n" +
				"\n" +
				"Completed at Mon Jun 17 17:22:25 2013", 10, "wkf", new PrintWriterLog(), true);
	}

	@Test(expected = InformaticaError.class)
	public void badNoLog() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "", 10, "wkf", new PrintWriterLog(), false);
	}

	@Test
	public void badNoLogTestMode() throws IOException, InformaticaError, InterruptedException {
		InformaticaRepositoryClientImpl.checkExecution(0, "", 10, "wkf", new PrintWriterLog(), true);
	}

	@Test
	public void badNoLogTestMode1() throws IOException, InformaticaError, InterruptedException {
		Assert.assertFalse(InformaticaRepositoryClientImpl.checkExecution(0, "FINEST|1888/0|Service ETL-Agent|14-03-04 16:53:57|Executing process: Command[E:\\Informatica\\PowerCenter910\\server\\bin\\pmcmd]Arguments[[startworkflow, -service, INT_SVC_UT_DEV, -domain, DOM_DEV, -user, utrunner, -pv, INFAPASSWD, -folder, __bsmith_i3ynx8wix5lm_0_SCM_ANALYTICS, -paramfile, E:\\etl-agent\\temp\\executor_temp\\0\\d8b38e41-b0a2-43ff-974d-6706c1ac7841\\informatica_executeWorkflow_i3ynx8wix5lm_0_a27de38d-6cf0-458e-91a4-f20b4bd12ad0\\DOM_DEV\\ParameterFiles\\7c56262d-0a5c-4061-a331-d7910dae3ce2.PRM, -wait, __bsmith_i3ynx8wix5lm_0_wkf_EXTRACT_Masterfile_TO_EDW_ODS]]Env[{INFAPASSWD=O4X9VpAXoslQOsIqrzGmB0dqg/KbVHu8Tgn4pQC9lVE=, INFA_CLIENT_RESILIENCE_TIMEOUT=30, INFA_DOMAINS_FILE=E:\\etl-agent\\build\\informatica\\domains.infa.DOM_DEV}]Cwd[E:\\etl-agent\\temp\\executor_temp\\0\\ipc.3222]]Output[E:\\etl-agent\\temp\\executor_temp\\0\\process_log\\informatica\\pmcmd_startworkflow.0.out]\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|TestExecutionError{errorId='ERR_UNSPECIFIED',super='org.bitbucket.bradleysmithllc.etlunit.InformaticaError: Not sure what happened: \n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|Informatica(r) PMCMD, version [9.1.0 HotFix4], build [449.0224], Windows 64-bit\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|Copyright (c) Informatica Corporation 1994 - 2012\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|All Rights Reserved.\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|Invoked at Tue Mar 04 16:53:57 2014\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|Connected to Integration Service: [INT_SVC_UT_DEV].\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|Starting workflow [__bsmith_i3ynx8wix5lm_0_wkf_EXTRACT_Masterfile_TO_EDW_ODS]\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|Waiting for workflow [__bsmith_i3ynx8wix5lm_0_wkf_EXTRACT_Masterfile_TO_EDW_ODS] to complete\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|INFO: Workflow [__bsmith_i3ynx8wix5lm_0_wkf_EXTRACT_MasterFile_TO_EDW_ODS]: Execution succeeded.\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|Workflow __bsmith_i3ynx8wix5lm_0_wkf_EXTRACT_Masterfile_TO_EDW_ODS with run instance name [] and run id [16385734] started successfully.\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|Disconnecting from Integration Service\n" +
				"FINEST|1888/0|Service ETL-Agent|14-03-04 16:54:01|\n", 10, "__bsmith_i3ynx8wix5lm_0_wkf_EXTRACT_Masterfile_TO_EDW_ODS", new PrintWriterLog(), false));
	}
}