package org.bitbucket.bradleysmithllc.etlagent.informatica.test;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bitbucket.bradleysmithllc.etlagent.AgentDispatcher;
import org.bitbucket.bradleysmithllc.etlagent.Server;
import org.bitbucket.bradleysmithllc.etlagent.informatica.client.InformaticaAgentClient;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExecuteWorkflowRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.hamcrest.core.IsInstanceOf;
import org.junit.*;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class InformaticaWSHTest {
	private static final int testPort = 8288;

	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	private InformaticaAgentClient client;

	@Before
	public void startServer() throws Exception {
		// use a test root
		// create a configuration file for informatica
		File file = temporaryFolder.newFolder();

		ContextBase.initiate(true);
		ContextBase.setRoot(file);

		File config = new FileBuilder(ContextBase.runtimeSupport.getFeatureConfigurationDirectory("informatica")).mkdirs().name("informatica.json").file();

		URL url = getClass().getResource("/informatica.json");

		if (url == null)
		{
			throw new IllegalArgumentException("Could not find informatica.json");
		}

		String jsonText = IOUtils.toString(url);

		FileUtils.write(config, jsonText);

		System.out.println("Copying informatica config to " + config);

		ContextBase.initialize();

		// use a test port
		System.setProperty("jersey.test.port", String.valueOf(testPort));

		Server.start();

		client = new InformaticaAgentClient("testclient", testPort);
		client.receiveRuntimeSupport(ContextBase.runtimeSupport);
	}

	@After
	public void stopServer() throws IOException {
		// every test should result in an informatica agent requesy
		Assert.assertEquals("informatica", AgentDispatcher.lastRequestedAgentId);
		Server.stop();
	}

	@Test
	public void executeWorkflowWithHub() throws IOException {
		executeWithOptions("executeWorkflowWithHub", "DOM_TEST", "REP_TEST_II", "INT_SVC_TEST_IV", "WEB_SVC_HUB_II");
		executeWithOptions("executeWorkflowWithHubII", "DOM_DEV", "REP_SVC_DEV", "INT_SVC_CI_DEV", "WEB_SVC_HUB_I");
	}

	private void executeWithOptions(String name, String domain, String repository, String integrationService, String webSvcHubIi) throws IOException {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("folder", "workflow");

		request.setInformaticaDomain(domain);
		request.setInformaticaRepository(repository);
		request.setInformaticaIntegrationService(integrationService);

		if (webSvcHubIi != null)
		{
			request.setInformaticaWebServicesHub(webSvcHubIi);
		}

		Map<RemoteFile.fileType, List<RemoteFile>> resFiles = client.executeWorkflow(request).getWorkSpaceFiles();

		Assert.assertNotNull(resFiles.get(RemoteFile.fileType.parameterFiles));
		Assert.assertEquals(1, resFiles.size());

		Assert.assertThat(AgentDispatcher.lastRequest, new IsInstanceOf(ExecuteWorkflowRequestDTO.class));
		ExecuteWorkflowRequestDTO reqdto = (ExecuteWorkflowRequestDTO) AgentDispatcher.lastRequest;

		Assert.assertEquals(domain, reqdto.getInformaticaDomain());
		Assert.assertEquals(repository, reqdto.getInformaticaRepository());
		Assert.assertEquals(integrationService, reqdto.getInformaticaIntegrationService());

		if (webSvcHubIi != null)
		{
			Assert.assertEquals(webSvcHubIi, reqdto.getInformaticaWebServicesHub());
		}

		// look up the json file and assert the contents
		File jsonDir = new File(ContextBase.runtimeSupport.getGeneratedSourceDirectory("web-services-hub"), webSvcHubIi);

		Assert.assertTrue(jsonDir.exists());

		File [] files = jsonDir.listFiles();
		Assert.assertNotNull(files);
		Assert.assertEquals(1, files.length);

		String text = FileUtils.readFileToString(files[0]);
		System.out.println(text);
		String regex = text.replaceAll("\"[^\"]+\\.PRM\"", "\"ParameterFile.PRM\"");
		System.out.println(regex);
		JsonNode actNode = JsonLoader.fromString(regex);
		JsonNode expecNode = JsonLoader.fromURL(getClass().getClassLoader().getResource(name + ".json"));

		Assert.assertEquals(name, expecNode.toString(), actNode.toString());
	}
}