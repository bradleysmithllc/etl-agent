package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.regexp.InfaConcurrentWorkflowExpression;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.Incomplete;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class InformaticaOutOfProcessIntegrationServiceClient implements InformaticaIntegrationServiceClient {
	protected final Log applicationLlog;

	public static final String FOLDER_LIST_ATTRIBUTE = "folderList";
	private final List<String> temporaryFolders = new ArrayList();

	private final InformaticaDomain informaticaDomain;

	private final InformaticaIntegrationService informaticaIntegrationService;
	private final RuntimeSupport runtimeSupport;

	private PmCmdProcessImpl pmCmd;

	public InformaticaOutOfProcessIntegrationServiceClient(
			InformaticaIntegrationService informaticaRepository,
			File pmCmdDir,
			RuntimeSupport pRuntimeSupport,
			Log log
	) throws Exception {
		applicationLlog = log;
		this.informaticaIntegrationService = informaticaRepository;
		this.informaticaDomain = informaticaRepository.getInformaticaDomain();
		runtimeSupport = pRuntimeSupport;

		Map<String, String> env = new HashMap<String, String>();

		env.put("INFA_DOMAINS_FILE", informaticaDomain.getDomainFile().getAbsolutePath());
		env.put("INFAPASSWD", informaticaDomain.getPasswordEncrypted());

		String pmRepId = informaticaDomain.getDomainName() + "." + informaticaIntegrationService.getIntegrationServiceName();
		File workingPmrep = new FileBuilder(runtimeSupport.getTempDirectory()).subdir("informatica").subdir("pmrepWorking." + pmRepId).mkdirs().file();

		pmCmd = new PmCmdProcessImpl(new File(pmCmdDir, "pmcmd"), runtimeSupport, env, pmRepId, workingPmrep);

		connect();
	}

	public final void connect() throws Exception {
		pmCmd.send("connect -service " + informaticaIntegrationService.getIntegrationServiceName() + " -domain " + informaticaDomain.getDomainName() + " -passwordvar INFAPASSWD -user " + informaticaDomain.getUserName());
	}

	public InformaticaExecutionResult executeWorkflow(String workFlow, String runInstance, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, runInstance, null, folder, parmFile, null);
	}

	public InformaticaExecutionResult executeWorkflow(String workFlow, String runInstance, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> files) throws Exception {
		return executeWorkflowTask(workFlow, runInstance, null, folder, parmFile, files);
	}

	public InformaticaExecutionResult executeWorkflow(
			String workFlow,
			String folder,
			ParameterFile parmFile
	) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, null);
	}

	public InformaticaExecutionResult executeWorkflow(
			String workFlow,
			String folder,
			ParameterFile parmFile,
			Map<RemoteFile.fileType, List<RemoteFile>> files
	) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, files);
	}

	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String task, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, null);
	}

	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String task, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> files) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, files);
	}

	@Incomplete
	public InformaticaExecutionResult executeWorkflowTask(
			String workFlow,
			String runInstance,
			String task,
			String folder,
			ParameterFile parmFile
	) throws Exception {
		return executeWorkflowTask(workFlow, runInstance, task, folder, parmFile, null);
	}

	@Incomplete
	public InformaticaExecutionResult executeWorkflowTask(
			String workFlow,
			String runInstance,
			String task,
			String folder,
			ParameterFile parmFile,
			Map<RemoteFile.fileType, List<RemoteFile>> files
	) throws Exception {
		if (files != null) {
			// lay out the input files
			RemoteFile.copyRemoteToLocal(files, informaticaDomain);
		}

		// create a literal parameterFiles file
		File par = new File(new File(informaticaDomain.getWorkingRoot(), "ParameterFiles"), "TEMP_IGNORE_ME.PRM");

		parmFile.write(par);

		// execute the worklow
		StringBuilder command = new StringBuilder(task == null ? "startworkflow" : "starttask").append(' ')
				.append("-folder").append(' ')
				.append(folder).append(' ')
				.append("-paramfile").append(' ')
				.append(par.getAbsolutePath()).append(' ')
				.append("-wait").append(' ');

		if (runInstance != null) {
			command.append("-rin").append(' ').append(runInstance).append(' ');
		}

		if (task != null) {
			command.append("-workflow").append(' ').append(workFlow).append(' ');
			command.append(task);
		} else {
			command.append(workFlow);
		}

		boolean loop = true;
		int loopCount = 0;

		Map<RemoteFile.fileType, List<RemoteFile>> xfiles = new HashMap<RemoteFile.fileType, List<RemoteFile>>();
		String excStr = null;

		try {
			while (loop) {
				loopCount++;

				String out = pmCmd.send(command.toString());

				// TODO analyze output for successful completion
				InfaConcurrentWorkflowExpression match = InfaConcurrentWorkflowExpression.match(out);

				if (match.hasNext()) {
					if (loopCount >= 30) {
						throw new InformaticaError("Informatica workflow is too busy for testing: wkf_" + match.getWorkflowName());
					}

					applicationLlog.severe("Workflow is already running - will wait 10 seconds and retry.  This was attempt number [" + loopCount + "] of 30 maximum tries");

					Thread.sleep(10000);
				} else if (out.length() != 0 && out.indexOf("INFO: Workflow [" + workFlow + "]: Execution succeeded.") == -1) {
					throw new InformaticaError("Workflow did not execute properly: " + out);
				} else {
					applicationLlog.info("Infa workflow completed sucessfully.  Can break execution loop now");

					loop = false;
				}
			}
		} catch (Exception exc) {
			excStr = exc.toString();
		} finally {
			RemoteFile.copyLocalToRemote(informaticaDomain, xfiles);
		}

		return new InformaticaExecutionResult(xfiles, excStr, InformaticaResponseDTO.result.okay);
	}

	@Override
	public boolean validateWorkflowExists(String folder, String workflowName) {
		throw new UnsupportedOperationException();
	}

	public InformaticaDomain getInformaticaDomain() {
		return informaticaDomain;
	}

	public InformaticaIntegrationService getIntegrationService() {
		return informaticaIntegrationService;
	}

	public void dispose() {
		try {
			pmCmd.dispose();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}
}