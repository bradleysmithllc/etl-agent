package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExecuteWorkflowResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExportWorkflowResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.export_workflow_to_xml.ExportWorkflowToXmlRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.io.File;

public class ExportWorkflowToXml extends AbstractRequestHandler<ExportWorkflowToXmlRequest, ExportWorkflowResponseDTO> {
	@Override
	public String getId() {
		return "exportWorkflowToXml";
	}

	@Override
	public ExportWorkflowToXmlRequest getRequestContainerObject() {
		return new ExportWorkflowToXmlRequest();
	}

	@Override
	public ExportWorkflowResponseDTO process(JsonNode request, final ExportWorkflowToXmlRequest container) throws Exception {
		logger.info("Exporting workflow {}", container);

		String domain = container.getInformaticaDomain();

		String repo = container.getInformaticaRepository();

		InformaticaDomain informaticaDomain = getInformaticaDomain(domain);

		InformaticaRepository informaticaRepository = getInformaticaRepository(informaticaDomain, repo);

		String fileName = container.getWorkflowName();

		JsonNode requestUID = request.get("request-uid");

		if (requestUID != null && !requestUID.isNull())
		{
			fileName = requestUID.asText() + "_" + fileName;
		}

		final File temp = runtimeSupport.createTempFile(fileName);

		try
		{
			useRepository(informaticaRepository, new RepositoryVisitor() {
				@Override
				public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception {
					client.exportWorkflowToXml(
							container.getWorkflowName(),
							container.getFolder(),
							temp
					);
				}
			});

			ExportWorkflowResponseDTO dto = new ExportWorkflowResponseDTO(FileUtils.readFileToString(temp));
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.okay);

			return dto;
		}
		catch(InformaticaDeadlockException exc)
		{
			logger.error("Request produced a deadlock.  Passing on to the client.");

			// special case - let's return a retry error
			ExportWorkflowResponseDTO dto = new ExportWorkflowResponseDTO("<<failed>>");
			dto.setResponseMessage(exc.getMessage());
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.deadlock);
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_REPOSITORY_DEADLOCK);

			return dto;
		}
		catch(InformaticaError error)
		{
			ExportWorkflowResponseDTO dto = new ExportWorkflowResponseDTO("<<failed>>");
			dto.setResponseMessage(error.getMessage());
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.failed);
			return dto;
		}
	}
}