package org.bitbucket.bradleysmithllc.etlagent.informatica.dto;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RemoteFile {
	/**
	 * Prepare the given local files for being sent back to the client.
	 *
	 * @param informaticaDomain
	 * @param responseFiles
	 */
	public static void copyLocalToRemote(
			InformaticaDomain informaticaDomain,
			final Map<fileType,
					List<RemoteFile>> responseFiles
	) {
		// now that it is done, scan the output directories for artifacts
		for (final fileType type : RemoteFile.fileType.values()) {
			informaticaDomain.getWorkspaceDir(type).listFiles(new FileFilter() {
				@Override
				public boolean accept(File file) {
					try {
						addResponseFile(RemoteFile.newRemoteFile(file, type), responseFiles);
						return false;
					} catch (IOException e) {
						throw new RuntimeException(e);
					}
				}
			});
		}
	}

	public enum fileType {
		lookupFiles("LookupFiles"),
		sourceFiles("SourceFiles"),
		targetFiles("TargetFiles"),
		sessionLog("SessionLog"),
		workflowLog("WorkflowLog"),
		parameterFiles("ParameterFiles"),
		badFiles("BadFiles");

		private final String directoryName;

		private fileType(String directoryName) {
			this.directoryName = directoryName;
		}

		public String getDirectoryName() {
			return directoryName;
		}
	}

	/**
	 * The file data handler has the job of moving file data into and out of the remote file reference.
	 */
	public interface FileDataHandler {
		/**
		 * Copy the data referenced by the remote file into a local file.
		 *
		 * @param file
		 */
		void extractFileData(RemoteFile file) throws IOException;

		/**
		 * Take the source data referenced by the remote and store it somewhere.
		 *
		 * @param file
		 */
		void persistFileData(RemoteFile file) throws IOException;
	}

	private final fileType type;
	private transient File file;
	private String fileName;

	private transient FileDataHandler fileDataHandler;
	private String fileContents;

	public RemoteFile(File path, fileType type) {
		this.file = path;
		this.type = type;
		this.fileName = file.getName();
	}

	public RemoteFile(File path, String name, fileType type) {
		this.file = path;
		this.type = type;
		this.fileName = name;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public File getFile() {
		return file;
	}

	public String getFileContents() {
		return fileContents;
	}

	public void setFileContents(String fileContents) {
		this.fileContents = fileContents;
	}

	public fileType getType() {
		return type;
	}

	public void setFileDataHandler(FileDataHandler fileDataHandler) {
		this.fileDataHandler = fileDataHandler;
	}

	public static RemoteFile newRemoteFile(File source, fileType type) throws IOException {
		return new RemoteFile(source, type);
	}

	public static RemoteFile newRemoteFile(File source, String overrideFilename, fileType type) throws IOException {
		return new RemoteFile(source, overrideFilename, type);
	}

	public boolean sameId(RemoteFile file) {
		return getFileName().equals(file.getFileName()) && file.type == type;
	}

	public void persist() throws IOException {
		fileDataHandler.persistFileData(this);
	}

	public void extract() throws IOException {
		fileDataHandler.extractFileData(this);
	}

	/**
	 * Copy all files pointed to by this remote file collection into the specified informatica workspace.  This process
	 * requires that the virtual filesystem already be extracted and the file pointer set in the remote file.
	 * <p/>
	 * This is local to the informatica executor.
	 *
	 * @param sourceFiles1
	 * @param domain
	 * @throws IOException
	 */
	public static void copyRemoteToLocal(Map<fileType, List<RemoteFile>> sourceFiles1, InformaticaDomain domain) throws IOException {
		// first, recursively purge the workspace
		File workingRoot = domain.getLocalizedWorkingRoot();
		if (workingRoot.exists()) {
			FileUtils.forceDelete(workingRoot);
		}

		FileUtils.forceMkdir(workingRoot);

		// recreate
		for (fileType type : RemoteFile.fileType.values()) {
			File targetFiles = domain.getWorkspaceDir(type);
			FileUtils.forceMkdir(targetFiles);
		}

		if (sourceFiles1 != null) {
			for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> sourceEntry : sourceFiles1.entrySet()) {
				List<RemoteFile> value = sourceEntry.getValue();

				for (RemoteFile remoteFile : value) {
					// copy the data file to the workspace
					FileUtils.copyFile(remoteFile.getFile(), new File(domain.getWorkspaceDir(remoteFile.getType()), remoteFile.getFileName()));
				}
			}
		}
	}

	public static String getPath(RemoteFile remoteFile) {
		return "workspace/" + remoteFile.getType().name() + "/" + remoteFile.getFileName();
	}

	/**
	 * Adds a response file reference to the response before sending back to the client.
	 *
	 * @param remoteFile
	 * @param responseFiles
	 * @throws IOException
	 */
	private static void addResponseFile(RemoteFile remoteFile, Map<fileType, List<RemoteFile>> responseFiles) throws IOException {
		if (!responseFiles.containsKey(remoteFile.getType())) {
			responseFiles.put(remoteFile.getType(), new ArrayList<RemoteFile>());
		}

		// add the ref
		responseFiles.get(remoteFile.getType()).add(remoteFile);
	}

	@Override
	public String toString() {
		return "RemoteFile{" +
				"type=" + type +
				", fileName='" + fileName + '\'' +
				'}';
	}
}