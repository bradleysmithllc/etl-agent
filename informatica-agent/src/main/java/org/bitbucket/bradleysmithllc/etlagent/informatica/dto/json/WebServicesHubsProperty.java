
package org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "host-name",
    "host-port"
})
public class WebServicesHubsProperty {

    @JsonProperty("host-name")
    private String hostName;
    @JsonProperty("host-port")
    private Long hostPort;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("host-name")
    public String getHostName() {
        return hostName;
    }

    @JsonProperty("host-name")
    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    @JsonProperty("host-port")
    public Long getHostPort() {
        return hostPort;
    }

    @JsonProperty("host-port")
    public void setHostPort(Long hostPort) {
        this.hostPort = hostPort;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
