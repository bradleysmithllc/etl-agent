package org.bitbucket.bradleysmithllc.etlagent.informatica.dto;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.VirtualFiles;
import org.bitbucket.bradleysmithllc.etlagent.dto.MultipartResponseDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecuteWorkflowMultipartResponseDTO extends MultipartResponseDTO
{
	private response_code informaticaResponseCode = response_code.okay;

	private final Map<RemoteFile.fileType, List<RemoteFile>> responseFiles = new HashMap<RemoteFile.fileType, List<RemoteFile>>();

	public ExecuteWorkflowMultipartResponseDTO(VirtualFiles virtualFiles) {
		super(virtualFiles);
	}

	public void setInformaticaErrorMessage(String msg)
	{
		informaticaResponseCode = response_code.failed;
		setResponseMessage(msg);
	}

	public response_code getInformaticaResponseCode() {
		return informaticaResponseCode;
	}

	public void addResponseFile(RemoteFile remote) {
		List<RemoteFile> remoteFileList = responseFiles.get(remote.getType());

		if (remoteFileList != null)
		{
			for (RemoteFile remoteFile : remoteFileList)
			{
				if (remoteFile.sameId(remote))
				{
					throw new IllegalArgumentException("Remote file name and fileType already added");
				}
			}
		}
		else
		{
			remoteFileList = new ArrayList<RemoteFile>();
			responseFiles.put(remote.getType(), remoteFileList);
		}

		remoteFileList.add(remote);
	}

	public Map<RemoteFile.fileType, List<RemoteFile>> getResponseFiles() {
		return responseFiles;
	}
}