package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.google.common.cache.*;
import com.google.gson.GsonBuilder;
import org.apache.commons.pool.ObjectPool;
import org.apache.commons.pool.PoolableObjectFactory;
import org.apache.commons.pool.impl.GenericObjectPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.InformaticaHandler;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.BaseIntegrationServiceRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.BaseRepositoryRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.PrintWriterLog;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureOperation;

import java.util.concurrent.TimeUnit;

public abstract class AbstractHandler<T, J extends GenericResponseDTO> implements InformaticaHandler {
	private static Logger cacheLogger = LogManager.getLogger("INFORMATICA_HANDLER_CACHE");
	protected Logger logger = LogManager.getLogger(getClass());

	protected RuntimeSupport runtimeSupport;

	private InformaticaRepositoryClient testInformaticaRepositoryClient;
	private InformaticaExecutionClient testInformaticaExecutionClient;

	public void setOverrideInformaticaRepositoryClient(InformaticaRepositoryClient testInformaticaRepositoryClient) {
		this.testInformaticaRepositoryClient = testInformaticaRepositoryClient;
	}

	public void setOverrideInformaticaExecutionClient(InformaticaExecutionClient testInformaticaExecutionClient) {
		this.testInformaticaExecutionClient = testInformaticaExecutionClient;
	}

	protected void useExecutionService(BaseIntegrationServiceRequestDTO container, IntegrationServiceVisitor visitor) throws Exception {
		String domain = container.getInformaticaDomain();

		InformaticaDomain informaticaDomain = getInformaticaDomain(domain);

		String repo = container.getInformaticaRepository();

		InformaticaRepository informaticaRepository = getInformaticaRepository(informaticaDomain, repo);
		InformaticaWebServicesHub informaticaWebServicesHub = getInformaticaWebServicesHub(informaticaRepository, container.getInformaticaWebServicesHub());
		InformaticaIntegrationService integrationService = getInformaticaIntegrationService(informaticaRepository, container.getInformaticaIntegrationService());

		useExecutionService(informaticaRepository, integrationService, informaticaWebServicesHub, visitor);
	}

	protected void useRepository(BaseRepositoryRequestDTO container, RepositoryVisitor visitor) throws Exception {
		String domain = container.getInformaticaDomain();

		InformaticaDomain informaticaDomain = getInformaticaDomain(domain);

		String repo = container.getInformaticaRepository();

		InformaticaRepository informaticaRepository = getInformaticaRepository(informaticaDomain, repo);
		InformaticaWebServicesHub informaticaWebServicesHub = getInformaticaWebServicesHub(informaticaRepository, container.getInformaticaWebServicesHub());

		useRepository(informaticaRepository, visitor);
	}

	protected InformaticaIntegrationService getInformaticaIntegrationService(InformaticaRepository repository, String repo) {
		InformaticaIntegrationService informaticaWebServicesHub = null;

		if (repo == null)
		{
			informaticaWebServicesHub = repository.getDefaultIntegrationService();
		}
		else
		{
			informaticaWebServicesHub = repository.getIntegrationService(repo);
		}

		if (informaticaWebServicesHub == null)
		{
			throw new IllegalArgumentException("Repository '" + repository.getQualifiedName() + "' does not contain integration service '" + repo + "'");
		}

		return informaticaWebServicesHub;
	}

	protected InformaticaWebServicesHub getInformaticaWebServicesHub(InformaticaRepository repository, String repo) {
		InformaticaWebServicesHub informaticaWebServicesHub = null;

		if (repo == null)
		{
			informaticaWebServicesHub = repository.getDefaultWebServicesHub();
		}
		else
		{
			informaticaWebServicesHub = repository.getWebServicesHub(repo);

			if (informaticaWebServicesHub == null)
			{
				throw new IllegalArgumentException("Repository '" + repository.getQualifiedName() + "' does not contain web services hub '" + repo + "'");
			}
		}

		return informaticaWebServicesHub;
	}

	protected InformaticaDomain getInformaticaDomain(String domain) {
		// resolve to an informatica domain
		InformaticaDomain informaticaDomain2 = informaticaConfiguration.getDefaultDomain();

		if (domain != null) {
			logger.info("Resolving requested domain {}", domain);

			informaticaDomain2 = informaticaConfiguration.getDomain(domain);

			if (informaticaDomain2 == null) {
				logger.error("Requested domain does not exist {}", domain);
				throw new IllegalArgumentException("Domain '" + domain + "' not found");
			}
		} else {
			logger.info("No domain specified - using the default domain {}", informaticaDomain2);
		}
		return informaticaDomain2;
	}

	protected InformaticaRepository getInformaticaRepository(InformaticaDomain informaticaDomain, String repo)
	{
		InformaticaRepository informaticaRepository = null;

		if (repo == null)
		{
			informaticaRepository = informaticaDomain.getDefaultRepository();
		}
		else
		{
			informaticaRepository = informaticaDomain.getRepository(repo);
		}

		if (informaticaRepository == null)
		{
			throw new IllegalArgumentException("Domain '" + informaticaDomain.getDomainName() + "' does not contain repository '" + repo + "'");
		}

		return informaticaRepository;
	}

	private static class InformaticaExecutionService
	{
		private final InformaticaIntegrationService informaticaIntegrationService;
		private final InformaticaWebServicesHub informaticaWebServicesHub;

		private InformaticaExecutionService(InformaticaIntegrationService informaticaIntegrationService, InformaticaWebServicesHub informaticaWebServicesHub) {
			this.informaticaIntegrationService = informaticaIntegrationService;
			this.informaticaWebServicesHub = informaticaWebServicesHub;
		}
	}

	private static final LoadingCache<InformaticaRepository, ObjectPool<InformaticaRepositoryClient>> repositoryClientCaches = CacheBuilder.newBuilder()
			.expireAfterAccess(60, TimeUnit.MINUTES)
			.removalListener(new RemovalListener<InformaticaRepository, ObjectPool<InformaticaRepositoryClient>>() {
				@Override
				public void onRemoval(RemovalNotification<InformaticaRepository, ObjectPool<InformaticaRepositoryClient>> objectObjectRemovalNotification) {
					if (objectObjectRemovalNotification.wasEvicted())
					{
						cacheLog("Disposing Repository service pool for " + objectObjectRemovalNotification.getKey() + " with id " + System.identityHashCode(objectObjectRemovalNotification.getValue()));
						try {
							objectObjectRemovalNotification.getValue().clear();
						} catch (Exception e) {
							cacheLog("Pool could not be disposed " + e.toString());
						}
					}
					else
					{
						cacheLog("Reusing Repository service pool id " + System.identityHashCode(objectObjectRemovalNotification.getValue()));
					}
				}
			})
			.build(
					new CacheLoader<InformaticaRepository, ObjectPool<InformaticaRepositoryClient>>() {
						public ObjectPool<InformaticaRepositoryClient> load(final InformaticaRepository key) {
							GenericObjectPool<InformaticaRepositoryClient> informaticaRepositoryClientGenericObjectPool = new GenericObjectPool<InformaticaRepositoryClient>(new PoolableObjectFactory<InformaticaRepositoryClient>() {
								@Override
								public InformaticaRepositoryClient makeObject() throws Exception {
									cacheLog("Allocating repository service client for " + key);
									InformaticaRepositoryClient informaticaRepositoryClient = null;

									try {
										informaticaRepositoryClient = key.newInformaticaRepositoryClient();
									} finally {
										if (informaticaRepositoryClient != null)
										{
											cacheLog("Allocated repository service client for " + key + " with id " + System.identityHashCode(informaticaRepositoryClient) + " of type {" + informaticaRepositoryClient.getClass() + "}");
										}
										else
										{
											cacheLog("Allocated null repository service client for " + key);
										}
									}

									return informaticaRepositoryClient;
								}

								@Override
								public void destroyObject(final InformaticaRepositoryClient informaticaRepositoryClient) throws Exception {
									new Thread(new Runnable() {
										@Override
										public void run() {
											cacheLog("Disposing client [" + System.identityHashCode(informaticaRepositoryClient) + "]");
											informaticaRepositoryClient.dispose();
											//cacheLog("Disposed client [" + System.identityHashCode(informaticaRepositoryClient) + "]");
										}
									}).start();
								}

								@Override
								public boolean validateObject(InformaticaRepositoryClient informaticaRepositoryClient) {
									cacheLog("Validating client.");
									boolean ping = informaticaRepositoryClient.ping();

									if (ping)
									{
										cacheLog("Client valid.");
									}
									else
									{
										cacheLog("Client expired.");
									}

									return ping;
								}

								@Override
								public void activateObject(InformaticaRepositoryClient informaticaRepositoryClient) throws Exception {
								}

								@Override
								public void passivateObject(InformaticaRepositoryClient informaticaRepositoryClient) throws Exception {
								}
							});

							informaticaRepositoryClientGenericObjectPool.setMaxActive(10);
							informaticaRepositoryClientGenericObjectPool.setTestOnBorrow(true);
							informaticaRepositoryClientGenericObjectPool.setTestOnReturn(true);
							informaticaRepositoryClientGenericObjectPool.setMaxIdle(10);
							informaticaRepositoryClientGenericObjectPool.setTimeBetweenEvictionRunsMillis(60000L);
							informaticaRepositoryClientGenericObjectPool.setWhenExhaustedAction(GenericObjectPool.WHEN_EXHAUSTED_GROW);
							informaticaRepositoryClientGenericObjectPool.setTestWhileIdle(true);
							informaticaRepositoryClientGenericObjectPool.setMinEvictableIdleTimeMillis(600000L);

							return informaticaRepositoryClientGenericObjectPool;
						}
					});

	private static void cacheLog(String println) {
		cacheLogger.debug("[CACHE][thread:" + Thread.currentThread().hashCode() + "] " + println);
	}

	protected static InformaticaRepositoryClient newRepositoryClient(AbstractHandler handler, InformaticaRepository repository) throws Exception {
		if (handler.testInformaticaRepositoryClient != null)
		{
			handler.runtimeSupport.getApplicationLog().info("Using a test repository client");
			return handler.testInformaticaRepositoryClient;
		}

		cacheLog("Handler waiting for client");

		InformaticaRepositoryClient informaticaRepositoryClient = null;

		try
		{
			cacheLog("Handler receiving client [" + Thread.currentThread().hashCode() + "]");
			synchronized(repositoryClientCaches)
			{
				informaticaRepositoryClient = repositoryClientCaches.get(repository).borrowObject();
			}
		}
		finally
		{
			cacheLog(
				"Handler received client [" +
				Thread.currentThread().hashCode() +
				"]:[" +
				System.identityHashCode(informaticaRepositoryClient) +
				"]" +
				"{" + informaticaRepositoryClient.getClass() + "}"
			);
		}

		return informaticaRepositoryClient;
	}

	private static final LoadingCache<InformaticaExecutionService, ObjectPool<InformaticaExecutionClient>> integrationServiceClientCache = CacheBuilder.newBuilder()
			.expireAfterAccess(60, TimeUnit.MINUTES)
			.removalListener(new RemovalListener<InformaticaExecutionService, ObjectPool<InformaticaExecutionClient>>() {
				@Override
				public void onRemoval(RemovalNotification<InformaticaExecutionService, ObjectPool<InformaticaExecutionClient>> objectObjectRemovalNotification) {
					if (objectObjectRemovalNotification.wasEvicted())
					{
						cacheLog("Disposing Integration service pool for " + objectObjectRemovalNotification.getKey() + " with id " + System.identityHashCode(objectObjectRemovalNotification.getValue()));
						try {
							objectObjectRemovalNotification.getValue().clear();
						} catch (Exception e) {
							cacheLog("Pool could not be disposed " + e.toString());
						}
					}
					else
					{
						cacheLog("Reusing Integration service pool id " + System.identityHashCode(objectObjectRemovalNotification.getValue()));
					}
				}
			})
			.build(
					new CacheLoader<InformaticaExecutionService, ObjectPool<InformaticaExecutionClient>>() {
						public ObjectPool<InformaticaExecutionClient> load(final InformaticaExecutionService key) {
							return new GenericObjectPool<InformaticaExecutionClient>(new PoolableObjectFactory<InformaticaExecutionClient>() {
								@Override
								public InformaticaExecutionClient makeObject() throws Exception {
									InformaticaExecutionClient informaticaIntegrationServiceClient = null;

									// check for a web services hub, and use it if present
									if (key.informaticaWebServicesHub != null)
									{
										cacheLog("Allocating Web services hub client for " + key.informaticaIntegrationService);
										informaticaIntegrationServiceClient = key.informaticaWebServicesHub.newInformaticaWebServicesHubClient(key.informaticaIntegrationService);
										cacheLog("Allocated Web services hub client for " + key.informaticaIntegrationService + " with id " + System.identityHashCode(informaticaIntegrationServiceClient));
									}
									else
									{
										cacheLog("Allocating Integration service client for " + key.informaticaIntegrationService);
										informaticaIntegrationServiceClient = key.informaticaIntegrationService.newInformaticaIntegrationServiceClient();
										cacheLog("Allocated Integration service client for " + key.informaticaIntegrationService + " with id " + System.identityHashCode(informaticaIntegrationServiceClient));
									}

									return informaticaIntegrationServiceClient;
								}

								@Override
								public void destroyObject(InformaticaExecutionClient obj) throws Exception {
									//obj.dispose();
								}

								@Override
								public boolean validateObject(InformaticaExecutionClient obj) {
									return true;
								}

								@Override
								public void activateObject(InformaticaExecutionClient obj) throws Exception {
								}

								@Override
								public void passivateObject(InformaticaExecutionClient obj) throws Exception {
								}
							});
						}
					});

	protected static InformaticaExecutionClient newIntegrationClient(AbstractHandler handler, InformaticaExecutionService service) throws Exception {
		if (handler.testInformaticaExecutionClient != null)
		{
			handler.runtimeSupport.getApplicationLog().info("Using a test repository client");
			return handler.testInformaticaExecutionClient;
		}

		synchronized(integrationServiceClientCache)
		{
			return integrationServiceClientCache.get(service).borrowObject();
		}
	}

	static
	{
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				// clean up both caches
				while (true)
				{
					repositoryClientCaches.cleanUp();
					integrationServiceClientCache.cleanUp();

					try {
						Thread.sleep(60000L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});

		thread.setDaemon(true);

		thread.start();
	}

	public GsonBuilder configureBuilder(GsonBuilder builder) {
		return builder;
	}

	protected PrintWriterLog log = new PrintWriterLog();

	private FeatureOperation featureOperation;
	protected InformaticaConfiguration informaticaConfiguration;

	@Override
	public void setInformaticaConfiguration(InformaticaConfiguration informaticaConfiguration) {
		this.informaticaConfiguration = informaticaConfiguration;
	}

	@Override
	public void receiveRuntimeSupport(RuntimeSupport runtimeSupport) {
		this.runtimeSupport = runtimeSupport;
	}

	@Override
	public final void setFeatureOperation(FeatureOperation featureOperation) {
		this.featureOperation = featureOperation;
	}

	public final JsonNode getValidator() {
		return featureOperation.getSignatures().get(0).getValidatorNode();
	}

	public interface RepositoryVisitor
	{
		public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception;
	}

	protected void useRepository(InformaticaRepository repository, RepositoryVisitor visitor) throws Exception {

		InformaticaRepositoryClient informaticaRepositoryClient = newRepositoryClient(this, repository);

		try
		{
			visitor.useClient(informaticaRepositoryClient, runtimeSupport);
		} finally
		{
			cacheLog("Returning Repository service client id " + System.identityHashCode(informaticaRepositoryClient));
			synchronized(repositoryClientCaches)
			{
				repositoryClientCaches.get(repository).returnObject(informaticaRepositoryClient);
			}
		}
	}

	public interface IntegrationServiceVisitor
	{
		public void useClient(
			InformaticaExecutionClient client,
			InformaticaDomain domain,
			InformaticaRepository repository,
			InformaticaIntegrationService service,
			InformaticaWebServicesHub hub,
			RuntimeSupport runtimeSupport
		) throws Exception;
	}

	protected void useExecutionService(InformaticaRepository repository, InformaticaIntegrationService intsvc, InformaticaWebServicesHub informaticaWebServicesHub, IntegrationServiceVisitor visitor) throws Exception {
		InformaticaExecutionService informaticaExecutionService = new InformaticaExecutionService(intsvc, informaticaWebServicesHub);
		InformaticaExecutionClient informaticaRepositoryClient = newIntegrationClient(this, informaticaExecutionService);

		try
		{
			visitor.useClient(informaticaRepositoryClient, repository.getInformaticaDomain(), repository, intsvc, informaticaWebServicesHub, runtimeSupport);
		} finally
		{
			cacheLog("Returning Integration service client id " + System.identityHashCode(informaticaRepositoryClient));
			synchronized(integrationServiceClientCache)
			{
				integrationServiceClientCache.get(informaticaExecutionService).returnObject(informaticaRepositoryClient);
			}
		}
	}
}