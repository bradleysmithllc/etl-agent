package org.bitbucket.bradleysmithllc.etlagent.informatica.dto;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.ConnectionDetails;

/**
 * Make this class handle both request types.
 */
public class CreateConnectionRequestDTO extends BaseRepositoryRequestDTO
{
	private ConnectionDetails details;

	private String relationalConnectionName;
	private String databaseName;
	private String serverName;
	private String databaseUserName;
	private String databasePassword;

	public CreateConnectionRequestDTO() {
	}

	public CreateConnectionRequestDTO(
			ConnectionDetails details
	) {
		this.details = details;
	}

	@Override
	public String toString() {
		return "CreateConnectionRequestDTO{" +
				"details=" + details +
				", relationalConnectionName='" + relationalConnectionName + '\'' +
				", databaseName='" + databaseName + '\'' +
				", serverName='" + serverName + '\'' +
				", databaseUserName='" + databaseUserName + '\'' +
				", databasePassword='" + databasePassword + '\'' +
				'}';
	}

	public ConnectionDetails getDetails() {
		return details;
	}

	/* These properties are for compatibility */
	public String getRelationalConnectionName() {
		return relationalConnectionName;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public String getServerName() {
		return serverName;
	}

	public String getDatabaseUserName() {
		return databaseUserName;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}
}