package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExecuteWorkflowRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ExecuteWorkflowResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecuteWorkflow extends AbstractRequestHandler<ExecuteWorkflowRequestDTO, GenericResponseDTO> {
	Map<String, String> velocityContext = new HashMap<String, String>();

	@Override
	public String getId() {
		return "executeWorkflow";
	}

	@Override
	public ExecuteWorkflowRequestDTO getRequestContainerObject() {
		return new ExecuteWorkflowRequestDTO("", "");
	}

	@Override
	public GenericResponseDTO process(JsonNode request, final ExecuteWorkflowRequestDTO container) throws Exception {
		logger.info("Executing workflow {}", container);

		// deserialize the results from the json container into the files
		Map<RemoteFile.fileType, List<RemoteFile>> rfm = container.getRequestFiles();

		for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> fEntry : rfm.entrySet())
		{
			for (RemoteFile rf : fEntry.getValue())
			{
				File tf = runtimeSupport.createAnonymousTempFile();
				FileUtils.write(tf, rf.getFileContents());
				rf.setFile(tf);
			}
		}

		final ExecuteWorkflowResponseDTO executeWorkflowResponseDTO = new ExecuteWorkflowResponseDTO();

		useExecutionService(container, new IntegrationServiceVisitor() {
			@Override
			public void useClient(InformaticaExecutionClient client, InformaticaDomain domain, InformaticaRepository repository, InformaticaIntegrationService service, InformaticaWebServicesHub hub, RuntimeSupport runtimeSupport) throws Exception {
				// prepare the workspace
				prepareWorkspace(domain, container);

				String tag = container.getFolder() + "." + container.getWorkflowName() + (container.getTask() != null ? ("." + container.getTask()) : "") + (container.getRunInstanceName() != null ? ("." + container.getRunInstanceName()) : "");

				ParameterFile pfile = container.getParameterFile();

				File parmFile = runtimeSupport.createAnonymousTempFileWithPrefix(tag + ".PRM.");

				try {
					PrintWriter fw = new PrintWriter(new BufferedWriter(new FileWriter(parmFile)));

					try {
						pfile.write(fw);
					} finally {
						fw.close();
					}

					// rework velocity references
					String parmContents = FileUtils.readFileToString(parmFile);

					parmContents = parmContents.replaceAll("#[\\s]*\\{", "\\$\\{");

					FileUtils.write(parmFile, VelocityUtil.writeTemplate(parmContents, velocityContext));
					// reload to get the updated values
					pfile = new ParameterFile(parmFile);
				} finally {
					parmFile.delete();
				}

				InformaticaExecutionResult execResult;

				if (container.getTask() != null) {
					if (container.getRunInstanceName() != null) {
						execResult = client.executeWorkflowTask(
								container.getWorkflowName(),
								container.getRunInstanceName(),
								container.getTask(),
								container.getFolder(),
								pfile,
								container.getRequestFiles()
						);
					} else {
						execResult = client.executeWorkflowTask(
								container.getWorkflowName(),
								container.getTask(),
								container.getFolder(),
								pfile,
								container.getRequestFiles()
						);
					}
				} else {
					if (container.getRunInstanceName() != null) {
						execResult = client.executeWorkflow(
								container.getWorkflowName(),
								container.getRunInstanceName(),
								container.getFolder(),
								pfile,
								container.getRequestFiles()
						);
					} else {
						execResult = client.executeWorkflow(
								container.getWorkflowName(),
								container.getFolder(),
								pfile,
								container.getRequestFiles()
						);
					}
				}

				executeWorkflowResponseDTO.getResponseFiles().putAll(execResult.getWorkSpaceFiles());

				if (execResult.getFailureExc() != null)
				{
					executeWorkflowResponseDTO.setResponse(GenericResponseDTO.response_code.failed);
					executeWorkflowResponseDTO.setResponseMessage(execResult.getFailureExc());
				}

				// serialize the results to the json container from the files
				Map<RemoteFile.fileType, List<RemoteFile>> rfmres = executeWorkflowResponseDTO.getResponseFiles();

				for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> fEntry : rfmres.entrySet())
				{
					for (RemoteFile rf : fEntry.getValue())
					{
						rf.setFileContents(FileUtils.readFileToString(rf.getFile()));
					}
				}

				// wipe out the temporary workspace
				FileUtils.forceDelete(domain.getLocalizedWorkingRoot());
			}
		});

		return executeWorkflowResponseDTO;
	}

	private void prepareWorkspace(InformaticaDomain domain, ExecuteWorkflowRequestDTO container) throws IOException {
		velocityContext.clear();

		RemoteFile.copyRemoteToLocal(container.getRequestFiles(), domain);

		for (RemoteFile.fileType type : RemoteFile.fileType.values()) {
			File targetFiles = domain.getWorkspaceDir(type);
			velocityContext.put(type.name(), targetFiles.getAbsolutePath());
		}
	}
}