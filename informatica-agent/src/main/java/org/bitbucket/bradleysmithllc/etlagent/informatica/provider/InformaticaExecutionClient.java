package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;

import java.util.List;
import java.util.Map;

public interface InformaticaExecutionClient
{
	InformaticaExecutionResult executeWorkflow(
			String workFlow,
			String folder,
			ParameterFile parmFile
	) throws Exception;

	InformaticaExecutionResult executeWorkflow(
			String workFlow,
			String folder,
			ParameterFile parmFile,
			Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles
	) throws Exception;

	InformaticaExecutionResult executeWorkflow(
			String workFlow,
			String runInstanceName,
			String folder,
			ParameterFile parmFile
	) throws Exception;

	InformaticaExecutionResult executeWorkflow(
			String workFlow,
			String runInstanceName,
			String folder,
			ParameterFile parmFile,
			Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles
	) throws Exception;

	InformaticaExecutionResult executeWorkflowTask(
			String workFlow,
			String task,
			String folder,
			ParameterFile parmFile
	) throws Exception;

	InformaticaExecutionResult executeWorkflowTask(
			String workFlow,
			String task,
			String folder,
			ParameterFile parmFile,
			Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles
	) throws Exception;

	InformaticaExecutionResult executeWorkflowTask(
			String workFlow,
			String runInstanceName,
			String task,
			String folder,
			ParameterFile parmFile
	) throws Exception;

	InformaticaExecutionResult executeWorkflowTask(
			String workFlow,
			String runInstanceName,
			String task,
			String folder,
			ParameterFile parmFile,
			Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles
	) throws Exception;

	boolean validateWorkflowExists(
			String folder,
			String workflowName
	) throws Exception;
}