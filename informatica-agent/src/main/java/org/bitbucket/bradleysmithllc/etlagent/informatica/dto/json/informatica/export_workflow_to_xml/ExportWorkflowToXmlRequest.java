
package org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.export_workflow_to_xml;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.Domains;

import javax.annotation.Generated;
import javax.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "default-domain",
    "domains",
    "folder",
    "informatica-domain",
    "informatica-repository",
    "workflow-name"
})
public class ExportWorkflowToXmlRequest {

    @JsonProperty("default-domain")
    private String defaultDomain;
    @JsonProperty("domains")
    @Valid
    private Domains domains;
    @JsonProperty("folder")
    private String folder;
    @JsonProperty("informatica-domain")
    private String informaticaDomain;
    @JsonProperty("informatica-repository")
    private String informaticaRepository;
    @JsonProperty("workflow-name")
    private String workflowName;

    @JsonProperty("default-domain")
    public String getDefaultDomain() {
        return defaultDomain;
    }

    @JsonProperty("default-domain")
    public void setDefaultDomain(String defaultDomain) {
        this.defaultDomain = defaultDomain;
    }

    @JsonProperty("domains")
    public Domains getDomains() {
        return domains;
    }

    @JsonProperty("domains")
    public void setDomains(Domains domains) {
        this.domains = domains;
    }

    @JsonProperty("folder")
    public String getFolder() {
        return folder;
    }

    @JsonProperty("folder")
    public void setFolder(String folder) {
        this.folder = folder;
    }

    @JsonProperty("informatica-domain")
    public String getInformaticaDomain() {
        return informaticaDomain;
    }

    @JsonProperty("informatica-domain")
    public void setInformaticaDomain(String informaticaDomain) {
        this.informaticaDomain = informaticaDomain;
    }

    @JsonProperty("informatica-repository")
    public String getInformaticaRepository() {
        return informaticaRepository;
    }

    @JsonProperty("informatica-repository")
    public void setInformaticaRepository(String informaticaRepository) {
        this.informaticaRepository = informaticaRepository;
    }

    @JsonProperty("workflow-name")
    public String getWorkflowName() {
        return workflowName;
    }

    @JsonProperty("workflow-name")
    public void setWorkflowName(String workflowName) {
        this.workflowName = workflowName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
