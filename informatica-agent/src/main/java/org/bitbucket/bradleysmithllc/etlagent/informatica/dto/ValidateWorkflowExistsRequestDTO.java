package org.bitbucket.bradleysmithllc.etlagent.informatica.dto;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class ValidateWorkflowExistsRequestDTO extends BaseIntegrationServiceRequestDTO
{
	private String folder;
	private String workflow;

	public ValidateWorkflowExistsRequestDTO() {
	}

	public ValidateWorkflowExistsRequestDTO(String folder, String wkf) {
		this.folder = folder;
		workflow = wkf;
	}

	public String getWorkflow() {
		return workflow;
	}

	public void setWorkflow(String workflow) {
		this.workflow = workflow;
	}

	public void setFolder(String folder) {
		this.folder = folder;
	}

	public String getFolder() {
		return folder;
	}

	@Override
	public String toString() {
		return "VerifyWorkflowExistsRequestDTO{" +
				"folder='" + folder + '\'' +
				", workflow='" + workflow + '\'' +
				'}';
	}
}