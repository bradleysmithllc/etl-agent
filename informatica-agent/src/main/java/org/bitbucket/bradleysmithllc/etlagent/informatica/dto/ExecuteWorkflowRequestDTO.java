package org.bitbucket.bradleysmithllc.etlagent.informatica.dto;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.execute.Parameter;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecuteWorkflowRequestDTO extends BaseIntegrationServiceRequestDTO
{
	private final String folder;
	private final String task;
	private final String runInstanceName;
	private String sessionId;
	private String requestUid;

	private final String workflowName;
	private final List<Parameter> parameterList = new ArrayList<Parameter>();

	private final Map<RemoteFile.fileType, List<RemoteFile>> requestFiles = new HashMap<RemoteFile.fileType, List<RemoteFile>>();

	public ExecuteWorkflowRequestDTO(String folder, String workflowName) {
		this(folder, workflowName, null, null, null);
	}

	public ExecuteWorkflowRequestDTO(String folder, String workflowName, ParameterFile parameterFile) {
		this(folder, workflowName, null, null, parameterFile);
	}

	public ExecuteWorkflowRequestDTO(String folder, String workflowName, String task) {
		this(folder, workflowName, task, null, null);
	}

	public ExecuteWorkflowRequestDTO(String folder, String workflowName, String task, ParameterFile parameterFile) {
		this(folder, workflowName, task, null, parameterFile);
	}

	public ExecuteWorkflowRequestDTO(String folder, String workflowName, String task, String runInstance) {
		this(folder, workflowName, task, runInstance, null);
	}

	public ExecuteWorkflowRequestDTO(String folder, String workflowName, String task, String runInstance, ParameterFile parameterFile) {
		this.folder = folder;
		this.workflowName = workflowName;
		runInstanceName = runInstance;
		this.task = task;

		if (parameterFile != null)
		{
			for (ScopeSection section : parameterFile.getSections())
			{
				for (org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.Parameter parameter : section.getParameters())
				{
					Parameter p = new Parameter();

					p.setScope(section.getHeader());

					if (parameter.getMappletName() != null)
					{
						p.setMappletName(parameter.getMappletName());
					}

					p.setParameterName(parameter.getParameterName());
					p.setParameterValue(parameter.getParameterValue());

					parameterList.add(p);
				}
			}
		}
	}

	public void addRequestFile(RemoteFile file)
	{
		for (RemoteFile remoteFile : getOrCreate(file.getType()))
		{
			if (remoteFile.sameId(file))
			{
				throw new IllegalArgumentException("Remote file name and fileType already added");
			}
		}

		requestFiles.get(file.getType()).add(file);
	}

	private List<RemoteFile> getOrCreate(RemoteFile.fileType type) {
		List<RemoteFile> list = requestFiles.get(type);

		if (list == null)
		{
			list = new ArrayList<RemoteFile>();

			requestFiles.put(type, list);
		}

		return list;
	}

	public String getTask() {
		return task;
	}

	public String getRunInstanceName() {
		return runInstanceName;
	}

	public String getFolder() {
		return folder;

	}

	public Map<RemoteFile.fileType, List<RemoteFile>> getRequestFiles() {
		return requestFiles;
	}

	public ParameterFile getParameterFile() {
		ParameterFile parameterFile = new ParameterFile();

		for (Parameter parameter : parameterList)
		{
			ScopeSection section = parameterFile.getSection(parameter.getScope());

			if (section == null)
			{
				section = parameterFile.appendSection(parameter.getScope());
			}

			if (parameter.getMappletName() != null)
			{
				section.appendParameter(parameter.getMappletName(), parameter.getParameterName(), parameter.getParameterValue());
			}
			else
			{
				section.appendParameter(parameter.getParameterName(), parameter.getParameterValue());
			}
		}

		return parameterFile;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public String getRequestUid() {
		return requestUid;
	}

	public String getSessionId() {
		return sessionId;
	}

	@Override
	public String toString() {
		return "ExecuteWorkflowRequestDTO{" +
				"folder='" + folder + '\'' +
				", task='" + task + '\'' +
				", runInstanceName='" + runInstanceName + '\'' +
				", sessionId='" + sessionId + '\'' +
				", requestUid='" + requestUid + '\'' +
				", workflowName='" + workflowName + '\'' +
				'}';
	}
}