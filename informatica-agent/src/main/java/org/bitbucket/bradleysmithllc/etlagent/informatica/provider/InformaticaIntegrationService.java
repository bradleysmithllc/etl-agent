package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.client.AgentClient;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

public class InformaticaIntegrationService {
	private final String name;
	private final InformaticaDomain informaticaDomain;
	private final InformaticaRepository informaticaRepository;
	private final RuntimeSupport runtimeSupport;

	private int numRequestsSent = 0;

	private InformaticaIntegrationServiceClient informaticaIntegrationServiceClient;

	public InformaticaIntegrationService(String name, InformaticaRepository repository, RuntimeSupport runtimeSupport) {
		this.name = name;
		informaticaRepository = repository;
		informaticaDomain = informaticaRepository.getInformaticaDomain();
		this.runtimeSupport = runtimeSupport;

		informaticaIntegrationServiceClient = newInformaticaIntegrationServiceClient();
	}

	public String getIntegrationServiceName() {
		return name;
	}

	public InformaticaDomain getInformaticaDomain() {
		return informaticaDomain;
	}

	public InformaticaRepository getInformaticaRepository() {
		return informaticaRepository;
	}

	public InformaticaIntegrationServiceClient getInformaticaIntegrationServiceClient() {
		return informaticaIntegrationServiceClient;
	}

	public InformaticaIntegrationServiceClient newInformaticaIntegrationServiceClient() {
		switch (informaticaRepository.getInformaticaDomain().getClientType()) {
			case AGENT:
				return new AgentClient(this, runtimeSupport, runtimeSupport.getProjectUID());
			case LOCAL_OUT_OF_PROCESS:
			case LOCAL:
				return new InformaticaRepositoryClientImpl(informaticaRepository, this, informaticaDomain.getInformaticaBinDir(), runtimeSupport, runtimeSupport.getApplicationLog());
			case MOCK:
				return ClientProxyFactory.getInformaticaIntegrationServiceClientProxy(this, runtimeSupport);
		}

		throw new Error();
	}

	void request()
	{
		numRequestsSent++;
		informaticaDomain.request();
		informaticaRepository.request();
	}

	public String getQualifiedName()
	{
		return informaticaRepository.getQualifiedName() + "." + getIntegrationServiceName();
	}

	public int getNumRequestsSent() {
		return numRequestsSent;
	}

	@Override
	public String toString() {
		return "InformaticaIntegrationService{" +
				"name='" + name + '\'' +
				", informaticaDomain=" + informaticaDomain +
				", informaticaRepository=" + informaticaRepository +
				'}';
	}
}
