
package org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;
import javax.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "domain-defaults",
    "default-domain",
    "domains"
})
public class InformaticaFeatureModuleConfiguration {

    @JsonProperty("domain-defaults")
    @Valid
    private DomainDefaults domainDefaults;
    @JsonProperty("default-domain")
    private String defaultDomain;
    @JsonProperty("domains")
    @Valid
    private Domains domains;

    @JsonProperty("domain-defaults")
    public DomainDefaults getDomainDefaults() {
        return domainDefaults;
    }

    @JsonProperty("domain-defaults")
    public void setDomainDefaults(DomainDefaults domainDefaults) {
        this.domainDefaults = domainDefaults;
    }

    @JsonProperty("default-domain")
    public String getDefaultDomain() {
        return defaultDomain;
    }

    @JsonProperty("default-domain")
    public void setDefaultDomain(String defaultDomain) {
        this.defaultDomain = defaultDomain;
    }

    @JsonProperty("domains")
    public Domains getDomains() {
        return domains;
    }

    @JsonProperty("domains")
    public void setDomains(Domains domains) {
        this.domains = domains;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
