package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.CreateConnectionRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaAgentConstants;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDeadlockException;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepositoryClient;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

public class CreateInformaticaConnection extends AbstractRequestHandler<CreateConnectionRequestDTO, InformaticaResponseDTO>
{
	@Override
	public String getId() {
		return "createInformaticaConnection";
	}

	@Override
	public CreateConnectionRequestDTO getRequestContainerObject() {
		return new CreateConnectionRequestDTO();
	}

	@Override
	public InformaticaResponseDTO process(JsonNode request, final CreateConnectionRequestDTO container) throws Exception {
		logger.info("Creating connection {}", container);

		InformaticaResponseDTO dto = new InformaticaResponseDTO();
		dto.setInformaticaResultCode(InformaticaResponseDTO.result.okay);

		try
		{
			useRepository(container, new RepositoryVisitor() {
				@Override
				public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception {
					if (container.getDetails() != null)
					{
						client.createConnection(
								container.getDetails()
						);
					}
					else
					{
						// legacy API
						client.createConnection(
							container.getServerName(),
							container.getDatabaseName(),
							container.getDatabaseUserName(),
							container.getDatabasePassword(),
							container.getRelationalConnectionName()
						);
					}
				}
			});
		}
		catch(InformaticaDeadlockException exc)
		{
			logger.error("Request produced a deadlock.  Passing on to the client.");

			// special case - let's return a retry error
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.deadlock);
			dto.setResponseMessage(exc.getMessage());
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_REPOSITORY_DEADLOCK);

			return dto;
		}
		catch(InformaticaError exc)
		{
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.failed);
			dto.setResponseMessage(exc.getMessage());
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_ERROR);
		}

		return dto;
	}
}