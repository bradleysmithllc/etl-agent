package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class ConnectionDetails
{
	private String relationalConnectionName;
	private SqlServerConnectionDetails sqlServerConnectionDetails;
	private OracleConnectionDetails oracleConnectionDetails;

	public OracleConnectionDetails getOracleConnectionDetails() {
		return oracleConnectionDetails;
	}

	public SqlServerConnectionDetails getSqlServerConnectionDetails() {
		return sqlServerConnectionDetails;
	}

	public ConnectionDetails(String relationalConnectionName, SqlServerConnectionDetails sql) {
		this.relationalConnectionName = relationalConnectionName;
		sqlServerConnectionDetails = sql;
	}

	public ConnectionDetails(String relationalConnectionName, OracleConnectionDetails sql) {
		this.relationalConnectionName = relationalConnectionName;
		oracleConnectionDetails = sql;
	}

	enum type
	{
		sqlserver,
		oracle
	}

	public type getRelationalConnectionType()
	{
		return sqlServerConnectionDetails != null ? type.sqlserver : type.oracle;
	}

	public String getRelationalConnectionName()
	{
		return relationalConnectionName;
	}

	@Override
	public String toString() {
		return "ConnectionDetails{" +
				"relationalConnectionName='" + relationalConnectionName + '\'' +
				", sqlServerConnectionDetails=" + sqlServerConnectionDetails +
				", oracleConnectionDetails=" + oracleConnectionDetails +
				'}';
	}
}