
package org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;
import javax.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "client-type",
    "agent",
    "informatica-bin-directory",
    "security-domain",
    "working-root",
    "username",
    "password-encrypted",
		"password",
    "client-version",
    "connectivity-host",
    "connectivity-port"
})
public class DomainDefaults {

    @JsonProperty("client-type")
    private DomainsProperty.ClientType clientType;
    @JsonProperty("agent")
    @Valid
    private Agent agent;
    @JsonProperty("informatica-bin-directory")
    private String informaticaBinDirectory;
    @JsonProperty("security-domain")
    private String securityDomain;
    @JsonProperty("working-root")
    private String workingRoot;
    @JsonProperty("username")
    private String username;
    @JsonProperty("password-encrypted")
    private String passwordEncrypted;
		@JsonProperty("password")
		private String password;
    @JsonProperty("client-version")
    private String clientVersion;
    @JsonProperty("connectivity-host")
    private String connectivityHost;
    @JsonProperty("connectivity-port")
    private Long connectivityPort;

    @JsonProperty("client-type")
    public DomainsProperty.ClientType getClientType() {
        return clientType;
    }

    @JsonProperty("client-type")
    public void setClientType(DomainsProperty.ClientType clientType) {
        this.clientType = clientType;
    }

    @JsonProperty("agent")
    public Agent getAgent() {
        return agent;
    }

    @JsonProperty("agent")
    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    @JsonProperty("informatica-bin-directory")
    public String getInformaticaBinDirectory() {
        return informaticaBinDirectory;
    }

    @JsonProperty("informatica-bin-directory")
    public void setInformaticaBinDirectory(String informaticaBinDirectory) {
        this.informaticaBinDirectory = informaticaBinDirectory;
    }

    @JsonProperty("security-domain")
    public String getSecurityDomain() {
        return securityDomain;
    }

    @JsonProperty("security-domain")
    public void setSecurityDomain(String securityDomain) {
        this.securityDomain = securityDomain;
    }

    @JsonProperty("working-root")
    public String getWorkingRoot() {
        return workingRoot;
    }

    @JsonProperty("working-root")
    public void setWorkingRoot(String workingRoot) {
        this.workingRoot = workingRoot;
    }

    @JsonProperty("username")
    public String getUsername() {
        return username;
    }

    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    @JsonProperty("password-encrypted")
    public String getPasswordEncrypted() {
        return passwordEncrypted;
    }

    @JsonProperty("password-encrypted")
    public void setPasswordEncrypted(String passwordEncrypted) {
        this.passwordEncrypted = passwordEncrypted;
    }

		@JsonProperty("password")
		public String getPassword() {
			return password;
		}

		@JsonProperty("password")
		public void setPassword(String passwordEncrypted) {
			this.password = passwordEncrypted;
		}

    @JsonProperty("client-version")
    public String getClientVersion() {
        return clientVersion;
    }

    @JsonProperty("client-version")
    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    @JsonProperty("connectivity-host")
    public String getConnectivityHost() {
        return connectivityHost;
    }

    @JsonProperty("connectivity-host")
    public void setConnectivityHost(String connectivityHost) {
        this.connectivityHost = connectivityHost;
    }

    @JsonProperty("connectivity-port")
    public Long getConnectivityPort() {
        return connectivityPort;
    }

    @JsonProperty("connectivity-port")
    public void setConnectivityPort(Long connectivityPort) {
        this.connectivityPort = connectivityPort;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }
}
