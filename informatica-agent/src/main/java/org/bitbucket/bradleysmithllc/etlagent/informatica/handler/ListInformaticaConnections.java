package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ListConnectionsRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ListConnectionsResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ListFoldersRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ListFoldersResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepositoryClient;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.util.List;

public class ListInformaticaConnections extends AbstractRequestHandler<ListConnectionsRequestDTO, ListConnectionsResponseDTO>
{
	@Override
	public String getId() {
		return "listInformaticaConnections";
	}

	@Override
	public ListConnectionsRequestDTO getRequestContainerObject() {
		return new ListConnectionsRequestDTO();
	}

	@Override
	public ListConnectionsResponseDTO process(JsonNode request, ListConnectionsRequestDTO container) throws Exception {
		logger.info("listing connections {}", container);

		final ListConnectionsResponseDTO dto = new ListConnectionsResponseDTO();

		useRepository(container, new RepositoryVisitor() {
			@Override
			public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception {
				List<InformaticaRepositoryClient.InformaticaConnection> folders = client.listConnections();

				dto.setConnections(folders);
			}
		});

		return dto;
	}
}