package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.*;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.util.List;

public class ValidateInformaticaWorkflowExists extends AbstractRequestHandler<ValidateWorkflowExistsRequestDTO, ValidateWorkflowExistsResponseDTO>
{
	@Override
	public String getId() {
		return "validateWorkflowExists";
	}

	@Override
	public ValidateWorkflowExistsRequestDTO getRequestContainerObject() {
		return new ValidateWorkflowExistsRequestDTO();
	}

	@Override
	public ValidateWorkflowExistsResponseDTO process(JsonNode request, final ValidateWorkflowExistsRequestDTO container) throws Exception {
		logger.info("validating workflow {}", container);

		final ValidateWorkflowExistsResponseDTO dto = new ValidateWorkflowExistsResponseDTO();

		useExecutionService(container, new IntegrationServiceVisitor() {
			@Override
			public void useClient(InformaticaExecutionClient client, InformaticaDomain domain, InformaticaRepository repository, InformaticaIntegrationService service, InformaticaWebServicesHub hub, RuntimeSupport runtimeSupport) throws Exception {
				boolean exists = client.validateWorkflowExists(container.getFolder(), container.getWorkflow());

				dto.setWorkflowExists(exists);
			}
		});

		return dto;
	}
}