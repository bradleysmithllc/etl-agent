package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.*;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.Incomplete;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InformaticaDomain {
	private final String domainName;
	private final String clientVersion;
	private final String connectivityHost;
	private final int connectivityPort;
	private final String userName;
	private final String passwordEncrypted;
	private final String password;
	private final String securityDomain;

	private final File domainFile;

	private final File informaticaBinDirectory;
	private final File workingRoot;

	private final RuntimeSupport runtimeSupport;

	private final InformaticaRepository defaultRepository;
	private final InformaticaWebServicesHub defaultWebServicesHub;
	private final DomainsProperty.ClientType clientType;

	private int numRequestsSent = 0;

	private final InformaticaIntegrationService defaultIntegrationService;

	private final List<InformaticaRepository> repositories = new ArrayList<InformaticaRepository>();

	private final String agentHost;
	private final int agentPort;

	@Incomplete(explanation = "Implement domains.infa as a user-supplied file")
	public InformaticaDomain(String name, DomainsProperty value, DomainDefaults domainDefaults, RuntimeSupport runtimeSupport, File domainsFile) {
		domainName = name;

		String cv = value.getClientVersion();

		if (cv == null && domainDefaults != null)
		{
			cv = domainDefaults.getClientVersion();
		}

		checkRequiredAttribute("client-version", cv);

		clientVersion = cv;

		String ch  = value.getConnectivityHost();

		if (ch == null && domainDefaults != null)
		{
			ch = domainDefaults.getConnectivityHost();
		}

		connectivityHost = ch;

		String sd = value.getSecurityDomain();

		if (sd == null && domainDefaults != null)
		{
			sd = domainDefaults.getSecurityDomain();
		}

		securityDomain = sd;

		this.runtimeSupport = runtimeSupport;

		DomainsProperty.ClientType cltype = value.getClientType();

		if (cltype == null && domainDefaults != null)
		{
			cltype = domainDefaults.getClientType();
		}

		clientType = resolveType(cltype);

		switch (clientType) {
			case AGENT:
				// agent is a required property now
				Agent agentNode = value.getAgent();

				if (agentNode == null && domainDefaults != null)
				{
					agentNode = domainDefaults.getAgent();
				}

				if (agentNode == null) {
					throw new IllegalArgumentException("agent property is required when client type is 'agent'");
				}

				agentHost = agentNode.getHostName();

				if (agentHost == null) {
					throw new IllegalArgumentException("agent object requires host-name");
				}

				Long jsonNode = agentNode.getHostPort();

				if (jsonNode != null) {
					agentPort = jsonNode.intValue();
				} else {
					agentPort = 8188;
				}
				break;
			case MOCK:
			case LOCAL:
			default:
				agentHost = null;
				agentPort = 0;
				break;
		}

		String root = value.getWorkingRoot();

		// use the value provided, then fall back to the domain defaults if not present
		if (root == null && domainDefaults != null)
		{
			root = domainDefaults.getWorkingRoot();
		}

		// if still not available, then nobody cares about it so use an anonymous folder
		if (root == null)
		{
			workingRoot = runtimeSupport.createAnonymousTempFolder();
		}
		else
		{
			workingRoot = new File(root);
		}

		String binRoot = value.getInformaticaBinDirectory();

		if (binRoot == null && domainDefaults != null)
		{
			binRoot = domainDefaults.getInformaticaBinDirectory();
		}

		if (binRoot != null) {
			informaticaBinDirectory = new File(binRoot);
		} else {
			informaticaBinDirectory = null;
		}

		Long port = value.getConnectivityPort();

		if (port == null && domainDefaults != null)
		{
			port = domainDefaults.getConnectivityPort();
		}

		if (port != null) {
			connectivityPort = port.intValue();
		} else {
			connectivityPort = -1;
		}

		String un = value.getUsername();

		if (un == null && domainDefaults != null)
		{
			un = domainDefaults.getUsername();
		}

		userName = un;

		checkRequiredAttribute("username", userName);

		String pw = value.getPasswordEncrypted();

		if (pw == null && domainDefaults != null)
		{
			pw = domainDefaults.getPasswordEncrypted();
		}

		passwordEncrypted = pw;

		checkRequiredAttribute("password-encrypted", passwordEncrypted);

		pw = value.getPassword();

		if (pw == null && domainDefaults != null)
		{
			pw = domainDefaults.getPassword();
		}

		password = pw;

		Repositories erepositories = value.getRepositories();

		if (erepositories == null) {
			throw new IllegalArgumentException("Informatica domain missing repositories");
		}

		Map<String, RepositoriesProperty> it = erepositories.getAdditionalProperties();

		for (Map.Entry<String, RepositoriesProperty> entry : it.entrySet()) {
			InformaticaRepository repository = new InformaticaRepository(this, entry.getKey(), runtimeSupport, entry.getValue());

			if (getRepository(entry.getKey()) != null) {
				throw new IllegalArgumentException("Duplicate repository declaration: " + entry.getKey());
			}

			repositories.add(repository);
		}

		if (repositories.size() == 0) {
			throw new IllegalArgumentException("Informatica domain repositories element is empty");
		}

		String defRepo = value.getDefaultRepository();

		InformaticaRepository repo = repositories.get(0);

		if (defRepo != null) {
			repo = getRepository(defRepo);

			if (repo == null) {
				throw new IllegalArgumentException("Default repository does not exist in domain: " + defRepo);
			}
		}

		defaultRepository = repo;

		InformaticaIntegrationService dis = getDefaultRepository().getDefaultIntegrationService();

		defaultIntegrationService = dis;

		InformaticaWebServicesHub dish = getDefaultRepository().getDefaultWebServicesHub();

		defaultWebServicesHub = dish;

		if (getConnectivityHost() != null) {
			String template = null;
			try {
				template = IOUtils.readURLToString(getClass().getResource("/domains.infa.vm"));

				Map<String, String> bean = new HashMap<String, String>();
				bean.put("domainName", getDomainName());
				bean.put("connectivityHost", getConnectivityHost());
				bean.put("connectivityPort", String.valueOf(getConnectivityPort()));

				String vel = VelocityUtil.writeTemplate(template, bean);

				domainFile = runtimeSupport.createGeneratedSourceFile("informatica", "domains.infa." + getDomainName());

				IOUtils.writeBufferToFile(domainFile, new StringBuffer(vel));
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		} else {
			domainFile = domainsFile;
		}
	}

	private void checkRequiredAttribute(String s, String clientVersion) {
		if (clientVersion == null) {
			throw new IllegalArgumentException("Missing required attribute: '" + s + "'");
		}
	}

	private DomainsProperty.ClientType resolveType(DomainsProperty.ClientType name) {
		if (name == null) {
			return DomainsProperty.ClientType.LOCAL;
		}

		return name;
	}

	public InformaticaWebServicesHub getWebServicesHub(String name) {
		for (InformaticaRepository irepo : repositories) {
			for (InformaticaWebServicesHub iis : irepo.getWebServicesHubs()) {
				if (iis.getName().equals(name)) {
					return iis;
				}
			}
		}

		return null;
	}

	public String getSecurityDomain() {
		return securityDomain;
	}

	public File getInformaticaBinDirectory() {
		return informaticaBinDirectory;
	}

	private String getOptionalAttribute(String s, JsonNode value) {
		return getAttribute(s, value, false);
	}

	private String getRequiredAttribute(String s, JsonNode value) {
		return getAttribute(s, value, true);
	}

	private String getAttribute(String s, JsonNode value, boolean required) {
		JsonNode res = JsonUtils.query(value, s);

		if (required && res == null) {
			throw new IllegalArgumentException("Informatica domain missing attribute: " + s);
		}

		return res == null ? null : res.asText();
	}

	public String getDomainName() {
		return domainName;
	}

	public String getClientVersion() {
		return clientVersion;
	}

	public String getConnectivityHost() {
		return connectivityHost;
	}

	public int getConnectivityPort() {
		return connectivityPort;
	}

	public String getUserName() {
		return userName;
	}

	public String getPasswordEncrypted() {
		return passwordEncrypted;
	}

	public String getPassword() {
		return password;
	}

	public InformaticaRepository getRepository(String name) {
		for (InformaticaRepository repo : repositories) {
			if (repo.getRepositoryName().equals(name)) {
				return repo;
			}
		}

		return null;
	}

	public List<InformaticaRepository> getRepositories() {
		return repositories;
	}

	public InformaticaRepository getDefaultRepository() {
		return defaultRepository;
	}

	public File getWorkingRoot() {
		return workingRoot;
	}

	public File getLocalizedWorkingRoot() {
		return new FileBuilder(workingRoot).subdir(runtimeSupport.getProjectUID()).subdir(getDomainName()).file();
	}

	public InformaticaIntegrationService getDefaultIntegrationService() {
		return defaultIntegrationService;
	}

	public InformaticaWebServicesHub getDefaultWebServicesHub() {
		return defaultWebServicesHub;
	}

	public InformaticaIntegrationService getIntegrationService(String isName) {
		for (InformaticaRepository irepo : repositories) {
			for (InformaticaIntegrationService iis : irepo.getIntegrationServices()) {
				if (iis.getIntegrationServiceName().equals(isName)) {
					return iis;
				}
			}
		}

		return null;
	}

	public boolean containsIntegrationService(String isName) {
		return getIntegrationService(isName) != null;
	}

	public File getDomainFile() {
		if (domainFile == null) {
			throw new UnsupportedOperationException("Domain file not supported yet");
		}

		return domainFile;
	}

	public DomainsProperty.ClientType getClientType() {
		return clientType;
	}

	public String getAgentHost() {
		return agentHost;
	}

	public int getAgentPort() {
		return agentPort;
	}

	public void dispose() {
		for (InformaticaRepository repository : getRepositories()) {
			repository.dispose();
		}
	}

	File getInformaticaBinDir() {
		// get the client version for the binary
		String ver = getClientVersion();
		// construct a path to the power center binaries using a well-known path combined with the version number

		// check for an explicit bin dir, then use the etl unit dir
		if (informaticaBinDirectory != null) {
			return informaticaBinDirectory;
		} else {
			return new FileBuilder(runtimeSupport.getVendorBinaryDir()).subdir("PowerCenter").subdir(ver).subdir("server").subdir("bin").file();
		}
	}

	public File getWorkspaceDir(RemoteFile.fileType type) {
		return new FileBuilder(getLocalizedWorkingRoot()).mkdirs().subdir(type.getDirectoryName()).mkdirs().file();
	}

	public File getTargetFilesDir() {
		return getWorkspaceDir(RemoteFile.fileType.targetFiles);
	}

	public File getBadFilesDir() {
		return getWorkspaceDir(RemoteFile.fileType.badFiles);
	}

	public File getSessionLogDir() {
		return getWorkspaceDir(RemoteFile.fileType.sessionLog);
	}

	public File getWorkflowLogDir() {
		return getWorkspaceDir(RemoteFile.fileType.workflowLog);
	}

	public File getParameterFilesDir() {
		return getWorkspaceDir(RemoteFile.fileType.parameterFiles);
	}

	public File getSourceFilesDir() {
		return getWorkspaceDir(RemoteFile.fileType.sourceFiles);
	}

	public File getLookupFilesDir() {
		return getWorkspaceDir(RemoteFile.fileType.lookupFiles);
	}

	void request() {
		numRequestsSent++;
	}

	public int getNumRequestsSent() {
		return numRequestsSent;
	}

	@Override
	public String toString() {
		return "InformaticaDomain{" +
				"domainName='" + domainName + '\'' +
				'}';
	}
}