package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.Expectorator;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.StringUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class PmRepProcessImpl implements PmRepProcess {
	public static final String PMREP = "pmrep>";
	public static final String NEWLINE = "\r\n";
	private Expectorator expectorator;
	private final ProcessFacade processFacade;
	private final RuntimeSupport runtimeSupport;
	private final String identifier;

	private final File outputPath;

	public static final long DEFAULT_TIMEOUT = 120000L;

	public PmRepProcessImpl(
			ProcessFacade facade,
			RuntimeSupport runtimeSupport,
			Map<String, String> environment,
			String identifier
	) throws Exception {
		this.runtimeSupport = runtimeSupport;
		this.identifier = identifier;

		processFacade = facade;

		try {
			outputPath = null;
			processFacade.waitForStreams();

			expectorator = new Expectorator(new BufferedReader(processFacade.getReader()), processFacade.getWriter());

			// wait for the first pmrep prompt
			expectorator.expect(PMREP, DEFAULT_TIMEOUT);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public PmRepProcessImpl(
			File pmRep,
			RuntimeSupport runtimeSupport,
			Map<String, String> environment,
			String identifier,
			File workingDir
	) throws Exception {
		this.runtimeSupport = runtimeSupport;
		this.identifier = identifier;

		ProcessDescription pd = new ProcessDescription(pmRep.getAbsolutePath());
		pd.getEnvironment().putAll(environment);

		outputPath = new FileBuilder(runtimeSupport.getReportDirectory("log"))
			.subdir("process_log").subdir("informatica").mkdirs().name("pmrep-" + identifier + ".out").file();
		pd.output(outputPath).workingDirectory(workingDir);

		// make sure the file is available
		if (outputPath.exists())
		{
			FileUtils.forceDelete(outputPath);
		}

		try {
			processFacade = runtimeSupport.execute(pd);
			processFacade.waitForStreams();

			expectorator = new Expectorator(new BufferedReader(processFacade.getReader()), processFacade.getWriter());

			// wait for the first pmrep prompt
			expectorator.expect(PMREP, DEFAULT_TIMEOUT);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public String send(String line) throws IOException, ProcessTimeoutException {
		String resp = send(line, DEFAULT_TIMEOUT);

		return resp;
	}

	@Override
	public synchronized String send(String line, long timeout) throws IOException, ProcessTimeoutException {
		if (expectorator == null)
		{
			throw new IllegalStateException("Pmrep client disposed");
		}

		try
		{
			// get the command name from the string - just call it the first word
			String command = line;
			int index = line.indexOf(' ');

			if (index != -1)
			{
				command = command.substring(0, index);
			}

			command = StringUtils.sanitize(command, '_');

			// log the dialog
			String subdir = "pmrep/dialogs/" + identifier + "_" + command;
			File dialogRequest = runtimeSupport.createAnonymousTempFile(subdir, "say");
			FileUtils.write(dialogRequest, line);

			String response = expectorator.sayAndExpect(line + NEWLINE, PMREP, timeout);

			File dialogResponse = runtimeSupport.createAnonymousTempFile(subdir, "respond");
			FileUtils.write(dialogResponse, response);

			return response;
		}
		catch (InterruptedException e)
		{
			throw new ProcessTimeoutException("Process timed out:" + line, e);
		}
	}

	@Override
	public synchronized void dispose() throws IOException, ProcessTimeoutException
	{
		if (expectorator == null)
		{
			throw new IllegalStateException("Pmrep client disposed");
		}

		expectorator.say("exit" + NEWLINE);

		// don't let this go for more than 10 seconds
		final CountDownLatch cdl = new CountDownLatch(1);

		Thread thr = new Thread(new Runnable(){
			@Override
			public void run() {
				try
				{
					processFacade.waitForCompletion();
					cdl.countDown();
				}
				catch(RuntimeException exc)
				{
					// make sure to dispose the process
					processFacade.kill();
				}
			}
		});
		thr.start();

		try {
			cdl.await(10, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			// free it up
			thr.interrupt();
		}

		expectorator = null;
	}

	@Override
	public String toString() {
		return "PmRepProcess{" +
				"identifier='" + identifier + '\'' +
				", outputPath=" + outputPath +
				'}';
	}
}