package org.bitbucket.bradleysmithllc.etlagent.informatica.dto;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;

import java.util.List;

public class ListWorkflowsResponseDTO extends GenericResponseDTO
{
	private List<String> workflows;

	public List<String> getWorkflows() {
		return workflows;
	}

	public void setWorkflows(List<String> folders) {
		this.workflows = folders;
	}

	@Override
	public String toString() {
		return "ListFoldersResponseDTO{" +
				"workflows=" + workflows +
				'}';
	}
}
