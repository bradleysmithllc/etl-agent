package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ListFoldersRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ListFoldersResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.PurgeTestFoldersResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.purge_test_folders.PurgeTestFoldersRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepositoryClient;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.util.List;

public class ListInformaticaFolders extends AbstractRequestHandler<ListFoldersRequestDTO, ListFoldersResponseDTO>
{
	@Override
	public String getId() {
		return "listInformaticaFolders";
	}

	@Override
	public ListFoldersRequestDTO getRequestContainerObject() {
		return new ListFoldersRequestDTO();
	}

	@Override
	public ListFoldersResponseDTO process(JsonNode request, ListFoldersRequestDTO container) throws Exception {
		logger.info("listing folders {}", container);

		final ListFoldersResponseDTO dto = new ListFoldersResponseDTO();

		useRepository(container, new RepositoryVisitor() {
			@Override
			public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception {
				List<String> folders = client.listFolders();

				dto.setFolders(folders);
			}
		});

		return dto;
	}
}