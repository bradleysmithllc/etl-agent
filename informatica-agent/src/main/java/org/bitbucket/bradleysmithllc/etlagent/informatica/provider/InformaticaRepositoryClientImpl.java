package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.regexp.*;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.*;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public final class InformaticaRepositoryClientImpl implements InformaticaRepositoryClient, InformaticaIntegrationServiceClient {
	protected final Log applicationLog;

	public static final String FOLDER_LIST_ATTRIBUTE = "folderList";
	private final List<String> temporaryFolders = new ArrayList();

	private final InformaticaDomain informaticaDomain;

	private final InformaticaRepository informaticaRepository;
	private final InformaticaIntegrationService informaticaIntegrationService;

	private final File working;
	private final File pmCmdDir;
	private final RuntimeSupport runtimeSupport;

	private boolean connected = false;

	private static int client = 0;

	private final boolean testMode = ContextBase.agentTestMode();

	private static final AtomicInteger command = new AtomicInteger();

	public InformaticaRepositoryClientImpl(
			InformaticaRepository informaticaRepository,
			InformaticaIntegrationService service,
			File pmCmdDir,
			RuntimeSupport pRuntimeSupport,
			Log log
	) {
		applicationLog = log;
		this.informaticaRepository = informaticaRepository;
		informaticaIntegrationService = service;
		this.informaticaDomain = informaticaRepository.getInformaticaDomain();
		this.pmCmdDir = pmCmdDir;
		runtimeSupport = pRuntimeSupport;

		working = new FileBuilder(runtimeSupport.getTempDirectory()).subdir("ipc." + client++).mkdirs().file();
	}

	public static void resetClientCount() {
		client = 0;
	}

	public final void connect() throws Exception {
		connect("pmrep");
	}

	private void connect(String tool) throws Exception {
		// connect only once
		if (!connected) {
			// connect
			ProcessDescription pb = new ProcessDescription(
					new File(pmCmdDir, tool).getAbsolutePath())
					.argument("connect")
					.argument("-r")
					.argument(informaticaRepository.getRepositoryName())
					.argument("-n")
					.argument(informaticaDomain.getUserName())
					.argument("-X")
					.argument("INFAPASSWD");

			prepareLogon(pb);

			ProcessFacade process = executeAndWait(pb);

			int res = process.getCompletionCode();

			applicationLog.info(process.getInputBuffered().toString());

			if (res != 0) {
				throw new InformaticaError("Could not connect to informatica " + process.getInputBuffered());
			}

			connected = true;
		} else {
			throw new IllegalStateException("Already connected");
		}
	}

	private void prepareLogon(ProcessDescription pb) {
		// use the domain file options
		pb = pb.argument("-d").argument(informaticaDomain.getDomainName());

		prepareConnectivity(pb);
	}

	private void prepareConnectivity(ProcessDescription pb) {
		// use the domain file options
		pb.environment("INFA_DOMAINS_FILE", informaticaDomain.getDomainFile().getAbsolutePath())
			.environment("INFAPASSWD", informaticaDomain.getPasswordEncrypted())
			.environment("INFA_CLIENT_RESILIENCE_TIMEOUT", "30");


		runtimeSupport.getTempDirectory();

		String str = pb.getCommand();

		int lastIndex = str.lastIndexOf(File.separator);

		if (lastIndex != -1) {
			str = str.substring(lastIndex + 1);
		}

		File output = new FileBuilder(runtimeSupport.getTempDirectory())
				.subdir("process_log").subdir("informatica").mkdirs().name(str + "_" + pb.getArguments().get(0) + "." + command.getAndIncrement() + ".out").file();
		pb.output(output);
		pb.workingDirectory(working);
	}

	private void connected() throws InformaticaError {
		connected("pmrep");
	}

	private void connected(String tool) throws InformaticaError {
		if (!connected) {
			try {
				connect(tool);
			} catch (Exception exc) {
				throw new InformaticaError(exc.toString());
			}
		}
	}

	@Override
	public void createConnection(
			String serverName,
			String databaseName,
			String databaseUsername,
			String databasePassword,
			String relationalConnectionName
	) throws Exception, InformaticaError {
		connected();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("createconnection")
				.argument("-s")
				.argument("Microsoft SQL Server")
				.argument("-n")
				.argument(relationalConnectionName)
				.argument("-u")
				.argument(databaseUsername)
				.argument("-p")
				.argument(databasePassword)
				.argument("-v")
				.argument(serverName)
				.argument("-b")
				.argument(databaseName)
				.argument("-e")
				.argument("SET QUOTED_IDENTIFIER ON")
				.argument("-l")
				.argument("MS1252");

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		int res = process.getCompletionCode();

		StringBuffer outBuff = process.getInputBuffered();

		applicationLog.info(process.getInputBuffered().toString());

		if (res != 0) {
			InfaCreateConnectionExistsExpression iccre = InfaCreateConnectionExistsExpression.match(outBuff);

			if (!iccre.hasNext()) {
				throw new InformaticaError("Could not create connection " + outBuff);
			}
		}
	}

	@Override
	public void createConnection(
			ConnectionDetails details
	) throws Exception, InformaticaError {
		connected();

		if (details.getRelationalConnectionType() != ConnectionDetails.type.sqlserver)
		{
			throw new UnsupportedOperationException("Connection type " + details.getRelationalConnectionType() + " not implemented");
		}

		SqlServerConnectionDetails sqldet = details.getSqlServerConnectionDetails();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("createconnection")
				.argument("-s")
				.argument("Microsoft SQL Server")
				.argument("-n")
				.argument(details.getRelationalConnectionName())
				.argument("-u")
				.argument(sqldet.getDatabaseUsername())
				.argument("-p")
				.argument(sqldet.getDatabasePassword())
				.argument("-b")
				.argument(sqldet.getDatabaseName())
				.argument("-e")
				.argument("SET QUOTED_IDENTIFIER ON")
				.argument("-l")
				.argument("MS1252");

		pb.argument("-v");

		String instanceName = sqldet.getInstanceName();
		if (instanceName != null)
		{
			pb.argument(sqldet.getServerName() + "\\" + instanceName);
		}
		else
		{
			pb.argument(sqldet.getServerName());
		}

		String adDomain = sqldet.getDomainName();
		if (adDomain != null)
		{
			pb.argument("-d").argument(adDomain);
		}

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		int res = process.getCompletionCode();

		StringBuffer outBuff = process.getInputBuffered();

		applicationLog.info(process.getInputBuffered().toString());

		if (res != 0) {
			InfaCreateConnectionExistsExpression iccre = InfaCreateConnectionExistsExpression.match(outBuff);

			if (!iccre.hasNext()) {
				throw new InformaticaError("Could not create connection " + outBuff);
			}
		}
	}

	public void createFolder(String folder) throws Exception, InformaticaError {
		createFolder(folder, false);
	}

	public void createFolder(String folder, boolean temporary) throws Exception, InformaticaError {
		connected();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("createfolder")
				.argument("-n")
				.argument(folder)
				.argument("-s");

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		int res = process.getCompletionCode();

		applicationLog.info(process.getInputBuffered().toString());

		StringBuffer outBuff = process.getInputBuffered();

		if (res != 0) {
			InfaCreateFolderExistsExpression icfee = InfaCreateFolderExistsExpression.match(outBuff, folder, informaticaRepository.getRepositoryName());

			if (!icfee.hasNext()) {
				// check whether the folder already exists.  This is not an error.
				throw new InformaticaError("Could not create folder " + outBuff);
			}
		}

		if (temporary) {
			temporaryFolders.add(folder);
		}
	}

	public synchronized void deleteConnection(
			String connectionName,
			boolean failIfNotExists
	) throws Exception, InformaticaError {
		connected();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("deleteconnection")
				.argument("-n")
				.argument(connectionName)
				.argument("-f");

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		int res = process.getCompletionCode();

		String message = process.getInputBuffered().toString();
		applicationLog.info(message);

		if (res != 0) {
			// look for the connection does not exist message
			InfaDeleteConnectionDoesNotExistExpression idcdnee = new InfaDeleteConnectionDoesNotExistExpression(message, connectionName);

			if (idcdnee.hasNext()) {
				// handle according to the failIfNotExists parameterFiles
				if (failIfNotExists) {
					throw new InformaticaError("Connection to delete does not exist");
				} else {
					applicationLog.info("User does not care if connection does not exist");
				}
			} else {
				throw new InformaticaError("Could not delete connection " + process.getInputBuffered());
			}
		}
	}

	@Override
	public void deleteConnection(String connectionName) throws Exception, InformaticaError {
		deleteConnection(connectionName, true);
	}

	public void deleteFolder(String folder) throws Exception, InformaticaError {
		deleteFolder(folder, true);
	}

	public void deleteFolder(String folder, boolean failIfNotExists) throws Exception, InformaticaError {
		connected();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("deletefolder")
				.argument("-n")
				.argument(folder);

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		applicationLog.info(process.getInputBuffered().toString());

		StringBuffer outBuff = process.getInputBuffered();

		InfaDeleteFolderInUseExpression idiu = new InfaDeleteFolderInUseExpression(outBuff);

		if (idiu.hasNext())
		{
			throw new InformaticaDeadlockException("Folder " + folder + " does not exist.");
		}

		if (process.getCompletionCode() != 0) {
			// check for allowed not to exist
			InfaDeleteFolderFailedExpression idcfe = new InfaDeleteFolderFailedExpression(outBuff);

			if (idcfe.hasNext()) {
				InfaDeleteFolderDoesNotExistExpression idcdnee = new InfaDeleteFolderDoesNotExistExpression(outBuff, folder);

				if (idcdnee.hasNext()) {
					if (failIfNotExists) {
						throw new InformaticaError("Folder " + folder + " does not exist.");
					}
				} else {
					throw new InformaticaError("Could not delete folder and could not determine if folder exists [user " + (failIfNotExists ? "cares" : "doesNotCare") + "]" + outBuff);
				}
			} else {
				throw new InformaticaError("Could not delete folder and could not locate failed to execute " + outBuff);
			}
		}
	}

	private ProcessFacade executeAndWait(ProcessDescription pb) throws IOException {
		ProcessFacade process = runtimeSupport.execute(pb);
		process.waitForStreams();
		process.waitForCompletion();
		process.waitForOutputStreamsToComplete();

		return process;
	}

	public void deleteTemporaryFolders() throws Exception, InformaticaError {
		connected();

		Iterator<String> it = temporaryFolders.iterator();

		while (it.hasNext()) {
			String folder = it.next();

			deleteFolder(folder);

			it.remove();
		}
	}

	public void exportWorkflowToXml(
			String workFlow,
			String folder,
			File output
	) throws Exception, InformaticaError {
		connected();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("objectexport")
				.argument("-n")
				.argument(workFlow)
				.argument("-o")
				.argument("workflow")
				.argument("-m")
				.argument("-s")
				.argument("-r")
				.argument("-b")
				.argument("-f")
				.argument(folder)
				.argument("-u")
				.argument(output.getAbsolutePath());

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		StringBuffer outBuff = process.getInputBuffered();

		applicationLog.info(process.getInputBuffered().toString());

		if (process.getCompletionCode() != 0) {
			throw new InformaticaError("Could not export from informatica " + outBuff);
		}

		String pText = outBuff.toString();

		if (!testMode) {
			// check for success
			InfaExportSuccessExpression iese = new InfaExportSuccessExpression(pText);

			boolean success = iese.hasNext();

			InfaExportResultExpression iere = InfaExportResultExpression.match(pText);

			if (!iere.hasNext()) {
				throw new InformaticaError("Unexpected results from pmrep");
			}

			if (!success && (iere.getErrors() + iere.getWarnings()) != 0) {
				throw new InformaticaError("Errors and/or warnings received from pmrep.");
			}

			File tempf = new File(output.getParentFile(), output.getName() + ".tmp");

			// correct silly stuff in xml output
			// generify repository name
			IOUtils.replace(output, tempf, informaticaRepository.getRepositoryName(), "svn_pc_repo");

			// generify integration service
			IOUtils.visitLines(tempf, output, new IOUtils.LineVisitor() {
				public String visit(String line, int lineNo) {
					InfaServerNameExpression isne = new InfaServerNameExpression(line);

					return isne.replaceAll("SERVERNAME =\"svn_is\"");
				}
			});

			// generify domain name
			IOUtils.visitLines(output, tempf, new IOUtils.LineVisitor() {
				public String visit(String line, int lineNo) {
					InfaDomainNameExpression isne = new InfaDomainNameExpression(line, informaticaDomain.getDomainName());

					return isne.replaceAll("SERVER_DOMAINNAME =\"Domain_etlsvn01\"");
				}
			});

			if (!output.delete()) {
				throw new IOException("Obnoxious file won't delete!");
			}

			// strip out the time stamp
			IOUtils.replace(tempf, output, "<POWERMART CREATION_DATE=\"\\d{1,2}/\\d{1,2}/\\d{4} \\d{1,2}:\\d{1,2}:\\d{1,2}\" REPOSITORY_VERSION=\"(\\d{1,4}.\\d{1,9})\">", "<POWERMART CREATION_DATE=\"11/30/2010 12:00:00\" REPOSITORY_VERSION=\"$1\">");

			if (!tempf.delete()) {
				throw new IOException("Obnoxious temp file won't delete!");
			}
		}
	}

	public void exportAllWorkflowsToXml() throws Exception, InformaticaError {
		connected();

		File queryf = runtimeSupport.createAnonymousTempFileWithPrefix("ExportAll.txt.");

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("executequery")
				.argument("-q")
				.argument("ExportAll")
				.argument("-t")
				.argument("shared")
				.argument("-u")
				.argument(queryf.getAbsolutePath());

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		int res = process.getCompletionCode();

		applicationLog.info(process.getInputBuffered().toString());

		if (res != 0) {
			throw new InformaticaError("Could not export from informatica " + process.getInputBuffered());
		}

		// read the file line-by-line and export each workflow
		BufferedReader br = new BufferedReader(new FileReader(queryf));

		String line = null;
		int expCount = 0;

		while ((line = br.readLine()) != null) {
			expCount++;

			StringTokenizer st = new StringTokenizer(line, ",");

			String crap = st.nextToken();
			String folder = st.nextToken();
			String workflow = st.nextToken();

			// strip off the 'wkf_'
			if (!workflow.startsWith("wkf_")) {
				continue;
			}

			String workflowName = workflow.substring(4);

			String type = st.nextToken();
			String none = st.nextToken();
			String number = st.nextToken();

			exportWorkflowToXml(
					workflowName,
					folder,
					new File("")
			);
		}
	}

	public void importWorkflowFromXml(
			final String folderPrefix,
			InformaticaIntegrationService newService,
			File xmlFile,
			final List<String> flist
	) throws InformaticaDeadlockException, InformaticaError, IOException {
		connected();

		// prepare the import file
		File importFile = runtimeSupport.createAnonymousTempFileWithPrefix(xmlFile.getName() + ".import.");

		applicationLog.info("Substituting " + newService.getIntegrationServiceName() + " for svn_is");
		IOUtils.replace(xmlFile, importFile, "svn_is", newService.getIntegrationServiceName());

		// this needs to be a resource
		URL template = getClass().getResource("/objectimport-control.vm");

		if (template == null) {
			throw new IllegalStateException("Could not find objectimport-control.vm template");
		}

		File controlFile = runtimeSupport.createAnonymousTempFileWithPrefix(xmlFile.getName() + ".control.xml.");

		try {
			String insta = VelocityUtil.writeTemplate(IOUtils.readURLToString(template), new VelocityBean(folderPrefix, informaticaRepository.getRepositoryName(), flist));

			IOUtils.writeBufferToFile(controlFile, new StringBuffer(insta));

			Map<String, String> env = new HashMap<String, String>();

			ProcessDescription pb = new ProcessDescription(
					new File(pmCmdDir, "pmrep").getAbsolutePath())
					.argument("objectimport")
					.argument("-i")
					.argument(importFile.getAbsolutePath())
					.argument("-c")
					.argument(controlFile.getAbsolutePath());

			prepareConnectivity(pb);

			ProcessFacade process = executeAndWait(pb);

			int res = process.getCompletionCode();

			String message = process.getInputBuffered().toString();
			applicationLog.info(message);

			if (res != 0) {
				throw new InformaticaError("Could not import into informatica : objectimport returned " + res + " saying " + process.getInputBuffered());
			}

			// check for success message only if not in test mode
			if (!testMode) {
				InfaImportSuccessfulExpression iise = new InfaImportSuccessfulExpression(message);

				if (!iise.hasNext()) {
					// check for repo deadlock
					InfaRepositoryDeadlockExpression ide = new InfaRepositoryDeadlockExpression(message);

					if (ide.hasNext())
					{
						throw new InformaticaDeadlockException(message);
					}

					InfaImportResultsExpression ire = new InfaImportResultsExpression(message);

					if (!ire.hasNext()) {
						throw new InformaticaError("Failed to import object: " + message);
					} else if (ire.getErrorCount() > 0) {
						throw new InformaticaError("Failed to import object - too many errors: " + message);
					}
				}
			}
		} catch (Exception exc) {
			throw new InformaticaError(exc.toString());
		}
	}

	public void importWorkflowFromXml(
			String folderPrefix,
			InformaticaIntegrationService newService,
			File xml
	) throws InformaticaDeadlockException, InformaticaError, IOException {
		importWorkflowFromXml(folderPrefix, newService, xml, getFolderList(xml));
	}

	public static List<String> getFolderList(File xml) throws InformaticaError {
		List<String> li = new ArrayList<String>();

		try {
			String str = IOUtils.readFileToString(xml);

			// list all folders
			FolderNameExpression fne = new FolderNameExpression(str);

			while (fne.hasNext()) {
				String name = fne.getFolderName();

				if (!li.contains(name)) {
					li.add(name);
				}
			}

			// list all folder shortcuts
			FolderShortcutExpression fse = new FolderShortcutExpression(str);

			while (fse.hasNext()) {
				String name = fse.getShortcutFolderName();

				if (!li.contains(name)) {
					li.add(name);
				}
			}

			return li;
		} catch (Exception exc) {
			throw new InformaticaError(exc.toString());
		}
	}

	public List<String> listFolders() throws Exception, InformaticaError {
		connected();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("listobjects")
				.argument("-o")
				.argument("folder");

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		StringBuffer outBuff = process.getInputBuffered();

		int res = process.getCompletionCode();

		applicationLog.info(outBuff.toString());

		if (res != 0) {
			throw new InformaticaError("Could not read folders from informatica " + outBuff);
		}

		List<String> folderList = new ArrayList<String>();

		InfaFolderNameExpression ifne = new InfaFolderNameExpression(outBuff.toString());

		while (ifne.hasNext()) {
			String folder = ifne.getFolderName();

			folderList.add(folder);
		}

		return folderList;
	}

	public List<InformaticaConnection> listConnections() throws Exception, InformaticaError {
		connected();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("listconnections");

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		StringBuffer outBuff = process.getInputBuffered();

		int res = process.getCompletionCode();

		applicationLog.info(outBuff.toString());

		if (res != 0) {
			throw new InformaticaError("Could not read connections from informatica " + outBuff);
		}

		List<InformaticaConnection> connList = new ArrayList<InformaticaConnection>();

		InfaConnectionNameExpression ifne = new InfaConnectionNameExpression(outBuff.toString());

		while (ifne.hasNext()) {
			InformaticaConnection ic = new InformaticaConnection();
			ic.setConnectionName(ifne.getConnectionName());
			ic.setConnectionType(ifne.getConnectionType());

			connList.add(ic);
		}

		return connList;
	}

	@Override
	public List<String> listWorkflows(String folder) throws Exception, InformaticaError {
		connected();

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmrep").getAbsolutePath())
				.argument("listobjects")
				.argument("-o")
				.argument("folder")
				.argument("-f")
				.argument(folder);

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		StringBuffer outBuff = process.getInputBuffered();

		int res = process.getCompletionCode();

		applicationLog.info(outBuff.toString());

		if (res != 0) {
			throw new InformaticaError("Could not read workflows from informatica " + outBuff);
		}

		List<String> folderList = new ArrayList<String>();

		InfaWorkflowNameExpression ifne = new InfaWorkflowNameExpression(outBuff.toString());

		while (ifne.hasNext()) {
			String workflow = ifne.getWorkflowName();

			folderList.add(workflow);
		}

		return folderList;
	}

	public int purgeTestFolders() throws Exception, InformaticaError {
		connected();

		List<String> folderList = listFolders();

		Iterator<String> it = folderList.iterator();

		int count = 0;

		while (it.hasNext()) {
			String folder = it.next();

			TestFolderNameExpression tfne = new TestFolderNameExpression(folder);

			if (tfne.matches()) {
				deleteFolder(folder);
				count++;
			}
		}

		return count;
	}

	public InformaticaDomain getInformaticaDomain() {
		return informaticaDomain;
	}

	public InformaticaRepository getInformaticaRepository() {
		return informaticaRepository;
	}

	@Override
	public boolean ping() {
		return true;
	}

	public void dispose() {
	}

	public InformaticaExecutionResult executeWorkflow(String workFlow, String runInstance, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, runInstance, null, folder, parmFile, null);
	}

	public InformaticaExecutionResult executeWorkflow(String workFlow, String runInstance, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> files) throws Exception {
		return executeWorkflowTask(workFlow, runInstance, null, folder, parmFile, files);
	}

	public InformaticaExecutionResult executeWorkflow(String workFlow, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, null);
	}

	public InformaticaExecutionResult executeWorkflow(String workFlow, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> files) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, files);
	}

	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String task, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, null);
	}

	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String task, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> files) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, files);
	}

	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String runInstance, String task, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, runInstance, task, folder, null);
	}

	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String runInstance, String task, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> files) throws Exception {
		if (informaticaIntegrationService == null) {
			throw new UnsupportedOperationException();
		}

		if (files != null) {
			// lay out the input files
			RemoteFile.copyRemoteToLocal(files, informaticaDomain);
		}

		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmcmd").getAbsolutePath())
				.argument(task == null ? "startworkflow" : "starttask")
				.argument("-service")
				.argument(informaticaIntegrationService.getIntegrationServiceName());

		if (informaticaDomain != null) {
			pb = pb.argument("-domain")
					.argument(informaticaDomain.getDomainName());
		}

		// create a literal parameterFiles file
		File par = new File(
				informaticaDomain.getParameterFilesDir(),
				UUID.randomUUID().toString() + ".PRM"
				//.folder + "." + workFlow + (task != null ? ("." + task) : "") + (runInstance != null ? ("." + runInstance) : "") + ".PRM"
		);

		parmFile.write(par);

		pb = pb.argument("-user")
				.argument(informaticaDomain.getUserName())
				.argument("-pv")
				.argument("INFAPASSWD")
				.argument("-folder")
				.argument(folder)
				.argument("-paramfile")
				.argument(par.getAbsolutePath())
				.argument("-wait");

		if (runInstance != null) {
			pb.argument("-rin").argument(runInstance);
		}

		if (task != null) {
			pb.argument("-workflow").argument(workFlow).argument(task);
		} else {
			pb.argument(workFlow);
		}

		prepareConnectivity(pb);

		boolean loop = true;
		int loopCount = 0;

		Map<RemoteFile.fileType, List<RemoteFile>> xfiles = new HashMap<RemoteFile.fileType, List<RemoteFile>>();
		String excStr = null;

		try {
			while (loop) {
				++loopCount;

				ProcessFacade process = executeAndWait(pb);

				int res = process.getCompletionCode();

				String out = process.getInputBuffered().toString();

				applicationLog.info(out);

				InfaIntSvcDoesntExistExpression iisdee = new InfaIntSvcDoesntExistExpression(out);

				if (iisdee.hasNext())
				{
					runtimeSupport.getApplicationLog().severe("Integration service does not exist.");

					// special case - return an out of date result so the client can retry
					InformaticaExecutionResult informaticaExecutionResult = new InformaticaExecutionResult(xfiles, out, InformaticaResponseDTO.result.intSvcDoesntExist);
					informaticaExecutionResult.setFailureExc("Integration service does not exist: " + out);
					informaticaExecutionResult.setServiceCacheFailure(true);
					return informaticaExecutionResult;
				}

				// check for int service disabled
				InfaIntSvcDisabledExpression iisde = new InfaIntSvcDisabledExpression(out);

				if (iisde.hasNext())
				{
					runtimeSupport.getApplicationLog().severe("Integration service disabled.");

					// special case - return an out of date result so the client can retry
					InformaticaExecutionResult informaticaExecutionResult = new InformaticaExecutionResult(xfiles, out, InformaticaResponseDTO.result.intSvcDisabled);
					informaticaExecutionResult.setFailureExc("Integration service disabled: " + out);
					informaticaExecutionResult.setServiceCacheFailure(true);
					return informaticaExecutionResult;
				}

				// check for int service disabled
				InfaCantConnectIntSvcExpression iccise = new InfaCantConnectIntSvcExpression(out);

				if (iccise.hasNext())
				{
					runtimeSupport.getApplicationLog().severe("Integration service unreachable.");

					// special case - return an out of date result so the client can retry
					InformaticaExecutionResult informaticaExecutionResult = new InformaticaExecutionResult(xfiles, out, InformaticaResponseDTO.result.cantConnectToIntegrationSvc);
					informaticaExecutionResult.setFailureExc("Integration service disabled: " + out);
					informaticaExecutionResult.setServiceCacheFailure(true);
					return informaticaExecutionResult;
				}

				// check for the folder not found crap
				InfaIntSvcFolderNotFoundExpression intexp = new InfaIntSvcFolderNotFoundExpression(out);
				InfaWorkflowNotFoundInFolderExpression intworknot = new InfaWorkflowNotFoundInFolderExpression(out);

				if (intexp.hasNext() || intworknot.hasNext()) {
					runtimeSupport.getApplicationLog().severe("Caught a cache failure.  Passing on to client.");

					// special case - return an out of date result so the client can retry
					InformaticaExecutionResult informaticaExecutionResult = new InformaticaExecutionResult(xfiles, out, InformaticaResponseDTO.result.cacheFailure);
					informaticaExecutionResult.setFailureExc(out);
					informaticaExecutionResult.setServiceCacheFailure(true);
					return informaticaExecutionResult;
				} else {
					loop = checkExecution(res, out, loopCount, workFlow, applicationLog, ContextBase.agentTestMode());
				}

				if (loop) {
					Thread.sleep(1000L);
				}
			}
		} catch (Exception exc) {
			excStr = exc.toString();
			exc.printStackTrace();
		} finally {
			RemoteFile.copyLocalToRemote(informaticaDomain, xfiles);
		}

		return new InformaticaExecutionResult(xfiles, excStr, excStr == null ? InformaticaResponseDTO.result.okay : InformaticaResponseDTO.result.failed);
	}

	@Override
	public boolean validateWorkflowExists(String folder, String workflowName) throws Exception {
		ProcessDescription pb = new ProcessDescription(
				new File(pmCmdDir, "pmcmd").getAbsolutePath())
				.argument("getworkflowdetails")
				.argument("-service")
				.argument(informaticaIntegrationService.getIntegrationServiceName());

		if (informaticaDomain != null) {
			pb = pb.argument("-domain")
					.argument(informaticaDomain.getDomainName());
		}

		pb = pb.argument("-user")
				.argument(informaticaDomain.getUserName())
				.argument("-pv")
				.argument("INFAPASSWD")
				.argument("-folder")
				.argument(folder);

		pb.argument("-workflow").argument(workflowName);

		prepareConnectivity(pb);

		ProcessFacade process = executeAndWait(pb);

		int res = process.getCompletionCode();

		String buff = process.getInputBuffered().toString();

		if (res != 0) {
			throw new InformaticaError("Could not execute pmcmd command: " + res + " : " + buff);
		}

		applicationLog.info(buff);

		// check for success and failure
		InfaGetWorkflowDetailsFailureExpression igwdfe = new InfaGetWorkflowDetailsFailureExpression(buff);

		if (igwdfe.hasNext()) {
			// does not exist
			return false;
		} else {
			// check for success
			InfaGetWorkflowDetailsExpression igwde = new InfaGetWorkflowDetailsExpression(buff);

			if (igwde.hasNext()) {
				// awesomeness
				return true;
			}
		}

		throw new InformaticaError("Error querying workflow details: " + buff);
	}

	public static boolean checkExecution(int res, String buff, int loopCount, String workflow, Log applicationLog, boolean agentInTestMode) throws InformaticaError, InterruptedException {
		// check for the folder thing
		InfaConcurrentWorkflowExpression match = InfaConcurrentWorkflowExpression.match(buff);

		if (loopCount >= 30) {
			throw new InformaticaError("Informatica workflow is too busy for testing: wkf_" + workflow);
		}

		if (match.hasNext()) {
			applicationLog.severe("Workflow is already running - will wait 10 seconds and retry.  This was attempt number [" + loopCount + "] of 30 maximum tries");

			return true;
		}

		if (res != 0) {
			throw new InformaticaError("Could not execute infa command: " + res + " : " + buff);
		}

		if (agentInTestMode) {
			return false;
		}

		String s = ("INFO: Workflow [" + workflow + "]: Execution succeeded.").toLowerCase();
		if (buff.toLowerCase().contains(s)) {
			applicationLog.info("Infa workflow completed sucessfully.  Can break execution loop now");

			return false;
		}

		throw new InformaticaError("Not sure what happened: " + buff);
	}

	public InformaticaIntegrationService getIntegrationService() {
		return informaticaIntegrationService;
	}
}