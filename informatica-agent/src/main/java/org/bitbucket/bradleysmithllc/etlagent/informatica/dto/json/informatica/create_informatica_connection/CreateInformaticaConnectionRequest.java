
package org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.create_informatica_connection;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.Domains;

import javax.annotation.Generated;
import javax.validation.Valid;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "databaseName",
    "databasePassword",
    "databaseUserName",
    "default-domain",
    "domains",
    "informatica-domain",
    "informatica-repository",
    "relationalConnectionName",
    "serverName"
})
public class CreateInformaticaConnectionRequest {

    @JsonProperty("databaseName")
    private String databaseName;
    @JsonProperty("databasePassword")
    private String databasePassword;
    @JsonProperty("databaseUserName")
    private String databaseUserName;
    @JsonProperty("default-domain")
    private String defaultDomain;
    @JsonProperty("domains")
    @Valid
    private Domains domains;
    @JsonProperty("informatica-domain")
    private String informaticaDomain;
    @JsonProperty("informatica-repository")
    private String informaticaRepository;
    @JsonProperty("relationalConnectionName")
    private String relationalConnectionName;
    @JsonProperty("serverName")
    private String serverName;

    @JsonProperty("databaseName")
    public String getDatabaseName() {
        return databaseName;
    }

    @JsonProperty("databaseName")
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    @JsonProperty("databasePassword")
    public String getDatabasePassword() {
        return databasePassword;
    }

    @JsonProperty("databasePassword")
    public void setDatabasePassword(String databasePassword) {
        this.databasePassword = databasePassword;
    }

    @JsonProperty("databaseUserName")
    public String getDatabaseUserName() {
        return databaseUserName;
    }

    @JsonProperty("databaseUserName")
    public void setDatabaseUserName(String databaseUserName) {
        this.databaseUserName = databaseUserName;
    }

    @JsonProperty("default-domain")
    public String getDefaultDomain() {
        return defaultDomain;
    }

    @JsonProperty("default-domain")
    public void setDefaultDomain(String defaultDomain) {
        this.defaultDomain = defaultDomain;
    }

    @JsonProperty("domains")
    public Domains getDomains() {
        return domains;
    }

    @JsonProperty("domains")
    public void setDomains(Domains domains) {
        this.domains = domains;
    }

    @JsonProperty("informatica-domain")
    public String getInformaticaDomain() {
        return informaticaDomain;
    }

    @JsonProperty("informatica-domain")
    public void setInformaticaDomain(String informaticaDomain) {
        this.informaticaDomain = informaticaDomain;
    }

    @JsonProperty("informatica-repository")
    public String getInformaticaRepository() {
        return informaticaRepository;
    }

    @JsonProperty("informatica-repository")
    public void setInformaticaRepository(String informaticaRepository) {
        this.informaticaRepository = informaticaRepository;
    }

    @JsonProperty("relationalConnectionName")
    public String getRelationalConnectionName() {
        return relationalConnectionName;
    }

    @JsonProperty("relationalConnectionName")
    public void setRelationalConnectionName(String relationalConnectionName) {
        this.relationalConnectionName = relationalConnectionName;
    }

    @JsonProperty("serverName")
    public String getServerName() {
        return serverName;
    }

    @JsonProperty("serverName")
    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
