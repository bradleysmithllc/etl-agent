package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.stream.JsonWriter;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ClientProxyFactory {
	public static InformaticaWebServicesHubClient getInformaticaWebServicesHubClientProxy(final InformaticaWebServicesHub hub, final RuntimeSupport rs) {
		return (InformaticaWebServicesHubClient) Proxy.newProxyInstance
				(InformaticaWebServicesHubClient.class.getClassLoader(),
						new Class[]{InformaticaWebServicesHubClient.class},
						new InvocationHandler() {
							public synchronized Object invoke(Object proxy, Method method,
																	 Object[] args) throws Throwable {
								hub.request();

								// record the method invocation
								String subPath = hub.getInformaticaDomain().getDomainName() + File.separator + hub.getInformaticaRepository() + File.separator + hub.getName();

								record(InformaticaWebServicesHub.class, method, args, rs, hub.getInformaticaDomain().getNumRequestsSent(), hub.getNumRequestsSent(), subPath);

								if (method.getReturnType().isPrimitive()) {
									return new Integer(0);
								} else {
									return null;
								}
							}
						});
	}

	public static InformaticaIntegrationServiceClient getInformaticaIntegrationServiceClientProxy(final InformaticaIntegrationService service, final RuntimeSupport isc) {
		return (InformaticaIntegrationServiceClient) Proxy.newProxyInstance
				(InformaticaIntegrationServiceClient.class.getClassLoader(),
						new Class[]{InformaticaIntegrationServiceClient.class},
						new InvocationHandler() {
							public synchronized Object invoke(Object proxy, Method method,
																	 Object[] args) throws Throwable {
								service.request();

								// record the method invocation
								String subPath = service.getInformaticaDomain().getDomainName() + File.separator + service.getInformaticaRepository().getRepositoryName() + File.separator + service.getIntegrationServiceName();

								record(InformaticaIntegrationService.class, method, args, isc, service.getInformaticaDomain().getNumRequestsSent(), service.getNumRequestsSent(), subPath);

								if (method.getReturnType().isPrimitive()) {
									return new Integer(0);
								} else {
									return null;
								}
							}
						});
	}

	public static InformaticaRepositoryClient getInformaticaRepositoryClientProxy(final InformaticaRepository repository, final RuntimeSupport irc) {
		return (InformaticaRepositoryClient) Proxy.newProxyInstance(
				InformaticaRepositoryClient.class.getClassLoader(),
				new Class[]{InformaticaRepositoryClient.class},
				new InvocationHandler() {
					public synchronized Object invoke(Object proxy, Method method,
															 Object[] args) throws Throwable {
						repository.request();

						// record the method invocation
						String subPath = repository.getInformaticaDomain().getDomainName() + File.separator + repository.getRepositoryName();

						record(InformaticaRepository.class, method, args, irc, repository.getInformaticaDomain().getNumRequestsSent(), repository.getNumRequestsSent(), subPath);

						String key = repository.getQualifiedName() + "." + method.getName();

						if (System.getProperties().containsKey(key))
						{
							return System.getProperties().get(key);
						}
						else
						{
							if (method.getReturnType().isPrimitive()) {
								return new Integer(0);
							} else {
								return null;
							}
						}
					}
				});
	}

	private static void record(Class interfaceClass, Method method, Object[] args, RuntimeSupport irc, int domainRequestNum, int serviceRequestNum, String subPath) {
		File targ = new FileBuilder(irc.getGeneratedSourceDirectory("mockAgent")).subdir(subPath).subdir(interfaceClass.getSimpleName()).mkdirs().name(method.getName() + "_" + domainRequestNum + "_" + serviceRequestNum + ".json").file();

		try {
			BufferedWriter fw = new BufferedWriter(new FileWriter(targ));

			try {
				JsonWriter jwr = new JsonWriter(fw);
				jwr.setIndent("  ");

				jwr.beginObject();
				jwr.name("proxy-class").value(interfaceClass.getSimpleName());
				jwr.name("domain-request-num").value(domainRequestNum);
				jwr.name("service-request-num").value(serviceRequestNum);
				jwr.name("method").value(method.getName());
				jwr.name("arguments");
				jwr.beginArray();

				if (args != null)
				{
					for (Object arg : args) {
						jwr.beginObject();

						// get the type
						String type = "null";
						String strVal = String.valueOf(arg);

						if (arg != null)
						{
							type = arg.getClass().getSimpleName();

							// in this case, check for a file path so the separators can be standardized
							if (arg.getClass() == File.class)
							{
								strVal = ((File) arg).getAbsolutePath().replace(File.separator, "/");
							}
						}

						jwr.name("type").value(type);
						jwr.name("string-value").value(strVal);
						jwr.endObject();
					}
				}

				jwr.endArray();
				jwr.endObject();

				jwr.close();
			} finally {
				fw.close();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}