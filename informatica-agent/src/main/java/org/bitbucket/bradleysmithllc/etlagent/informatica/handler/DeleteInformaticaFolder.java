package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.delete_informatica_folder.DeleteInformaticaFolderRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

public class DeleteInformaticaFolder extends AbstractRequestHandler<DeleteInformaticaFolderRequest, InformaticaResponseDTO>
{
	@Override
	public String getId() {
		return "deleteInformaticaFolder";
	}

	@Override
	public DeleteInformaticaFolderRequest getRequestContainerObject() {
		return new DeleteInformaticaFolderRequest();
	}

	@Override
	public InformaticaResponseDTO process(JsonNode request, final DeleteInformaticaFolderRequest container) throws Exception {
		logger.info("Deleting folder {}", container);

		String domain = container.getInformaticaDomain();

		String repo = container.getInformaticaRepository();

		// resolve to an informatica domain
		InformaticaDomain informaticaDomain = getInformaticaDomain(domain);

		InformaticaRepository informaticaRepository = getInformaticaRepository(informaticaDomain, repo);

		final InformaticaResponseDTO dto = new InformaticaResponseDTO();

		try
		{
			useRepository(informaticaRepository, new RepositoryVisitor() {
				@Override
				public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception {
					String folder = container.getFolder();

					boolean fine = true;

					if (container.getFailIfNotExists() != null)
					{
						fine = container.getFailIfNotExists().booleanValue();
					}

					client.deleteFolder(folder, fine);
				}
			});

			dto.setInformaticaResultCode(InformaticaResponseDTO.result.okay);
		}
		catch(InformaticaDeleteFolderException error)
		{
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.failed);
			dto.setResponseMessage(error.getMessage());
		}
		catch(InformaticaDeadlockException error)
		{
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.deadlock);
			dto.setResponseMessage(error.getMessage());
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_REPOSITORY_DEADLOCK);
		}
		catch(InformaticaError error)
		{
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.failed);
			dto.setResponseMessage(error.getMessage());
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_ERROR);
		}

		return dto;
	}
}