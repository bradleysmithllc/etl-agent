package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ImportWorkflowResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.create_informatica_folder.CreateInformaticaFolderRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

public class CreateInformaticaFolder extends AbstractRequestHandler<CreateInformaticaFolderRequest, InformaticaResponseDTO>
{
	@Override
	public String getId() {
		return "createInformaticaFolder";
	}

	@Override
	public CreateInformaticaFolderRequest getRequestContainerObject() {
		return new CreateInformaticaFolderRequest();
	}

	@Override
	public InformaticaResponseDTO process(JsonNode request, final CreateInformaticaFolderRequest container) throws Exception {
		logger.info("Creating folder {}", container);

		String domain = container.getInformaticaDomain();

		String repo = container.getInformaticaRepository();

		// resolve to an informatica domain
		InformaticaDomain informaticaDomain = getInformaticaDomain(domain);

		InformaticaRepository informaticaRepository = getInformaticaRepository(informaticaDomain, repo);

		try
		{
			useRepository(informaticaRepository, new RepositoryVisitor() {
				@Override
				public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception {
					String folder = container.getFolder();

					client.createFolder(folder);
				}
			});
		}
		catch(InformaticaDeadlockException exc)
		{
			logger.error("Request produced a deadlock.  Passing on to the client.");

			// special case - let's return a retry error
			ImportWorkflowResponseDTO dto = new ImportWorkflowResponseDTO();
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.deadlock);
			dto.setResponseMessage(exc.getMessage());
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_REPOSITORY_DEADLOCK);

			return dto;
		}
		catch(InformaticaCreateFolderException exc)
		{
			InformaticaResponseDTO dto = new InformaticaResponseDTO();
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.failed);
			dto.setResponseMessage(exc.toString());

			return dto;
		}
		catch(InformaticaError exc)
		{
			InformaticaResponseDTO dto = new InformaticaResponseDTO();
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.failed);
			dto.setResponseMessage(exc.toString());
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_ERROR);

			return dto;
		}

		InformaticaResponseDTO dto = new InformaticaResponseDTO();
		dto.setInformaticaResultCode(InformaticaResponseDTO.result.okay);

		return dto;
	}
}