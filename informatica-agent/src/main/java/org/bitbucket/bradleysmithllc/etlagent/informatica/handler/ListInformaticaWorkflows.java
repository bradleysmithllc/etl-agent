package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ListWorkflowsRequestDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ListWorkflowsResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.delete_informatica_folder.DeleteInformaticaFolderRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaDomain;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepository;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaRepositoryClient;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.util.List;

public class ListInformaticaWorkflows extends AbstractRequestHandler<ListWorkflowsRequestDTO, ListWorkflowsResponseDTO>
{
	@Override
	public String getId() {
		return "listInformaticaWorkflows";
	}

	@Override
	public ListWorkflowsRequestDTO getRequestContainerObject() {
		return new ListWorkflowsRequestDTO();
	}

	@Override
	public ListWorkflowsResponseDTO process(JsonNode request, final ListWorkflowsRequestDTO container) throws Exception {
		logger.info("Listing workflows {}", container);

		final ListWorkflowsResponseDTO dto = new ListWorkflowsResponseDTO();

		useRepository(container, new RepositoryVisitor() {
			@Override
			public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception {
				List<String> works = client.listWorkflows(container.getFolder());

				dto.setWorkflows(works);
			}
		});


		return dto;
	}
}