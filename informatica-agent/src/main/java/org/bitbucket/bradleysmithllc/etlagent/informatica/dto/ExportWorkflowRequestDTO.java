package org.bitbucket.bradleysmithllc.etlagent.informatica.dto;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;

public class ExportWorkflowRequestDTO extends BaseRepositoryRequestDTO
{
	private final String folder;
	private final String workflowName;
	private final File target;

	public ExportWorkflowRequestDTO(String folder, String workflowName, File target) {
		this.folder = folder;
		this.workflowName = workflowName;
		this.target = target;
	}

	public String getFolder() {
		return folder;
	}

	public String getWorkflowName() {
		return workflowName;
	}

	public File getTarget() {
		return target;
	}
}