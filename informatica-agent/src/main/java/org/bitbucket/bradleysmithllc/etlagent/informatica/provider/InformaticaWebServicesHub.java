package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.DomainsProperty;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

public class InformaticaWebServicesHub
{
	private final String name;
	private final InformaticaDomain informaticaDomain;
	private final InformaticaRepository informaticaRepository;
	private final String hostName;
	private final int port;
	private final RuntimeSupport runtimeSupport;

	private int numRequestsSent = 0;

	private final InformaticaWebServicesHubClient informaticaWebServicesHubClient;

	public InformaticaWebServicesHub(String name, InformaticaRepository informaticaRepository, String hostName, int port, RuntimeSupport runtimeSupport) {
		this.informaticaRepository = informaticaRepository;
		this.hostName = hostName;
		this.port = port;
		this.name = name;

		informaticaDomain = informaticaRepository.getInformaticaDomain();
		this.runtimeSupport = runtimeSupport;

		if (informaticaDomain.getClientType() == DomainsProperty.ClientType.MOCK)
		{
			// return a logging proxy
			informaticaWebServicesHubClient = ClientProxyFactory.getInformaticaWebServicesHubClientProxy(this, runtimeSupport);
		}
		else
		{
			informaticaWebServicesHubClient = null;
		}
	}

	public InformaticaDomain getInformaticaDomain() {
		return informaticaDomain;
	}

	public InformaticaRepository getInformaticaRepository() {
		return informaticaRepository;
	}

	public String getName() {
		return name;
	}

	public String getQualifiedName()
	{
		return informaticaRepository.getQualifiedName() + "." + getName();
	}

	public String getHostName() {
		return hostName;
	}

	public int getPort() {
		return port;
	}

	public InformaticaWebServicesHubClient getInformaticaWebServicesHubClient(InformaticaIntegrationService target) {
		return newInformaticaWebServicesHubClient(target);
	}

	public InformaticaWebServicesHubClient newInformaticaWebServicesHubClient(InformaticaIntegrationService target) {
		if (informaticaRepository.getIntegrationService(target.getIntegrationServiceName()) == null)
		{
			throw new IllegalArgumentException("Integration service '" + target.getIntegrationServiceName() + "' not found in repository '" + informaticaRepository.getQualifiedName() + "'");
		}

		return new InformaticaWebServicesHubClientImpl(this, target, runtimeSupport);
	}

	void request()
	{
		numRequestsSent++;
		informaticaDomain.request();
		informaticaRepository.request();
	}

	public int getNumRequestsSent() {
		return numRequestsSent;
	}

	@Override
	public String toString() {
		return "InformaticaWebServicesHub{" +
				"name='" + name + '\'' +
				", informaticaDomain=" + informaticaDomain +
				", informaticaRepository=" + informaticaRepository +
				'}';
	}
}