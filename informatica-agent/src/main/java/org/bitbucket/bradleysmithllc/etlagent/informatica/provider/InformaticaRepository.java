package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.client.AgentClient;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.RepositoriesProperty;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.WebServicesHubs;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.WebServicesHubsProperty;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class InformaticaRepository {
	private final String repositoryName;
	private final InformaticaDomain informaticaDomain;

	private int numRequestsSent = 0;

	private final List<InformaticaIntegrationService> integrationServices = new ArrayList<InformaticaIntegrationService>();
	private final RuntimeSupport runtimeSupport;

	private final InformaticaIntegrationService defaultIntegrationService;

	private final List<InformaticaWebServicesHub> informaticaWebServicesHubs = new ArrayList<InformaticaWebServicesHub>();

	private final InformaticaWebServicesHub defaultWebServiceHub;

	private InformaticaRepositoryClient informaticaRepositoryClient;

	public InformaticaRepository(InformaticaDomain domain, String name, RuntimeSupport runtimeSupport, RepositoriesProperty repositoriesProperty) {
		repositoryName = name;
		informaticaDomain = domain;
		this.runtimeSupport = runtimeSupport;


		// load integration services
		List<String> intSvcs = repositoriesProperty.getIntegrationServices();

		if (intSvcs != null) {
			for (String service : intSvcs) {
				if (getIntegrationService(service) != null) {
					throw new IllegalArgumentException("Integration service " + service + " specified more than once");
				}

				getIntegrationServices().add(new InformaticaIntegrationService(service, this, runtimeSupport));
			}
		}

		// load integration services
		WebServicesHubs webSvcs = repositoriesProperty.getWebServicesHubs();

		if (webSvcs != null) {
			for (Map.Entry<String, WebServicesHubsProperty> webSvc : webSvcs.getAdditionalProperties().entrySet()) {
				if (getWebServicesHub(webSvc.getKey()) != null) {
					throw new IllegalArgumentException("Web services hub " + webSvc.getKey() + " specified more than once");
				}

				WebServicesHubsProperty webSvcHubValue = webSvc.getValue();

				// use the connectivity host name if provided
				String hostName = null;

				try {
					hostName = ObjectUtils.firstNonNull(webSvcHubValue.getHostName(), domain.getConnectivityHost(), InetAddress.getLocalHost().getHostAddress());

					} catch (UnknownHostException e) {
						throw new RuntimeException("Could not determine local host name", e);
					}

				Long hostPort = webSvcHubValue.getHostPort();

				if (hostPort == null) {
					hostPort = new Long(7333L);
				}

				getWebServicesHubs().add(new InformaticaWebServicesHub(webSvc.getKey(), this, hostName, hostPort.intValue(), runtimeSupport));
			}
		}

		// go for the defaults
		String defIs = repositoriesProperty.getDefaultIntegrationService();

		if (defIs != null)
		{
			defaultIntegrationService = getIntegrationService(defIs);

			if (defaultIntegrationService == null)
			{
				throw new IllegalArgumentException("Default integration service not found in repository [" + getQualifiedName() + "]: " + defIs);
			}
		}
		else
		{
			defaultIntegrationService = integrationServices.size() > 0 ? integrationServices.get(0) : null;
		}

		String defWs = repositoriesProperty.getDefaultWebServicesHub();

		if (defWs != null)
		{
			defaultWebServiceHub = getWebServicesHub(defWs);

			if (defaultWebServiceHub == null)
			{
				throw new IllegalArgumentException("Default web services hub not found in repository [" + getQualifiedName() + "]: " + defWs);
			}
		}
		else
		{
			defaultWebServiceHub = informaticaWebServicesHubs.size() > 0 ? informaticaWebServicesHubs.get(0) : null;
		}
	}

	public List<InformaticaIntegrationService> getIntegrationServices() {
		return integrationServices;
	}

	public List<InformaticaWebServicesHub> getWebServicesHubs() {
		return informaticaWebServicesHubs;
	}

	public InformaticaIntegrationService getDefaultIntegrationService() {
		return defaultIntegrationService;
	}

	public InformaticaWebServicesHub getDefaultWebServicesHub() {
		return defaultWebServiceHub;
	}

	public String getRepositoryName() {
		return repositoryName;
	}

	public String getQualifiedName() {
		return informaticaDomain.getDomainName() + "." + repositoryName;
	}

	public InformaticaDomain getInformaticaDomain() {
		return informaticaDomain;
	}

	public void dispose() {
		if (informaticaRepositoryClient != null)
		{
			informaticaRepositoryClient.dispose();
		}
	}

	public InformaticaIntegrationService getIntegrationService(String isName) {
		for (InformaticaIntegrationService iis : getIntegrationServices()) {
			if (iis.getIntegrationServiceName().equals(isName)) {
				return iis;
			}
		}

		return null;
	}

	public InformaticaWebServicesHub getWebServicesHub(String isName) {
		for (InformaticaWebServicesHub iis : getWebServicesHubs()) {
			if (iis.getName().equals(isName)) {
				return iis;
			}
		}

		return null;
	}

	public InformaticaRepositoryClient getInformaticaRepositoryClient() {
		// Fake out the client.  This will fail for out of process
		if (
			informaticaRepositoryClient == null
			//&&
			//(
			//	informaticaDomain.getClientType() == DomainsProperty.ClientType.AGENT ||
			//	informaticaDomain.getClientType() == DomainsProperty.ClientType.MOCK
			//)
		)
		{
			informaticaRepositoryClient = newInformaticaRepositoryClient();
		}

		return informaticaRepositoryClient;
	}

	public InformaticaRepositoryClient newInformaticaRepositoryClient() {
		switch (informaticaDomain.getClientType()) {
			case AGENT:
				return new AgentClient(this, runtimeSupport, runtimeSupport.getProjectUID());
			case LOCAL:
				return new InformaticaRepositoryClientImpl(this, null, informaticaDomain.getInformaticaBinDir(), runtimeSupport, runtimeSupport.getApplicationLog());
			case LOCAL_OUT_OF_PROCESS:
				// try this a few times - in case we get the odd connection error
				int tries = 0;
				while(tries++ < 5)
				{
					try {
						return new InformaticaOutOfProcessRepositoryClient(
								this,
								informaticaDomain.getInformaticaBinDir(),
								runtimeSupport
						);
					} catch (Exception e) {
						e.printStackTrace();
						try {
							// pause to allow the problem to pass
							Thread.sleep(800L + (((long) (Math.random() * Long.MAX_VALUE)) % 500L));
						} catch (InterruptedException e1) {
							e1.printStackTrace();
						}
					}
				}

				throw new RuntimeException("Could not connect client");
			case MOCK:
				return ClientProxyFactory.getInformaticaRepositoryClientProxy(this, runtimeSupport);
		}

		throw new Error();
	}

	void request()
	{
		numRequestsSent++;
		informaticaDomain.request();
	}

	public int getNumRequestsSent() {
		return numRequestsSent;
	}

	@Override
	public String toString() {
		return "InformaticaRepository{" +
				"repositoryName='" + repositoryName + '\'' +
				", informaticaDomain=" + informaticaDomain +
				'}';
	}
}