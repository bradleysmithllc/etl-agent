package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public class OracleConnectionDetails
{
	String serverName;
	String instanceName;
	String databaseName;
	String databaseUsername;
	String databasePassword;
	String adDomain;

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public void setInstanceName(String instanceName) {
		this.instanceName = instanceName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public void setDatabaseUsername(String databaseUsername) {
		this.databaseUsername = databaseUsername;
	}

	public void setDatabasePassword(String databasePassword) {
		this.databasePassword = databasePassword;
	}

	public void setAdDomain(String adDomain) {
		this.adDomain = adDomain;
	}

	public String getServerName() {
		return serverName;
	}

	public String getInstanceName() {
		return instanceName;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public String getDatabaseUsername() {
		return databaseUsername;
	}

	public String getDatabasePassword() {
		return databasePassword;
	}

	public String getAdDomain() {
		return adDomain;
	}

	/** Builder methods **/
	public OracleConnectionDetails withServerName(String server)
	{
		serverName = server;

		return this;
	}

	public OracleConnectionDetails withInstanceName(String inst)
	{
		instanceName = inst;

		return this;
	}

	public OracleConnectionDetails withDatabaseName(String name)
	{
		databaseName = name;

		return this;
	}

	public OracleConnectionDetails withDatabaseUserName(String name)
	{
		databaseUsername = name;

		return this;
	}

	public OracleConnectionDetails withDatabasePassword(String pass)
	{
		databasePassword = pass;

		return this;
	}

	public OracleConnectionDetails withAdDomain(String dom)
	{
		adDomain = dom;

		return this;
	}
}