package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface InformaticaRepositoryClient extends InformaticaClient
{
	void createConnection(
			ConnectionDetails details
	) throws Exception, InformaticaError;

	void createConnection(
			String serverName,
			String databaseName,
			String databaseUsername,
			String databasePassword,
			String relationalConnectionName
	) throws Exception, InformaticaError;

	void createFolder(String folder) throws Exception, InformaticaError;
	void createFolder(String folder, boolean temporary) throws Exception, InformaticaError;
	void deleteFolder(String folder) throws Exception, InformaticaError;
	void deleteFolder(String folder, boolean failIfNotExists) throws Exception, InformaticaError;

	void deleteConnection(
			String connectionName
	) throws Exception, InformaticaError;

	void deleteConnection(
			String connectionName,
			boolean failIfNotExists
	) throws Exception, InformaticaError;


	void deleteTemporaryFolders() throws Exception, InformaticaError;

	void exportWorkflowToXml(
			String workFlow,
			String folder,
			File output
	) throws Exception, InformaticaError;

	void exportAllWorkflowsToXml() throws Exception, InformaticaError;

	void importWorkflowFromXml(
			String folderPrefix,
			InformaticaIntegrationService newService,
			File xmlFile,
			List<String> flist
	) throws InformaticaDeadlockException, InformaticaError, IOException;

	void importWorkflowFromXml(
			String folderPrefix,
			InformaticaIntegrationService newService,
			File xml
	) throws InformaticaDeadlockException, InformaticaError, IOException;

	class InformaticaConnection
	{
		private String connectionName;
		private String connectionType;

		public String getConnectionName() {
			return connectionName;
		}

		public void setConnectionName(String connectionName) {
			this.connectionName = connectionName;
		}

		public String getConnectionType() {
			return connectionType;
		}

		public void setConnectionType(String connectionType) {
			this.connectionType = connectionType;
		}
	}

	List<InformaticaConnection> listConnections() throws Exception, InformaticaError;
	List<String> listFolders() throws Exception, InformaticaError;
	List<String> listWorkflows(String folder) throws Exception, InformaticaError;

	/**
	 * Purges all folders which match the names for test folders, E.G., prefixed with '__'.
	 * @return The number of folders removed.
	 * @throws Exception
	 * @throws org.bitbucket.bradleysmithllc.etlunit.InformaticaError
	 */
	int purgeTestFolders() throws Exception, InformaticaError;

	InformaticaRepository getInformaticaRepository();

	/**
	 * Validate that the client is connected.
	 * @return
	 */
	boolean ping();
}