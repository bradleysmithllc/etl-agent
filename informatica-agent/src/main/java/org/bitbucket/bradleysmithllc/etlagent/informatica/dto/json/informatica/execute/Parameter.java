
package org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.execute;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "scope",
		"mapplet-name",
    "parameter-name",
    "parameter-value"
})
public class Parameter {

    @JsonProperty("scope")
    private String scope;
		@JsonProperty("mapplet-name")
		private String mappletName;
    @JsonProperty("parameter-name")
    private String parameterName;
    @JsonProperty("parameter-value")
    private String parameterValue;

    @JsonProperty("scope")
    public String getScope() {
        return scope;
    }

    @JsonProperty("scope")
    public void setScope(String scope) {
        this.scope = scope;
    }

    @JsonProperty("parameter-name")
    public String getParameterName() {
        return parameterName;
    }

    @JsonProperty("parameter-name")
    public void setParameterName(String parameterName) {
        this.parameterName = parameterName;
    }

		@JsonProperty("mapplet-name")
		public String getMappletName() {
			return mappletName;
		}

		@JsonProperty("mapplet-name")
		public void setMappletName(String mappletName) {
			this.mappletName = mappletName;
		}

    @JsonProperty("parameter-value")
    public String getParameterValue() {
        return parameterValue;
    }

    @JsonProperty("parameter-value")
    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
