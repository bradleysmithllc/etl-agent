package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.bitbucket.bradleysmithllc.etlagent.VirtualFiles;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.*;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecuteWorkflowMultipart extends AbstractMultipartHandler<ExecuteWorkflowRequestDTO, ExecuteWorkflowResponseDTO> {
	Map<String, String> velocityContext = new HashMap<String, String>();

	@Override
	public String getId() {
		return "executeWorkflow";
	}

	@Override
	public ExecuteWorkflowRequestDTO getRequestContainerObject() {
		return new ExecuteWorkflowRequestDTO("", "");
	}

	@Override
	public ExecuteWorkflowResponseDTO process(JsonNode request, final VirtualFiles vflRequest, final VirtualFiles vflResponse, final ExecuteWorkflowRequestDTO container) throws Exception {
		logger.info("Executing workflow {}", container);

		final ExecuteWorkflowResponseDTO executeWorkflowResponseDTO = new ExecuteWorkflowResponseDTO();
		executeWorkflowResponseDTO.setInformaticaResultCode(InformaticaResponseDTO.result.okay);

		useExecutionService(container, new IntegrationServiceVisitor() {
			@Override
			public void useClient(InformaticaExecutionClient client, InformaticaDomain informaticaDomain, InformaticaRepository informaticaRepository, InformaticaIntegrationService informaticaIntegrationService, InformaticaWebServicesHub informaticaWebServicesHub, RuntimeSupport runtimeSupport) throws Exception {
				// record choices for domain, repository, integration service and web services hub.
				String path = "[" + informaticaDomain.getDomainName()
						+ "].[" + informaticaRepository.getQualifiedName() + "].["
						+ informaticaIntegrationService.getQualifiedName() + "].["
						+ (informaticaWebServicesHub == null ? "none" : (informaticaWebServicesHub.getQualifiedName()) + "]"
				);

				// store the data
				ContextBase.testContextMap.put("lastRequestDomain", path);

				// prepare the workspace
				prepareWorkspace(informaticaDomain, container, vflRequest);

				ExecuteDetails ed = executeWorkflowResponseDTO.getExecuteDetails();
				ed.setInformaticaIntegrationService(informaticaIntegrationService.getQualifiedName());
				ed.setInformaticaRepository(informaticaRepository.getQualifiedName());
				ed.setInformaticaDomain(informaticaDomain.getDomainName());

				if (informaticaWebServicesHub != null) {
					ed.setInformaticaWebServicesHub(informaticaWebServicesHub.getQualifiedName());
				}

				String tag = container.getFolder() + "." + container.getWorkflowName() + (container.getTask() != null ? ("." + container.getTask()) : "") + (container.getRunInstanceName() != null ? ("." + container.getRunInstanceName()) : "");

				ParameterFile pfile = container.getParameterFile();

				// this needs to be better - not partitioned well enough
				File parmFile = runtimeSupport.createTempFile(tag + "_" + container.getRequestUid() + ".PRM");

				try {
					PrintWriter fw = new PrintWriter(new BufferedWriter(new FileWriter(parmFile)));

					try {
						pfile.write(fw);
					} finally {
						fw.close();
					}

					// rework velocity references
					String parmContents = FileUtils.readFileToString(parmFile);

					parmContents = parmContents.replaceAll("#[\\s]*\\{", "\\$\\{");

					FileUtils.write(parmFile, VelocityUtil.writeTemplate(parmContents, velocityContext));
					// reload to get the updated values
					pfile = new ParameterFile(parmFile);
				} finally {
					parmFile.delete();
				}

				InformaticaExecutionResult execResult;

				if (container.getTask() != null) {
					if (container.getRunInstanceName() != null) {
						execResult = client.executeWorkflowTask(
								container.getWorkflowName(),
								container.getRunInstanceName(),
								container.getTask(),
								container.getFolder(),
								pfile,
								container.getRequestFiles()
						);
					} else {
						execResult = client.executeWorkflowTask(
								container.getWorkflowName(),
								container.getTask(),
								container.getFolder(),
								pfile,
								container.getRequestFiles()
						);
					}
				} else {
					if (container.getRunInstanceName() != null) {
						execResult = client.executeWorkflow(
								container.getWorkflowName(),
								container.getRunInstanceName(),
								container.getFolder(),
								pfile,
								container.getRequestFiles()
						);
					} else {
						execResult = client.executeWorkflow(
								container.getWorkflowName(),
								container.getFolder(),
								pfile,
								container.getRequestFiles()
						);
					}
				}

				if (execResult.isServiceCacheFailure()) {
					logger.error("Cache failure result.");

					executeWorkflowResponseDTO.setResponseMessage(execResult.getFailureExc());
					executeWorkflowResponseDTO.setInformaticaResultCode(InformaticaResponseDTO.result.cacheFailure);

					logger.error("Cache failure result. {}", executeWorkflowResponseDTO);
				} else {
					executeWorkflowResponseDTO.getResponseFiles().putAll(execResult.getWorkSpaceFiles());

					/**
					 * This data is going from the local workspace back to the remote client
					 */
					VFSFileDataHandler fileDataHandler = new VFSFileDataHandler(vflResponse, VFSFileDataHandler.direction.persist);

					try {
						for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> rflist : executeWorkflowResponseDTO.getResponseFiles().entrySet()) {
							for (RemoteFile rf : rflist.getValue()) {
								// assign the data handler
								rf.setFileDataHandler(fileDataHandler);
								rf.persist();
							}
						}
					} finally {
						fileDataHandler.dispose();
					}

					if (execResult.getFailureExc() != null) {
						executeWorkflowResponseDTO.setInformaticaResultCode(InformaticaResponseDTO.result.failed);
						executeWorkflowResponseDTO.setResponseMessage(execResult.getFailureExc());
					}
				}

				final File localizedWorkingRoot = informaticaDomain.getLocalizedWorkingRoot();

				// wipe out the temporary workspace
				new Thread(new Runnable() {
					@Override
					public void run() {
						FileUtils.deleteQuietly(localizedWorkingRoot);
					}
				}).start();
			}
		});

		return executeWorkflowResponseDTO;
	}

	private void prepareWorkspace(InformaticaDomain domain, ExecuteWorkflowRequestDTO container, VirtualFiles vfiles) throws IOException {
		velocityContext.clear();

		/**
		 * This data is going from the remote client to the local workspace
		 */
		VFSFileDataHandler fileDataHandler = new VFSFileDataHandler(vfiles, VFSFileDataHandler.direction.extract);

		try
		{
			for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> rflist : container.getRequestFiles().entrySet())
			{
				for (RemoteFile rf : rflist.getValue())
				{
					// assign the data handler
					rf.setFileDataHandler(fileDataHandler);
					rf.extract();
				}
			}

			RemoteFile.copyRemoteToLocal(container.getRequestFiles(), domain);
		}
		finally
		{
			fileDataHandler.dispose();
		}

		for (RemoteFile.fileType type : RemoteFile.fileType.values()) {
			File targetFiles = domain.getWorkspaceDir(type);
			velocityContext.put(type.name(), targetFiles.getAbsolutePath());
		}
	}

	private static class VFSFileDataHandler implements RemoteFile.FileDataHandler {
		enum direction {persist, extract};

		VirtualFiles.VWriter writer;
		VirtualFiles.VReader reader;

		private final direction _direction;

		public VFSFileDataHandler(VirtualFiles vfs, direction dir) throws IOException {
			_direction = dir;

			if (_direction == direction.extract)
			{
				reader = vfs.getReader();
			}
			else
			{
				writer = vfs.getWriter();
			}
		}

		@Override
		public void extractFileData(RemoteFile rfile) throws IOException {
			if (_direction == direction.persist)
			{
				throw new IllegalArgumentException("Writing");
			}

			File file = ContextBase.runtimeSupport.createAnonymousTempFile();

			rfile.setFile(file);

			reader.extractPath(RemoteFile.getPath(rfile), file);
		}

		@Override
		public void persistFileData(RemoteFile file) throws IOException {
			if (_direction == direction.extract)
			{
				throw new IllegalArgumentException("Reading");
			}

			writer.writePath(RemoteFile.getPath(file), file.getFile());
		}

		public void dispose() throws IOException {
			if (_direction == direction.persist)
			{
				writer.dispose();
			}
			else
			{
				reader.dispose();
			}
		}
	}

	public static String getLastRequestedDomain()
	{
		return ContextBase.testContextMap.get("lastRequestDomain");
	}
}