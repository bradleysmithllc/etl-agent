
package org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.execute;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.Domains;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "context",
    "context-name",
    "default-domain",
    "domains",
    "folder",
    "informatica-domain",
    "informatica-integration-service",
    "informatica-repository",
    "log-name",
    "lookup-files",
    "parameters",
    "run-instance-name",
    "source-files",
    "task",
    "workflow"
})
public class ExecuteRequest {

    @JsonProperty("context")
    @Valid
    private Context context;
    @JsonProperty("context-name")
    private String contextName;
    @JsonProperty("default-domain")
    private String defaultDomain;
    @JsonProperty("domains")
    @Valid
    private Domains domains;
    @JsonProperty("folder")
    private String folder;
    @JsonProperty("informatica-domain")
    private String informaticaDomain;
    @JsonProperty("informatica-integration-service")
    private String informaticaIntegrationService;
		@JsonProperty("informatica-web-services-hub")
		private String informaticaWebServicesHub;
    @JsonProperty("informatica-repository")
    private String informaticaRepository;
    @JsonProperty("log-name")
    private String logName;
    @JsonProperty("lookup-files")
    @Valid
    private LookupFiles lookupFiles;
    @JsonProperty("parameters")
    @Size(min = 1)
    @Valid
    private Set<Parameter> parameters = new HashSet<Parameter>();
    @JsonProperty("run-instance-name")
    private String runInstanceName;
    @JsonProperty("source-files")
    @Valid
    private SourceFiles sourceFiles;
    @JsonProperty("task")
    private String task;
    @JsonProperty("workflow")
    private String workflow;

    @JsonProperty("context")
    public Context getContext() {
        return context;
    }

    @JsonProperty("context")
    public void setContext(Context context) {
        this.context = context;
    }

    @JsonProperty("context-name")
    public String getContextName() {
        return contextName;
    }

    @JsonProperty("context-name")
    public void setContextName(String contextName) {
        this.contextName = contextName;
    }

    @JsonProperty("default-domain")
    public String getDefaultDomain() {
        return defaultDomain;
    }

    @JsonProperty("default-domain")
    public void setDefaultDomain(String defaultDomain) {
        this.defaultDomain = defaultDomain;
    }

    @JsonProperty("domains")
    public Domains getDomains() {
        return domains;
    }

    @JsonProperty("domains")
    public void setDomains(Domains domains) {
        this.domains = domains;
    }

    @JsonProperty("folder")
    public String getFolder() {
        return folder;
    }

    @JsonProperty("folder")
    public void setFolder(String folder) {
        this.folder = folder;
    }

    @JsonProperty("informatica-domain")
    public String getInformaticaDomain() {
        return informaticaDomain;
    }

    @JsonProperty("informatica-domain")
    public void setInformaticaDomain(String informaticaDomain) {
        this.informaticaDomain = informaticaDomain;
    }

    @JsonProperty("informatica-integration-service")
    public String getInformaticaIntegrationService() {
        return informaticaIntegrationService;
    }

    @JsonProperty("informatica-integration-service")
    public void setInformaticaIntegrationService(String informaticaIntegrationService) {
        this.informaticaIntegrationService = informaticaIntegrationService;
    }

		@JsonProperty("informatica-web-services-hub")
		public String getInformaticaWebServicesHub() {
			return informaticaWebServicesHub;
		}

		@JsonProperty("informatica-web-services-hub")
		public void setInformaticaWebServicesHub(String informaticaIntegrationService) {
			this.informaticaWebServicesHub = informaticaIntegrationService;
		}

    @JsonProperty("informatica-repository")
    public String getInformaticaRepository() {
        return informaticaRepository;
    }

    @JsonProperty("informatica-repository")
    public void setInformaticaRepository(String informaticaRepository) {
        this.informaticaRepository = informaticaRepository;
    }

    @JsonProperty("log-name")
    public String getLogName() {
        return logName;
    }

    @JsonProperty("log-name")
    public void setLogName(String logName) {
        this.logName = logName;
    }

    @JsonProperty("lookup-files")
    public LookupFiles getLookupFiles() {
        return lookupFiles;
    }

    @JsonProperty("lookup-files")
    public void setLookupFiles(LookupFiles lookupFiles) {
        this.lookupFiles = lookupFiles;
    }

    @JsonProperty("parameters")
    public Set<Parameter> getParameters() {
        return parameters;
    }

    @JsonProperty("parameters")
    public void setParameters(Set<Parameter> parameters) {
        this.parameters = parameters;
    }

    @JsonProperty("run-instance-name")
    public String getRunInstanceName() {
        return runInstanceName;
    }

    @JsonProperty("run-instance-name")
    public void setRunInstanceName(String runInstanceName) {
        this.runInstanceName = runInstanceName;
    }

    @JsonProperty("source-files")
    public SourceFiles getSourceFiles() {
        return sourceFiles;
    }

    @JsonProperty("source-files")
    public void setSourceFiles(SourceFiles sourceFiles) {
        this.sourceFiles = sourceFiles;
    }

    @JsonProperty("task")
    public String getTask() {
        return task;
    }

    @JsonProperty("task")
    public void setTask(String task) {
        this.task = task;
    }

    @JsonProperty("workflow")
    public String getWorkflow() {
        return workflow;
    }

    @JsonProperty("workflow")
    public void setWorkflow(String workflow) {
        this.workflow = workflow;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

}
