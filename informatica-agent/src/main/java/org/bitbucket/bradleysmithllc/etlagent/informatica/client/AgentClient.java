package org.bitbucket.bradleysmithllc.etlagent.informatica.client;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.agent.AgentConstants;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.*;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.TestExecutionError;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class AgentClient implements InformaticaRepositoryClient, InformaticaIntegrationServiceClient, InformaticaWebServicesHubClient {
	//private final InformaticaAgentClient informaticaAgentClient;

	private final InformaticaIntegrationService informaticaIntegrationService;
	private final RuntimeSupport runtimeSupport;
	private final InformaticaRepository informaticaRepository;

	public AgentClient(InformaticaRepository informaticaRepository, RuntimeSupport runtime, String clientId) {
		this.informaticaRepository = informaticaRepository;
		informaticaIntegrationService = null;
		runtimeSupport = runtime;
	}

	public AgentClient(InformaticaIntegrationService informaticaIntegrationService, RuntimeSupport runtime, String clientId) {
		runtimeSupport = runtime;
		this.informaticaIntegrationService = informaticaIntegrationService;
		this.informaticaRepository = informaticaIntegrationService.getInformaticaRepository();
	}

	@Override
	public InformaticaExecutionResult executeWorkflow(String workFlow, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, null);
	}

	@Override
	public InformaticaExecutionResult executeWorkflow(String workFlow, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, remoteFiles);
	}

	@Override
	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String task, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, null);
	}

	@Override
	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String task, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, remoteFiles);
	}

	@Override
	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String runInstanceName, String task, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, null);
	}

	@Override
	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String runInstanceName, String task, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles) throws Exception {
		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO(folder, workFlow, task, runInstanceName, parmFile);

		request.setInformaticaDomain(informaticaRepository.getInformaticaDomain().getDomainName());
		request.setInformaticaIntegrationService(informaticaIntegrationService.getIntegrationServiceName());
		request.setInformaticaRepository(informaticaRepository.getRepositoryName());

		// if files are posted, pass them on
		if (remoteFiles != null) {
			for (List<RemoteFile> remoteFileList : remoteFiles.values()) {
				for (RemoteFile remoteFile : remoteFileList) {
					request.addRequestFile(remoteFile);
				}
			}
		}

		runtimeSupport.getApplicationLog().info("Execute informatica workflow " + request);

		int tries = 0;

		while (tries++ < 10) {
			InformaticaExecutionResult result = getInformaticaAgentClient().executeWorkflow(request);

			if (result.getResult() != InformaticaResponseDTO.result.cacheFailure) {
				return result;
			}

			runtimeSupport.getApplicationLog().info("Service cache failure.  Retrying.");

			Thread.sleep(2000L);
		}

		throw new InformaticaError("Frickin Informatica service can't cache worth crap.");
	}

	private InformaticaAgentClient getInformaticaAgentClient() {
		InformaticaAgentClient informaticaAgentClient = new InformaticaAgentClient(runtimeSupport.getProjectUID(), informaticaRepository.getInformaticaDomain().getAgentHost(), informaticaRepository.getInformaticaDomain().getAgentPort());
		informaticaAgentClient.receiveRuntimeSupport(runtimeSupport);

		return informaticaAgentClient;
	}

	@Override
	public boolean validateWorkflowExists(String folder, String workflowName) throws Exception {
		return getInformaticaAgentClient().validateWorkflowExists(new ValidateWorkflowExistsRequestDTO(folder, workflowName));
	}

	@Override
	public InformaticaExecutionResult executeWorkflow(String workFlow, String runInstanceName, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, runInstanceName, null, folder, parmFile);
	}

	@Override
	public InformaticaExecutionResult executeWorkflow(String workFlow, String runInstanceName, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles) throws Exception {
		return executeWorkflowTask(workFlow, runInstanceName, null, folder, parmFile, remoteFiles);
	}

	@Override
	public InformaticaIntegrationService getIntegrationService() {
		return informaticaIntegrationService;
	}

	@Override
	public void createConnection(String serverName, String databaseName, String databaseUsername, String databasePassword, String relationalConnectionName) throws Exception, InformaticaError {
		throw new UnsupportedOperationException("Deprecated API");
	}

	@Override
	public void createConnection(
		ConnectionDetails details
	) throws Exception, InformaticaError {
		final CreateConnectionRequestDTO createConnectionRequestDTO = new CreateConnectionRequestDTO(details);

		runtimeSupport.getApplicationLog().info("Create informatica connection " + createConnectionRequestDTO);

		createConnectionRequestDTO.setInformaticaDomain(informaticaRepository.getInformaticaDomain().getDomainName());
		createConnectionRequestDTO.setInformaticaRepository(informaticaRepository.getRepositoryName());

		tryNTimes(new Attempt() {
			@Override
			public int getNumTries() {
				return 10;
			}

			@Override
			public InformaticaResponseDTO attemptIt(InformaticaAgentClient informaticaAgentClient) throws InformaticaError, IOException {
				return informaticaAgentClient.createConnection(createConnectionRequestDTO);
			}

			@Override
			public String getOperation() {
				return "createConnection";
			}
		});
	}

	@Override
	public void createFolder(final String folder) throws Exception, InformaticaError {
		createFolder(folder, false);
	}

	@Override
	public void createFolder(final String folder, boolean temporary) throws Exception, InformaticaError {
		runtimeSupport.getApplicationLog().info("Create informatica folder " + folder);

		tryNTimes(new Attempt() {
			@Override
			public int getNumTries() {
				return 10;
			}

			@Override
			public InformaticaResponseDTO attemptIt(InformaticaAgentClient informaticaAgentClient) throws InformaticaError, IOException {
				CreateFolderRequestDTO createFolderRequestDTO = new CreateFolderRequestDTO(folder);

				createFolderRequestDTO.setInformaticaDomain(informaticaRepository.getInformaticaDomain().getDomainName());
				createFolderRequestDTO.setInformaticaRepository(informaticaRepository.getRepositoryName());

				return informaticaAgentClient.createFolder(createFolderRequestDTO);
			}

			@Override
			public String getOperation() {
				return "createFolder";
			}
		});
	}

	@Override
	public void deleteFolder(String folder) throws Exception, InformaticaError {
		deleteFolder(folder, true);
	}

	@Override
	public void deleteFolder(final String folder, final boolean failIfNotExists) throws Exception, InformaticaError {
		runtimeSupport.getApplicationLog().info("Delete informatica folder " + folder);

		tryNTimes(new Attempt() {
			@Override
			public int getNumTries() {
				return 10;
			}

			@Override
			public InformaticaResponseDTO attemptIt(InformaticaAgentClient informaticaAgentClient) throws InformaticaError, IOException {
				DeleteFolderRequestDTO deleteFolderRequestDTO = new DeleteFolderRequestDTO(folder, failIfNotExists);

				deleteFolderRequestDTO.setInformaticaDomain(informaticaRepository.getInformaticaDomain().getDomainName());
				deleteFolderRequestDTO.setInformaticaRepository(informaticaRepository.getRepositoryName());

				return informaticaAgentClient.deleteFolder(deleteFolderRequestDTO);
			}

			@Override
			public String getOperation() {
				return "createFolder";
			}
		});
	}

	@Override
	public void deleteConnection(String connectionName) throws Exception, InformaticaError {
		deleteConnection(connectionName, true);
	}

	@Override
	public void deleteConnection(String connectionName, boolean failIfNotExists) throws Exception, InformaticaError {
		final DeleteConnectionRequestDTO deleteConnectionRequestDTO = new DeleteConnectionRequestDTO(connectionName, failIfNotExists);

		deleteConnectionRequestDTO.setInformaticaDomain(informaticaRepository.getInformaticaDomain().getDomainName());
		deleteConnectionRequestDTO.setInformaticaRepository(informaticaRepository.getRepositoryName());

		runtimeSupport.getApplicationLog().info("Delete informatica connection " + deleteConnectionRequestDTO);

		tryNTimes(new Attempt() {
			@Override
			public int getNumTries() {
				return 10;
			}

			@Override
			public InformaticaResponseDTO attemptIt(InformaticaAgentClient informaticaAgentClient) throws InformaticaError, IOException {
				return informaticaAgentClient.deleteConnection(deleteConnectionRequestDTO);
			}

			@Override
			public String getOperation() {
				return "deleteConnection";
			}
		});
	}

	@Override
	public void deleteTemporaryFolders() throws Exception, InformaticaError {
		throw new UnsupportedOperationException();
	}

	@Override
	public void exportWorkflowToXml(String workFlow, String folder, File output) throws Exception, InformaticaError {
		runtimeSupport.getApplicationLog().info("Export informatica workflow " + folder + "." + workFlow);

		final ExportWorkflowRequestDTO exportWorkflowRequestDTO = new ExportWorkflowRequestDTO(folder, workFlow, output);

		exportWorkflowRequestDTO.setInformaticaDomain(informaticaRepository.getInformaticaDomain().getDomainName());
		exportWorkflowRequestDTO.setInformaticaRepository(informaticaRepository.getRepositoryName());

		tryNTimes(new Attempt(){
			@Override
			public int getNumTries() {
				return 10;
			}

			@Override
			public InformaticaResponseDTO attemptIt(InformaticaAgentClient informaticaAgentClient) throws InformaticaError, IOException {
				return informaticaAgentClient.exportWorkflow(exportWorkflowRequestDTO);
			}

			@Override
			public String getOperation() {
				return "exportWorkflowToXml";
			}
		});
	}

	@Override
	public void exportAllWorkflowsToXml() throws Exception, InformaticaError {
		throw new UnsupportedOperationException();
	}

	@Override
	public void importWorkflowFromXml(String folderPrefix, InformaticaIntegrationService newService, File xmlFile, List<String> flist) throws InformaticaError, IOException {
		String name = xmlFile.getName();

		if (name.toLowerCase().endsWith(".xml")) {
			name = name.substring(0, name.length() - 4);
		}

		final ImportWorkflowRequestDTO importWorkflowRequestDTO = new ImportWorkflowRequestDTO(folderPrefix, name, xmlFile, flist);

		importWorkflowRequestDTO.setInformaticaDomain(informaticaRepository.getInformaticaDomain().getDomainName());
		importWorkflowRequestDTO.setInformaticaRepository(informaticaRepository.getRepositoryName());
		importWorkflowRequestDTO.setInformaticaIntegrationService(newService.getIntegrationServiceName());

		runtimeSupport.getApplicationLog().info("Import informatica workflow " + importWorkflowRequestDTO);

		tryNTimes(new Attempt() {
			@Override
			public int getNumTries() {
				return 10;
			}

			@Override
			public InformaticaResponseDTO attemptIt(InformaticaAgentClient informaticaAgentClient) throws InformaticaError, IOException {
				return informaticaAgentClient.importWorkflow(importWorkflowRequestDTO);
			}

			@Override
			public String getOperation() {
				return "importWorkflowFromXml";
			}
		});
	}

	@Override
	public void importWorkflowFromXml(String folderPrefix, InformaticaIntegrationService newService, File xml) throws InformaticaDeadlockException, InformaticaError, IOException {
		importWorkflowFromXml(folderPrefix, newService, xml, null);
	}

	@Override
	public List<InformaticaConnection> listConnections() throws Exception, InformaticaError {
		return getInformaticaAgentClient().listConnections(new ListConnectionsRequestDTO());
	}

	@Override
	public List<String> listFolders() throws Exception, InformaticaError {
		return getInformaticaAgentClient().listFolders(new ListFoldersRequestDTO());
	}

	@Override
	public List<String> listWorkflows(String folder) throws Exception, InformaticaError {
		ListWorkflowsRequestDTO dto = new ListWorkflowsRequestDTO(folder);

		return getInformaticaAgentClient().listWorkflows(dto);
	}

	@Override
	public int purgeTestFolders() throws Exception {
		PurgeTestFoldersRequestDTO purgeTestFoldersRequestDTO = new PurgeTestFoldersRequestDTO();

		purgeTestFoldersRequestDTO.setInformaticaDomain(informaticaRepository.getInformaticaDomain().getDomainName());
		purgeTestFoldersRequestDTO.setInformaticaRepository(informaticaRepository.getRepositoryName());

		return getInformaticaAgentClient().purgeTestFolders(purgeTestFoldersRequestDTO);
	}

	@Override
	public InformaticaRepository getInformaticaRepository() {
		return informaticaRepository;
	}

	@Override
	public boolean ping() {
		return true;
	}

	@Override
	public InformaticaDomain getInformaticaDomain() {
		return informaticaRepository.getInformaticaDomain();
	}

	@Override
	public void connect() throws Exception {
		// nothing required here
	}

	@Override
	public void dispose() {
		// nothing
	}

	@Override
	public InformaticaWebServicesHub getWebServicesHub() {
		return null;
	}

	private void tryNTimes(Attempt attempt) throws InformaticaError {
		int numTries = 0;

		while (numTries++ < attempt.getNumTries()) {
			InformaticaResponseDTO dto = null;
			try {
				dto = attempt.attemptIt(getInformaticaAgentClient());
			} catch (IOException e) {
				throw new InformaticaError(e.toString(), AgentConstants.ERR_AGENT_FAILURE);
			}

			// check for agent failure
			if (dto.getResponse() != GenericResponseDTO.response_code.okay)
			{
				throw new InformaticaError("Failure in " + attempt.getOperation() + ": " + dto.getResponseMessage(), AgentConstants.ERR_AGENT_FAILURE);
			}

			switch(dto.getInformaticaResultCode())
			{
				case okay:
					return;
				case failed:
					// No need to retry
					throw new InformaticaError("Failure creating folder: " + dto.getResponseMessage(), dto.getErrorId());
				case cacheFailure:
				case deadlock:
					runtimeSupport.getApplicationLog().severe("Transient failure in repository: " + dto.getInformaticaResultCode());
					// retry after a brief wait
					try {
						Thread.sleep(1000L);
					} catch (InterruptedException e) {
						throw new InformaticaError("Narcoleptic alert.", e);
					}
					break;
			}
		}

		throw new InformaticaError("Failure in " + attempt.getOperation() + " - I tried " + (numTries - 1) + " times.", InformaticaAgentConstants.ERR_INFORMATICA_ERROR);
	}
}