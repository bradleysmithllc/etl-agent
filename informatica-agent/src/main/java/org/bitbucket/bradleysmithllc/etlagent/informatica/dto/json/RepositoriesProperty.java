
package org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.annotation.*;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.annotation.Generated;
import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
		"default-integration-service",
		"default-web-services-hub",
    "integration-services",
    "web-services-hubs"
})
public class RepositoriesProperty {
	@JsonProperty("default-integration-service")
	private String defaultIntegrationService;
	@JsonProperty("default-web-services-hub")
	private String defaultWebServicesHub;

	@JsonProperty("default-integration-service")
	public String getDefaultIntegrationService() {
		return defaultIntegrationService;
	}

	@JsonProperty("default-integration-service")
	public void setDefaultIntegrationService(String defaultIntegrationService) {
		this.defaultIntegrationService = defaultIntegrationService;
	}

	@JsonProperty("default-web-services-hub")
	public String getDefaultWebServicesHub() {
		return defaultWebServicesHub;
	}

	@JsonProperty("default-web-services-hub")
	public void setDefaultWebServicesHub(String defaultWebServicesHub) {
		this.defaultWebServicesHub = defaultWebServicesHub;
	}

    @JsonProperty("integration-services")
    @Size(min = 1)
    private List<String> integrationServices = new ArrayList<String>();
    @JsonProperty("web-services-hubs")
    @Valid
    private WebServicesHubs webServicesHubs;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("integration-services")
    public List<String> getIntegrationServices() {
        return integrationServices;
    }

    @JsonProperty("integration-services")
    public void setIntegrationServices(List<String> integrationServices) {
        this.integrationServices = integrationServices;
    }

    @JsonProperty("web-services-hubs")
    public WebServicesHubs getWebServicesHubs() {
        return webServicesHubs;
    }

    @JsonProperty("web-services-hubs")
    public void setWebServicesHubs(WebServicesHubs webServicesHubs) {
        this.webServicesHubs = webServicesHubs;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object other) {
        return EqualsBuilder.reflectionEquals(this, other);
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperties(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
