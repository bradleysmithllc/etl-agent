package org.bitbucket.bradleysmithllc.etlagent.informatica;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.etlagent.AbstractAgent;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.InformaticaConfiguration;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.JsonFeatureModule;
import org.bitbucket.bradleysmithllc.etlunit.feature.FeatureOperation;
import org.bitbucket.bradleysmithllc.etlunit.feature.ResourceFeatureMetaInfo;

import java.util.*;

public class InformaticaAgent extends AbstractAgent
{
	private JsonNode configurationNode;
	private InformaticaConfiguration informaticaConfiguration;
	private final ResourceFeatureMetaInfo resourceFeatureMetaInfo;

	public InformaticaAgent() {
		resourceFeatureMetaInfo = new ResourceFeatureMetaInfo(new JsonFeatureModule());
	}

	@Override
	public String getId() {
		return "informatica";
	}

	@Override
	public String getVersion() {
		return "1.1";
	}

	@Override
	public JsonNode getConfigurationValidator() {
		return resourceFeatureMetaInfo.getFeatureConfigurationValidatorNode();
	}

	@Override
	public void setAgentConfiguration(JsonNode jsonNode) {
		configurationNode = jsonNode;

		informaticaConfiguration = new InformaticaConfiguration(jsonNode, runtimeSupport, null);
	}

	@Override
	public List<RequestHandler> getRequestHandlers() {
		List<RequestHandler> handlers = new ArrayList<RequestHandler>();

		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		ServiceLoader<InformaticaRequestHandler> sl = ServiceLoader.load(InformaticaRequestHandler.class, cl);

		Iterator<InformaticaRequestHandler> it = sl.iterator();

		while (it.hasNext())
		{
			InformaticaRequestHandler informaticaRequestHandler = it.next();

			Map<String,FeatureOperation> operationMap = resourceFeatureMetaInfo.getExportedOperations();

			if (!operationMap.containsKey(informaticaRequestHandler.getId()))
			{
				throw new IllegalArgumentException("Informatica operation not found: " + informaticaRequestHandler.getId());
			}

			informaticaRequestHandler.setInformaticaConfiguration(informaticaConfiguration);
			informaticaRequestHandler.receiveRuntimeSupport(runtimeSupport);
			informaticaRequestHandler.setFeatureOperation(operationMap.get(informaticaRequestHandler.getId()));

			handlers.add(informaticaRequestHandler);
		}

		return handlers;
	}

	@Override
	public List<MultipartRequestHandler> getMultipartRequestHandlers() {
		List<MultipartRequestHandler> handlers = new ArrayList<MultipartRequestHandler>();

		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		ServiceLoader<InformaticaMultipartRequestHandler> sl = ServiceLoader.load(InformaticaMultipartRequestHandler.class, cl);

		Iterator<InformaticaMultipartRequestHandler> it = sl.iterator();

		while (it.hasNext())
		{
			InformaticaMultipartRequestHandler informaticaRequestHandler = it.next();

			Map<String,FeatureOperation> operationMap = resourceFeatureMetaInfo.getExportedOperations();

			if (!operationMap.containsKey(informaticaRequestHandler.getId()))
			{
				throw new IllegalArgumentException("Informatica operation not found: " + informaticaRequestHandler.getId());
			}

			informaticaRequestHandler.setInformaticaConfiguration(informaticaConfiguration);
			informaticaRequestHandler.receiveRuntimeSupport(runtimeSupport);
			informaticaRequestHandler.setFeatureOperation(operationMap.get(informaticaRequestHandler.getId()));

			handlers.add(informaticaRequestHandler);
		}

		return handlers;
	}
}