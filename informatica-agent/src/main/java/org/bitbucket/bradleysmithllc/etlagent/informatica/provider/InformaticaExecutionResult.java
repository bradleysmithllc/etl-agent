package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;

import java.util.List;
import java.util.Map;

public class InformaticaExecutionResult
{
	private Map<RemoteFile.fileType, List<RemoteFile>> workSpaceFiles;
	private String failureExc;
	private InformaticaResponseDTO.result result;

	private boolean serviceCacheFailure = false;

	public InformaticaExecutionResult(Map<RemoteFile.fileType, List<RemoteFile>> workSpaceFiles, String failureExc, InformaticaResponseDTO.result res) {
		this.workSpaceFiles = workSpaceFiles;
		this.failureExc = failureExc;
		this.result = res;
	}

	public Map<RemoteFile.fileType, List<RemoteFile>> getWorkSpaceFiles() {
		return workSpaceFiles;
	}

	public void setFailureExc(String failureExc) {
		this.failureExc = failureExc;
	}

	public String getFailureExc() {
		return failureExc;
	}

	public boolean isServiceCacheFailure() {
		return serviceCacheFailure;
	}

	public void setServiceCacheFailure(boolean serviceCacheFailure) {
		this.serviceCacheFailure = serviceCacheFailure;
	}

	public InformaticaResponseDTO.result getResult() {
		return result;
	}

	@Override
	public String toString() {
		return "InformaticaExecutionResult{" +
				"failureExc='" + failureExc + '\'' +
				", result=" + result +
				", serviceCacheFailure=" + serviceCacheFailure +
				'}';
	}
}