package org.bitbucket.bradleysmithllc.etlagent.informatica.handler;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.ImportWorkflowResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.informatica.import_workflow_from_xml.ImportWorkflowFromXmlRequest;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;

import java.io.File;
import java.util.List;

public class ImportWorkflowFromXml extends AbstractRequestHandler<ImportWorkflowFromXmlRequest, ImportWorkflowResponseDTO>
{
	@Override
	public String getId() {
		return "importWorkflowFromXml";
	}

	@Override
	public ImportWorkflowFromXmlRequest getRequestContainerObject() {
		return new ImportWorkflowFromXmlRequest();
	}

	@Override
	public ImportWorkflowResponseDTO process(final JsonNode request, final ImportWorkflowFromXmlRequest container) throws Exception {
		logger.info("Importing workflow {}", container.getWorkflowName());

		String domain = container.getInformaticaDomain();

		String repo = container.getInformaticaRepository();

		InformaticaDomain informaticaDomain = getInformaticaDomain(domain);

		InformaticaRepository informaticaRepository = getInformaticaRepository(informaticaDomain, repo);

		try
		{
			useRepository(informaticaRepository, new RepositoryVisitor() {
				@Override
				public void useClient(InformaticaRepositoryClient client, RuntimeSupport runtimeSupport) throws Exception {
					InformaticaDomain informaticaDomain1 = client.getInformaticaDomain();
					InformaticaIntegrationService iintsvc = informaticaDomain1.getDefaultIntegrationService();

					if (container.getInformaticaIntegrationService() != null)
					{
						iintsvc = informaticaDomain1.getIntegrationService(container.getInformaticaIntegrationService());
					}

					if (iintsvc == null)
					{
						throw new IllegalArgumentException("Domain '" + informaticaDomain1.getDomainName() + "' does not contain integration service '" + container.getInformaticaIntegrationService() + "'");
					}

					String fileName = container.getWorkflowName();

					JsonNode requestUID = request.get("request-uid");

					if (requestUID != null && !requestUID.isNull())
					{
						fileName = requestUID.asText() + "_" + fileName;
					}

					File temp = runtimeSupport.createTempFile(fileName);

					FileUtils.writeStringToFile(temp, container.getWorkflowXml());

					List<String> fl = container.getFolderList();

					if (fl != null && fl.size() != 0)
					{
						client.importWorkflowFromXml(
								container.getFolderPrefix(),
								iintsvc,
								temp,
								fl
						);
					}
					else
					{
						client.importWorkflowFromXml(
								container.getFolderPrefix(),
								iintsvc,
								temp
						);
					}

					temp.delete();
				}
			});
		}
		catch(InformaticaDeadlockException exc)
		{
			logger.error("Request produced a deadlock.  Passing on to the client.");

			// special case - let's return a retry error
			ImportWorkflowResponseDTO dto = new ImportWorkflowResponseDTO();
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.deadlock);
			dto.setResponseMessage(exc.getMessage());
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_REPOSITORY_DEADLOCK);

			return dto;
		}
		catch(InformaticaError exc)
		{
			logger.error("Informatica failure.");

			ImportWorkflowResponseDTO dto = new ImportWorkflowResponseDTO();
			dto.setInformaticaResultCode(InformaticaResponseDTO.result.failed);
			dto.setResponseMessage(exc.getMessage());
			dto.setErrorId(InformaticaAgentConstants.ERR_INFORMATICA_ERROR);

			return dto;
		}

		ImportWorkflowResponseDTO dto = new ImportWorkflowResponseDTO();
		dto.setInformaticaResultCode(InformaticaResponseDTO.result.okay);

		return dto;
	}
}