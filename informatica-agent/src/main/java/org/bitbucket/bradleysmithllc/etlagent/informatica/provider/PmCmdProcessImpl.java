package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.bitbucket.bradleysmithllc.etlunit.ProcessDescription;
import org.bitbucket.bradleysmithllc.etlunit.ProcessFacade;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.Expectorator;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class PmCmdProcessImpl
{
	public static final String PMCMD = "pmcmd>";
	public static final String NEWLINE = "\r\n";
	private Expectorator expectorator;
	private final ProcessFacade processFacade;

	public static final long DEFAULT_TIMEOUT = 60000L;

	public PmCmdProcessImpl(
			File pmRep,
			RuntimeSupport runtimeSupport,
			Map<String, String> environment,
			String identifier,
			File workingDir
	) throws Exception {
		ProcessDescription pd = new ProcessDescription(pmRep.getAbsolutePath());
		pd.getEnvironment().putAll(environment);

		File output = new FileBuilder(runtimeSupport.getTempDirectory())
			.subdir("process_log").subdir("informatica").mkdirs().name("pmcmd-" + identifier + ".out").file();
		pd.output(output).workingDirectory(workingDir);

		try {
			processFacade = runtimeSupport.execute(pd);
			processFacade.waitForStreams();

			expectorator = new Expectorator(
					new BufferedReader(processFacade.getReader()),
					processFacade.getWriter());

			// wait for the first pmcmd prompt
			expectorator.expect(PMCMD, DEFAULT_TIMEOUT);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public String send(String line) throws IOException {
		String resp = send(line, DEFAULT_TIMEOUT);

		return resp;
	}

	public synchronized String send(String line, long timeout) throws IOException {
		if (expectorator == null)
		{
			throw new IllegalStateException("Pmcmd client disposed");
		}

		try
		{
			String response = expectorator.sayAndExpect(line + NEWLINE, PMCMD, timeout);

			return response;
		}
		catch (InterruptedException e)
		{
			throw new IllegalStateException("Process timed out:" + line, e);
		}
	}

	public synchronized void dispose() throws IOException
	{
		if (expectorator == null)
		{
			throw new IllegalStateException("Pmcmd client disposed");
		}

		expectorator.say("exit");

		processFacade.kill();

		expectorator = null;
	}
}