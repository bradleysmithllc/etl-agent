package org.bitbucket.bradleysmithllc.etlagent.informatica.client;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.github.fge.jackson.JsonLoader;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.InstanceCreator;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.VirtualFiles;
import org.bitbucket.bradleysmithllc.etlagent.ZipVirtualFilesImpl;
import org.bitbucket.bradleysmithllc.etlagent.client.EtlAgentClient;
import org.bitbucket.bradleysmithllc.etlagent.dto.GenericResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.*;
import org.bitbucket.bradleysmithllc.etlagent.informatica.provider.*;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSection;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ScopeSectionImpl;
import org.bitbucket.bradleysmithllc.webserviceshubclient.regexp.ScopeExpression;

import javax.ws.rs.core.MediaType;
import java.io.*;
import java.lang.reflect.Type;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

public class InformaticaAgentClient extends EtlAgentClient {
	private final Gson gson = new GsonBuilder()
			.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_DASHES)
			.registerTypeAdapter(ScopeSection.class, new InstanceCreator<ScopeSection>() {
				@Override
				public ScopeSection createInstance(Type type) {
					return new ScopeSectionImpl(new ScopeExpression("[Global]"));
				}
			})
			.create();

	public enum multipart_status {none, use, ignore}

	private multipart_status multipart = multipart_status.none;

	public InformaticaAgentClient(String clientId) throws UnknownHostException {
		super(clientId);
	}

	public InformaticaAgentClient(String clientId, int port) throws UnknownHostException {
		super(clientId, port);
	}

	public InformaticaAgentClient(String clientId, String hostName) {
		super(clientId, hostName);
	}

	public InformaticaAgentClient(String clientId, String hostName, int port) {
		super(clientId, hostName, port);
	}

	public int purgeTestFolders(PurgeTestFoldersRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "purgeTestFolders");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		PurgeTestFoldersResponseDTO dto = gson.fromJson(json, PurgeTestFoldersResponseDTO.class);

		if (dto.getResponse() != GenericResponseDTO.response_code.okay) {
			throw new IOException(dto.getResponseMessage());
		}

		return dto.getNumFoldersRemoved();
	}

	public List<String> listFolders(ListFoldersRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "listInformaticaFolders");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		ListFoldersResponseDTO dto = gson.fromJson(json, ListFoldersResponseDTO.class);

		if (dto.getResponse() != GenericResponseDTO.response_code.okay) {
			throw new IOException(dto.getResponseMessage());
		}

		return dto.getFolders();
	}

	public List<InformaticaRepositoryClient.InformaticaConnection> listConnections(ListConnectionsRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "listInformaticaConnections");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		ListConnectionsResponseDTO dto = gson.fromJson(json, ListConnectionsResponseDTO.class);

		if (dto.getResponse() != GenericResponseDTO.response_code.okay) {
			throw new IOException(dto.getResponseMessage());
		}

		return dto.getConnections();
	}

	public List<String> listWorkflows(ListWorkflowsRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "listInformaticaWorkflows");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		ListWorkflowsResponseDTO dto = gson.fromJson(json, ListWorkflowsResponseDTO.class);

		if (dto.getResponse() != GenericResponseDTO.response_code.okay) {
			throw new IOException(dto.getResponseMessage());
		}

		return dto.getWorkflows();
	}

	public InformaticaResponseDTO createFolder(CreateFolderRequestDTO request) throws IOException, InformaticaDeadlockException, InformaticaError {
		WebResource webResource = agentRequest("informatica", "createInformaticaFolder");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		return gson.fromJson(json, InformaticaResponseDTO.class);
	}

	public InformaticaResponseDTO deleteFolder(DeleteFolderRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "deleteInformaticaFolder");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		return gson.fromJson(json, InformaticaResponseDTO.class);
	}

	public InformaticaResponseDTO createConnection(CreateConnectionRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "createInformaticaConnection");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		return gson.fromJson(json, InformaticaResponseDTO.class);
	}

	public InformaticaResponseDTO deleteConnection(DeleteConnectionRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "deleteInformaticaConnection");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		return gson.fromJson(json, InformaticaResponseDTO.class);
	}

	public boolean validateWorkflowExists(ValidateWorkflowExistsRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "validateWorkflowExists");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		ValidateWorkflowExistsResponseDTO dto = gson.fromJson(json, ValidateWorkflowExistsResponseDTO.class);

		if (dto.getResponse() != GenericResponseDTO.response_code.okay) {
			throw new IOException(dto.getResponseMessage());
		}

		return dto.isWorkflowExists();
	}

	public ImportWorkflowResponseDTO importWorkflow(ImportWorkflowRequestDTO request) throws IOException, InformaticaDeadlockException {
		WebResource webResource = agentRequest("informatica", "importWorkflowFromXml");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		return gson.fromJson(json, ImportWorkflowResponseDTO.class);
	}

	public ExportWorkflowResponseDTO exportWorkflow(ExportWorkflowRequestDTO request) throws IOException {
		WebResource webResource = agentRequest("informatica", "exportWorkflowToXml");

		String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, gson.toJson(request));

		ExportWorkflowResponseDTO dto = gson.fromJson(json, ExportWorkflowResponseDTO.class);

		// transfer the response into a file
		FileUtils.write(request.getTarget(), dto.getWorkflowXml());

		return dto;
	}

	public InformaticaExecutionResult executeWorkflow(ExecuteWorkflowRequestDTO request) throws IOException {
		if (multipart == multipart_status.none) {
			try {
				WebResource pingResource = multipartAgentRequest("informatica", "executeWorkflow");

				String pingRes = pingResource.get(String.class);

				if (pingRes != null && pingRes.equals(ContextBase.MULTIPART_OKAY)) {
					multipart = multipart_status.use;
				} else {
					multipart = multipart_status.ignore;
				}
			} catch (UniformInterfaceException uexc) {
				multipart = multipart_status.ignore;
			} catch (ClientHandlerException che) {
				multipart = multipart_status.ignore;
			}
		}

		ExecuteWorkflowResponseDTO dto = null;

		if (multipart == multipart_status.use) {
			// use the multipart interface
			WebResource webResource = multipartAgentRequest("informatica", "executeWorkflow");

			String requestJson = gson.toJson(request);

			File zfile = runtimeSupport.createAnonymousTempFile("informaticaAgentRequests", "req.zip");

			// I should know better than to create classes with the same name as apache classes . . .
			// use a common prefix for all files
			String base = org.bitbucket.bradleysmithllc.etlunit.util.IOUtils.removeExtension(zfile.getName());

			VirtualFiles vfiles = new ZipVirtualFilesImpl(zfile, runtimeSupport.createTempFolder(base + "_req"));

			final VirtualFiles.VWriter writer = vfiles.getWriter();
			writer.writeRequestNode(JsonLoader.fromString(requestJson));

			// populate the remote files here
			Map<RemoteFile.fileType, List<RemoteFile>> rfm = request.getRequestFiles();

			for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> fEntry : rfm.entrySet()) {
				for (RemoteFile rf : fEntry.getValue()) {
					rf.setFileDataHandler(new RemoteFile.FileDataHandler() {
						@Override
						public void extractFileData(RemoteFile file) throws IOException {
						}

						@Override
						public void persistFileData(RemoteFile file) throws IOException {
							String path = RemoteFile.getPath(file);

							writer.writePath(path, file.getFile());
						}
					});

					rf.persist();
				}
			}

			writer.dispose();
			writer._package();

			File reszfile = runtimeSupport.createTempFile("informaticaAgentResponses",
					base + ".res.zip"
			);

			BufferedInputStream requestEntity = new BufferedInputStream(new FileInputStream(zfile));
			try {
				InputStream stream = webResource.accept(MediaType.APPLICATION_OCTET_STREAM).post(InputStream.class, requestEntity);
				FileUtils.copyInputStreamToFile(stream, reszfile);
			} finally {
				requestEntity.close();
			}

			VirtualFiles vfilesres = new ZipVirtualFilesImpl(reszfile, runtimeSupport.createTempFolder(base + "_res"));

			final VirtualFiles.VReader reader = vfilesres.getReader();

			try {
				dto = gson.fromJson(reader.getRequestNode().toString(), ExecuteWorkflowResponseDTO.class);

				// populate the remote files to local here
				rfm = dto.getResponseFiles();

				for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> fEntry : rfm.entrySet()) {
					for (RemoteFile rf : fEntry.getValue()) {
						rf.setFileDataHandler(new RemoteFile.FileDataHandler() {
							@Override
							public void extractFileData(RemoteFile file) throws IOException {
								String path = RemoteFile.getPath(file);

								File tmp = runtimeSupport.createAnonymousTempFile();

								reader.extractPath(path, tmp);
								file.setFile(tmp);
							}

							@Override
							public void persistFileData(RemoteFile file) throws IOException {
							}
						});
						rf.extract();
					}
				}
			} finally {
				reader.dispose();
			}
		} else {
			// use the old interface
			WebResource webResource = agentRequest("informatica", "executeWorkflow");

			// before passing on - each remote file has to be serialized using the json container
			Map<RemoteFile.fileType, List<RemoteFile>> rfm = request.getRequestFiles();

			for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> fEntry : rfm.entrySet()) {
				for (RemoteFile rf : fEntry.getValue()) {
					rf.setFileContents(FileUtils.readFileToString(rf.getFile()));
				}
			}

			String requestEntity = gson.toJson(request);
			File zfile = runtimeSupport.createAnonymousTempFile("informaticaAgentRequests", "json");
			FileUtils.write(zfile, requestEntity);

			String json = webResource.accept(MediaType.APPLICATION_JSON_TYPE).post(String.class, requestEntity);

			dto = gson.fromJson(json, ExecuteWorkflowResponseDTO.class);

			// use the same root file name as the request
			zfile = runtimeSupport.createTempFile(
					"informaticaAgentResponses",
					org.bitbucket.bradleysmithllc.etlunit.util.IOUtils.removeExtension(zfile) + ".json"
			);
			FileUtils.write(zfile, requestEntity);

			// reverse the process here - deserialize
			rfm = dto.getResponseFiles();

			for (Map.Entry<RemoteFile.fileType, List<RemoteFile>> fEntry : rfm.entrySet()) {
				for (RemoteFile rf : fEntry.getValue()) {
					File tf = runtimeSupport.createAnonymousTempFile();
					FileUtils.write(tf, rf.getFileContents());
					rf.setFile(tf);
				}
			}
		}

		String failure = null;

		if (dto.getInformaticaResultCode() != InformaticaResponseDTO.result.okay) {
			failure = dto.getResponseMessage();
		}

		InformaticaExecutionResult informaticaExecutionResult = new InformaticaExecutionResult(dto.getResponseFiles(), failure, dto.getInformaticaResultCode());
		informaticaExecutionResult.setServiceCacheFailure(dto.getInformaticaResultCode() == InformaticaResponseDTO.result.cacheFailure);

		if (informaticaExecutionResult.isServiceCacheFailure())
		{
			runtimeSupport.getApplicationLog().info("Is Cache Failure: " + informaticaExecutionResult);
		}

		return informaticaExecutionResult;
	}

	public static void main(String[] argv) throws Exception {
		InformaticaAgentClient informaticaAgentClient = new InformaticaAgentClient("smoketest", "etlqa02");
		informaticaAgentClient.purgeTestFolders(new PurgeTestFoldersRequestDTO());
		informaticaAgentClient.deleteFolder(new DeleteFolderRequestDTO("__bsmithvm_test_SHARED_EDW", false));
		informaticaAgentClient.createFolder(new CreateFolderRequestDTO("__bsmithvm_test_SHARED_EDW"));

		File src = new File("/Users/bsmith/git/etl-unit/integration/informatica-integration-test/src/main/informatica/SHARED_EDW/wkf_INT_CONVERSION_TEST.xml");
		File tgt = new File("wkf_INT_CONVERSION_TEST.xml");

		informaticaAgentClient.importWorkflow(new ImportWorkflowRequestDTO("__bsmithvm_test", "wkf_INT_CONVERSION_TEST", src));

		informaticaAgentClient.exportWorkflow(new ExportWorkflowRequestDTO("__bsmithvm_test_SHARED_EDW", "wkf_INT_CONVERSION_TEST", tgt));

		final String[][] parameters =
				{
						new String[]{"$BadFileName1", "crap"},
						new String[]{"$InputFile1", "inputfile"},
						new String[]{"$OutputFile1", "outputfile"},
						new String[]{"$PMTargetFileDir", "#{targetFiles}"},
						new String[]{"$PMSourceFileDir", "#{sourceFiles}"},
						new String[]{"$PMLookupFileDir", "#{lookupFiles}"},
						new String[]{"$PMBadFileDir", "#{badFiles}"},
						new String[]{"$PMSessionLogDir", "#{sessionLog}"},
						new String[]{"$PMWorkflowLogDir", "#{workflowLog}"},
						new String[]{"$PMSessionLogFile", "s_m_int_conversion_test.log"}
				};

		ParameterFile pf = new ParameterFile();

		ScopeSection ss = pf.appendSection("[Global]");

		for (String[] params : parameters) {
			ss.appendParameter(params[0], params[1]);
		}

		ExecuteWorkflowRequestDTO request = new ExecuteWorkflowRequestDTO("__bsmithvm_test_SHARED_EDW", "wkf_INT_CONVERSION_TEST", pf);

		File source1 = new File("source1");
		FileUtils.write(source1, "source1");
		File source2 = new File("source2");
		FileUtils.write(source2, "source2");

		File lookup1 = new File("lookup1");
		FileUtils.write(lookup1, "lookup1");
		File lookup2 = new File("lookup2");
		FileUtils.write(lookup2, "lookup2");

		request.addRequestFile(RemoteFile.newRemoteFile(source1, RemoteFile.fileType.sourceFiles));
		request.addRequestFile(RemoteFile.newRemoteFile(source2, RemoteFile.fileType.sourceFiles));
		request.addRequestFile(RemoteFile.newRemoteFile(lookup1, RemoteFile.fileType.lookupFiles));
		request.addRequestFile(RemoteFile.newRemoteFile(lookup2, RemoteFile.fileType.lookupFiles));

		source2 = new File("inputfile");
		FileUtils.write(source2, "Ordermatic|1D6|x|x|x|x\n" +
				"Ordermatic|0.01|x|x|x|x\n" +
				"Ordermatic|1.045|x|x|x|x\n" +
				"Ordermatic|1.033335|x|x|x|x\n" +
				"Ordermatic|-1D6|x|x|x|x\n" +
				"Ordermatic|-0.01|x|x|x|x\n" +
				"Ordermatic|-1.045|x|x|x|x\n" +
				"Ordermatic|-1.033335|x|x|x|x\n" +
				"SONIC NT|1D6|1D6|1D6|1D6|1D6\n" +
				"SONIC NT|0.01|0.01|0.01|0.01|0.01\n" +
				"SONIC NT|1.045|1.045|1.045|1.045|1.045\n" +
				"SONIC NT|1.033335|x|x|x|x\n" +
				"SONIC NT|-1D6|1D6|1D6|1D6|1D6\n" +
				"SONIC NT|-0.01|0.01|0.01|0.01|0.01\n" +
				"SONIC NT|-1.045|1.045|1.045|1.045|1.045\n" +
				"SONIC NT|-1.033335|x|x|x|x\n" +
				"Ordermatic|1234567890|x123|1.23||1 11\n" +
				"Ordermatic|100000|x|x|x|x\n" +
				"Ordermatic|x|100000|x|x|x\n" +
				"Ordermatic|x|x|100000|x|x\n" +
				"Ordermatic|x|x|x|100000|x\n" +
				"Ordermatic|x|x|x|x|100000\n" +
				"Ordermatic|10000|x|x|x|x\n" +
				"Ordermatic|x|10000|x|x|x\n" +
				"Ordermatic|x|x|10000|x|x\n" +
				"Ordermatic|x|x|x|10000|x\n" +
				"Ordermatic|x|x|x|x|10000\n" +
				"SONIC NT|100000|x|x|x|x\n" +
				"SONIC NT|x|100000|x|x|x\n" +
				"SONIC NT|x|x|100000|x|x\n" +
				"SONIC NT|x|x|x|100000|x\n" +
				"SONIC NT|x|x|x|x|100000\n" +
				"SONIC NT|10000|x|x|x|x\n" +
				"SONIC NT|x|10000|x|x|x\n" +
				"SONIC NT|x|x|10000|x|x\n" +
				"SONIC NT|x|x|x|10000|x\n" +
				"SONIC NT|x|x|x|x|10000\n" +
				"SONIC NT|1}|1}|1}|1}|1}\n" +
				"SONIC NT|1J|1J|1J|1J|1J\n" +
				"SONIC NT|1K|1K|1K|1K|1K\n" +
				"SONIC NT|1L|1L|1L|1L|1L\n" +
				"SONIC NT|1M|1M|1M|1M|1M\n" +
				"SONIC NT|1N|1N|1N|1N|1N\n" +
				"SONIC NT|1O|1O|1O|1O|1O\n" +
				"SONIC NT|1P|1P|1P|1P|1P\n" +
				"SONIC NT|1Q|1Q|1Q|1Q|1Q\n" +
				"SONIC NT|1R|1R|1R|1R|1R\n" +
				"SONIC NT|0|x|x|x|x\n" +
				"SONIC NT|x|0|x|x|x\n" +
				"SONIC NT|x|x|0|x|x\n" +
				"SONIC NT|x|x|x|0|x\n" +
				"SONIC NT|x|x|x|x|0\n" +
				"Ordermatic|0|x|x|x|x\n" +
				"Ordermatic|x|0|x|x|x\n" +
				"Ordermatic|x|x|0|x|x\n" +
				"Ordermatic|x|x|x|0|x\n" +
				"Ordermatic|x|x|x|x|0\n" +
				"SONIC NT|5|05|005|0005|00005\n" +
				"SONIC NT|50|050|0050|00050|000050\n" +
				"SONIC NT|500|0500|00500|000500|0000500\n" +
				"Ordermatic|5|05|005|0005|00005\n" +
				"Ordermatic|50|050|0050|00050|000050\n" +
				"Ordermatic|500|0500|00500|000500|0000500\n");
		request.addRequestFile(RemoteFile.newRemoteFile(source2, RemoteFile.fileType.sourceFiles));

		informaticaAgentClient.executeWorkflow(request);

		String srcText = FileUtils.readFileToString(src);
		String tgtText = FileUtils.readFileToString(tgt);

		informaticaAgentClient.deleteFolder(new DeleteFolderRequestDTO("__bsmithvm_test_SHARED_EDW", true));

		informaticaAgentClient.deleteConnection(new DeleteConnectionRequestDTO("test", false));
		informaticaAgentClient.createConnection(new CreateConnectionRequestDTO(
				new ConnectionDetails("test", new SqlServerConnectionDetails().withServerName("server").withDatabaseName("database")
				.withDatabaseUserName("user").withDatabasePassword("password")))
		);
		informaticaAgentClient.deleteConnection(new DeleteConnectionRequestDTO("test", true));
	}

	public multipart_status getMultipart() {
		return multipart;
	}

	public void setMultipart(multipart_status multipart) {
		this.multipart = multipart;
	}
}