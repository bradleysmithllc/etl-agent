package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.gson.stream.JsonWriter;
import com.informatica.wsh.Fault;
import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.InformaticaResponseDTO;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.RemoteFile;
import org.bitbucket.bradleysmithllc.etlagent.informatica.regexp.InfaIntSvcFolderNotFoundExpression;
import org.bitbucket.bradleysmithllc.etlagent.informatica.regexp.InfaWorkflowNotFoundInFolderExpression;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.webserviceshubclient.GetWorkflowDetails;
import org.bitbucket.bradleysmithllc.webserviceshubclient.StartWorkflow;
import org.bitbucket.bradleysmithllc.webserviceshubclient.parameter.ParameterFile;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.util.*;

public class InformaticaWebServicesHubClientImpl implements InformaticaWebServicesHubClient {
	private final InformaticaWebServicesHub informaticaWebServicesHub;
	private final InformaticaIntegrationService informaticaIntegrationService;
	private final RuntimeSupport runtimeSupport;

	public InformaticaWebServicesHubClientImpl(InformaticaWebServicesHub informaticaWebServicesHub, InformaticaIntegrationService informaticaIntegrationService, RuntimeSupport runtimeSupport) {
		this.informaticaWebServicesHub = informaticaWebServicesHub;
		this.informaticaIntegrationService = informaticaIntegrationService;
		this.runtimeSupport = runtimeSupport;
	}

	@Override
	public InformaticaWebServicesHub getWebServicesHub() {
		return informaticaWebServicesHub;
	}

	@Override
	public InformaticaDomain getInformaticaDomain() {
		return informaticaWebServicesHub.getInformaticaDomain();
	}

	@Override
	public void connect() throws Exception {
	}

	@Override
	public void dispose() {
	}

	@Override
	public InformaticaExecutionResult executeWorkflow(String workFlow, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, null);
	}

	@Override
	public InformaticaExecutionResult executeWorkflow(String workFlow, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, remoteFiles);
	}

	@Override
	public InformaticaExecutionResult executeWorkflow(String workFlow, String runInstanceName, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, null, folder, parmFile, null);
	}

	@Override
	public InformaticaExecutionResult executeWorkflow(String workFlow, String runInstanceName, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles) throws Exception {
		return executeWorkflowTask(workFlow, runInstanceName, null, folder, parmFile, remoteFiles);
	}

	@Override
	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String task, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, null);
	}

	@Override
	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String task, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles) throws Exception {
		return executeWorkflowTask(workFlow, null, task, folder, parmFile, remoteFiles);
	}

	@Override
	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String runInstanceName, String task, String folder, ParameterFile parmFile) throws Exception {
		return executeWorkflowTask(workFlow, runInstanceName, task, folder, parmFile, null);
	}

	/**
	 * This method expects the remote files to be populated at the beginning of the method,
	 * and the remote files coming out will contain file pointers.
	 *
	 * @param workFlow
	 * @param runInstanceName
	 * @param task
	 * @param folder
	 * @param parmFile
	 * @param remoteFiles
	 * @return
	 * @throws Exception
	 */
	@Override
	public InformaticaExecutionResult executeWorkflowTask(String workFlow, String runInstanceName, String task, String folder, ParameterFile parmFile, Map<RemoteFile.fileType, List<RemoteFile>> remoteFiles) throws Exception {
		if (remoteFiles != null) {
			// lay out the input files
			RemoteFile.copyRemoteToLocal(remoteFiles, getInformaticaDomain());
		}

		List<String> args = makeBaseArgs(folder, workFlow);

		args.add("--debug");

		if (runInstanceName != null) {
			args.add("--run-instance");
			args.add(runInstanceName);
		}

		String uuid = UUID.randomUUID().toString();

		File par = new File(
				getInformaticaDomain().getParameterFilesDir(),
				uuid + ".PRM"
		);

		parmFile.write(par);

		args.add("--parameter-file-path");
		args.add(par.getAbsolutePath());

		if (task != null) {
			args.add("--task-instance-path");
			args.add(task);
		}

		// save the web service call into a json file
		File wshRoot = new FileBuilder(runtimeSupport.getGeneratedSourceDirectory("web-services-hub")).subdir(informaticaWebServicesHub.getName()).name(uuid + ".json").file();

		StringWriter stw = new StringWriter();
		JsonWriter jwr = new JsonWriter(stw);

		try {
			jwr.beginObject();
			jwr.name("jcmd-arguments");
			jwr.beginArray();

			for (String arg : args) {
				jwr.value(arg);
			}

			jwr.endArray();
			jwr.endObject();
		} finally {
			jwr.close();
		}

		FileUtils.write(wshRoot, stw.toString());

		Map<RemoteFile.fileType, List<RemoteFile>> xfiles = new HashMap<RemoteFile.fileType, List<RemoteFile>>();
		String excStr = null;

		try {
			if (!ContextBase.agentTestMode()) {
				StartWorkflow jcmd = new StartWorkflow();

				jcmd.mainWithExceptions(args.toArray(new String[args.size()]));
			}
		} catch (Fault fault) {
			excStr = fault.toString();

			// check for the folder not found crap
			InfaIntSvcFolderNotFoundExpression intexp = new InfaIntSvcFolderNotFoundExpression(excStr);
			InfaWorkflowNotFoundInFolderExpression intworknot = new InfaWorkflowNotFoundInFolderExpression(excStr);

			if (intexp.hasNext() || intworknot.hasNext()) {
				// special case - return an out of date result so the client can retry
				InformaticaExecutionResult informaticaExecutionResult = new InformaticaExecutionResult(xfiles, excStr, InformaticaResponseDTO.result.cacheFailure);
				informaticaExecutionResult.setFailureExc(excStr);
				informaticaExecutionResult.setServiceCacheFailure(true);
				return informaticaExecutionResult;
			}
		} catch (IOException exc) {
			StringWriter stringWriter = new StringWriter();
			PrintWriter pw = new PrintWriter(stringWriter);

			exc.printStackTrace(pw);

			excStr = stringWriter.toString();

			runtimeSupport.getApplicationLog().severe("Web Services Hub IOException", exc);

			return new InformaticaExecutionResult(xfiles, excStr, InformaticaResponseDTO.result.failed);
		} catch (Exception exc) {
			StringWriter stringWriter = new StringWriter();
			PrintWriter pw = new PrintWriter(stringWriter);

			exc.printStackTrace(pw);

			excStr = stringWriter.toString();

			// check for the folder not found crap
			InfaIntSvcFolderNotFoundExpression intexp = new InfaIntSvcFolderNotFoundExpression(excStr);
			InfaWorkflowNotFoundInFolderExpression intworknot = new InfaWorkflowNotFoundInFolderExpression(excStr);

			boolean intexpb = intexp.hasNext();
			boolean intworknotb = intworknot.hasNext();

			runtimeSupport.getApplicationLog().info(excStr + "[" + intexpb + "][" + intworknotb + "]");

			if (intexpb || intworknotb) {
				// special case - return an out of date result so the client can retry
				InformaticaExecutionResult informaticaExecutionResult = new InformaticaExecutionResult(xfiles, excStr, InformaticaResponseDTO.result.cacheFailure);
				informaticaExecutionResult.setFailureExc(excStr);
				informaticaExecutionResult.setServiceCacheFailure(true);
				return informaticaExecutionResult;
			} else {
				runtimeSupport.getApplicationLog().severe("", exc);
			}
		} finally {
			RemoteFile.copyLocalToRemote(getInformaticaDomain(), xfiles);
		}

		return new InformaticaExecutionResult(xfiles, excStr, InformaticaResponseDTO.result.okay);
	}

	private List<String> makeBaseArgs(String folder, String workflow) {
		List<String> args = new ArrayList<String>();

		args.add("--verbose");

		if (getInformaticaDomain().getSecurityDomain() != null) {
			args.add("--security-domain");
			args.add(getInformaticaDomain().getSecurityDomain());
		}

		args.add("--domain-name");
		args.add(getInformaticaDomain().getDomainName());

		args.add("--repository-name");
		args.add(informaticaWebServicesHub.getInformaticaRepository().getRepositoryName());

		args.add("--integration-service");
		args.add(informaticaIntegrationService.getIntegrationServiceName());

		args.add("--user-name");
		args.add(getInformaticaDomain().getUserName());

		args.add("--password");
		String password = getInformaticaDomain().getPassword();

		if (password == null) {
			throw new IllegalArgumentException("Web services hub requires a plain-text password");
		}

		args.add(password);

		args.add("--wsdl-url");
		args.add("http://" + informaticaWebServicesHub.getHostName() + ":" + informaticaWebServicesHub.getPort() + "/wsh/services/BatchServices/DataIntegration?WSDL");

		args.add("--folder");
		args.add(folder);

		args.add("--workflow");
		args.add(workflow);

		return args;
	}

	@Override
	public boolean validateWorkflowExists(String folder, String workflowName) {
		List<String> args = makeBaseArgs(folder, workflowName);

		try {
			GetWorkflowDetails.mainWithReturn(args.toArray(new String[args.size()]));
		} catch (Exception exc) {
			runtimeSupport.getApplicationLog().severe("Workflow not available", exc);
			return false;
		}

		// if we are here, the call succeeded.
		return true;
	}
}