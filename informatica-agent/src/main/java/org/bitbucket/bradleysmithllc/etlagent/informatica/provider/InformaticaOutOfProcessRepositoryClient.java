package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.informatica.regexp.*;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.etlunit.InformaticaError;
import org.bitbucket.bradleysmithllc.etlunit.Log;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.etlunit.io.FileBuilder;
import org.bitbucket.bradleysmithllc.etlunit.util.IOUtils;
import org.bitbucket.bradleysmithllc.etlunit.util.Incomplete;
import org.bitbucket.bradleysmithllc.etlunit.util.VelocityUtil;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;

public final class InformaticaOutOfProcessRepositoryClient implements InformaticaRepositoryClient {
	enum result {
		success,
		failure,
		failure_retriable
	}

	protected final Log applicationLog;

	public static final String FOLDER_LIST_ATTRIBUTE = "folderList";
	private final List<String> temporaryFolders = new ArrayList();

	private final InformaticaDomain informaticaDomain;

	private final InformaticaRepository informaticaRepository;
	private final RuntimeSupport runtimeSupport;

	private static int processNum = 0;

	private final int thisProcessNum = nextProcessNum();

	private final boolean testMode = ContextBase.agentTestMode();

	private static synchronized int nextProcessNum() {
		return processNum++;
	}

	private final PmRepProcess pmRep;

	public InformaticaOutOfProcessRepositoryClient(
			InformaticaRepository informaticaRepository,
			PmRepProcess pmRepProcess,
			RuntimeSupport pRuntimeSupport
	) throws Exception {
		applicationLog = pRuntimeSupport.getApplicationLog();
		this.informaticaRepository = informaticaRepository;
		this.informaticaDomain = informaticaRepository.getInformaticaDomain();
		runtimeSupport = pRuntimeSupport;

		Map<String, String> env = new HashMap<String, String>();

		env.put("INFA_DOMAINS_FILE", informaticaDomain.getDomainFile().getAbsolutePath());
		env.put("INFAPASSWD", informaticaDomain.getPasswordEncrypted());

		String pmRepId = informaticaDomain.getDomainName() + "." + informaticaRepository.getRepositoryName() + "." + thisProcessNum;
		File workingPmrep = new FileBuilder(runtimeSupport.getReportDirectory("log")).subdir("informatica").subdir("pmrepWorking." + pmRepId).mkdirs().file();

		// make sure the directory is empty
		FileUtils.cleanDirectory(workingPmrep);

		pmRep = pmRepProcess;

		connect();
	}

	public InformaticaOutOfProcessRepositoryClient(
			InformaticaRepository informaticaRepository,
			File pmCmdDir,
			RuntimeSupport pRuntimeSupport
	) throws Exception {
		applicationLog = pRuntimeSupport.getApplicationLog();
		this.informaticaRepository = informaticaRepository;
		this.informaticaDomain = informaticaRepository.getInformaticaDomain();
		runtimeSupport = pRuntimeSupport;

		Map<String, String> env = new HashMap<String, String>();

		env.put("INFA_DOMAINS_FILE", informaticaDomain.getDomainFile().getAbsolutePath());
		env.put("INFAPASSWD", informaticaDomain.getPasswordEncrypted());
		env.put("INFA_CLIENT_RESILIENCE_TIMEOUT", "30");

		String pmRepId = informaticaDomain.getDomainName() + "." + informaticaRepository.getRepositoryName() + "." + thisProcessNum;
		File workingPmrep = new FileBuilder(runtimeSupport.getReportDirectory("log")).subdir("informatica").subdir("pmrepWorking." + pmRepId).mkdirs().file();

		// make sure the directory is empty
		FileUtils.cleanDirectory(workingPmrep);

		pmRep = new PmRepProcessImpl(new File(pmCmdDir, "pmrep"), runtimeSupport, env, pmRepId, workingPmrep);

		connect();
	}

	public final void connect() throws Exception {
		String res = pmRep.send("connect -r " + informaticaRepository.getRepositoryName() + " -n " + informaticaDomain.getUserName() + " -X INFAPASSWD -d " + informaticaDomain.getDomainName());

		InfaConnectResultExpression icre = new InfaConnectResultExpression(res);

		if (!icre.hasNext()) {
			throw new InformaticaError("Could not connect: " + res);
		}
	}

	@Incomplete
	@Override
	public void createConnection(
			String serverName,
			String databaseName,
			String databaseUsername,
			String databasePassword,
			String relationalConnectionName
	) throws Exception, InformaticaError {
		String command = new StringBuilder("createconnection ")
				.append("-s ")
				.append("\"Microsoft SQL Server\" ")
				.append("-n ")
				.append(relationalConnectionName)
				.append(" -u ")
				.append(databaseUsername)
				.append(" -p ")
				.append(databasePassword)
				.append(" -v ")
				.append(serverName)
				.append(" -b ")
				.append(databaseName)
				.append(" -e")
				.append(" \"SET QUOTED_IDENTIFIER ON\"")
				.append(" -l")
				.append(" MS1252").toString();

		String outBuff = pmRep.send(command);

		InfaCreateConnectionSuccessExpression icsre = InfaCreateConnectionSuccessExpression.match(outBuff);

		if (icsre.hasNext()) {
			return;
		}

		InfaCreateConnectionExistsExpression iccre = InfaCreateConnectionExistsExpression.match(outBuff);

		if (iccre.hasNext()) {
			throw new InformaticaCreateConnectionExistsException("Connection exists " + outBuff);
		}

		throw new InformaticaError("Failure creating connection: " + outBuff);
	}

	@Incomplete
	@Override
	public void createConnection(
			ConnectionDetails details
	) throws Exception, InformaticaError {
		if (details.getRelationalConnectionType() != ConnectionDetails.type.sqlserver)
		{
			throw new UnsupportedOperationException("Connection type " + details.getRelationalConnectionType() + " not implemented");
		}

		SqlServerConnectionDetails sqldet = details.getSqlServerConnectionDetails();

		StringBuilder commandb = new StringBuilder("createconnection ")
				.append("-s ")
				.append("\"Microsoft SQL Server\" ")
				.append("-n ")
				.append(details.getRelationalConnectionName())
				.append(" -u ")
				.append(sqldet.getDatabaseUsername())
				.append(" -p ")
				.append(sqldet.getDatabasePassword())
				.append(" -b ")
				.append(sqldet.getDatabaseName())
				.append(" -e")
				.append(" \"SET QUOTED_IDENTIFIER ON\"")
				.append(" -l")
				.append(" MS1252");

		commandb.append(" -v ");

		String instanceName = sqldet.getInstanceName();
		if (instanceName != null)
		{
			commandb.append(sqldet.getServerName() + "\\" + instanceName);
		}
		else
		{
			commandb.append(sqldet.getServerName());
		}

		String adDomain = sqldet.getDomainName();
		if (adDomain != null)
		{
			commandb.append(" -d ").append(adDomain);
		}

		String outBuff = pmRep.send(commandb.toString());

		InfaCreateConnectionSuccessExpression icsre = InfaCreateConnectionSuccessExpression.match(outBuff);

		if (icsre.hasNext()) {
			return;
		}

		InfaCreateConnectionExistsExpression iccre = InfaCreateConnectionExistsExpression.match(outBuff);

		if (iccre.hasNext()) {
			throw new InformaticaCreateConnectionExistsException("Connection exists " + outBuff);
		}

		throw new InformaticaError("Failure creating connection: " + outBuff);
	}

	public void createFolder(String folder) throws Exception, InformaticaError {
		createFolder(folder, false);
	}

	@Incomplete
	public void createFolder(String folder, boolean temporary) throws Exception, InformaticaError {
		String outBuff = pmRep.send("createfolder -n " + folder + " -s");

		checkForErrors(outBuff);

		// check for a successful result
		InfaCreateFolderResultExpression icfre = new InfaCreateFolderResultExpression(outBuff);

		if (!icfre.hasNext()) {
			// check whether the folder already exists.  This is not an error.
			InfaCreateFolderExistsExpression icfee = InfaCreateFolderExistsExpression.match(outBuff, folder, informaticaRepository.getRepositoryName());

			if (!icfee.hasNext()) {
				throw new InformaticaCreateFolderException("Could not create folder " + outBuff);
			}
		}

		if (temporary) {
			temporaryFolders.add(folder);
		}
	}

	@Incomplete
	public void deleteConnection(
			String connectionName
	) throws Exception, InformaticaError {
		deleteConnection(connectionName, true);
	}

	@Incomplete
	public void deleteConnection(
			String connectionName,
			boolean failIfNotExists
	) throws Exception, InformaticaError {
		String res = pmRep.send("deleteconnection -n " + connectionName + " -f");

		checkForErrors(res);

		// look for success
		InfaDeleteConnectionSuccessExpression idcse = new InfaDeleteConnectionSuccessExpression(res);

		if (!idcse.hasNext()) {
			InfaDeleteConnectionDoesNotExistExpression idcdnee = new InfaDeleteConnectionDoesNotExistExpression(res, connectionName);

			if (idcdnee.hasNext()) {
				// handle according to the failIfNotExists parameter
				if (failIfNotExists) {
					throw new InformaticaError("Connection to delete does not exist");
				} else {
					applicationLog.info("User does not care if connection does not exist");
					return;
				}
			}

			throw new InformaticaError("Delete connection failed for some reason: " + res);
		}
	}

	public void deleteFolder(String folder) throws Exception, InformaticaError {
		deleteFolder(folder, true);
	}

	@Override
	public void deleteFolder(String folder, boolean failIfNotExists) throws Exception, InformaticaError {
		String res = pmRep.send("deletefolder -n " + folder);

		checkForErrors(res);

		InfaDeleteFolderSuccessExpression idfre = new InfaDeleteFolderSuccessExpression(res);

		if (!idfre.hasNext()) {
			InfaDeleteFolderInUseExpression idiu = new InfaDeleteFolderInUseExpression(res);

			if (idiu.hasNext()) {
				throw new InformaticaDeadlockException(res);
			}

			// check for allowed not to exist
			InfaDeleteFolderFailedExpression idcfe = new InfaDeleteFolderFailedExpression(res);

			if (idcfe.hasNext()) {
				InfaDeleteFolderDoesNotExistExpression idcdnee = new InfaDeleteFolderDoesNotExistExpression(res, folder);

				if (idcdnee.hasNext()) {
					if (failIfNotExists) {
						throw new InformaticaDeleteFolderException("Folder " + folder + " does not exist.");
					}
				} else {
					throw new InformaticaError("Could not delete folder " + res);
				}
			} else {
				throw new InformaticaError("Could not delete folder " + res);
			}
		}

		return;
	}

	@Incomplete
	public void deleteTemporaryFolders() throws Exception, InformaticaError {
		Iterator<String> it = temporaryFolders.iterator();

		while (it.hasNext()) {
			String folder = it.next();

			deleteFolder(folder);

			it.remove();
		}
	}

	@Incomplete
	public void exportWorkflowToXml(
			String workFlow,
			String folder,
			File output
	) throws Exception, InformaticaError {
		String command = new StringBuilder("objectexport ")
				.append("-n ")
				.append(workFlow)
				.append(" -o ")
				.append("workflow ")
				.append("-m ")
				.append("-s ")
				.append("-r ")
				.append("-b ")
				.append("-f ")
				.append(folder)
				.append(" -u \"")
				.append(output.getAbsolutePath()).append("\"").toString();

		String outBuff = pmRep.send(command);

		checkForErrors(outBuff);

		InfaExportResultExpression iere = InfaExportResultExpression.match(outBuff);

		if (!iere.hasNext()) {
			throw new InformaticaError("Unexpected results from pmrep: " + outBuff);
		}

		if ((iere.getErrors() + iere.getWarnings()) != 0) {
			throw new InformaticaError("Errors and/or warnings received from pmrep: " + outBuff);
		}

		File tempf = new File(output.getParentFile(), output.getName() + ".tmp");

		// correct silly stuff in xml output
		// generify repository name
		IOUtils.replace(output, tempf, informaticaRepository.getRepositoryName(), "svn_pc_repo");

		// generify integration service
		IOUtils.visitLines(tempf, output, new IOUtils.LineVisitor() {
			public String visit(String line, int lineNo) {
				InfaServerNameExpression isne = new InfaServerNameExpression(line);

				return isne.replaceAll("SERVERNAME =\"vcs_is\"");
			}
		});

		// generify domain name
		IOUtils.visitLines(output, tempf, new IOUtils.LineVisitor() {
			public String visit(String line, int lineNo) {
				InfaDomainNameExpression isne = new InfaDomainNameExpression(line, informaticaDomain.getDomainName());

				return isne.replaceAll("SERVER_DOMAINNAME =\"DOM_VCS\"");
			}
		});

		if (!output.delete()) {
			throw new IOException("Obnoxious file won't delete!");
		}

		// strip out the time stamp
		IOUtils.replace(tempf, output, "<POWERMART CREATION_DATE=\"\\d{1,2}/\\d{1,2}/\\d{4} \\d{1,2}:\\d{1,2}:\\d{1,2}\" REPOSITORY_VERSION=\"(\\d{1,4}.\\d{1,9})\">", "<POWERMART CREATION_DATE=\"11/30/2010 12:00:00\" REPOSITORY_VERSION=\"$1\">");

		if (!tempf.delete()) {
			throw new IOException("Obnoxious temp file won't delete!");
		}
	}

	@Incomplete
	public void exportAllWorkflowsToXml() throws Exception, InformaticaError {
		if (true) throw new UnsupportedOperationException();

		/*
		File queryf = runtimeSupport.createTempFile("ExportAll.txt");

		String command = new StringBuilder("executequery ")
			.append("-q ")
			.append("ExportAll ")
			.append("-t ")
			.append("shared ")
			.append("-u ")
			.append(queryf.getAbsolutePath()).toString();

		String output = pmRep.send(command);

		/**
		 * Handle this
		 * /
		//int res = process.getCompletionCode();

		//if (res != 0)
		//{
		//	throw new InformaticaError("Could not export from bitbucket " + process.getInputBuffered());
		//}

		// read the file line-by-line and export each workflow
		BufferedReader br = new BufferedReader(new FileReader(queryf));

		String line = null;
		int expCount = 0;

		while ((line = br.readLine()) != null)
		{
			expCount++;

			StringTokenizer st = new StringTokenizer(line, ",");

			String crap = st.nextToken();
			String folder = st.nextToken();
			String workflow = st.nextToken();

			// strip off the 'wkf_'
			if (!workflow.startsWith("wkf_"))
			{
				continue;
			}

			String workflowName = workflow.substring(4);

			String type = st.nextToken();
			String none = st.nextToken();
			String number = st.nextToken();

			exportWorkflowToXml(
				workflowName,
				folder,
				new File("")
			);
		}
		 */
	}

	@Incomplete
	public void importWorkflowFromXml(
			final String folderPrefix,
			InformaticaIntegrationService informaticaIntegrationService,
			File xmlFile,
			final List<String> flist
	) throws InformaticaDeadlockException, InformaticaError, IOException {
		// prepare the import file
		File importFile = runtimeSupport.createAnonymousTempFile("xml", "import.xml");

		applicationLog.info("Substituting " + informaticaIntegrationService.getIntegrationServiceName() + " for svn_is");
		IOUtils.replace(xmlFile, importFile, "svn_is", informaticaIntegrationService.getIntegrationServiceName());

		// this needs to be a resource
		URL template = getClass().getResource("/objectimport-control.vm");

		if (template == null) {
			throw new IllegalStateException("Could not find objectimport-control.vm template");
		}

		File controlFile = runtimeSupport.createAnonymousTempFile("xml", "control.xml");

		try {
			String insta = VelocityUtil.writeTemplate(IOUtils.readURLToString(template), new VelocityBean(folderPrefix, informaticaRepository.getRepositoryName(), flist));

			IOUtils.writeBufferToFile(controlFile, new StringBuffer(insta));

			String command = new StringBuilder("objectimport ")
					.append("-i \"")
					.append(importFile.getAbsolutePath())
					.append("\" -c \"")
					.append(controlFile.getAbsolutePath()).append("\"").toString();

			applicationLog.info("Sending command [" + command + "] to pmrep [" + pmRep + "]");

			String message = pmRep.send(command);

			checkForErrors(message);

			// check for success message only if not in test mode
			if (!testMode) {
				InfaImportSuccessfulExpression iise = new InfaImportSuccessfulExpression(message);

				if (!iise.hasNext()) {
					runtimeSupport.getApplicationLog().severe("Import not successful.  Looking for a reason.");

					InfaImportResultsExpression ire = new InfaImportResultsExpression(message);

					if (!ire.hasNext()) {
						throw new InformaticaError("Failed to import object: " + message);
					} else if (ire.getErrorCount() > 0) {
						throw new InformaticaError("Failed to import object - too many errors: " + message);
					}
				}
			}
		} catch (InformaticaDeadlockException e) {
			runtimeSupport.getApplicationLog().severe("Rethrowing deadlock.");
			throw e;
		} catch (InformaticaError e) {
			throw e;
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			throw new InformaticaError(e.toString());
		}
	}

	public void importWorkflowFromXml(
			String folderPrefix,
			InformaticaIntegrationService informaticaIntegrationService,
			File xml
	) throws InformaticaDeadlockException, InformaticaError, IOException {
		importWorkflowFromXml(folderPrefix, informaticaIntegrationService, xml, getFolderList(xml));
	}

	@Override
	public List<InformaticaConnection> listConnections() throws Exception, InformaticaError {
		String res = pmRep.send("listconnections");

		checkForErrors(res);

		List<InformaticaConnection> folderList = new ArrayList<InformaticaConnection>();

		InfaConnectionNameExpression ifne = new InfaConnectionNameExpression(res);

		while (ifne.hasNext()) {
			InformaticaConnection ic = new InformaticaConnection();
			ic.setConnectionName(ifne.getConnectionName());
			ic.setConnectionType(ifne.getConnectionType());

			folderList.add(ic);
		}

		return folderList;
	}

	public static List<String> getFolderList(File xml) throws InformaticaError {
		List<String> li = new ArrayList<String>();

		try {
			String str = IOUtils.readFileToString(xml);

			// list all folders
			FolderNameExpression fne = new FolderNameExpression(str);

			while (fne.hasNext()) {
				String name = fne.getFolderName();

				if (!li.contains(name)) {
					li.add(name);
				}
			}

			// list all folder shortcuts
			FolderShortcutExpression fse = new FolderShortcutExpression(str);

			while (fse.hasNext()) {
				String name = fse.getShortcutFolderName();

				if (!li.contains(name)) {
					li.add(name);
				}
			}

			return li;
		} catch (IOException e) {
			throw new InformaticaError(e.toString());
		}
	}

	public List<String> listFolders() throws Exception, InformaticaError {
		String res = pmRep.send("listobjects -o folder");

		checkForErrors(res);

		List<String> folderList = new ArrayList<String>();

		InfaFolderNameExpression ifne = new InfaFolderNameExpression(res);

		while (ifne.hasNext()) {
			String folder = ifne.getFolderName();

			folderList.add(folder);
		}

		return folderList;
	}

	@Override
	public List<String> listWorkflows(String folder) throws Exception, InformaticaError {
		String res = pmRep.send("listobjects -o workflow -f " + folder);

		checkForErrors(res);

		List<String> folderList = new ArrayList<String>();

		//System.out.println(folder + ":" + res);
		InfaWorkflowNameExpression ifne = new InfaWorkflowNameExpression(res);

		while (ifne.hasNext()) {
			String workflow = ifne.getWorkflowName();

			folderList.add(workflow);
		}

		return folderList;
	}

	public int purgeTestFolders() throws Exception {
		List<String> folderList = listFolders();

		Iterator<String> it = folderList.iterator();

		int count = 0;

		while (it.hasNext()) {
			String folder = it.next();

			TestFolderNameExpression tfne = new TestFolderNameExpression(folder);

			if (tfne.matches()) {
				deleteFolder(folder);
				count++;
			}
		}

		return count;
	}

	public InformaticaDomain getInformaticaDomain() {
		return informaticaDomain;
	}

	public InformaticaRepository getInformaticaRepository() {
		return informaticaRepository;
	}

	@Override
	public boolean ping() {
		// say hello
		try {
			applicationLog.info("Pinging client process . . .");
			pmRep.send("ping " + new Date(), 1000L);
			applicationLog.info("Client process alive.");
			return true;
		} catch (Exception e) {
			applicationLog.info("Client process dead.");
			return false;
		}
	}

	public void dispose() {
		try {
			pmRep.dispose();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		} catch (ProcessTimeoutException e) {
			throw new IllegalStateException(e);
		}
	}

	private void checkForErrors(String output) throws InformaticaDeadlockException {
		// deadlock
		InfaRepositoryDeadlockExpression iddr = new InfaRepositoryDeadlockExpression(output);

		if (iddr.hasNext()) {
			throw new InformaticaDeadlockException(output);
		}
	}
}