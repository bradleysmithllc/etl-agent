package org.bitbucket.bradleysmithllc.etlagent.informatica.provider;

/*
 * #%L
 * informatica-agent
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.DomainDefaults;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.Domains;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.DomainsProperty;
import org.bitbucket.bradleysmithllc.etlagent.informatica.dto.json.InformaticaFeatureModuleConfiguration;
import org.bitbucket.bradleysmithllc.etlunit.RuntimeSupport;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/*
		default-domain: 'Domain_etldev01',
		domains: {
			Domain_etldev01: {
				client-version: '9.1.0hf3',
				connectivity-host: 'etldev01',
				connectivity-port: 6005,
				username: 'bsmith',
				password-encrypted: '',
				repositories: {
					dev_pc_repo: {
						integration-services: [
							'dev_ci_is',
							'dev_is',
							'dev_java_is'
						]
					}
				}
			}
			(
			,
			Domain_etlqa01: {
				client-version: '9.1.0hf3',
				connectivity-host: 'etlqa01',
				connectivity-port: 6005,
				username: 'bsmith',
				password-encrypted: '',
				repositories: {
					qa_pc_repo: {
						integration-services: [
							'qa_is'
						]
					}
				}
			}
			)*
		}
*/
public class InformaticaConfiguration {
	private final InformaticaDomain defaultDomain;
	private final Map<String, InformaticaDomain> domains = new HashMap<String, InformaticaDomain>();

	public InformaticaConfiguration(JsonNode value, RuntimeSupport runtimeSupport, File domainsFile)
	{
		InformaticaFeatureModuleConfiguration ifmc = null;

		try {
			ifmc = new ObjectMapper().readValue(value.toString(), InformaticaFeatureModuleConfiguration.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		// grab the defaults
		DomainDefaults domainDefaults = ifmc.getDomainDefaults();

		Domains edomains = ifmc.getDomains();

		if (edomains == null)
		{
			throw new IllegalArgumentException("Missing domains");
		}

		Map<String,DomainsProperty> dlist = edomains.getAdditionalProperties();

		if (dlist.size() == 0)
		{
			throw new IllegalArgumentException("Domains element does not contain any entries");
		}

		InformaticaDomain firstInformaticaDomain = null;

		for (Map.Entry<String, DomainsProperty> entry : dlist.entrySet())
		{
			InformaticaDomain informaticaDomain = new InformaticaDomain(entry.getKey(), entry.getValue(), domainDefaults, runtimeSupport, domainsFile);

			if (domains.containsKey(entry.getKey()))
			{
				throw new IllegalArgumentException("Domains element contains the same domain repeated: " + entry.getKey());
			}
			else
			{
				domains.put(entry.getKey(), informaticaDomain);
			}

			if (firstInformaticaDomain == null)
			{
				firstInformaticaDomain = informaticaDomain;
			}
		}

		JsonNode idefDomain = JsonUtils.query(value, "default-domain");

		if (idefDomain == null)
		{
			defaultDomain = firstInformaticaDomain;
		}
		else
		{
			String name = idefDomain.asText();

			defaultDomain = domains.get(name);

			if (defaultDomain == null)
			{
				throw new IllegalArgumentException("Default domain " + name + " not found in domains list");
			}
		}
	}

	public InformaticaDomain getDefaultDomain() {
		return defaultDomain;
	}

	public Map<String, InformaticaDomain> getDomains() {
		return domains;
	}

	public InformaticaDomain getDomain(String name) {
		InformaticaDomain dom = domains.get(name);

		if (dom == null)
		{
			throw new IllegalArgumentException("Domain not found " + name);
		}

		return dom;
	}
}