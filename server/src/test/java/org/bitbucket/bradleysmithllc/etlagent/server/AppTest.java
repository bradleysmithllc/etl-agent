package org.bitbucket.bradleysmithllc.etlagent.server;

/*
 * #%L
 * server
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.NullOutputStream;
import org.bitbucket.bradleysmithllc.etlagent.Server;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.IOException;
import java.net.URL;

public class AppTest
{
	@Rule
	public TemporaryFolder temporaryFolder = new TemporaryFolder();

	@Test
	public void testService() throws IOException, InterruptedException {
		System.setProperty("jersey.test.port", "7262");

		ContextBase.initiate(true);
		ContextBase.setRoot(temporaryFolder.newFolder());
		ContextBase.initialize();

		Server.start();

		URL url = new URL(Server.getBaseURI().toURL().toExternalForm() + "rest/agent/informatica");

		IOUtils.copy(url.openStream(), new NullOutputStream());

		//Thread.sleep(120000L);
	}

	//@Test
	public void st()
	{
		EtlAgent.main(new String [] {"--agent-root", "/tmp/root"});
	}
}