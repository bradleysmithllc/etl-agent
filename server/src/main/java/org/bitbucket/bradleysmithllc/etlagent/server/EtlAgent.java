package org.bitbucket.bradleysmithllc.etlagent.server;

/*
 * #%L
 * server
 * %%
 * Copyright (C) 2012 - 2014 bradleysmithllc
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.io.FileUtils;
import org.bitbucket.bradleysmithllc.etlagent.Server;
import org.bitbucket.bradleysmithllc.etlagent.resources.ContextBase;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIEntry;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIMain;
import org.bitbucket.bradleysmithllc.java_cl_parser.CLIOption;
import org.bitbucket.bradleysmithllc.java_cl_parser.CommonsCLILauncher;

import java.io.File;
import java.io.IOException;

@CLIEntry(
		version = "1.0",
		description = "Launches the etl agent container",
		nickName = "etlAgent",
		versionControl = "https://bradleysmithllc@bitbucket.org/bradleysmithllc/etl-agent.git"
)
public class EtlAgent {
	private File etlRoot;

	@CLIOption(
			name = "root",
			longName = "agent-root",
			required = true
	)
	public void setAgentRoot(String path) throws IOException {
		etlRoot = new File(path);
		FileUtils.forceMkdir(etlRoot);

		ContextBase.setRoot(etlRoot);
		ContextBase.initiate(false);
	}

	@CLIMain
	public void runServer() throws IOException, InterruptedException {
		ContextBase.initialize();
		Server.start();

		Thread.sleep(Long.MAX_VALUE);
	}

	public static void main(String[] args) {
		CommonsCLILauncher.mainClean(args);
	}
}